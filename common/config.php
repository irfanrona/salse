<?php
define('SITE_TITLE', 'Salse Bandung');
define('TIME_ZONE', 'Asia/Jakarta');
define('DATE_FORMAT', '%F');
define('TIME_FORMAT', '%R');
define('CURRENCY_ENABLED', '1');
define('CURRENCY_POS', 'before'); // before or after
define('LANG_ENABLED', '1');
define('ADMIN_LANG_FILE', 'en.ini');
define('ENABLE_COOKIES_NOTICE', '1');
define('MAINTENANCE_MODE', '0');
define('MAINTENANCE_MSG', '<h1><i class=\"fa fa-rocket\"></i> Coming soon...</h1><p>We are sorry, our website is down for maintenance.</p>');
define('TEMPLATE', 'default');
define('OWNER', 'Salse Bandung');
define('EMAIL', 'villatelsalse@gmail.com');
define('ADDRESS', 'Jl. Sukanagara No.88, Pagerwangi, Lembang, Kabupaten Bandung Barat, Jawa Barat 40391');
define('PHONE', '0222504105');
define('MOBILE', '08156016666');
define('FAX', '');
define('DB_NAME', 'hotel_gobooking');
define('DB_HOST', 'localhost'); //117.53.44.53
define('DB_PORT', '3306');
define('DB_USER', 'root');
define('DB_PASS', '');
define('SENDER_EMAIL', 'villatelsalse@gmail.com');
define('SENDER_NAME', 'VillaTel Salse');
define('USE_SMTP', '1');
define('SMTP_SECURITY', 'tls');
define('SMTP_AUTH', '1');
define('SMTP_HOST', 'smtp.gmail.com');
define('SMTP_USER', 'villatelsalse@gmail.com');
define('SMTP_PASS', 'salserileks123');
define('SMTP_PORT', '587');
define('GMAPS_API_KEY', 'AIzaSyAhKBhIzHGhfNkVZ4BO4zKb3Bgh0M5ts0c');
define('ANALYTICS_CODE', '');
define('ADMIN_FOLDER', 'admin');
define('CAPTCHA_PKEY', ''); // ReCAPTCHA public key
define('CAPTCHA_SKEY', ''); // ReCAPTCHA secret key
define('AUTOGEOLOCATE', '0'); // Change the currency according to the country (https required)
define('PAYMENT_TYPE', 'doku'); // 2checkout,paypal,check,arrival
define('PAYPAL_EMAIL', '');
define('VENDOR_ID', ''); // 2Checkout.com account number
define('SECRET_WORD', ''); // 2Checkout.com secret word
define('PAYMENT_TEST_MODE', '1');
define('ENABLE_DOWN_PAYMENT', '0');
define('DOWN_PAYMENT_RATE', '30'); // %
define('DOWN_PAYMENT_AMOUNT', '50'); // amount required to activate the down payment
define('ENABLE_TOURIST_TAX', '1');
define('TOURIST_TAX', '0');
define('TOURIST_TAX_TYPE', 'fixed');
define('ALLOW_COMMENTS', '1');
define('ALLOW_RATINGS', '1'); // If comments enabled
define('ENABLE_BOOKING_REQUESTS', '0'); // Possibility to make a reservation request if no availability
define('ENABLE_MULTI_VENDORS', '0'); // Payments are made on the PayPal account of the hotels
define('BRAINTREE_MERCHANT_ID', '');
define('BRAINTREE_PUBLIC_KEY', '');
define('BRAINTREE_PRIVATE_KEY', '');
define('CURRENCY_CONVERTER_KEY', ''); // currencyconverterapi.com API key
/*DEFINE FOR DOKU THIRD PARTY*/
define('DOKU_KEY', 'SK-3Op3k7IBs2Vc4Fvwfqnj'); // Shared doku key Staging
define('MALLID', 'BRN-0213-1671523993384'); // Mall Id doku key Staging
// define('DOKU_KEY', 'SK-3Op3k7IBs2Vc4Fvwfqnj'); // Shared doku key Production
// define('MALLID', 'BRN-0213-1671523993384'); // Mall Id doku key Production
