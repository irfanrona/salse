-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 08 Apr 2022 pada 20.51
-- Versi server: 10.3.28-MariaDB-cll-lve
-- Versi PHP: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `salseban_mahna`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `doku`
--

CREATE TABLE `doku` (
  `id` int(11) NOT NULL,
  `transidmerchant` varchar(125) NOT NULL,
  `totalamount` double DEFAULT NULL,
  `words` varchar(200) DEFAULT NULL,
  `statustype` varchar(1) DEFAULT NULL,
  `response_code` varchar(50) DEFAULT NULL,
  `approvalcode` char(6) DEFAULT NULL,
  `trxstatus` varchar(50) NOT NULL,
  `payment_channel` int(2) DEFAULT NULL,
  `paymentcode` int(8) DEFAULT NULL,
  `session_id` varchar(48) DEFAULT NULL,
  `bank_issuer` varchar(100) DEFAULT NULL,
  `creditcard` varchar(16) DEFAULT NULL,
  `payment_date_time` datetime DEFAULT '0000-00-00 00:00:00',
  `verifyid` varchar(30) DEFAULT NULL,
  `verifyscore` int(3) DEFAULT NULL,
  `verifystatus` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `doku`
--

INSERT INTO `doku` (`id`, `transidmerchant`, `totalamount`, `words`, `statustype`, `response_code`, `approvalcode`, `trxstatus`, `payment_channel`, `paymentcode`, `session_id`, `bank_issuer`, `creditcard`, `payment_date_time`, `verifyid`, `verifyscore`, `verifystatus`) VALUES
(1, 'satu1', 1000, '', '', '', '', 'Requested', 0, 0, '', '', '', '0000-00-00 00:00:00', '', 0, ''),
(2, '15d8dd4', 100, '', '', '', '', 'SUCCESS', 0, 0, '', '', '', '0000-00-00 00:00:00', '', 0, ''),
(3, '15d8de3', 200, 'fef50bc0d93de4901a69299603b363ba5192d10b', 'P', '0000', '', 'SUCCESS', 41, 2147483647, '5d8de33f2308b', 'MANDIRI', '8889950500000028', '2019-09-27 17:30:15', '', -1, 'NA'),
(4, '15d9098', NULL, NULL, NULL, NULL, NULL, 'Requested', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '15d9099', 150, '4512cf2425359fc2bb035b8a73e94af7b77fac7c', 'P', '0000', '', 'SUCCESS', 41, 2147483647, '5d90998eab2ae', 'MANDIRI', '8889950500000030', '2019-09-29 18:50:11', '', -1, 'NA'),
(6, '15d91a6', NULL, NULL, NULL, NULL, NULL, 'Failed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '15d91cc', NULL, NULL, NULL, NULL, NULL, 'Requested', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '15d91ce', 100, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(9, '15d91cf', 100, '2d095cc30b2fbb9eccce5c040c091cfa05d7b3bd', 'P', '0000', '210465', 'SUCCESS', 15, 0, '5d91cf38d94b0', 'Bank BNI', '542640******8754', '2019-09-30 16:49:13', '', -1, 'NA'),
(10, '15d9596', 410, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(11, '15d9597', 2610, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(12, '15d96e5', 1270, '', '', NULL, '', 'Failed', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(13, '15e0450', 750000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(14, '15e0db7', 1500000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(15, '15e0dcf', 10000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(16, '15e0eb6', 10000, '2DBO', 'P', '0000', '', 'SUCCESS', 15, 2147483647, '5d91cf38d94b0', 'MANDIRI', '8889950500000030', '2020-09-29 18:50:11', '', -1, 'NA'),
(17, '15e0f2c', 5000, '2DBO', 'P', '0000', '', 'Requested', 15, 2147483647, '5d91cf38d94b0', 'MANDIRI', '8889950500000030', '2020-09-29 18:50:11', '', -1, 'NA'),
(18, '15e12db', 10000, 'c0c47b2560eb94e89df60f17800d2e7db9511f38', 'P', '0000', '211841', 'SUCCESS', 41, 2147483647, '5e12daeec4197', 'MANDIRI', '8902238300000008', '2020-01-06 14:05:43', '', -1, 'NA'),
(19, '15e253c', 750000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(20, '15e2a6d', 10000, 'cc9d0dfdc18698fbf261b1814d918ed646a81fcc', 'P', '0000', '216048', 'SUCCESS', 41, 2147483647, '5e2a6d492d7d0', 'MANDIRI', '8902238300000010', '2020-01-24 11:17:09', '', -1, 'NA'),
(21, '15e5b8e', 495000, '46a648003a09d544665c61b4c7c76b45c6d4c84a', 'P', '0000', '472505', 'SUCCESS', 36, 2147483647, '5e5b8dd4cb850', 'PERMATA', '8856134300000011', '2020-03-01 17:37:23', '', -1, 'NA'),
(22, '15e5e2d', 440000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(23, '15e5f27', 440000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(24, '15e5f30', 440000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(25, '15e5f33', 440000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(26, '15e5f5b', 440000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(27, '15e5f86', 440000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(28, '15e61e8', 1080000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(29, '15e685b', 520000, '0e23f8a2e2fcba824fa7ea3baf095116167f3dd3', 'P', '0000', '645296', 'SUCCESS', 36, 2147483647, '5e68589dc857e', 'PERMATA', '8856134300000012', '2020-03-11 11:03:09', '', -1, 'NA'),
(30, '15e6b04', 585000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(31, '15e6d89', 440000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(32, '15ef9ab', 440000, '1772ec0e3af615c46a5f156cba7827c46325469d', 'P', '0000', '256589', 'SUCCESS', 41, 2147483647, '5ef9a925e3915', 'MANDIRI', '8902238300000013', '2020-06-29 16:13:18', '', -1, 'NA'),
(33, '15f0b25', 510000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(34, '15f157b', 575000, 'ce5191b1f7429f416c3c9f964f947f006174d641', 'P', '0000', '262586', 'SUCCESS', 41, 2147483647, '5f157b122dd08', 'MANDIRI', '8902238300000016', '2020-07-20 18:15:33', '', -1, 'NA'),
(35, '15f216c', 820000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(36, '15f3a51', 550000, '29154b4e476650f1e05fec407f978cb075aefd21', 'P', '0000', 'T70502', 'SUCCESS', 15, 0, '5f3a5106583a7', 'Bank BNI', '531857******8199', '2020-08-17 16:47:22', '', -1, 'APPROVE'),
(37, '15f3d6c', 1280000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(38, '15f44ac', 550000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(39, '15f76ad', 300000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(40, '15f76af', 300000, '41072dd209703293d4bea9a4180dfbeae25b5b2b', 'P', '0000', '284048', 'SUCCESS', 36, 2147483647, '5f76af3f13a65', 'PERMATA', '8856134300000023', '2020-10-02 11:43:49', '', -1, 'NA'),
(41, '15f8017', 590000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(42, '15f9681', 510000, '74cb7abf28f32e78f0d6f1f9b7903ce68b5c083d', 'P', '0000', '291792', 'SUCCESS', 41, 2147483647, '5f9680f105ce4', 'MANDIRI', '8902238300000025', '2020-10-26 15:09:54', '', -1, 'NA'),
(43, '15f9c4e', 415000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(44, '15f9c4f', 415000, 'df3ada7548ee469e1f20487904740d1755d1202a', 'P', '0000', '414853', 'SUCCESS', 36, 2147483647, '5f9c4a7d6f009', 'PERMATA', '8856134300000028', '2020-10-31 00:39:03', '', -1, 'NA'),
(45, '15f9c53', 415000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(46, '15f9fdd', 650000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(47, '15fa007', 650000, '60e462e9e30521191ab230c317e065ff45aac1a7', 'P', '0000', '350034', 'SUCCESS', 15, 0, '5fa006fea8b05', 'Bank Tabungan Pensiunan Nasional', '466160******4709', '2020-11-02 20:23:24', '', -1, 'APPROVE'),
(48, '15fb29c', 1020000, '', '', NULL, '', 'Failed', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(49, '15fb758', 510000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(50, '15fb75d', 510000, 'd87e21544e42a652c454352a5ac849e138855279', 'P', '0000', '691780', 'SUCCESS', 15, 0, '5fb7587369a89', 'Bank Tabungan Pensiunan Nasional', '466160******0152', '2020-11-20 13:12:18', '', -1, 'APPROVE'),
(51, '15fc65b', 1630000, '', '', NULL, '', 'Failed', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(52, '15fd6bf', 700000, '7098ad9a7ec80649edea45a0e96d63be7f570535', 'P', '0000', '308383', 'SUCCESS', 41, 2147483647, '5fd6befdd4f81', 'MANDIRI', '8902238300000037', '2020-12-14 09:07:51', '', -1, 'NA'),
(53, '15ff2be', 500000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(54, '15ff6c2', 500000, 'c5d68550455515d11b79627858a1f99772147fde', 'P', '0000', '316803', 'SUCCESS', 41, 2147483647, '5ff6bec962c60', 'MANDIRI', '8902238300000038', '2021-01-07 15:13:54', '', -1, 'NA'),
(55, '160257f', 500000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(56, '1602e76', 340000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(57, '1605594', 300000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(58, '160608c', 750000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(59, '1606154', 750000, '0e6068644d55ca289035b9582af537415b084122', 'P', '0000', '348372', 'SUCCESS', 41, 2147483647, '6061536392b65', 'MANDIRI', '8902238300000042', '2021-03-29 11:16:56', '', -1, 'NA'),
(60, '160620d', 1050000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(61, '1606271', 1050000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(62, '1606315', 1100000, '5b062350d78ae1bcddee317015315bec60af04ad', 'P', '0000', '751638', 'SUCCESS', 36, 2147483647, '606315be446ae', 'PERMATA', '8856134300000045', '2021-03-30 19:19:14', '', -1, 'NA'),
(63, '1606444', 325000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(64, '1606445', 325000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(65, '160644a', 325000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(66, '1606a96', 325000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(67, '1606acf', 325000, '', '', NULL, '', 'Failed', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(68, '1606c7d', 325000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(69, '1606d20', 1000000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(70, '1606d24', 1000000, '', '', NULL, '', 'Failed', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(71, '1606d2d', 2000000, '37f25f40e3592a04f4fff25b31f264cb42051555', 'P', '0000', '352339', 'SUCCESS', 41, 2147483647, '606d2cf7a3021', 'MANDIRI', '8902238300000054', '2021-04-07 10:58:52', '', -1, 'NA'),
(72, '1606efe', 325000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(73, '1606f05', 325000, '5d24f16b908ca8eb6f94bce846d4f762c4eed716', 'P', '0000', '352917', 'SUCCESS', 41, 2147483647, '606f0581094b7', 'MANDIRI', '8902238300000056', '2021-04-08 20:34:09', '', -1, 'NA'),
(74, '160718a', 680000, 'fd3a1352c084aba5ff560ffde067e007f9de08dd', 'P', '0000', '353636', 'SUCCESS', 41, 2147483647, '607188f2b7e05', 'MANDIRI', '8902238300000058', '2021-04-10 18:25:53', '', -1, 'NA'),
(75, '1609a89', 325000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(76, '1609c1a', 325000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(77, '1609cc9', 325000, '6e24ef173c47c75d409b8b64eb865833b6abaaa7', 'P', '0000', '367229', 'SUCCESS', 41, 2147483647, '609cc8d2126c5', 'MANDIRI', '8902238300000061', '2021-05-13 13:40:00', '', -1, 'NA'),
(78, '160acfa', 1000000, '', '', NULL, '', 'Failed', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(79, '160b980', 600000, 'fb754a61681121c6ab0e116e46f9175bc795dee5', 'P', '0000', '458666', 'SUCCESS', 36, 2147483647, '60b9808556f79', 'PERMATA', '8856134300000063', '2021-06-04 08:35:13', '', -1, 'NA'),
(80, '160c028', 500000, '3ff5331f2df0430949c492b0e71d94e10591e42e', 'P', '0000', '378227', 'SUCCESS', 41, 2147483647, '60c02786165c5', 'MANDIRI', '8902238300000064', '2021-06-09 09:35:18', '', -1, 'NA'),
(81, '161097f', 500000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(82, '1610a46', 500000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(83, '1610a4e', 500000, 'a56300d2a07b720bb1b3fed916d118cfc86ef64d', 'P', '0000', '544362', 'SUCCESS', 36, 2147483647, '610a4e04c0aa3', 'PERMATA', '8856134300000067', '2021-08-04 15:27:54', '', -1, 'NA'),
(84, '1611344', 355000, '42d66c1da213fe23b10a5d8db1ba5468961aaa05', 'P', '0000', '403350', 'SUCCESS', 41, 2147483647, '611343e9c2d93', 'MANDIRI', '8902238300000068', '2021-08-11 10:55:07', '', -1, 'NA'),
(85, '16116fe', 355000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(86, '1612856', 2800000, '63719927129b3521846809926b4f3ae8e71e05f1', 'P', '0000', '691889', 'SUCCESS', 15, 0, '612855987991a', 'DBS BANK', '460238******5514', '2021-08-27 10:11:53', '', -1, 'APPROVE'),
(87, '16134a7', 500000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(88, '16134c3', 500000, '12f15a4b563e0d81fea1db3a0d4e7bfd196addaf', 'P', '0000', '412531', 'SUCCESS', 41, 2147483647, '6134c3073dbda', 'MANDIRI', '8902238300000073', '2021-09-05 20:48:53', '', -1, 'NA'),
(89, '1614310', 1000000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(90, '1614310', 1000000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(91, '1614315', 2000000, 'c7d546a1d618bf36e0cda09ee6826cfe1249ab08', 'P', '0000', '416163', 'SUCCESS', 41, 2147483647, '614315144aec3', 'MANDIRI', '8902238300000077', '2021-09-16 17:20:58', '', -1, 'NA'),
(92, '1614b50', 600000, '76749a7d7d53a4a60c838d9aebd0feb6d873ff48', 'P', '0000', '405813', 'SUCCESS', 15, 0, '614b5009027b6', 'PT. BANK CIMB NIAGA TBK.', '528919******2382', '2021-09-22 22:54:29', '', -1, 'APPROVE'),
(93, '16159bf', 1500000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(94, '1615a94', 500000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(95, '1616768', 500000, '374fd43c337857af33e227968153ee2e85dfb388', 'P', '0000', '205033', 'SUCCESS', 41, 2147483647, '6167685978dde', 'MANDIRI', '8902238300000080', '2021-10-14 06:22:52', '', -1, 'NA'),
(96, '1618a56', 500000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(97, '1618a56', 500000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(98, '1618b2d', 1600000, '', '', NULL, '', 'Failed', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(99, '1618d0f', 500000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(100, '1618d1b', 500000, '62586812f53982f94fbbbf5b6de7670a4c473f22', 'P', '0000', '238573', 'SUCCESS', 15, 0, '618d1b8196b27', 'DBS BANK', '460238******5371', '2021-11-11 20:39:27', '', -1, 'APPROVE'),
(101, '1618d55', 500000, '188c468879599245c63f0d70524ee7274f3f6095', 'P', '0000', '825508', 'SUCCESS', 36, 2147483647, '618d5257aab19', 'PERMATA', '8856134300000086', '2021-11-12 00:44:38', '', -1, 'NA'),
(102, '161930f', 500000, 'fd5e62514e2957f6d5d1e7714723554702eee4c0', 'P', '0000', '437823', 'SUCCESS', 41, 2147483647, '61930dae8bef8', 'MANDIRI', '8902238300000087', '2021-11-16 09:07:57', '', -1, 'NA'),
(103, '1619ce6', 2800000, '92a04d6aec50e349d66b4b31afffa635afcb2a65', 'P', '0000', '827180', 'SUCCESS', 15, 0, '619ce559d9b0d', 'Bank Mandiri', '557338******3953', '2021-11-23 20:06:52', '', -1, 'APPROVE'),
(104, '1619e21', 800000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(105, '1619e23', 800000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(106, '161b8b1', 600000, '10bf0593a5eb116776345a7d5591f16e741da722', 'P', '0000', '261647', 'SUCCESS', 36, 2147483647, '61b8b045476a1', 'PERMATA', '8856134300000091', '2021-12-14 22:03:18', '', -1, 'NA'),
(107, '161cc06', 500000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(108, '161cc41', 325000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(109, '161d8e5', 325000, '3e6c38dc57347e100e73bbe694a57ac0062201f2', 'P', '0000', '529254', 'SUCCESS', 36, 2147483647, '61d8e4b605018', 'PERMATA', '8856134300000093', '2022-01-08 08:20:11', '', -1, 'NA'),
(110, '161eed4', 550000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(111, '1621cea', 650000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, ''),
(112, '1621dfb', 650000, '30a0aa4f9fd44fd7ff28e8aaa4f6730e3605d5bb', 'P', '0000', '903811', 'SUCCESS', 41, 2147483647, '621dfbc1bcc02', 'MANDIRI', '8902238300000096', '2022-03-01 17:58:23', '', -1, 'NA'),
(113, '162206e', 550000, '', '', NULL, '', 'Requested', NULL, NULL, '', '', '', NULL, '', NULL, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_activity`
--

CREATE TABLE `pm_activity` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `hotels` varchar(250) DEFAULT NULL,
  `users` text DEFAULT NULL,
  `max_children` int(11) DEFAULT 1,
  `max_adults` int(11) DEFAULT 1,
  `max_people` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `subtitle` varchar(250) DEFAULT NULL,
  `alias` varchar(100) DEFAULT NULL,
  `descr` longtext DEFAULT NULL,
  `duration` float DEFAULT 0,
  `duration_unit` varchar(50) DEFAULT NULL,
  `price` double DEFAULT 0,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `home` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 0,
  `rank` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_activity`
--

INSERT INTO `pm_activity` (`id`, `lang`, `hotels`, `users`, `max_children`, `max_adults`, `max_people`, `title`, `subtitle`, `alias`, `descr`, `duration`, `duration_unit`, `price`, `lat`, `lng`, `home`, `checked`, `rank`) VALUES
(1, 1, '1', '1', 10, 10, 100, 'Gamelan Show', 'Pertunjukan Gamelan', 'gamelan-perfotmance', '', 60, 'minutes', 1000, -6.8453585, 107.6290052, 1, 1, 1),
(1, 2, '1', '1', 10, 10, 100, 'Gamelan', '', 'gamelan-show', '<p style=\"text-align: justify;\">Sundanese culture is one of the oldest cultures in the archipelago. Initially, Sundanese traditional music and dance were part of a ritual or traditional ceremony, but now it has changed its function to become a performing arts which is more concerned with entertainment and / or commercial elements. To enjoy it, Salse presents Sundanese traditional music (Gamelan) and Dance every Tuesday that was followed by villagers and public every Tuesday at 03:00 - 05:00 pm.</span> For reservations, contact us.</span></span></p>\r\n', 60, 'minutes', 100000, -6.8453585, 107.6290052, 1, 1, 1),
(1, 3, '1', '1', 10, 10, 100, 'Gamelan Show', 'Pertunjukan Gamelan', 'gamelan-perfotmance', '', 60, 'minutes', 1000, -6.8453585, 107.6290052, 1, 1, 1),
(1, 4, '1', '1', 10, 10, 100, 'Gamelan', '', 'gamelan-show', '<p style=\"text-align: justify;\">Kebudayaan Sunda merupakan salah satu kebudayaan tertua di Nusantara. Pada mulanya, musik dan tarian tradisional Sunda merupakan bagian dari sebuah ritual atau upacara adat, namun kini berubah fungsi menjadi seni pertunjukkan yang lebih mementingkan unsur hiburan dan atau komersil. Untuk menikmatinya, Salse menyajikan </span>Musik Tradisional (Gamelan) dan Tari yang diikuti oleh masyarakat desa dan publik setiap hari Selasa jam 15.00 – 17.00</span>. </span>Untuk reservasi bisa menghubungi kami.</span></span></p>\r\n', 60, 'minutes', 100000, -6.8453585, 107.6290052, 1, 1, 1),
(2, 1, '1', '1', 10, 10, 100, 'Gamelan Show', 'Pertunjukan Gamelan', 'gamelan-perfotmance', '', 60, 'minutes', 1000, -6.8453585, 107.6290052, 1, 1, 3),
(2, 2, '1', '1', 10, 10, 100, 'Sundanese Dance', '', 'jaipong-dance', '<p style=\"text-align: justify;\">Sundanese culture is one of the oldest cultures in the archipelago. Initially, Sundanese traditional music and dance were part of a ritual or traditional ceremony, but now it has changed its function to become a performing arts which is more concerned with entertainment and / or commercial elements. To enjoy it, Salse presents Sundanese traditional music (Gamelan) and Dance every Tuesday that was followed by villagers and public at 03:00 - 05:00 pm.</span> For further information, please contact us.</span></span></p>\r\n', 60, 'minutes', 100000, -6.8453585, 107.6290052, 1, 1, 3),
(2, 3, '1', '1', 10, 10, 100, 'Gamelan Show', 'Pertunjukan Gamelan', 'gamelan-perfotmance', '', 60, 'minutes', 1000, -6.8453585, 107.6290052, 1, 1, 3),
(2, 4, '1', '1', 10, 10, 100, 'Tarian Sunda', '', 'tari-jaipong', '<p style=\"text-align: justify;\">Kebudayaan Sunda merupakan salah satu kebudayaan tertua di Nusantara. Pada mulanya, musik dan tarian tradisional Sunda merupakan bagian dari sebuah ritual atau upacara adat, namun kini berubah fungsi menjadi seni pertunjukkan yang lebih mementingkan unsur hiburan dan atau komersil. Untuk menikmatinya, Salse menyajikan </span>Musik Tradisional (Gamelan) dan Tari yang diikuti oleh masyarakat desa dan publik setiap hari Selasa jam 15.00 – 17.00</span>. </span>Untuk informasi lebih lanjut, silahkan menghubungi kami.</span></span></p>\r\n', 60, 'minutes', 100000, -6.8453585, 107.6290052, 1, 1, 3),
(3, 1, '1', '1', 10, 10, 100, 'Gamelan Show', 'Pertunjukan Gamelan', 'gamelan-perfotmance', '', 60, 'minutes', 100000, -6.8453585, 107.6290052, 1, 1, 2),
(3, 2, '1', '1', 10, 10, 100, 'Ikebana', '', 'ikebana', '<p style=\"text-align: justify;\">Ikebana</span>, traditionally, the classical art of <u></span></u>Japanese flower arranging; the meaning of the term was later extended to </span>encompass all the various styles of Japanese floral art. Based on a harmony of simple linear construction and an appreciation of the subtle beauty of flowers and natural material, ikebana has separated into several major schools according to historical periods and differing theories of artistic composition. Now, you can enjoy Ikebana and do it at Salse Bandung. For further information, please contact us.</span></p>\r\n', 60, 'minutes', 100000, -6.8453585, 107.6290052, 1, 1, 2),
(3, 3, '1', '1', 10, 10, 100, 'Gamelan Show', 'Pertunjukan Gamelan', 'gamelan-perfotmance', '', 60, 'minutes', 100000, -6.8453585, 107.6290052, 1, 1, 2),
(3, 4, '1', '1', 10, 10, 100, 'Seni Merangkai Bunga', '', 'seni-merangkai-bunga', '<p style=\"text-align: justify;\">Ikebana, secara tradisional, merupakan seni merangkai bunga gaya Jepang; makna istilah ini kemudian diperluas untuk mencakup semua gaya seni bunga Jepang. Didasarkan pada harmoni konstruksi linier dan  keindahan bunga serta bahan alaminya, ikebana terbagi menjadi beberapa sekolah besar menurut periode sejarah dan teori komposisi artistik yang berbeda. Ikebana kini dapat anda nikmati dan lakukan di Salse Bandung. Untuk informasi lebih lanjut, silahkan menghubungi kami.</span></p>\r\n', 60, 'minutes', 100000, -6.8453585, 107.6290052, 1, 1, 2),
(5, 2, '1', '1', 10, 10, 50, 'Outbond Training Activity', '', 'outbond-traing', '<p style=\"text-align: justify;\">Outbound Training generally revolves around outbound activities designed to improve leadership, communication skill, planning, change management, delegation, teamwork and motivation. The spacious Resto Joglo Salse area allows you to do outbound activities. For further information, please contact us.</span></span></span></p>\r\n', 60, 'minutes', 100000, -6.8453585, 107.6290052, 1, 2, 4),
(5, 4, '1', '1', 10, 10, 50, 'Kegiatan Pelatihan Outbond', '', 'pelatihan-outbond', '<p style=\"text-align: justify;\">Pelatihan Outbound umumnya mencakup kegiatan outbound yang dirancang untuk meningkatkan kepemimpinan, keterampilan komunikasi, perencanaan, perubahan manajemen, delegasi, kerja tim, dan motivasi. Areal Resto Joglo Salse yang luas, memungkinkan anda untuk melakukan kegiatan outbound. Untuk informasi lebih lanjut, silahkan hubungi kami.</span></p>\r\n', 60, 'minutes', 100000, -6.8453585, 107.6290052, 1, 2, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_activity_file`
--

CREATE TABLE `pm_activity_file` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `home` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 1,
  `rank` int(11) DEFAULT 0,
  `file` varchar(250) DEFAULT NULL,
  `label` varchar(250) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_activity_file`
--

INSERT INTO `pm_activity_file` (`id`, `lang`, `id_item`, `home`, `checked`, `rank`, `file`, `label`, `type`) VALUES
(2, 1, 1, NULL, 2, 1, 'gamelan.jpg', '', 'image'),
(2, 2, 1, NULL, 2, 1, 'gamelan.jpg', '', 'image'),
(2, 3, 1, NULL, 2, 1, 'gamelan.jpg', '', 'image'),
(2, 4, 1, NULL, 2, 1, 'gamelan.jpg', '', 'image'),
(17, 1, 2, NULL, 1, 3, 'jaipongg.jpg', '', 'image'),
(17, 2, 2, NULL, 1, 3, 'jaipongg.jpg', '', 'image'),
(17, 3, 2, NULL, 1, 3, 'jaipongg.jpg', '', 'image'),
(17, 4, 2, NULL, 1, 3, 'jaipongg.jpg', '', 'image'),
(18, 1, 2, NULL, 2, 7, 'jaipong5.jpg', NULL, 'image'),
(18, 2, 2, NULL, 2, 7, 'jaipong5.jpg', '', 'image'),
(18, 3, 2, NULL, 2, 7, 'jaipong5.jpg', NULL, 'image'),
(18, 4, 2, NULL, 2, 7, 'jaipong5.jpg', '', 'image'),
(19, 1, 2, NULL, 2, 4, 'jaipong3.jpg', NULL, 'image'),
(19, 2, 2, NULL, 2, 4, 'jaipong3.jpg', '', 'image'),
(19, 3, 2, NULL, 2, 4, 'jaipong3.jpg', NULL, 'image'),
(19, 4, 2, NULL, 2, 4, 'jaipong3.jpg', '', 'image'),
(20, 1, 2, NULL, 1, 2, 'jaipong-4.jpg', NULL, 'image'),
(20, 2, 2, NULL, 1, 2, 'jaipong-4.jpg', '', 'image'),
(20, 3, 2, NULL, 1, 2, 'jaipong-4.jpg', NULL, 'image'),
(20, 4, 2, NULL, 1, 2, 'jaipong-4.jpg', '', 'image'),
(21, 1, 2, NULL, 2, 5, 'jaipong2.jpg', NULL, 'image'),
(21, 2, 2, NULL, 2, 5, 'jaipong2.jpg', '', 'image'),
(21, 3, 2, NULL, 2, 5, 'jaipong2.jpg', NULL, 'image'),
(21, 4, 2, NULL, 2, 5, 'jaipong2.jpg', '', 'image'),
(22, 1, 2, NULL, 2, 6, 'jaipong6.jpg', NULL, 'image'),
(22, 2, 2, NULL, 2, 6, 'jaipong6.jpg', '', 'image'),
(22, 3, 2, NULL, 2, 6, 'jaipong6.jpg', NULL, 'image'),
(22, 4, 2, NULL, 2, 6, 'jaipong6.jpg', '', 'image'),
(25, 1, 1, NULL, 1, 2, 'gamelan.jpg', '', 'image'),
(25, 2, 1, NULL, 1, 2, 'gamelan.jpg', '', 'image'),
(25, 3, 1, NULL, 1, 2, 'gamelan.jpg', '', 'image'),
(25, 4, 1, NULL, 1, 2, 'gamelan.jpg', '', 'image'),
(29, 2, 3, NULL, 1, 2, 'ikebana3.jpg', '', 'image'),
(29, 4, 3, NULL, 1, 2, 'ikebana3.jpg', '', 'image'),
(30, 2, 3, NULL, 1, 1, 'ikebana2.jpg', '', 'image'),
(30, 4, 3, NULL, 1, 1, 'ikebana2.jpg', '', 'image'),
(32, 2, 1, NULL, 1, 3, 'whatsapp-image-2020-01-29-at-17-28-32.jpeg', '', 'image'),
(32, 4, 1, NULL, 1, 3, 'whatsapp-image-2020-01-29-at-17-28-32.jpeg', '', 'image'),
(34, 2, 2, NULL, 1, 1, 'whatsapp-image-2020-01-29-at-17-28-18.jpeg', '', 'image'),
(34, 4, 2, NULL, 1, 1, 'whatsapp-image-2020-01-29-at-17-28-18.jpeg', '', 'image'),
(35, 2, 1, NULL, 1, 4, 'gamelan1.jpg', '', 'image'),
(35, 4, 1, NULL, 1, 4, 'gamelan1.jpg', '', 'image'),
(36, 2, 1, NULL, 2, 5, 'gamelan2.jpg', '', 'image'),
(36, 4, 1, NULL, 2, 5, 'gamelan2.jpg', '', 'image'),
(37, 2, 5, NULL, 1, 8, 'outbond3.jpg', '', 'image'),
(37, 4, 5, NULL, 1, 8, 'outbond3.jpg', '', 'image'),
(38, 2, 5, NULL, 1, 9, 'outbond2.jpg', '', 'image'),
(38, 4, 5, NULL, 1, 9, 'outbond2.jpg', '', 'image'),
(39, 2, 5, NULL, 1, 10, 'outbond1.jpg', '', 'image'),
(39, 4, 5, NULL, 1, 10, 'outbond1.jpg', '', 'image');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_activity_session`
--

CREATE TABLE `pm_activity_session` (
  `id` int(11) NOT NULL,
  `id_activity` int(11) NOT NULL,
  `days` varchar(20) DEFAULT NULL,
  `start_date` int(11) DEFAULT NULL,
  `end_date` int(11) DEFAULT NULL,
  `users` text DEFAULT NULL,
  `price` double DEFAULT 0,
  `price_child` double DEFAULT 0,
  `discount` double DEFAULT 0,
  `discount_type` varchar(10) DEFAULT NULL,
  `id_tax` int(11) DEFAULT NULL,
  `taxes` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_activity_session`
--

INSERT INTO `pm_activity_session` (`id`, `id_activity`, `days`, `start_date`, `end_date`, `users`, `price`, `price_child`, `discount`, `discount_type`, `id_tax`, `taxes`) VALUES
(4, 1, '2', 1580515200, 1609372800, '6', 100000, 50000, 0, 'fixed', NULL, '1'),
(5, 2, '2', 1580515200, 1609372800, '6', 100000, 50000, 0, 'fixed', NULL, '1'),
(6, 3, '2,5', 1580515200, 1609372800, '6', 100000, 50000, 0, 'fixed', NULL, '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_activity_session_hour`
--

CREATE TABLE `pm_activity_session_hour` (
  `id` int(11) NOT NULL,
  `id_activity_session` int(11) NOT NULL,
  `start_h` int(11) DEFAULT NULL,
  `start_m` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_activity_session_hour`
--

INSERT INTO `pm_activity_session_hour` (`id`, `id_activity_session`, `start_h`, `start_m`) VALUES
(3, 6, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_article`
--

CREATE TABLE `pm_article` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `subtitle` varchar(250) DEFAULT NULL,
  `alias` varchar(100) DEFAULT NULL,
  `text` longtext DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `tags` varchar(250) DEFAULT NULL,
  `id_page` int(11) DEFAULT NULL,
  `users` text DEFAULT NULL,
  `home` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 0,
  `rank` int(11) DEFAULT 0,
  `add_date` int(11) DEFAULT NULL,
  `edit_date` int(11) DEFAULT NULL,
  `publish_date` int(11) DEFAULT NULL,
  `unpublish_date` int(11) DEFAULT NULL,
  `comment` int(11) DEFAULT 0,
  `rating` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_article`
--

INSERT INTO `pm_article` (`id`, `lang`, `title`, `subtitle`, `alias`, `text`, `url`, `tags`, `id_page`, `users`, `home`, `checked`, `rank`, `add_date`, `edit_date`, `publish_date`, `unpublish_date`, `comment`, `rating`) VALUES
(4, 1, 'Première gallery', '', 'premiere-gallery', '', '', '', 7, '1', 0, 1, 4, 1563677945, 1580097615, NULL, NULL, 0, 0),
(4, 2, 'First gallery', '', 'first-gallery', '', '', '', 7, '1', 0, 1, 4, 1563677945, 1580287234, NULL, NULL, 0, 0),
(4, 3, 'First gallery', '', 'first-gallery', '', '', '', 7, '1', 0, 1, 4, 1563677945, 1580097615, NULL, NULL, 0, 0),
(4, 4, 'Galeri Pertama', '', 'galeri-pertama', '', '', '', 7, '1', 0, 1, 4, 1563677945, 1580287234, NULL, NULL, 0, 0),
(5, 1, '', '', '', '', '', '', 24, '6', 0, 1, 5, 1580095488, 1580267971, 1580095200, NULL, 0, NULL),
(5, 2, 'Gallery of Resto Joglo Salse', '', 'joglo-gallery', '', '', '', 24, '6', 0, 1, 5, 1580095488, 1581398258, 1580095200, NULL, 0, NULL),
(5, 3, '', '', '', '', '', '', 24, '6', 0, 1, 5, 1580095488, 1580267971, 1580095200, NULL, 0, NULL),
(5, 4, 'Galeri Resto Joglo Salse', '', 'galeri-joglo', '', '', '', 24, '6', 0, 1, 5, 1580095488, 1581398258, 1580095200, NULL, 0, NULL),
(6, 1, '', '', '', '', '', '', 7, '6', 0, 0, 6, 1580181787, 1580276095, 1580181720, NULL, 0, NULL),
(6, 2, 'Exterior of Warung Salse', '', 'exterior-warungsalse', '', '', '', 25, '6', 0, 1, 6, 1580181787, 1580358578, 1580181720, NULL, 0, NULL),
(6, 3, '', '', '', '', '', '', 7, '6', 0, 0, 6, 1580181787, 1580276095, 1580181720, NULL, 0, NULL),
(6, 4, 'Bagian Luar Warung Salse', '', 'bagianluar-warungsalse', '', '', '', 25, '6', 0, 1, 6, 1580181787, 1580358578, 1580181720, NULL, 0, NULL),
(7, 1, '', '', '', '', '', '', 7, '6', 0, 0, 7, 1580181835, 1580275871, 1580181720, NULL, 0, NULL),
(7, 2, 'Interior of Warung Salse', '', 'interior-warungsalse', '', '', '', 25, '6', 0, 1, 7, 1580181835, 1581581263, 1580181720, NULL, 0, NULL),
(7, 3, '', '', '', '', '', '', 7, '6', 0, 0, 7, 1580181835, 1580275871, 1580181720, NULL, 0, NULL),
(7, 4, 'Bagian Dalam Warung Salse', '', 'bagiandalam-warungsalse', '', '', '', 25, '6', 0, 1, 7, 1580181835, 1581581263, 1580181720, NULL, 0, NULL),
(8, 1, '', '', '', '', '', '', 7, '6', 0, 2, 8, 1580183599, 1580275727, 1580181720, NULL, 0, NULL),
(8, 2, 'Food of Warung Salse', '', 'food-warungsalse', '', '', '', 25, '6', 0, 2, 8, 1580183599, 1581398379, 1580181720, NULL, 0, NULL),
(8, 3, '', '', '', '', '', '', 7, '6', 0, 2, 8, 1580183599, 1580275727, 1580181720, NULL, 0, NULL),
(8, 4, 'Makanan Warung Salse', '', 'makanan-warungsalse', '', '', '', 25, '6', 0, 2, 8, 1580183599, 1581398379, 1580181720, NULL, 0, NULL),
(9, 2, 'Family Gathering in Villatel Salse', '', 'gathering-salse', '', '', '', 7, '1', 0, 1, 9, 1581391969, 1581396119, NULL, NULL, 0, NULL),
(9, 4, 'Family Gathering di Villatel Salse', '', 'pertemuan-salse', '', '', '', 7, '1', 0, 1, 9, 1581391969, 1581396119, NULL, NULL, 0, NULL),
(10, 2, 'Meeting Room In Villatel Salse', '', 'meeting-room-salse', '', '', '', 7, '1', 0, 1, 10, 1581654087, 1581654619, NULL, NULL, 0, NULL),
(10, 4, 'Ruang Rapat di Villatel Salse', '', 'ruang-rapat', '', '', '', 7, '1', 0, 1, 10, 1581654087, 1581654619, NULL, NULL, 0, NULL),
(11, 2, 'Semi Outdoor of Resto Joglo Salse', '', 'outdoor-salse', '', '', '', 24, '6', 0, 1, 11, 1582688966, 1582691461, 1580095200, NULL, 0, NULL),
(11, 4, 'Semi Terbuka Resto Joglo Salse', '', 'outdoor-joglo', '', '', '', 24, '6', 0, 1, 11, 1582688966, 1582691461, 1580095200, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_article_file`
--

CREATE TABLE `pm_article_file` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `home` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 1,
  `rank` int(11) DEFAULT 0,
  `file` varchar(250) DEFAULT NULL,
  `label` varchar(250) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_article_file`
--

INSERT INTO `pm_article_file` (`id`, `lang`, `id_item`, `home`, `checked`, `rank`, `file`, `label`, `type`) VALUES
(5, 1, 4, NULL, 1, 1, 'whatsapp-image-2019-12-10-at-17-39-03-1.jpeg', '', 'image'),
(5, 2, 4, NULL, 1, 1, 'whatsapp-image-2019-12-10-at-17-39-03-1.jpeg', '', 'image'),
(5, 3, 4, NULL, 1, 1, 'whatsapp-image-2019-12-10-at-17-39-03-1.jpeg', '', 'image'),
(5, 4, 4, NULL, 1, 1, 'whatsapp-image-2019-12-10-at-17-39-03-1.jpeg', '', 'image'),
(6, 1, 4, NULL, 1, 2, 'whatsapp-image-2019-12-10-at-17-39-02-1.jpeg', '', 'image'),
(6, 2, 4, NULL, 1, 2, 'whatsapp-image-2019-12-10-at-17-39-02-1.jpeg', '', 'image'),
(6, 3, 4, NULL, 1, 2, 'whatsapp-image-2019-12-10-at-17-39-02-1.jpeg', '', 'image'),
(6, 4, 4, NULL, 1, 2, 'whatsapp-image-2019-12-10-at-17-39-02-1.jpeg', '', 'image'),
(7, 1, 4, NULL, 1, 3, 'whatsapp-image-2019-12-10-at-17-39-00.jpeg', '', 'image'),
(7, 2, 4, NULL, 1, 3, 'whatsapp-image-2019-12-10-at-17-39-00.jpeg', '', 'image'),
(7, 3, 4, NULL, 1, 3, 'whatsapp-image-2019-12-10-at-17-39-00.jpeg', '', 'image'),
(7, 4, 4, NULL, 1, 3, 'whatsapp-image-2019-12-10-at-17-39-00.jpeg', '', 'image'),
(8, 1, 4, NULL, 1, 4, 'whatsapp-image-2019-12-10-at-17-39-04-1.jpeg', '', 'image'),
(8, 2, 4, NULL, 1, 4, 'whatsapp-image-2019-12-10-at-17-39-04-1.jpeg', '', 'image'),
(8, 3, 4, NULL, 1, 4, 'whatsapp-image-2019-12-10-at-17-39-04-1.jpeg', '', 'image'),
(8, 4, 4, NULL, 1, 4, 'whatsapp-image-2019-12-10-at-17-39-04-1.jpeg', '', 'image'),
(9, 1, 4, NULL, 1, 5, 'whatsapp-image-2019-12-10-at-17-39-01-1.jpeg', '', 'image'),
(9, 2, 4, NULL, 1, 5, 'whatsapp-image-2019-12-10-at-17-39-01-1.jpeg', '', 'image'),
(9, 3, 4, NULL, 1, 5, 'whatsapp-image-2019-12-10-at-17-39-01-1.jpeg', '', 'image'),
(9, 4, 4, NULL, 1, 5, 'whatsapp-image-2019-12-10-at-17-39-01-1.jpeg', '', 'image'),
(10, 1, 4, NULL, 1, 6, 'whatsapp-image-2019-12-10-at-17-39-03.jpeg', '', 'image'),
(10, 2, 4, NULL, 1, 6, 'whatsapp-image-2019-12-10-at-17-39-03.jpeg', '', 'image'),
(10, 3, 4, NULL, 1, 6, 'whatsapp-image-2019-12-10-at-17-39-03.jpeg', '', 'image'),
(10, 4, 4, NULL, 1, 6, 'whatsapp-image-2019-12-10-at-17-39-03.jpeg', '', 'image'),
(11, 1, 4, NULL, 1, 7, 'whatsapp-image-2019-12-10-at-17-39-02.jpeg', '', 'image'),
(11, 2, 4, NULL, 1, 7, 'whatsapp-image-2019-12-10-at-17-39-02.jpeg', '', 'image'),
(11, 3, 4, NULL, 1, 7, 'whatsapp-image-2019-12-10-at-17-39-02.jpeg', '', 'image'),
(11, 4, 4, NULL, 1, 7, 'whatsapp-image-2019-12-10-at-17-39-02.jpeg', '', 'image'),
(13, 1, 4, NULL, 1, 9, 'whatsapp-image-2019-12-10-at-17-39-03.jpeg', '', 'image'),
(13, 2, 4, NULL, 1, 9, 'whatsapp-image-2019-12-10-at-17-39-03.jpeg', '', 'image'),
(13, 3, 4, NULL, 1, 9, 'whatsapp-image-2019-12-10-at-17-39-03.jpeg', '', 'image'),
(13, 4, 4, NULL, 1, 9, 'whatsapp-image-2019-12-10-at-17-39-03.jpeg', '', 'image'),
(14, 1, 4, NULL, 1, 10, 'whatsapp-image-2019-12-10-at-17-39-04.jpeg', '', 'image'),
(14, 2, 4, NULL, 1, 10, 'whatsapp-image-2019-12-10-at-17-39-04.jpeg', '', 'image'),
(14, 3, 4, NULL, 1, 10, 'whatsapp-image-2019-12-10-at-17-39-04.jpeg', '', 'image'),
(14, 4, 4, NULL, 1, 10, 'whatsapp-image-2019-12-10-at-17-39-04.jpeg', '', 'image'),
(15, 1, 4, NULL, 1, 11, 'warungsalse2.jpg', '', 'image'),
(15, 2, 4, NULL, 1, 11, 'warungsalse2.jpg', '', 'image'),
(15, 3, 4, NULL, 1, 11, 'warungsalse2.jpg', '', 'image'),
(15, 4, 4, NULL, 1, 11, 'warungsalse2.jpg', '', 'image'),
(16, 1, 4, NULL, 1, 12, 'warungsalse4.jpg', '', 'image'),
(16, 2, 4, NULL, 1, 12, 'warungsalse4.jpg', '', 'image'),
(16, 3, 4, NULL, 1, 12, 'warungsalse4.jpg', '', 'image'),
(16, 4, 4, NULL, 1, 12, 'warungsalse4.jpg', '', 'image'),
(17, 1, 4, NULL, 1, 13, 'warungsalse1.jpg', '', 'image'),
(17, 2, 4, NULL, 1, 13, 'warungsalse1.jpg', '', 'image'),
(17, 3, 4, NULL, 1, 13, 'warungsalse1.jpg', '', 'image'),
(17, 4, 4, NULL, 1, 13, 'warungsalse1.jpg', '', 'image'),
(18, 1, 4, NULL, 1, 14, 'warungsalse3.jpg', '', 'image'),
(18, 2, 4, NULL, 1, 14, 'warungsalse3.jpg', '', 'image'),
(18, 3, 4, NULL, 1, 14, 'warungsalse3.jpg', '', 'image'),
(18, 4, 4, NULL, 1, 14, 'warungsalse3.jpg', '', 'image'),
(30, 1, 7, NULL, 1, 2, 'interior-salse-1.jpeg', '', 'image'),
(30, 2, 7, NULL, 1, 2, 'interior-salse-1.jpeg', '', 'image'),
(30, 3, 7, NULL, 1, 2, 'interior-salse-1.jpeg', '', 'image'),
(30, 4, 7, NULL, 1, 2, 'interior-salse-1.jpeg', '', 'image'),
(32, 1, 7, NULL, 1, 3, 'interior-salse-2.jpeg', '', 'image'),
(32, 2, 7, NULL, 1, 3, 'interior-salse-2.jpeg', '', 'image'),
(32, 3, 7, NULL, 1, 3, 'interior-salse-2.jpeg', '', 'image'),
(32, 4, 7, NULL, 1, 3, 'interior-salse-2.jpeg', '', 'image'),
(44, 1, 6, NULL, 2, 4, 'exterior-salse-5.jpeg', '', 'image'),
(44, 2, 6, NULL, 2, 4, 'exterior-salse-5.jpeg', '', 'image'),
(44, 3, 6, NULL, 2, 4, 'exterior-salse-5.jpeg', '', 'image'),
(44, 4, 6, NULL, 2, 4, 'exterior-salse-5.jpeg', '', 'image'),
(45, 1, 6, NULL, 2, 5, 'exterior-salse-3.jpeg', '', 'image'),
(45, 2, 6, NULL, 2, 5, 'exterior-salse-3.jpeg', '', 'image'),
(45, 3, 6, NULL, 2, 5, 'exterior-salse-3.jpeg', '', 'image'),
(45, 4, 6, NULL, 2, 5, 'exterior-salse-3.jpeg', '', 'image'),
(46, 1, 6, NULL, 1, 3, 'exterior-salse-2.jpeg', '', 'image'),
(46, 2, 6, NULL, 1, 3, 'exterior-salse-2.jpeg', '', 'image'),
(46, 3, 6, NULL, 1, 3, 'exterior-salse-2.jpeg', '', 'image'),
(46, 4, 6, NULL, 1, 3, 'exterior-salse-2.jpeg', '', 'image'),
(47, 1, 6, NULL, 1, 1, 'exterior-salse-1.jpeg', '', 'image'),
(47, 2, 6, NULL, 1, 1, 'exterior-salse-1.jpeg', '', 'image'),
(47, 3, 6, NULL, 1, 1, 'exterior-salse-1.jpeg', '', 'image'),
(47, 4, 6, NULL, 1, 1, 'exterior-salse-1.jpeg', '', 'image'),
(48, 1, 6, NULL, 1, 2, 'exterior-salse-4.jpeg', '', 'image'),
(48, 2, 6, NULL, 1, 2, 'exterior-salse-4.jpeg', '', 'image'),
(48, 3, 6, NULL, 1, 2, 'exterior-salse-4.jpeg', '', 'image'),
(48, 4, 6, NULL, 1, 2, 'exterior-salse-4.jpeg', '', 'image'),
(57, 2, 9, NULL, 1, 6, 'img-2400.jpg', NULL, 'image'),
(57, 4, 9, NULL, 1, 6, 'img-2400.jpg', NULL, 'image'),
(58, 2, 9, NULL, 1, 5, 'img-2448.jpg', NULL, 'image'),
(58, 4, 9, NULL, 1, 5, 'img-2448.jpg', NULL, 'image'),
(59, 2, 9, NULL, 1, 1, 'img-2445.jpg', NULL, 'image'),
(59, 4, 9, NULL, 1, 1, 'img-2445.jpg', NULL, 'image'),
(60, 2, 9, NULL, 1, 2, 'img-2443.jpg', NULL, 'image'),
(60, 4, 9, NULL, 1, 2, 'img-2443.jpg', NULL, 'image'),
(61, 2, 9, NULL, 1, 4, 'img-2440.jpg', NULL, 'image'),
(61, 4, 9, NULL, 1, 4, 'img-2440.jpg', NULL, 'image'),
(62, 2, 9, NULL, 1, 3, 'img-2437.jpg', NULL, 'image'),
(62, 4, 9, NULL, 1, 3, 'img-2437.jpg', NULL, 'image'),
(63, 2, 5, NULL, 1, 15, 'img-3460.jpg', '', 'image'),
(63, 4, 5, NULL, 1, 15, 'img-3460.jpg', '', 'image'),
(64, 2, 5, NULL, 1, 16, 'img-3458.jpg', '', 'image'),
(64, 4, 5, NULL, 1, 16, 'img-3458.jpg', '', 'image'),
(65, 2, 5, NULL, 1, 17, 'img-3456.jpg', '', 'image'),
(65, 4, 5, NULL, 1, 17, 'img-3456.jpg', '', 'image'),
(66, 2, 5, NULL, 1, 18, 'img-3455.jpg', '', 'image'),
(66, 4, 5, NULL, 1, 18, 'img-3455.jpg', '', 'image'),
(67, 2, 8, NULL, 1, 3, 'makanan-salse-3.jpg', NULL, 'image'),
(67, 4, 8, NULL, 1, 3, 'makanan-salse-3.jpg', NULL, 'image'),
(68, 2, 8, NULL, 1, 2, 'makanan-salse-2.jpg', NULL, 'image'),
(68, 4, 8, NULL, 1, 2, 'makanan-salse-2.jpg', NULL, 'image'),
(69, 2, 8, NULL, 2, 4, 'makanan-salse-4.jpg', NULL, 'image'),
(69, 4, 8, NULL, 2, 4, 'makanan-salse-4.jpg', NULL, 'image'),
(70, 2, 8, NULL, 1, 1, 'makanan-salse-5.jpg', NULL, 'image'),
(70, 4, 8, NULL, 1, 1, 'makanan-salse-5.jpg', NULL, 'image'),
(72, 2, 7, NULL, 2, 4, 'interior-salse.jpg', '', 'image'),
(72, 4, 7, NULL, 2, 4, 'interior-salse.jpg', '', 'image'),
(73, 2, 7, NULL, 1, 1, 'interior.jpg', NULL, 'image'),
(73, 4, 7, NULL, 1, 1, 'interior.jpg', NULL, 'image'),
(74, 2, 10, NULL, 1, 1, 'img-2286.jpg', '', 'image'),
(74, 4, 10, NULL, 1, 1, 'img-2286.jpg', '', 'image'),
(75, 2, 10, NULL, 1, 3, 'img-2300.jpg', '', 'image'),
(75, 4, 10, NULL, 1, 3, 'img-2300.jpg', '', 'image'),
(76, 2, 10, NULL, 1, 2, 'img-2297.jpg', '', 'image'),
(76, 4, 10, NULL, 1, 2, 'img-2297.jpg', '', 'image'),
(77, 2, 10, NULL, 1, 4, 'img-2293.jpg', '', 'image'),
(77, 4, 10, NULL, 1, 4, 'img-2293.jpg', '', 'image'),
(78, 2, 10, NULL, 1, 5, 'img-2299.jpg', '', 'image'),
(78, 4, 10, NULL, 1, 5, 'img-2299.jpg', '', 'image'),
(79, 2, 10, NULL, 1, 6, 'img-2285-1.jpg', '', 'image'),
(79, 4, 10, NULL, 1, 6, 'img-2285-1.jpg', '', 'image'),
(88, 2, 11, NULL, 1, 1, 'semi-outdoor1.jpg', '', 'image'),
(88, 4, 11, NULL, 1, 1, 'semi-outdoor1.jpg', '', 'image'),
(89, 2, 11, NULL, 1, 19, 'semi-outdoor4.jpg', NULL, 'image'),
(89, 4, 11, NULL, 1, 19, 'semi-outdoor4.jpg', NULL, 'image'),
(90, 2, 11, NULL, 1, 20, 'semi-outdoor7.jpg', NULL, 'image'),
(90, 4, 11, NULL, 1, 20, 'semi-outdoor7.jpg', NULL, 'image'),
(91, 2, 11, NULL, 1, 21, 'semi-outdoor8.jpg', NULL, 'image'),
(91, 4, 11, NULL, 1, 21, 'semi-outdoor8.jpg', NULL, 'image'),
(92, 2, 11, NULL, 1, 22, 'semi-outdoor2.jpg', NULL, 'image'),
(92, 4, 11, NULL, 1, 22, 'semi-outdoor2.jpg', NULL, 'image'),
(93, 2, 11, NULL, 1, 23, 'semi-outdoor6.jpg', NULL, 'image'),
(93, 4, 11, NULL, 1, 23, 'semi-outdoor6.jpg', NULL, 'image'),
(94, 2, 11, NULL, 1, 24, 'semi-outdoor5.jpg', NULL, 'image'),
(94, 4, 11, NULL, 1, 24, 'semi-outdoor5.jpg', NULL, 'image'),
(95, 2, 11, NULL, 1, 25, 'semi-outdoor3.jpg', NULL, 'image'),
(95, 4, 11, NULL, 1, 25, 'semi-outdoor3.jpg', NULL, 'image');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_booking`
--

CREATE TABLE `pm_booking` (
  `id` int(11) NOT NULL,
  `id_hotel` int(11) NOT NULL,
  `add_date` int(11) DEFAULT NULL,
  `edit_date` int(11) DEFAULT NULL,
  `from_date` int(11) DEFAULT NULL,
  `to_date` int(11) DEFAULT NULL,
  `nights` int(11) DEFAULT 1,
  `adults` int(11) DEFAULT 1,
  `children` int(11) DEFAULT 1,
  `amount` float DEFAULT NULL,
  `tourist_tax` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `ex_tax` float DEFAULT NULL,
  `tax_amount` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `down_payment` float DEFAULT NULL,
  `paid` float DEFAULT NULL,
  `balance` float DEFAULT NULL,
  `extra_services` text DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `company` varchar(50) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `postcode` varchar(10) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `comments` text DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `trans` varchar(50) DEFAULT NULL,
  `payment_date` int(11) DEFAULT NULL,
  `payment_option` varchar(250) DEFAULT NULL,
  `users` text DEFAULT NULL,
  `transidmerchant` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_booking`
--

INSERT INTO `pm_booking` (`id`, `id_hotel`, `add_date`, `edit_date`, `from_date`, `to_date`, `nights`, `adults`, `children`, `amount`, `tourist_tax`, `discount`, `ex_tax`, `tax_amount`, `total`, `down_payment`, `paid`, `balance`, `extra_services`, `id_user`, `firstname`, `lastname`, `email`, `company`, `address`, `postcode`, `city`, `phone`, `mobile`, `country`, `comments`, `status`, `trans`, `payment_date`, `payment_option`, `users`, `transidmerchant`) VALUES
(6, 1, 1578294038, NULL, 1578268800, 1578355200, 1, 1, 0, 10000, NULL, 0, 10000, 0, 10000, 0, 10000, NULL, NULL, 4, 'Stage1', NULL, 'irfanrona@outlook.com', NULL, 'booking test drive', NULL, NULL, '091028324', NULL, NULL, 'Testing', 4, NULL, 1578294346, 'doku', '1', '15e12db'),
(7, 1, 1579498626, NULL, 1579478400, 1579564800, 1, 1, 0, 750000, NULL, 0, 750000, 0, 750000, 0, NULL, NULL, NULL, NULL, 'Yogi Darusmana', '', 'yogi.darusmana87@gmail.com', '', 'Bandung', '', '', '087808786847', '', '', '', 1, NULL, NULL, 'doku', '1', '15e253c'),
(8, 1, 1579838971, NULL, 1579824000, 1579910400, 1, 1, 0, 10000, NULL, 0, 10000, 0, 10000, 0, 10000, NULL, NULL, NULL, 'irfan rona', '', 'irfanrona@outlook.com', '', 'Jalan Pelajar Pejuang 45', '', '', '08123456789', '', '', 'test special', 4, NULL, 1579839431, 'doku', '1', '15e2a6d'),
(10, 1, 1580053740, 1580054012, 1580256000, 1580428800, 2, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 'Grup Mitra Agung Tekindo', 'OVI Marketing', 'villatelsalse@gmail.com', '', 'Bandung', '40391', 'Bandung', '0222504105', '', 'Indonesia', '', 4, NULL, NULL, '', '6', NULL),
(11, 1, 1583058476, NULL, 1583020800, 1583107200, 1, 2, 0, 495000, NULL, 0, 495000, 0, 495000, 0, 495000, NULL, NULL, NULL, 'Difa sabila azka', '', 'Difasabilazka@gmail.com', '', 'Ancol timur', '', '', '087823201843', '', '', '', 4, NULL, 1583059047, 'doku', '1', '15e5b8e'),
(15, 1, 1583297326, NULL, 1583280000, 1583366400, 1, 1, 0, 440000, NULL, 0, 440000, 0, 440000, 0, NULL, NULL, NULL, 1, 'Administrator', NULL, 'payment.lawangwangi@gmail.com', NULL, 'Bandung', NULL, NULL, '0987654321', NULL, NULL, 'INI ORDER FIKTIK TOLONG ABAIKAN. UNTUK MENGECEK BOOKING', 1, NULL, NULL, 'arrival', '1', '15e5f33'),
(16, 1, 1583307707, NULL, 1583280000, 1583366400, 1, 1, 0, 440000, NULL, 0, 440000, 0, 440000, NULL, NULL, NULL, NULL, NULL, 'Andri', '', 'andribarker155@gmail.com', '', 'Kp. Langensari No.20 rt/rw 01/03', '', '', '08999757502', '', '', '', 1, NULL, NULL, 'arrival', '1', '15e5f5b'),
(17, 1, 1583318579, NULL, 1583280000, 1583366400, 1, 1, 0, 440000, NULL, 0, 440000, 0, 440000, NULL, NULL, NULL, NULL, NULL, 'Andri', '', 'andribarker155@gmail.com', '', 'Kp. Langensari No.20 rt/rw 01/03', '', '', '08999757502', '', '', '', 1, NULL, NULL, 'arrival', '1', '15e5f86'),
(18, 1, 1583474798, NULL, 1583971200, 1584144000, 2, 2, 0, 1080000, NULL, 0, 1080000, 0, 1080000, 0, NULL, NULL, NULL, NULL, 'dewi utari', '', 'riuta_311@yahoo.com', '', 'jl. gamprit remaja 2 no 7 jatiwaringin pondok gede', '', '', '08121961147', '', '', '', 1, NULL, NULL, 'arrival', '1', '15e61e8'),
(19, 1, 1583897349, NULL, 1584144000, 1584230400, 1, 2, 0, 520000, NULL, 0, 520000, 0, 520000, 0, 520000, NULL, NULL, NULL, 'TUNKI RACHMAN SANUSI', '', 'tunkirachman@gmail.com', '', 'JL. LAYAR IV-B NOMOR 2 RT/RW 003/007 KECAMATAN KELAPA DUA KELURAHAN KELAPA DUA KABUPATEN TANGERANG', '', '', '081280537999', '', '', '', 4, NULL, 1583899391, 'doku', '1', '15e685b'),
(20, 1, 1584071901, NULL, 1584144000, 1584230400, 1, 2, 0, 585000, NULL, 0, 585000, 0, 585000, NULL, NULL, NULL, NULL, NULL, 'Lisana R', '', 'lisana.rachmawati@gmail.com', '', 'Baleendah, Kab. Bandung', '', '', '085730533593', '', '', '', 1, NULL, NULL, 'arrival', '1', '15e6b04'),
(22, 1, 1593420561, 1593770393, 1595030400, 1595116800, 1, 2, 0, 440000, NULL, 0, 440000, 0, 440000, 0, 440000, 0, NULL, NULL, 'Muhammad Bakhtiar', 'Bakhtiar', 'cruyf_uncu@yahoo.com', '', 'Cielungsi, Kab. Bogor, Jawa Barat', '16820', 'Bogor', '081584833359', '', 'Indonesia', 'Kamar di lantai 2 /\r\n\r\nRESCHEDULE FROM CHECK - IN DATE : 04 JULI 2020 \r\n                                     CHECK - OUT DATE : 05 JULI 2020', 4, NULL, 1593422001, '', '1', '15ef9ab'),
(23, 1, 1594566041, NULL, 1595289600, 1595376000, 1, 1, 0, 510000, NULL, 0, 510000, 0, 510000, 0, NULL, NULL, NULL, NULL, 'Asep', '', 'Asep@gmail.com', '', 'Jayapura', '', '', '081234567890', '', '', '', 1, NULL, NULL, 'doku', '1', '15f0b25'),
(24, 1, 1595243505, NULL, 1595548800, 1595635200, 1, 2, 0, 575000, NULL, 0, 575000, 0, 575000, 0, 575000, NULL, NULL, NULL, 'Febi Fajar Dirana', '', 'fajar.dirana@yahoo.com', '', 'Palmerah Jakarta Barat', '', '', '85781800207', '', '', 'Twin Bed', 4, NULL, 1595243736, 'doku', '1', '15f157b'),
(25, 1, 1596025990, NULL, 1596240000, 1596326400, 1, 2, 0, 820000, NULL, 0, 820000, 0, 820000, 0, NULL, NULL, NULL, NULL, 'Inez ', '', 'inezstef@gmail.com', '', 'Setraduta Cemara 1 no 35 ', '', '', '+62 8122108124', '', '', '', 1, NULL, NULL, 'doku', '1', '15f216c'),
(26, 1, 1597657516, NULL, 1597881600, 1597968000, 1, 2, 0, 550000, NULL, 0, 550000, 0, 550000, 0, 550000, NULL, NULL, NULL, 'Juni Riski Amelia', '', 'juni.amelia@gmail.com', '', 'Jl. Batu Permata No.46 Batu Ampar Kramatjati Jakarta Timur', '', '', '08128572944', '', '', '', 4, NULL, 1597657646, 'doku', '1', '15f3a51'),
(27, 1, 1597861068, NULL, 1597881600, 1598054400, 2, 2, 0, 1280000, NULL, 0, 1280000, 0, 1280000, 0, NULL, NULL, NULL, NULL, 'yosa herfian', '', 'yosasoedarsono@yahoo.co.id', '', 'Jl. Qadr Raya No. 12, Karawaci Tangerang', '', '', '0816991497', '', '', '', 1, NULL, NULL, 'doku', '1', '15f3d6c'),
(28, 1, 1598336018, NULL, 1598659200, 1598745600, 1, 1, 0, 550000, NULL, 0, 550000, 0, 550000, 0, NULL, NULL, NULL, NULL, 'Okky Selvie Steliana', '', 'okkysteliana.8@gmail.com', '', 'Jl. Kalibata Selatan IIE rt.003 rw.04 No.56', '', '', '082112600693', '', '', '', 1, NULL, NULL, 'doku', '1', '15f44ac'),
(29, 2, 1601613121, NULL, 1602892800, 1602979200, 1, 1, 0, 300000, NULL, 0, 300000, 0, 300000, 0, NULL, NULL, NULL, NULL, 'Dea Delvita', '', 'nabilaputriv@gmail.com', '', 'Jl. Cisirung No 49', '', '', '085795174743', '', '', '', 1, NULL, NULL, 'doku', '6', '15f76ad'),
(30, 2, 1601613663, NULL, 1602892800, 1602979200, 1, 2, 0, 300000, NULL, 0, 300000, 0, 300000, 0, 300000, NULL, NULL, NULL, 'Dea Delvita', '', 'nabilaputriv@gmail.com', '', 'Jl. Cisirung No 49', '', '', '085795174743', '', '', '', 4, NULL, 1601613830, 'doku', '6', '15f76af'),
(31, 1, 1602230261, NULL, 1602288000, 1602374400, 1, 2, 0, 590000, NULL, 0, 590000, 0, 590000, 0, NULL, NULL, NULL, NULL, 'Heryanto Handaja', '', 'heryhandaja74@gmail.com', '', 'Gg. Mesjid Buntu no. 23, Jl. Tubagus Angke, Tambora, JakBar', '', '', '087787625808', '', '', '', 1, NULL, NULL, 'doku', '1', '15f8017'),
(32, 1, 1603699175, NULL, 1603843200, 1603929600, 1, 2, 0, 510000, NULL, 0, 510000, 0, 510000, 0, 510000, NULL, NULL, NULL, 'Arifin Amanto Putra', '', 'adrian.putra100@gmail.com', '', 'Bintaro', '', '', '085782440380', '', '', '', 4, NULL, 1603699797, 'doku', '1', '15f9681'),
(33, 2, 1604079137, NULL, 1604102400, 1604188800, 1, 2, 0, 415000, NULL, 0, 415000, 0, 415000, 0, NULL, NULL, NULL, NULL, 'Franciscus Andika SJahli', '', 'andika.syahli@yahoo.co.id', '', 'Jalan Karang Tineung Indah III/16', '', '', '081221200288', '', '', 'kalau bisa check in jam 12', 1, NULL, NULL, 'doku', '6', '15f9c4e'),
(34, 2, 1604079367, NULL, 1604102400, 1604188800, 1, 2, 0, 415000, NULL, 0, 415000, 0, 415000, 0, 415000, NULL, NULL, NULL, 'Franciscus Andika SJahli', NULL, 'andika.syahli@yahoo.co.id', NULL, 'Jalan Karang Tineung Indah III/16', NULL, NULL, '081221200288', NULL, NULL, '', 4, NULL, 1604079544, 'doku', '6', '15f9c4f'),
(35, 2, 1604080475, NULL, 1604102400, 1604188800, 1, 2, 0, 415000, NULL, 0, 415000, 0, 415000, 0, NULL, NULL, NULL, NULL, 'Franciscus Andika SJahli', NULL, 'andika.syahli@yahoo.co.id', NULL, 'Jalan Karang Tineung Indah III/16', NULL, NULL, '081221200288', NULL, NULL, '', 1, NULL, NULL, 'doku', '6', '15f9c53'),
(36, 2, 1604312401, NULL, 1605916800, 1606003200, 1, 4, 0, 650000, NULL, 0, 650000, 0, 650000, 0, NULL, NULL, NULL, NULL, 'Yunita Ng', '', 'yunita.ng36@gmail.com', '', 'Jl. Permata biru I H3/15', '', '', '081585550306', '', '', '', 1, NULL, NULL, 'doku', '6', '15f9fdd'),
(37, 2, 1604323238, 1605791191, 1606521600, 1606608000, 1, 4, 0, 650000, NULL, 0, 650000, 0, 650000, 0, 650000, 0, NULL, 16, 'Yunita Ng', 'Ng', 'yunita.ng36@gmail.com', '', 'Jl. Permata biru I H3/15', '14240', 'Kelapa Gading, jakarta Utara', '081585550306', '', 'Indonesia', 'Request Rechedule from 21- 22 Nov 2020 to 28 - 29 Nov 2020.\r\n\r\nNotes : Reschedule hanya berlaku satu kali penentuan.', 4, NULL, 1604323408, '', '6', '15fa007'),
(38, 1, 1605540991, NULL, 1605916800, 1606003200, 1, 4, 0, 1020000, NULL, 0, 1020000, 0, 1020000, 0, NULL, NULL, NULL, NULL, 'siti rezita aulia', '', 'rezitaulia@gmail.com', '', 'bekasi', '', '', '+6283899991349', '', '', '', 1, NULL, NULL, 'doku', '1', '15fb29c'),
(39, 1, 1605851351, NULL, 1605916800, 1606003200, 1, 2, 0, 510000, NULL, 0, 510000, 0, 510000, 0, NULL, NULL, NULL, NULL, 'Ametha Safa', '', 'amethasfns@gmail.com', '', 'JL. Tembok no.20b', '', '', '087825643677', '', '', '', 1, NULL, NULL, 'doku', '1', '15fb758'),
(40, 1, 1605852625, NULL, 1605916800, 1606003200, 1, 2, 0, 510000, NULL, 0, 510000, 0, 510000, 0, 510000, NULL, NULL, NULL, 'Ametha Safa', NULL, 'amethasfns@gmail.com', NULL, 'JL. Tembok no.20b', NULL, NULL, '087825643677', NULL, NULL, '', 4, NULL, 1605852741, 'doku', '1', '15fb75d'),
(41, 1, 1606835001, NULL, 1607731200, 1607817600, 1, 6, 0, 1630000, NULL, 0, 1630000, 0, 1630000, 0, NULL, NULL, NULL, NULL, 'Ribka', '', 'ribkaberliana8@gmail.com', '', 'Bekasi', '', '', '085723603439', '', '', '', 1, NULL, NULL, 'doku', '1', '15fc65b'),
(42, 1, 1607909231, NULL, 1607904000, 1607990400, 1, 2, 0, 700000, NULL, 0, 700000, 0, 700000, 0, 700000, NULL, NULL, NULL, 'Adinda umairo yasmine', '', 'Adindayasmin6@gmail.com', '', 'Kp cerewed rt 01/08 no.56 duren jaya bekasi timur', '', '', '085718724521', '', '', '', 4, NULL, 1607911672, 'doku', '1', '15fd6bf'),
(43, 1, 1609743975, NULL, 1611964800, 1612051200, 1, 2, 0, 500000, NULL, 0, 500000, 0, 500000, 0, NULL, NULL, NULL, NULL, 'Tresna Puja Lestari', '', 'risniry@gmail.com', '', 'Bandung', '', '', '0813-2427-6772', '', '', '', 1, NULL, NULL, 'doku', '1', '15ff2be'),
(44, 1, 1610007049, NULL, 1610323200, 1610409600, 1, 2, 0, 500000, NULL, 0, 500000, 0, 500000, 0, 500000, NULL, NULL, NULL, 'Razief Muhammad Irsyadillah', '', 'irsyadillahmuhammad@yahoo.com', '', 'Tangerang', '', '', '08111571471', '', '', '', 4, NULL, 1610007237, 'doku', '1', '15ff6c2'),
(45, 1, 1613070289, NULL, 1613088000, 1613174400, 1, 2, 0, 500000, NULL, 0, 500000, 0, 500000, 0, NULL, NULL, NULL, NULL, 'Fahri Akbar Nugraha', NULL, 'fahrianugraha@icloud.com', NULL, 'Jl Jurang Gg Pa Ehom No 706/181 RT05 RW06', NULL, NULL, '0811-1111-0095', NULL, NULL, '', 1, NULL, NULL, 'doku', '1', '160257f'),
(46, 2, 1613657811, NULL, 1613779200, 1613865600, 1, 1, 0, 340000, NULL, 0, 340000, 0, 340000, 0, NULL, NULL, NULL, NULL, 'Dennis Evans', '', 'dennis.evans.p@gmail.com', '', 'Jl. Kacapiring no.10/122', '', '', '088229135433', '', '', '', 1, NULL, NULL, 'doku', '6', '1602e76'),
(47, 2, 1616221384, NULL, 1616198400, 1616284800, 1, 1, 0, 300000, NULL, 0, 300000, 0, 300000, 0, NULL, NULL, NULL, NULL, 'Bella Sandra Pratiwi', '', 'bellasandrapratiwi@gmail.com', '', 'Bandung inten indah b1 no 13', '', '', '081321910149', '', '', '', 1, NULL, NULL, 'doku', '6', '1605594'),
(48, 1, 1616940042, NULL, 1617148800, 1617235200, 1, 2, 0, 750000, NULL, 0, 750000, 0, 750000, 0, NULL, NULL, NULL, NULL, 'donny boy', '', 'kid_lyfe@yahoo.com', '', 'Kaljiati 2 Bandung', '', '', '0895334063375', '', '', '', 1, NULL, NULL, 'doku', '1', '160608c'),
(49, 1, 1616991252, NULL, 1617148800, 1617235200, 1, 1, 0, 750000, NULL, 0, 750000, 0, 750000, 0, 750000, NULL, NULL, NULL, 'Donny Boy', NULL, 'kid_lyfe@yahoo.com', NULL, 'Jl. kalijati 2 Bandung', NULL, NULL, '0895334063375', NULL, NULL, '2 adults', 4, NULL, 1616991418, 'doku', '1', '1606154'),
(50, 1, 1617038727, NULL, 1617321600, 1617494400, 2, 2, 0, 1050000, NULL, 0, 1050000, 0, 1050000, 0, NULL, NULL, NULL, NULL, 'Hardi Pratama', '', 'radenhardi96@gmail.com', '', 'Matraman, Jakarta', '', '', '08118889433', '', '', '', 1, NULL, NULL, 'doku', '1', '160620d'),
(51, 1, 1617064379, NULL, 1617321600, 1617494400, 2, 2, 0, 1050000, NULL, 0, 1050000, 0, 1050000, 0, NULL, NULL, NULL, NULL, 'Hardi Pratama', '', 'radenhardi96@gmail.com', '', 'Matraman, Jakarta', '', '', '0811 888 9433', '', '', '', 1, NULL, NULL, 'doku', '1', '1606271'),
(52, 1, 1617106427, NULL, 1617321600, 1617494400, 2, 2, 0, 1100000, NULL, 0, 1100000, 0, 1100000, 0, 1100000, NULL, NULL, NULL, 'Hardi Pratama', '', 'radenhardi96@gmail.com', '', 'Matraman, Jakarta', '', '', '0811 888 9433', '', '', '', 4, NULL, 1617106757, 'doku', '1', '1606315'),
(53, 2, 1617183797, NULL, 1617148800, 1617235200, 1, 2, 0, 325000, NULL, 0, 325000, 0, 325000, 0, NULL, NULL, NULL, NULL, 'Dani Yudansyah', '', 'dani.lawangwangi@gmail.com', '', 'Kpm Pasir Ipis Lembang', '', '', '088218373819', '', '', '', 1, NULL, NULL, 'doku', '6', '1606444'),
(54, 2, 1617184105, NULL, 1618358400, 1618444800, 1, 1, 0, 325000, NULL, 0, 325000, 0, 325000, 0, NULL, NULL, NULL, NULL, 'Dani Yudansyah', '', 'dani.lawangwangi@gmail.com', '', 'Kpm Pasir Ipis Lembang', '', '', '088218373819', '', '', '', 1, NULL, NULL, 'doku', '6', '1606445'),
(55, 2, 1617185282, NULL, 1617148800, 1617235200, 1, 1, 0, 325000, NULL, 0, 325000, 0, 325000, 0, NULL, NULL, NULL, NULL, 'Esti Mayestika', '', 'estimayestika@gmail.com', '', 'antapani', '', '', '085722929844', '', '', '', 1, NULL, NULL, 'doku', '6', '160644a'),
(56, 2, 1617598009, NULL, 1618012800, 1618099200, 1, 2, 0, 325000, NULL, 0, 325000, 0, 325000, 0, NULL, NULL, NULL, NULL, 'Non tiara', '', 'nontiara789@gmail.com', '', 'Karawang, Jawa barat', '', '', '085738135149', '', '', '', 1, NULL, NULL, 'doku', '6', '1606a96'),
(57, 2, 1617612674, NULL, 1618012800, 1618099200, 1, 2, 0, 325000, NULL, 0, 325000, 0, 325000, 0, NULL, NULL, NULL, NULL, 'Non tiara', '', 'nontiara789@gmail.com', '', 'Karawang, jabar', '', '', '085738135149', '', '', '', 1, NULL, NULL, 'doku', '6', '1606acf'),
(58, 2, 1617722717, NULL, 1618012800, 1618099200, 1, 2, 0, 325000, NULL, 0, 325000, 0, 325000, 0, NULL, NULL, NULL, NULL, 'Firdayanti', '', 'Nontiara789@gmail.com', '', 'Karawang', '', '', '085738135149', '', '', '', 1, NULL, NULL, 'doku', '6', '1606c7d'),
(59, 1, 1617764566, NULL, 1617926400, 1618099200, 2, 2, 0, 1000000, NULL, 0, 1000000, 0, 1000000, 0, NULL, NULL, NULL, NULL, 'Hari Budi Santoso', '', 'daybyhari@gmail.com', '', 'Apartemen Serpong Garden Tower Bellerosa Lt. 26 Unit 12', '', '', '08978230292', '', '', '', 1, NULL, NULL, 'doku', '1', '1606d20'),
(60, 1, 1617765470, NULL, 1617926400, 1618099200, 2, 2, 0, 1000000, NULL, 0, 1000000, 0, 1000000, 0, NULL, NULL, NULL, NULL, 'Hari Budi Santoso', '', 'imoetcookies27011991@gmail.com', '', 'Apartemen Serpong Garden', '', '', '08978230292', '', '', '', 1, NULL, NULL, 'doku', '1', '1606d24'),
(61, 1, 1617767737, NULL, 1617926400, 1618099200, 2, 4, 0, 2000000, NULL, 0, 2000000, 0, 2000000, 0, 2000000, NULL, NULL, NULL, 'Hari Budi Santoso', '', 'imoetcookies27011991@gmail.com', '', 'Apartemen Serpong Garden', '', '', '08978230292', '', '', '', 4, NULL, 1617767932, 'doku', '1', '1606d2d'),
(62, 2, 1617886880, NULL, 1618012800, 1618099200, 1, 2, 0, 325000, NULL, 0, 325000, 0, 325000, 0, NULL, NULL, NULL, NULL, 'Non tiara', '', 'Nontiara789@gmail.com', '', 'Karawang', '', '', '085738135149', '', '', '', 1, NULL, NULL, 'doku', '6', '1606efe'),
(63, 2, 1617888696, NULL, 1618012800, 1618099200, 1, 2, 0, 325000, NULL, 0, 325000, 0, 325000, 0, 325000, NULL, NULL, NULL, 'Non tiara ', '', 'Nontiara789@gmail.com', '', 'Karawang', '', '', '085738135149', '', '', '', 4, NULL, 1617888854, 'doku', '6', '1606f05'),
(64, 1, 1618053677, NULL, 1618012800, 1618099200, 1, 2, 0, 680000, NULL, 0, 680000, 0, 680000, 0, 680000, NULL, NULL, NULL, 'Denes', '', 'septiandenes@gmail.com', '', 'Karawang', '', '', '087877362582', '', '', '', 4, NULL, 1618053957, 'doku', '1', '160718a'),
(65, 2, 1620740547, NULL, 1621814400, 1621900800, 1, 2, 0, 325000, NULL, 0, 325000, 0, 325000, 0, NULL, NULL, NULL, NULL, 'Milla Julianty', '', 'millaJlnty15@gmail.com', '', 'Jalan menes kav 122, bandung', '', '', '081573078267', '', '', '', 1, NULL, NULL, 'doku', '6', '1609a89'),
(66, 2, 1620843050, NULL, 1620950400, 1621036800, 1, 1, 0, 325000, NULL, 0, 325000, 0, 325000, 0, NULL, NULL, NULL, NULL, 'Milla Julianty', '', 'millayulianti@gmail.com', '', 'Jalan menes kav 122, bandung', '', '', '081573078267', '', '', '', 1, NULL, NULL, 'doku', '6', '1609c1a'),
(67, 2, 1620887811, NULL, 1620950400, 1621036800, 1, 1, 0, 325000, NULL, 0, 325000, 0, 325000, 0, 325000, NULL, NULL, NULL, 'Milla Julianty', '', 'millayulianti@gmail.com', '', 'Jalan menes kav 122, bandung', '', '', '081573078267', '', '', '', 4, NULL, 1620888003, 'doku', '6', '1609cc9'),
(68, 1, 1621949045, NULL, 1622160000, 1622332800, 2, 2, 0, 1000000, NULL, 0, 1000000, 0, 1000000, 0, NULL, NULL, NULL, NULL, 'Dwida Sujani', '', 'dwidasujani@gmail.com', '', 'Parahyangan Apartment', '', '', '0819 10137795', '', '', '', 1, NULL, NULL, 'doku', '1', '160acfa'),
(69, 1, 1622769898, NULL, 1622851200, 1622937600, 1, 2, 0, 600000, NULL, 0, 600000, 0, 600000, 0, 600000, NULL, NULL, NULL, 'guido van hofwegen', '', 'guido.vanhofwegen@gmail.com', '', 'Komplek Cimindi Raya Blok X No 2', '', '', '+6281360446295', '', '', 'no smoking', 4, NULL, 1622770515, 'doku', '1', '160b980'),
(70, 1, 1623205900, NULL, 1623715200, 1623801600, 1, 2, 0, 500000, NULL, 0, 500000, 0, 500000, 0, 500000, NULL, NULL, NULL, 'zaslyn annisa', '', 'zaslynannisa@gmail.com', '', 'Thamrin Residence, Tower Edelweiss, Unit 9EF', '', '', '081345144736', '', '', '', 4, NULL, 1623206120, 'doku', '1', '160c028'),
(71, 1, 1628012329, NULL, 1628208000, 1628294400, 1, 2, 0, 500000, NULL, 0, 500000, 0, 500000, 0, NULL, NULL, NULL, NULL, 'asep', '', 'asep@mail.com', '', 'bagdad', '', '', '081234567890', '', '', '', 1, NULL, NULL, 'doku', '1', '161097f'),
(72, 1, 1628063394, NULL, 1628294400, 1628380800, 1, 2, 0, 500000, NULL, 0, 500000, 0, 500000, 0, NULL, NULL, NULL, NULL, 'Felia Puspita Dewi', '', 'feliapuspitad@gmail.com', '', 'Jalan Purwakarta No.85 ', '', '', '081224159327', '', '', '', 1, NULL, NULL, 'doku', '1', '1610a46'),
(73, 1, 1628065350, NULL, 1628294400, 1628380800, 1, 2, 0, 500000, NULL, 0, 500000, 0, 500000, 0, 500000, NULL, NULL, NULL, 'Felia Puspita', '', 'feliapuspitad@gmail.com', '', 'Jalan Purwakarta No.85', '', '', '081224159327', '', '', '', 4, NULL, 1628065677, 'doku', '1', '1610a4e'),
(74, 2, 1628652571, NULL, 1628640000, 1628726400, 1, 2, 0, 355000, NULL, 0, 355000, 0, 355000, 0, 355000, NULL, NULL, NULL, 'Nadiyah nur fitri', '', 'Nadiyahnur@gmail.com', '', 'Jl. Pamekar Raya Cluster Myhome Ujungberung', '', '', '085720072045', '', '', '', 4, NULL, 1628654109, 'doku', '6', '1611344'),
(75, 2, 1628896963, NULL, 1628899200, 1628985600, 1, 2, 0, 355000, NULL, 0, 355000, 0, 355000, 0, NULL, NULL, NULL, NULL, 'Ruhiat Hermawan Andrianto', '', 'ruhiathermawana@gmail.com', '', 'Jl. Terusan Bojongsoang Kp. Cijagra Rt 01 Rw 09', '', '', '082217635856', '', '', '', 1, NULL, NULL, 'doku', '6', '16116fe'),
(76, 1, 1630033485, NULL, 1630108800, 1630195200, 1, 8, 0, 2800000, NULL, 0, 2800000, 0, 2800000, 0, 2800000, NULL, NULL, NULL, 'Raisya Hidayat', '', 'raisyarhidayat@yahoo.com', '', 'Jalan Riung Galih no 4 Bandung', '', '', '085156530595', '', '', 'Minta di gedung yang sama ya thank youu', 4, NULL, 1630033918, 'doku', '1', '1612856'),
(77, 1, 1630840582, NULL, 1632009600, 1632096000, 1, 2, 0, 500000, NULL, 0, 500000, 0, 500000, 0, NULL, NULL, NULL, NULL, 'Mutiara Tioni Asprilia', '', 'mtionias@hotmail.com', '', 'Jl. Tamansari no. 90', '', '', '+6281224737869', '', '', '', 1, NULL, NULL, 'doku', '1', '16134a7'),
(78, 1, 1630847888, NULL, 1632009600, 1632096000, 1, 2, 0, 500000, NULL, 0, 500000, 0, 500000, 0, 500000, NULL, NULL, NULL, 'Mutiara Tioni Asprilia', '', 'mtionias@hotmail.com', '', 'Jl. Tamansari no. 90', '', '', '+6281224737869', '', '', '', 4, NULL, 1630849737, 'doku', '1', '16134c3'),
(79, 1, 1631785138, NULL, 1631836800, 1632009600, 2, 1, 0, 1000000, NULL, 0, 1000000, 0, 1000000, 0, NULL, NULL, NULL, NULL, 'Iwan Supriyadi', '', 'iwan.supriyadi09@yahoo.com', '', 'jl. pejuang jaya blok B no 194, Kota Bekasi', '', '', '081349342057', '', '', '', 1, NULL, NULL, 'doku', '1', '1614310'),
(80, 1, 1631785164, NULL, 1631836800, 1632009600, 2, 2, 0, 1000000, NULL, 0, 1000000, 0, 1000000, 0, NULL, NULL, NULL, NULL, 'DEASY', '', 'ramatiadessi@gmail.com', '', 'Jakarta Utara', '', '', '081313525326', '', '', '', 1, NULL, NULL, 'doku', '1', '1614310'),
(81, 1, 1631786321, NULL, 1631836800, 1632009600, 2, 4, 0, 2000000, NULL, 0, 2000000, 0, 2000000, 0, 2000000, NULL, NULL, NULL, 'Deasy', '', 'ramatiadessi@gmail.com', '', 'Dramaga', '', '', '081313525326', '', '', '', 4, NULL, 1631787661, 'doku', '1', '1614315'),
(82, 1, 1632325884, NULL, 1632441600, 1632528000, 1, 2, 0, 600000, NULL, 0, 600000, 0, 600000, 0, 600000, NULL, NULL, NULL, 'Deborah widjaja', '', 'tipo_lala@yahoo.com', '', 'Jl adhyaksa VI blok i no.3 karang tengah tanggerang', '', '', '081219412603', '', '', '', 4, NULL, 1632326074, 'doku', '1', '1614b50'),
(83, 1, 1633271706, NULL, 1633305600, 1633564800, 3, 2, 0, 1500000, NULL, 0, 1500000, 0, 1500000, 0, NULL, NULL, NULL, NULL, 'Roland MCA', '', 'royoptic@gmail.com', '', 'Taman Anggrek', '', '', '081977832178', '', '', '', 1, NULL, NULL, 'doku', '1', '16159bf'),
(84, 1, 1633326139, NULL, 1633392000, 1633478400, 1, 1, 0, 500000, NULL, 0, 500000, 0, 500000, 0, NULL, NULL, NULL, NULL, 'Irfan', '', 'irfan.rona95@student.upi.edu', '', 'Jalan Sarimanah 2 Blok X No.37 Sarijadi, Bandung', '', '', '+62950722371', '', '', '', 1, NULL, NULL, 'doku', '1', '1615a94'),
(85, 1, 1634166941, NULL, 1634256000, 1634342400, 1, 1, 0, 500000, NULL, 0, 500000, 0, 500000, 0, 500000, NULL, NULL, NULL, 'Feriyadi Ramansyah', '', 'feriyadi@gmail.com', '', 'Bandung', '', '', '08562077730', '', '', '', 4, NULL, 1634167374, 'doku', '1', '1616768'),
(86, 1, 1636455975, NULL, 1637193600, 1637280000, 1, 2, 0, 500000, NULL, 0, 500000, 0, 500000, 0, NULL, NULL, NULL, NULL, 'Katlin Oktaviatri', '', 'Alineoktav@gmail.com', '', 'Jln. Cibunar No. 17', '', '', '0895489553736', '', '', '', 1, NULL, NULL, 'doku', '1', '1618a56'),
(87, 1, 1636456079, NULL, 1637193600, 1637280000, 1, 2, 0, 500000, NULL, 0, 500000, 0, 500000, 0, NULL, NULL, NULL, NULL, 'Katlin Oktaviatri', NULL, 'alineoktav@gmail.com', NULL, 'Jln. Cibunar No. 17', NULL, NULL, '0895389553736', NULL, NULL, '', 1, NULL, NULL, 'doku', '1', '1618a56'),
(88, 1, 1636511056, NULL, 1637971200, 1638057600, 1, 6, 0, 1600000, NULL, 0, 1600000, 0, 1600000, 0, NULL, NULL, NULL, NULL, 'Annisa Nuryuri Utami', '', 'nuryuri.nissa@gmail.com', '', 'Telaga Jambu 2 Depok', '', '', '081283678398', '', '', '', 1, NULL, NULL, 'doku', '1', '1618b2d'),
(89, 1, 1636634565, NULL, 1637193600, 1637280000, 1, 2, 0, 500000, NULL, 0, 500000, 0, 500000, 0, NULL, NULL, NULL, NULL, 'Katlin Oktaviatri', '', 'alineoktav@gmail.com', '', 'Jln. Cibunar No. 17', '', '', '0895389553736', '', '', '', 1, NULL, NULL, 'doku', '1', '1618d0f'),
(90, 1, 1636637618, NULL, 1637193600, 1637280000, 1, 2, 0, 500000, NULL, 0, 500000, 0, 500000, 0, 500000, NULL, NULL, NULL, 'Katlin Oktaviatri', '', 'alineoktav@gmail.com', '', 'Jl. Cibunar No. 17', '', '', '0895389553736', '', '', '', 4, NULL, 1636637971, 'doku', '1', '1618d1b'),
(91, 1, 1636652311, NULL, 1636675200, 1636761600, 1, 2, 0, 500000, NULL, 0, 500000, 0, 500000, 0, 500000, NULL, NULL, NULL, 'Annisa Putri Ariyanti', '', 'annisaputriariyanti@gmail.com', '', 'Taman Yasmin, Bogor', '', '', '0895410194309', '', '', '', 4, NULL, 1636652679, 'doku', '1', '1618d55'),
(92, 1, 1637027597, NULL, 1637971200, 1638057600, 1, 2, 0, 500000, NULL, 0, 500000, 0, 500000, 0, 500000, NULL, NULL, NULL, 'Yunita Saridewi', '', 'dwistiiiiiutari@gmail.com', '', 'Komp. Kiara Sari Asri, Jl. Kiara sari permai V no. 24, Margacinta, Buah batu', '', '', '082127692398', '', '', '', 4, NULL, 1637028480, 'doku', '1', '161930f'),
(93, 1, 1637672536, NULL, 1637884800, 1638057600, 2, 4, 0, 2800000, NULL, 0, 2800000, 0, 2800000, 0, 2800000, NULL, NULL, NULL, 'Nurvantina Pandina', '', 'nurvantina@gmail.com', '', 'Jl. Tropika XV Blok B1 No. 50 Cikarang Baru, Bekasi', '', '', '08159903110', '', '', '', 4, NULL, 1637672817, 'doku', '1', '1619ce6'),
(94, 1, 1637753326, NULL, 1637798400, 1637884800, 1, 2, 0, 800000, NULL, 0, 800000, 0, 800000, 0, NULL, NULL, NULL, NULL, 'Dya Shifa Fadhilla', NULL, 'shifa.social@gmail.com', NULL, 'jl riung bakti II No.154', NULL, NULL, '089678646431', NULL, NULL, '', 1, NULL, NULL, 'doku', '1', '1619e21'),
(95, 1, 1637753738, NULL, 1637798400, 1637884800, 1, 2, 0, 800000, NULL, 0, 800000, 0, 800000, 0, NULL, NULL, NULL, NULL, 'Dya Shifa Fadhilla', '', 'shifa.social@gmail.com', '', 'Jl Riung Bakti II No.154', '', '', '089678646431', '', '', '', 1, NULL, NULL, 'doku', '1', '1619e23'),
(96, 1, 1639493913, NULL, 1639699200, 1639785600, 1, 2, 0, 600000, NULL, 0, 600000, 0, 600000, 0, 600000, NULL, NULL, NULL, 'Budi Krisnadi', '', 'bdkrisnadi@gmail.com', '', 'The Paradise Regency C 29, Jln Parakan Saat, Antapani Tengah', '', '', '08112107080', '', '', '', 4, NULL, 1639494203, 'doku', '1', '161b8b1'),
(97, 1, 1640760857, NULL, 1642204800, 1642291200, 1, 2, 0, 500000, NULL, 0, 434783, 65217.4, 500000, 0, NULL, NULL, NULL, NULL, 'Ali fadli', '', 'grafitistinus@gmail.com', '', 'Vila nusa indah', '', '', '081288054689', '', '', '', 1, NULL, NULL, 'doku', '1', '161cc06'),
(98, 2, 1640775983, NULL, 1642204800, 1642291200, 1, 2, 0, 325000, NULL, 0, 282609, 42391.3, 325000, 0, NULL, NULL, NULL, NULL, 'Ali fadli', '', 'grafitistinus@gmail.com', '', 'Vila nusa indah', '', '', '081288054689', '', '', '', 1, NULL, NULL, 'doku', '6', '161cc41'),
(99, 2, 1641604477, NULL, 1641600000, 1641686400, 1, 2, 0, 325000, NULL, 0, 282609, 42391.3, 325000, 0, 325000, NULL, NULL, NULL, 'siti hoerunisa', '', 'anisanisa140@gmail.com', '', 'jl ah nasution no 117', '', '', '087826440800', '', '', '', 4, NULL, 1641604812, 'doku', '6', '161d8e5'),
(100, 1, 1643041932, NULL, 1646438400, 1646524800, 1, 2, 0, 550000, NULL, 0, 478261, 71739.1, 550000, 0, NULL, NULL, NULL, NULL, 'Adisti viscasari', '', 'adistivisca@yahoo.com', '', 'Grand bintaro asri E7 ciputat tangsel', '', '', '0818295919', '', '', '', 1, NULL, NULL, 'doku', '1', '161eed4'),
(101, 2, 1646062126, NULL, 1646352000, 1646524800, 2, 1, 0, 650000, NULL, 0, 565217, 84782.6, 650000, 0, NULL, NULL, NULL, NULL, 'Shaskia putri ramadhani', '', 'shaskia.ramadhani@yahoo.com', '', 'Jl telepon no 18 bandung', '', '', '081318605893', '', '', '', 1, NULL, NULL, 'doku', '6', '1621cea'),
(102, 2, 1646132193, NULL, 1646352000, 1646524800, 2, 2, 0, 650000, NULL, 0, 565217, 84782.6, 650000, 0, 650000, NULL, NULL, NULL, 'Shaskia putri', '', 'Shaskia.ramadhank@yahoo.com', '', 'Jl telepon 18 bandung', '', '', '081318605893', '', '', '', 4, NULL, 1646132307, 'doku', '6', '1621dfb'),
(103, 1, 1646292613, NULL, 1646265600, 1646352000, 1, 1, 0, 550000, NULL, 0, 478261, 71739.1, 550000, 0, NULL, NULL, NULL, NULL, 'Almeira', '', 'almeirawn@gmail.com', '', 'Jl. Lumbu Barat IV B No. 301', '', '', '081327476360', '', '', '', 1, NULL, NULL, 'doku', '1', '162206e');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_booking_activity`
--

CREATE TABLE `pm_booking_activity` (
  `id` int(11) NOT NULL,
  `id_booking` int(11) NOT NULL,
  `id_activity` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `children` int(11) DEFAULT 0,
  `adults` int(11) DEFAULT 0,
  `duration` varchar(50) DEFAULT NULL,
  `amount` double DEFAULT 0,
  `ex_tax` double DEFAULT 0,
  `tax_rate` double DEFAULT 0,
  `date` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_booking_payment`
--

CREATE TABLE `pm_booking_payment` (
  `id` int(11) NOT NULL,
  `id_booking` int(11) NOT NULL,
  `descr` varchar(100) DEFAULT NULL,
  `method` varchar(100) DEFAULT NULL,
  `amount` double DEFAULT 0,
  `trans` varchar(100) DEFAULT NULL,
  `date` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_booking_payment`
--

INSERT INTO `pm_booking_payment` (`id`, `id_booking`, `descr`, `method`, `amount`, `trans`, `date`) VALUES
(3, 6, NULL, '41', 10000, '15e12db', 1578294346),
(4, 8, NULL, '41', 10000, '15e2a6d', 1579839431),
(5, 11, NULL, '36', 495000, '15e5b8e', 1583059047),
(6, 19, NULL, '36', 520000, '15e685b', 1583899391),
(7, 22, '', '41', 440000, '15ef9ab', 1593388800),
(8, 24, NULL, '41', 575000, '15f157b', 1595243736),
(9, 26, NULL, '15', 550000, '15f3a51', 1597657646),
(10, 30, NULL, '36', 300000, '15f76af', 1601613830),
(11, 32, NULL, '41', 510000, '15f9681', 1603699797),
(12, 34, NULL, '36', 415000, '15f9c4f', 1604079544),
(13, 37, '', '15', 650000, '15fa007', 1604275200),
(14, 40, NULL, '15', 510000, '15fb75d', 1605852741),
(15, 42, NULL, '41', 700000, '15fd6bf', 1607911673),
(16, 44, NULL, '41', 500000, '15ff6c2', 1610007238),
(17, 49, NULL, '41', 750000, '1606154', 1616991418),
(18, 52, NULL, '36', 1100000, '1606315', 1617106757),
(19, 61, NULL, '41', 2000000, '1606d2d', 1617767932),
(20, 63, NULL, '41', 325000, '1606f05', 1617888854),
(21, 64, NULL, '41', 680000, '160718a', 1618053957),
(22, 67, NULL, '41', 325000, '1609cc9', 1620888003),
(23, 69, NULL, '36', 600000, '160b980', 1622770515),
(24, 70, NULL, '41', 500000, '160c028', 1623206120),
(25, 73, NULL, '36', 500000, '1610a4e', 1628065677),
(26, 74, NULL, '41', 355000, '1611344', 1628654109),
(27, 76, NULL, '15', 2800000, '1612856', 1630033919),
(28, 78, NULL, '41', 500000, '16134c3', 1630849737),
(29, 81, NULL, '41', 2000000, '1614315', 1631787661),
(30, 82, NULL, '15', 600000, '1614b50', 1632326074),
(31, 85, NULL, '41', 500000, '1616768', 1634167374),
(32, 90, NULL, '15', 500000, '1618d1b', 1636637972),
(33, 91, NULL, '36', 500000, '1618d55', 1636652680),
(34, 92, NULL, '41', 500000, '161930f', 1637028481),
(35, 93, NULL, '15', 2800000, '1619ce6', 1637672819),
(36, 96, NULL, '36', 600000, '161b8b1', 1639494204),
(37, 99, NULL, '36', 325000, '161d8e5', 1641604812),
(38, 102, NULL, '41', 650000, '1621dfb', 1646132307);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_booking_room`
--

CREATE TABLE `pm_booking_room` (
  `id` int(11) NOT NULL,
  `id_booking` int(11) NOT NULL,
  `id_room` int(11) DEFAULT NULL,
  `id_hotel` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `num` varchar(10) DEFAULT NULL,
  `children` int(11) DEFAULT 0,
  `adults` int(11) DEFAULT 0,
  `amount` double DEFAULT 0,
  `ex_tax` double DEFAULT 0,
  `tax_rate` double DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_booking_room`
--

INSERT INTO `pm_booking_room` (`id`, `id_booking`, `id_room`, `id_hotel`, `title`, `num`, `children`, `adults`, `amount`, `ex_tax`, `tax_rate`) VALUES
(7, 6, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 0, 1, 5000, 5000, NULL),
(8, 6, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 0, 1, 5000, 5000, NULL),
(9, 7, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 1, 750000, 750000, NULL),
(10, 8, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 0, 1, 10000, 10000, NULL),
(15, 11, 3, 1, 'Villatel - Executive Double Bedroom', NULL, 0, 2, 495000, 495000, NULL),
(19, 15, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 1, 440000, 440000, NULL),
(20, 16, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 1, 440000, 440000, NULL),
(21, 17, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 1, 1, 440000, 440000, NULL),
(22, 18, 4, 1, 'Villatel - Executive Twin Bedroom', NULL, 0, 2, 1080000, 1080000, NULL),
(23, 19, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 0, 2, 520000, 520000, NULL),
(24, 20, 3, 1, 'Villatel - Executive Double Bedroom', NULL, 0, 2, 585000, 585000, NULL),
(26, 22, 1, 1, 'Villatel - Deluxe Double Bedroom', '', 0, 2, 440000, 440000, 0),
(27, 23, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 1, 510000, 510000, NULL),
(28, 24, 4, 1, 'Villatel - Executive Twin Bedroom', NULL, 0, 2, 575000, 575000, NULL),
(29, 25, 5, 1, 'Villatel - Premium Double Bedroom', NULL, 0, 2, 820000, 820000, NULL),
(30, 26, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 550000, 550000, NULL),
(31, 27, 3, 1, 'Villatel - Executive Double Bedroom', NULL, 0, 2, 1280000, 1280000, NULL),
(32, 28, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 0, 1, 550000, 550000, NULL),
(33, 29, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 1, 300000, 300000, NULL),
(34, 30, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 300000, 300000, NULL),
(35, 31, 3, 1, 'Villatel - Executive Double Bedroom', NULL, 0, 2, 590000, 590000, NULL),
(36, 32, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 510000, 510000, NULL),
(37, 33, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 415000, 415000, NULL),
(38, 34, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 415000, 415000, NULL),
(39, 35, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 415000, 415000, NULL),
(40, 36, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 325000, 325000, NULL),
(41, 36, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 325000, 325000, NULL),
(42, 37, 7, 2, 'Villacamp - Villacamp Salse', '', 0, 2, 325000, 325000, 0),
(43, 37, 7, 2, 'Villacamp - Villacamp Salse', '', 0, 2, 325000, 325000, 0),
(44, 38, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 510000, 510000, NULL),
(45, 38, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 0, 2, 510000, 510000, NULL),
(46, 39, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 510000, 510000, NULL),
(47, 40, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 510000, 510000, NULL),
(48, 41, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 510000, 510000, NULL),
(49, 41, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 0, 2, 510000, 510000, NULL),
(50, 41, 3, 1, 'Villatel - Executive Double Bedroom', NULL, 0, 2, 610000, 610000, NULL),
(51, 42, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 700000, 700000, NULL),
(52, 43, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 500000, 500000, NULL),
(53, 44, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 500000, 500000, NULL),
(54, 45, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 500000, 500000, NULL),
(55, 46, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 1, 340000, 340000, NULL),
(56, 47, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 1, 300000, 300000, NULL),
(57, 48, 5, 1, 'Villatel - Premium Double Bedroom', NULL, 0, 2, 750000, 750000, NULL),
(58, 49, 5, 1, 'Villatel - Premium Double Bedroom', NULL, 0, 1, 750000, 750000, NULL),
(59, 50, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 1050000, 1050000, NULL),
(60, 51, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 1050000, 1050000, NULL),
(61, 52, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 1100000, 1100000, NULL),
(62, 53, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 325000, 325000, NULL),
(63, 54, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 1, 325000, 325000, NULL),
(64, 55, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 1, 325000, 325000, NULL),
(65, 56, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 325000, 325000, NULL),
(66, 57, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 325000, 325000, NULL),
(67, 58, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 325000, 325000, NULL),
(68, 59, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 0, 2, 1000000, 1000000, NULL),
(69, 60, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 1000000, 1000000, NULL),
(70, 61, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 1000000, 1000000, NULL),
(71, 61, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 0, 2, 1000000, 1000000, NULL),
(72, 62, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 325000, 325000, NULL),
(73, 63, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 325000, 325000, NULL),
(74, 64, 4, 1, 'Villatel - Executive Twin Bedroom', NULL, 0, 2, 680000, 680000, NULL),
(75, 65, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 325000, 325000, NULL),
(76, 66, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 1, 325000, 325000, NULL),
(77, 67, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 1, 325000, 325000, NULL),
(78, 68, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 1000000, 1000000, NULL),
(79, 69, 4, 1, 'Villatel - Executive Twin Bedroom', NULL, 0, 2, 600000, 600000, NULL),
(80, 70, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 500000, 500000, NULL),
(81, 71, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 500000, 500000, NULL),
(82, 72, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 500000, 500000, NULL),
(83, 73, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 500000, 500000, NULL),
(84, 74, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 355000, 355000, NULL),
(85, 75, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 355000, 355000, NULL),
(86, 76, 3, 1, 'Villatel - Executive Double Bedroom', NULL, 0, 2, 600000, 600000, NULL),
(87, 76, 4, 1, 'Villatel - Executive Twin Bedroom', NULL, 0, 2, 600000, 600000, NULL),
(88, 76, 5, 1, 'Villatel - Premium Double Bedroom', NULL, 0, 2, 800000, 800000, NULL),
(89, 76, 6, 1, 'Villatel - Premium Twin Bedroom', NULL, 0, 2, 800000, 800000, NULL),
(90, 77, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 0, 2, 500000, 500000, NULL),
(91, 78, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 0, 2, 500000, 500000, NULL),
(92, 79, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 0, 1, 1000000, 1000000, NULL),
(93, 80, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 0, 2, 1000000, 1000000, NULL),
(94, 81, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 0, 2, 1000000, 1000000, NULL),
(95, 81, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 0, 2, 1000000, 1000000, NULL),
(96, 82, 3, 1, 'Villatel - Executive Double Bedroom', NULL, 1, 2, 600000, 600000, NULL),
(97, 83, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 1500000, 1500000, NULL),
(98, 84, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 1, 500000, 500000, NULL),
(99, 85, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 1, 500000, 500000, NULL),
(100, 86, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 500000, 500000, NULL),
(101, 87, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 500000, 500000, NULL),
(102, 88, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 500000, 500000, NULL),
(103, 88, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 0, 2, 500000, 500000, NULL),
(104, 88, 3, 1, 'Villatel - Executive Double Bedroom', NULL, 0, 2, 600000, 600000, NULL),
(105, 89, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 500000, 500000, NULL),
(106, 90, 2, 1, 'Villatel - Deluxe Twin Bedroom', NULL, 0, 2, 500000, 500000, NULL),
(107, 91, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 500000, 500000, NULL),
(108, 92, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 1, 2, 500000, 500000, NULL),
(109, 93, 3, 1, 'Villatel - Executive Double Bedroom', NULL, 0, 2, 1200000, 1200000, NULL),
(110, 93, 5, 1, 'Villatel - Premium Double Bedroom', NULL, 0, 2, 1600000, 1600000, NULL),
(111, 94, 5, 1, 'Villatel - Premium Double Bedroom', NULL, 0, 2, 800000, 800000, NULL),
(112, 95, 6, 1, 'Villatel - Premium Twin Bedroom', NULL, 0, 2, 800000, 800000, NULL),
(113, 96, 3, 1, 'Villatel - Executive Double Bedroom', NULL, 0, 2, 600000, 600000, NULL),
(114, 97, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 500000, 434782.61, NULL),
(115, 98, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 325000, 282608.7, NULL),
(116, 99, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 325000, 282608.7, NULL),
(117, 100, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 2, 550000, 478260.87, NULL),
(118, 101, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 1, 650000, 565217.39, NULL),
(119, 102, 7, 2, 'Villacamp - Villacamp Salse', NULL, 0, 2, 650000, 565217.39, NULL),
(120, 103, 1, 1, 'Villatel - Deluxe Double Bedroom', NULL, 0, 1, 550000, 478260.87, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_booking_service`
--

CREATE TABLE `pm_booking_service` (
  `id` int(11) NOT NULL,
  `id_booking` int(11) NOT NULL,
  `id_service` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `qty` int(11) DEFAULT 0,
  `amount` double DEFAULT 0,
  `ex_tax` double DEFAULT 0,
  `tax_rate` double DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_booking_tax`
--

CREATE TABLE `pm_booking_tax` (
  `id` int(11) NOT NULL,
  `id_booking` int(11) NOT NULL,
  `id_tax` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `amount` double DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_booking_tax`
--

INSERT INTO `pm_booking_tax` (`id`, `id_booking`, `id_tax`, `name`, `amount`) VALUES
(1, 97, 1, 'TAX & SERVICE', 65217.39),
(2, 98, 1, 'TAX & SERVICE', 42391.3),
(3, 99, 1, 'TAX & SERVICE', 42391.3),
(4, 100, 1, 'TAX & SERVICE', 71739.13),
(5, 101, 1, 'TAX & SERVICE', 84782.61),
(6, 102, 1, 'TAX & SERVICE', 84782.61),
(7, 103, 1, 'TAX & SERVICE', 71739.13);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_comment`
--

CREATE TABLE `pm_comment` (
  `id` int(11) NOT NULL,
  `item_type` varchar(30) DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `checked` int(11) DEFAULT 0,
  `add_date` int(11) DEFAULT NULL,
  `edit_date` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `msg` longtext DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_comment`
--

INSERT INTO `pm_comment` (`id`, `item_type`, `id_item`, `rating`, `checked`, `add_date`, `edit_date`, `name`, `email`, `msg`, `ip`) VALUES
(1, 'hotel', 2, 0, NULL, 1595740140, NULL, 'Pika', 'priska.bdg@gmail.com', 'Untuk extra bed perlu tambahan biaya berapa ya?', '103.104.14.225'),
(2, 'activity', 2, 0, NULL, 1626769243, NULL, 'royargy', 'Injemia@sqmail.xyz', '<a href=http://propeciaset.com/>lower price on finasteride camber</a>', '5.188.48.10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_country`
--

CREATE TABLE `pm_country` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_country`
--

INSERT INTO `pm_country` (`id`, `name`, `code`) VALUES
(1, 'Afghanistan', 'AF'),
(2, 'Åland', 'AX'),
(3, 'Albania', 'AL'),
(4, 'Algeria', 'DZ'),
(5, 'American Samoa', 'AS'),
(6, 'Andorra', 'AD'),
(7, 'Angola', 'AO'),
(8, 'Anguilla', 'AI'),
(9, 'Antarctica', 'AQ'),
(10, 'Antigua and Barbuda', 'AG'),
(11, 'Argentina', 'AR'),
(12, 'Armenia', 'AM'),
(13, 'Aruba', 'AW'),
(14, 'Australia', 'AU'),
(15, 'Austria', 'AT'),
(16, 'Azerbaijan', 'AZ'),
(17, 'Bahamas', 'BS'),
(18, 'Bahrain', 'BH'),
(19, 'Bangladesh', 'BD'),
(20, 'Barbados', 'BB'),
(21, 'Belarus', 'BY'),
(22, 'Belgium', 'BE'),
(23, 'Belize', 'BZ'),
(24, 'Benin', 'BJ'),
(25, 'Bermuda', 'BM'),
(26, 'Bhutan', 'BT'),
(27, 'Bolivia', 'BO'),
(28, 'Bonaire', 'BQ'),
(29, 'Bosnia and Herzegovina', 'BA'),
(30, 'Botswana', 'BW'),
(31, 'Bouvet Island', 'BV'),
(32, 'Brazil', 'BR'),
(33, 'British Indian Ocean Territory', 'IO'),
(34, 'British Virgin Islands', 'VG'),
(35, 'Brunei', 'BN'),
(36, 'Bulgaria', 'BG'),
(37, 'Burkina Faso', 'BF'),
(38, 'Burundi', 'BI'),
(39, 'Cambodia', 'KH'),
(40, 'Cameroon', 'CM'),
(41, 'Canada', 'CA'),
(42, 'Cape Verde', 'CV'),
(43, 'Cayman Islands', 'KY'),
(44, 'Central African Republic', 'CF'),
(45, 'Chad', 'TD'),
(46, 'Chile', 'CL'),
(47, 'China', 'CN'),
(48, 'Christmas Island', 'CX'),
(49, 'Cocos [Keeling] Islands', 'CC'),
(50, 'Colombia', 'CO'),
(51, 'Comoros', 'KM'),
(52, 'Cook Islands', 'CK'),
(53, 'Costa Rica', 'CR'),
(54, 'Croatia', 'HR'),
(55, 'Cuba', 'CU'),
(56, 'Curacao', 'CW'),
(57, 'Cyprus', 'CY'),
(58, 'Czech Republic', 'CZ'),
(59, 'Democratic Republic of the Congo', 'CD'),
(60, 'Denmark', 'DK'),
(61, 'Djibouti', 'DJ'),
(62, 'Dominica', 'DM'),
(63, 'Dominican Republic', 'DO'),
(64, 'East Timor', 'TL'),
(65, 'Ecuador', 'EC'),
(66, 'Egypt', 'EG'),
(67, 'El Salvador', 'SV'),
(68, 'Equatorial Guinea', 'GQ'),
(69, 'Eritrea', 'ER'),
(70, 'Estonia', 'EE'),
(71, 'Ethiopia', 'ET'),
(72, 'Falkland Islands', 'FK'),
(73, 'Faroe Islands', 'FO'),
(74, 'Fiji', 'FJ'),
(75, 'Finland', 'FI'),
(76, 'France', 'FR'),
(77, 'French Guiana', 'GF'),
(78, 'French Polynesia', 'PF'),
(79, 'French Southern Territories', 'TF'),
(80, 'Gabon', 'GA'),
(81, 'Gambia', 'GM'),
(82, 'Georgia', 'GE'),
(83, 'Germany', 'DE'),
(84, 'Ghana', 'GH'),
(85, 'Gibraltar', 'GI'),
(86, 'Greece', 'GR'),
(87, 'Greenland', 'GL'),
(88, 'Grenada', 'GD'),
(89, 'Guadeloupe', 'GP'),
(90, 'Guam', 'GU'),
(91, 'Guatemala', 'GT'),
(92, 'Guernsey', 'GG'),
(93, 'Guinea', 'GN'),
(94, 'Guinea-Bissau', 'GW'),
(95, 'Guyana', 'GY'),
(96, 'Haiti', 'HT'),
(97, 'Heard Island and McDonald Islands', 'HM'),
(98, 'Honduras', 'HN'),
(99, 'Hong Kong', 'HK'),
(100, 'Hungary', 'HU'),
(101, 'Iceland', 'IS'),
(102, 'India', 'IN'),
(103, 'Indonesia', 'ID'),
(104, 'Iran', 'IR'),
(105, 'Iraq', 'IQ'),
(106, 'Ireland', 'IE'),
(107, 'Isle of Man', 'IM'),
(108, 'Israel', 'IL'),
(109, 'Italy', 'IT'),
(110, 'Ivory Coast', 'CI'),
(111, 'Jamaica', 'JM'),
(112, 'Japan', 'JP'),
(113, 'Jersey', 'JE'),
(114, 'Jordan', 'JO'),
(115, 'Kazakhstan', 'KZ'),
(116, 'Kenya', 'KE'),
(117, 'Kiribati', 'KI'),
(118, 'Kosovo', 'XK'),
(119, 'Kuwait', 'KW'),
(120, 'Kyrgyzstan', 'KG'),
(121, 'Laos', 'LA'),
(122, 'Latvia', 'LV'),
(123, 'Lebanon', 'LB'),
(124, 'Lesotho', 'LS'),
(125, 'Liberia', 'LR'),
(126, 'Libya', 'LY'),
(127, 'Liechtenstein', 'LI'),
(128, 'Lithuania', 'LT'),
(129, 'Luxembourg', 'LU'),
(130, 'Macao', 'MO'),
(131, 'Macedonia', 'MK'),
(132, 'Madagascar', 'MG'),
(133, 'Malawi', 'MW'),
(134, 'Malaysia', 'MY'),
(135, 'Maldives', 'MV'),
(136, 'Mali', 'ML'),
(137, 'Malta', 'MT'),
(138, 'Marshall Islands', 'MH'),
(139, 'Martinique', 'MQ'),
(140, 'Mauritania', 'MR'),
(141, 'Mauritius', 'MU'),
(142, 'Mayotte', 'YT'),
(143, 'Mexico', 'MX'),
(144, 'Micronesia', 'FM'),
(145, 'Moldova', 'MD'),
(146, 'Monaco', 'MC'),
(147, 'Mongolia', 'MN'),
(148, 'Montenegro', 'ME'),
(149, 'Montserrat', 'MS'),
(150, 'Morocco', 'MA'),
(151, 'Mozambique', 'MZ'),
(152, 'Myanmar [Burma]', 'MM'),
(153, 'Namibia', 'NA'),
(154, 'Nauru', 'NR'),
(155, 'Nepal', 'NP'),
(156, 'Netherlands', 'NL'),
(157, 'New Caledonia', 'NC'),
(158, 'New Zealand', 'NZ'),
(159, 'Nicaragua', 'NI'),
(160, 'Niger', 'NE'),
(161, 'Nigeria', 'NG'),
(162, 'Niue', 'NU'),
(163, 'Norfolk Island', 'NF'),
(164, 'North Korea', 'KP'),
(165, 'Northern Mariana Islands', 'MP'),
(166, 'Norway', 'NO'),
(167, 'Oman', 'OM'),
(168, 'Pakistan', 'PK'),
(169, 'Palau', 'PW'),
(170, 'Palestine', 'PS'),
(171, 'Panama', 'PA'),
(172, 'Papua New Guinea', 'PG'),
(173, 'Paraguay', 'PY'),
(174, 'Peru', 'PE'),
(175, 'Philippines', 'PH'),
(176, 'Pitcairn Islands', 'PN'),
(177, 'Poland', 'PL'),
(178, 'Portugal', 'PT'),
(179, 'Puerto Rico', 'PR'),
(180, 'Qatar', 'QA'),
(181, 'Republic of the Congo', 'CG'),
(182, 'Réunion', 'RE'),
(183, 'Romania', 'RO'),
(184, 'Russia', 'RU'),
(185, 'Rwanda', 'RW'),
(186, 'Saint Barthélemy', 'BL'),
(187, 'Saint Helena', 'SH'),
(188, 'Saint Kitts and Nevis', 'KN'),
(189, 'Saint Lucia', 'LC'),
(190, 'Saint Martin', 'MF'),
(191, 'Saint Pierre and Miquelon', 'PM'),
(192, 'Saint Vincent and the Grenadines', 'VC'),
(193, 'Samoa', 'WS'),
(194, 'San Marino', 'SM'),
(195, 'São Tomé and Príncipe', 'ST'),
(196, 'Saudi Arabia', 'SA'),
(197, 'Senegal', 'SN'),
(198, 'Serbia', 'RS'),
(199, 'Seychelles', 'SC'),
(200, 'Sierra Leone', 'SL'),
(201, 'Singapore', 'SG'),
(202, 'Sint Maarten', 'SX'),
(203, 'Slovakia', 'SK'),
(204, 'Slovenia', 'SI'),
(205, 'Solomon Islands', 'SB'),
(206, 'Somalia', 'SO'),
(207, 'South Africa', 'ZA'),
(208, 'South Georgia and the South Sandwich Islands', 'GS'),
(209, 'South Korea', 'KR'),
(210, 'South Sudan', 'SS'),
(211, 'Spain', 'ES'),
(212, 'Sri Lanka', 'LK'),
(213, 'Sudan', 'SD'),
(214, 'Suriname', 'SR'),
(215, 'Svalbard and Jan Mayen', 'SJ'),
(216, 'Swaziland', 'SZ'),
(217, 'Sweden', 'SE'),
(218, 'Switzerland', 'CH'),
(219, 'Syria', 'SY'),
(220, 'Taiwan', 'TW'),
(221, 'Tajikistan', 'TJ'),
(222, 'Tanzania', 'TZ'),
(223, 'Thailand', 'TH'),
(224, 'Togo', 'TG'),
(225, 'Tokelau', 'TK'),
(226, 'Tonga', 'TO'),
(227, 'Trinidad and Tobago', 'TT'),
(228, 'Tunisia', 'TN'),
(229, 'Turkey', 'TR'),
(230, 'Turkmenistan', 'TM'),
(231, 'Turks and Caicos Islands', 'TC'),
(232, 'Tuvalu', 'TV'),
(233, 'U.S. Minor Outlying Islands', 'UM'),
(234, 'U.S. Virgin Islands', 'VI'),
(235, 'Uganda', 'UG'),
(236, 'Ukraine', 'UA'),
(237, 'United Arab Emirates', 'AE'),
(238, 'United Kingdom', 'GB'),
(239, 'United States', 'US'),
(240, 'Uruguay', 'UY'),
(241, 'Uzbekistan', 'UZ'),
(242, 'Vanuatu', 'VU'),
(243, 'Vatican City', 'VA'),
(244, 'Venezuela', 'VE'),
(245, 'Vietnam', 'VN'),
(246, 'Wallis and Futuna', 'WF'),
(247, 'Western Sahara', 'EH'),
(248, 'Yemen', 'YE'),
(249, 'Zambia', 'ZM'),
(250, 'Zimbabwe', 'ZW');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_coupon`
--

CREATE TABLE `pm_coupon` (
  `id` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `discount` double DEFAULT 0,
  `discount_type` varchar(10) DEFAULT NULL,
  `rooms` text DEFAULT NULL,
  `checked` int(11) DEFAULT 0,
  `publish_date` int(11) DEFAULT NULL,
  `unpublish_date` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_coupon`
--

INSERT INTO `pm_coupon` (`id`, `title`, `code`, `discount`, `discount_type`, `rooms`, `checked`, `publish_date`, `unpublish_date`) VALUES
(1, 'Get Discount 10%', 'SALSEBDG88', 10, 'rate', '1,2,3,4,5,6', 1, 1593752400, 1601568000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_currency`
--

CREATE TABLE `pm_currency` (
  `id` int(11) NOT NULL,
  `code` varchar(5) DEFAULT NULL,
  `sign` varchar(5) DEFAULT NULL,
  `main` int(11) DEFAULT 0,
  `rank` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_currency`
--

INSERT INTO `pm_currency` (`id`, `code`, `sign`, `main`, `rank`) VALUES
(1, 'USD', '$', 0, 1),
(2, 'EUR', '€', 0, 2),
(3, 'GBP', '£', 0, 3),
(4, 'INR', '₹', 0, 4),
(5, 'AUD', 'A$', 0, 5),
(6, 'CAD', 'C$', 0, 6),
(7, 'CNY', '¥', 0, 7),
(8, 'TRY', '₺', 0, 8),
(9, 'IDR', 'Rp.', 1, 9);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_destination`
--

CREATE TABLE `pm_destination` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `subtitle` varchar(250) DEFAULT NULL,
  `title_tag` varchar(250) DEFAULT NULL,
  `alias` varchar(100) DEFAULT NULL,
  `descr` text DEFAULT NULL,
  `text` longtext DEFAULT NULL,
  `video` text DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `home` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 0,
  `rank` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_destination_file`
--

CREATE TABLE `pm_destination_file` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `home` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 1,
  `rank` int(11) DEFAULT 0,
  `file` varchar(250) DEFAULT NULL,
  `label` varchar(250) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_email_content`
--

CREATE TABLE `pm_email_content` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `subject` varchar(250) DEFAULT NULL,
  `content` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_email_content`
--

INSERT INTO `pm_email_content` (`id`, `lang`, `name`, `subject`, `content`) VALUES
(1, 1, 'CONTACT', 'Contact', '<b>Nom:</b> {name}<b>Adresse:</b> {address}<b>Téléphone:</b> {phone}<b>E-mail:</b> {email}<b>Message:</b>{msg}'),
(1, 2, 'CONTACT', 'Contact', '<p><b>Name:</b> {name}<br />\r\n<b>Address:</b> {address}<br />\r\n<b>Phone:</b> {phone}<br />\r\n<b>E-mail:</b> {email}<br />\r\n<b>Message:</b><br />\r\n{msg}</p>\r\n'),
(1, 3, 'CONTACT', 'Contact', '<b>Name:</b> {name}<b>Address:</b> {address}<b>Phone:</b> {phone}<b>E-mail:</b> {email}<b>Message:</b>{msg}'),
(1, 4, 'CONTACT', 'Kontak', '<p><b>Nama:</b> {nama}<br />\r\n<b>Alamat:</b> {alamat}<br />\r\n<b>Telepon:</b> {telepon}<br />\r\n<b>E-mail:</b> {email}<br />\r\n<b>Pesan:</b><br />\r\n{msg}</p>\r\n'),
(2, 1, 'BOOKING_REQUEST', 'Demande de réservation', '<p><b>Adresse de facturation</b><br />\r\n{firstname} {lastname}<br />\r\n{address}<br />\r\n{postcode} {city}<br />\r\nSociété : {company}<br />\r\nTéléphone : {phone}<br />\r\nMobile : {mobile}<br />\r\nEmail : {email}</p>\r\n\r\n<p><strong>Détails de la réservation</strong><br />\r\nArrivée : <b>{Check_in}</b><br />\r\nDépart : <b>{Check_out}</b><br />\r\n<b>{num_nights}</b> nuit(s)<br />\r\n<b>{num_guests}</b> personne(s) - Adulte(s) : <b>{num_adults}</b> / Enfant(s) : <b>{num_children}</b></p>\r\n\r\n<p><b>Chambres</b></p>\r\n\r\n<p>{rooms}</p>\r\n\r\n<p><b>Services supplémentaires</b></p>\r\n\r\n<p>{extra_services}</p>\r\n\r\n<p><b>Activités</b></p>\r\n\r\n<p>{activities}</p>\r\n\r\n<p><b>Commentaires</b><br />\r\n{comments}</p>\r\n'),
(2, 2, 'BOOKING_REQUEST', 'Booking request', '<p><b>Billing address</b><br />\r\n{firstname} {lastname}<br />\r\n{address}<br />\r\n{postcode} {city}<br />\r\nCompany: {company}<br />\r\nPhone: {phone}<br />\r\nMobile: {mobile}<br />\r\nEmail: {email}</p>\r\n\r\n<p><strong>Booking details</strong><br />\r\nCheck in <b>{Check_in}</b><br />\r\nCheck out <b>{Check_out}</b><br />\r\n<b>{num_nights}</b> nights<br />\r\n<b>{num_guests}</b> persons - Adults: <b>{num_adults}</b> / Children: <b>{num_children}</b></p>\r\n\r\n<p><strong>Rooms</strong></p>\r\n\r\n<p>{rooms}</p>\r\n\r\n<p><b>Extra services</b></p>\r\n\r\n<p>{extra_services}</p>\r\n\r\n<p><b>Activities</b></p>\r\n\r\n<p>{activities}</p>\r\n\r\n<p><b>Comments</b><br />\r\n{comments}</p>\r\n'),
(2, 3, 'BOOKING_REQUEST', 'Booking request', '<p><b>Billing address</b><br />\r\n{firstname} {lastname}<br />\r\n{address}<br />\r\n{postcode} {city}<br />\r\nCompany: {company}<br />\r\nPhone: {phone}<br />\r\nMobile: {mobile}<br />\r\nEmail: {email}</p>\r\n\r\n<p><strong>Booking details</strong><br />\r\nCheck in <b>{Check_in}</b><br />\r\nCheck out <b>{Check_out}</b><br />\r\n<b>{num_nights}</b> nights<br />\r\n<b>{num_guests}</b> persons - Adults: <b>{num_adults}</b> / Children: <b>{num_children}</b></p>\r\n\r\n<p><strong>Rooms</strong></p>\r\n\r\n<p>{rooms}</p>\r\n\r\n<p><b>Extra services</b></p>\r\n\r\n<p>{extra_services}</p>\r\n\r\n<p><b>Activities</b></p>\r\n\r\n<p>{activities}</p>\r\n\r\n<p><b>Comments</b><br />\r\n{comments}</p>\r\n'),
(2, 4, 'BOOKING_REQUEST', 'Permintaan Booking', '<p><b>Alamat Tagihan</b><br />\r\n{namadepan} {namabelakang}<br />\r\n{alamat}<br />\r\n{kodepos} {kota}<br />\r\nPerusahaan: {perusahaan}<br />\r\nTelepon: {telepon}<br />\r\nPonsel: {ponsel}<br />\r\nEmail: {email}</p>\r\n\r\n<p><strong>Booking details</strong><br />\r\nCheck in <b>{Check_in}</b><br />\r\nCheck out <b>{Check_out}</b><br />\r\n<b>{num_nights}</b> nights<br />\r\n<b>{num_guests}</b> persons - Adults: <b>{num_adults}</b> / Children: <b>{num_children}</b></p>\r\n\r\n<p><strong>Rooms</strong></p>\r\n\r\n<p>{rooms}</p>\r\n\r\n<p><b>Extra services</b></p>\r\n\r\n<p>{extra_services}</p>\r\n\r\n<p><b>Activities</b></p>\r\n\r\n<p>{activities}</p>\r\n\r\n<p><b>Comments</b><br />\r\n{comments}</p>\r\n'),
(3, 1, 'BOOKING_CONFIRMATION', 'Confirmation de réservation', '<p><b>Adresse de facturation</b><br />\r\n{firstname} {lastname}<br />\r\n{address}<br />\r\n{postcode} {city}<br />\r\nSociété : {company}<br />\r\nTéléphone : {phone}<br />\r\nMobile : {mobile}<br />\r\nEmail : {email}</p>\r\n\r\n<p><strong>Détails de la réservation</strong><br />\r\nArrivée : <b>{Check_in}</b><br />\r\nDépart : <b>{Check_out}</b><br />\r\n<b>{num_nights}</b> nuit(s)<br />\r\n<b>{num_guests}</b> personne(s) - Adulte(s) : <b>{num_adults}</b> / Enfant(s) : <b>{num_children}</b></p>\r\n\r\n<p><b>Chambres</b></p>\r\n\r\n<p>{rooms}</p>\r\n\r\n<p><b>Services supplémentaires</b></p>\r\n\r\n<p>{extra_services}</p>\r\n\r\n<p><b>Activités</b></p>\r\n\r\n<p>{activities}</p>\r\n\r\n<p>Taxe de séjour : {tourist_tax}<br />\r\nRéduction: {discount}<br />\r\n{taxes}<br />\r\nTotal : <strong>{total} TTC</strong></p>\r\n\r\n<p>Acompte : <strong>{down_payment} TTC</strong></p>\r\n\r\n<p><b>Commentaires</b><br />\r\n{comments}</p>\r\n\r\n<p>{payment_notice}</p>\r\n'),
(3, 2, 'BOOKING_CONFIRMATION', 'Booking confirmation', '<p><b>Billing address</b><br />\r\n{firstname} {lastname}<br />\r\n{address}<br />\r\n{postcode} {city}<br />\r\nCompany: {company}<br />\r\nPhone: {phone}<br />\r\nMobile: {mobile}<br />\r\nEmail: {email}</p>\r\n\r\n<p><strong>Booking details</strong><br />\r\nCheck in <b>{Check_in}</b><br />\r\nCheck out <b>{Check_out}</b><br />\r\n<b>{num_nights}</b> nights<br />\r\n<b>{num_guests}</b> persons - Adults: <b>{num_adults}</b> / Children: <b>{num_children}</b></p>\r\n\r\n<p><strong>Rooms</strong></p>\r\n\r\n<p>{rooms}</p>\r\n\r\n<p><b>Extra services</b></p>\r\n\r\n<p>{extra_services}</p>\r\n\r\n<p><b>Activities</b></p>\r\n\r\n<p>{activities}</p>\r\n\r\n<p>Tourist tax: {tourist_tax}<br />\r\nDiscount: {discount}<br />\r\n{taxes}<br />\r\nTotal: <strong>{total} incl. VAT</strong></p>\r\n\r\n<p>Down payment: <strong>{down_payment} incl. VAT</strong></p>\r\n\r\n<p><b>Comments</b><br />\r\n{comments}</p>\r\n\r\n<p>{payment_notice}</p>\r\n'),
(3, 3, 'BOOKING_CONFIRMATION', 'Booking confirmation', '<p><b>Billing address</b><br />\r\n{firstname} {lastname}<br />\r\n{address}<br />\r\n{postcode} {city}<br />\r\nCompany: {company}<br />\r\nPhone: {phone}<br />\r\nMobile: {mobile}<br />\r\nEmail: {email}</p>\r\n\r\n<p><strong>Booking details</strong><br />\r\nCheck in <b>{Check_in}</b><br />\r\nCheck out <b>{Check_out}</b><br />\r\n<b>{num_nights}</b> nights<br />\r\n<b>{num_guests}</b> persons - Adults: <b>{num_adults}</b> / Children: <b>{num_children}</b></p>\r\n\r\n<p><strong>Rooms</strong></p>\r\n\r\n<p>{rooms}</p>\r\n\r\n<p><b>Extra services</b></p>\r\n\r\n<p>{extra_services}</p>\r\n\r\n<p><b>Activities</b></p>\r\n\r\n<p>{activities}</p>\r\n\r\n<p>Tourist tax: {tourist_tax}<br />\r\nDiscount: {discount}<br />\r\n{taxes}<br />\r\nTotal: <strong>{total} incl. VAT</strong></p>\r\n\r\n<p>Down payment: <strong>{down_payment} incl. VAT</strong></p>\r\n\r\n<p><b>Comments</b><br />\r\n{comments}</p>\r\n\r\n<p>{payment_notice}</p>\r\n'),
(3, 4, 'BOOKING_CONFIRMATION', 'Konfirmasi Pemesanan', '<p><b>Alamat Tagihan</b><br />\r\n{namadepan} {namabelakang}<br />\r\n{alamat}<br />\r\n{kodepos} {kota}<br />\r\nPerusahaan: {perusahaan}<br />\r\nTelepon: {telepon}<br />\r\nPonsel: {ponsel}<br />\r\nEmail: {email}</p>\r\n\r\n<p><strong>Booking details</strong><br />\r\nCheck in <b>{Check_in}</b><br />\r\nCheck out <b>{Check_out}</b><br />\r\n<b>{num_nights}</b> nights<br />\r\n<b>{num_guests}</b> persons - Adults: <b>{num_adults}</b> / Children: <b>{num_children}</b></p>\r\n\r\n<p><strong>Rooms</strong></p>\r\n\r\n<p>{rooms}</p>\r\n\r\n<p><b>Extra services</b></p>\r\n\r\n<p>{extra_services}</p>\r\n\r\n<p><b>Activities</b></p>\r\n\r\n<p>{activities}</p>\r\n\r\n<p>Tourist tax: {tourist_tax}<br />\r\nDiscount: {discount}<br />\r\n{taxes}<br />\r\nTotal: <strong>{total} incl. VAT</strong></p>\r\n\r\n<p>Down payment: <strong>{down_payment} incl. VAT</strong></p>\r\n\r\n<p><b>Comments</b><br />\r\n{comments}</p>\r\n\r\n<p>{payment_notice}</p>\r\n');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_facility`
--

CREATE TABLE `pm_facility` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `rank` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_facility`
--

INSERT INTO `pm_facility` (`id`, `lang`, `name`, `rank`) VALUES
(1, 1, 'Climatisation', 1),
(1, 2, 'Air conditioning', NULL),
(1, 3, 'Climatisation', 1),
(1, 4, 'AC', NULL),
(2, 1, 'Lit bébé', 2),
(2, 2, 'Baby cot', NULL),
(2, 3, 'Lit bébé', 2),
(2, 4, 'Ranjang Bayi', NULL),
(3, 1, 'Balcon', 3),
(3, 2, 'Balcony', NULL),
(3, 3, 'Balcon', 3),
(3, 4, 'Balkon', NULL),
(4, 1, 'Barbecue', 4),
(4, 2, 'Barbecue', 4),
(4, 3, 'Barbecue', 4),
(4, 4, 'Barbecue', 4),
(5, 1, 'Salle de bain', 5),
(5, 2, 'Bathroom', NULL),
(5, 3, 'Salle de bain', 5),
(5, 4, 'Kamar Mandi', NULL),
(6, 1, 'Cafetière', 6),
(6, 2, 'Coffeemaker', 6),
(6, 3, 'Cafetière', 6),
(6, 4, 'Coffeemaker', 6),
(7, 1, 'Plaque de cuisson', 7),
(7, 2, 'Cooktop', 7),
(7, 3, 'Plaque de cuisson', 7),
(7, 4, 'Cooktop', 7),
(8, 1, 'Bureau', 8),
(8, 2, 'Desk', NULL),
(8, 3, 'Bureau', 8),
(8, 4, 'Meja Tulis', NULL),
(9, 1, 'Lave vaisselle', 9),
(9, 2, 'Dishwasher', NULL),
(9, 3, 'Lave vaisselle', 9),
(9, 4, 'Pencuci Piring', NULL),
(10, 1, 'Ventilateur', 10),
(10, 2, 'Fan', NULL),
(10, 3, 'Ventilateur', 10),
(10, 4, 'Kipas Angin', NULL),
(11, 1, 'Parking gratuit', 11),
(11, 2, 'Free parking', NULL),
(11, 3, 'Parking gratuit', 11),
(11, 4, 'Parkir Gratis', NULL),
(12, 1, 'Réfrigérateur', 12),
(12, 2, 'Fridge', NULL),
(12, 3, 'Réfrigérateur', 12),
(12, 4, 'Lemari Es', NULL),
(13, 1, 'Sèche-cheveux', 13),
(13, 2, 'Hairdryer', NULL),
(13, 3, 'Sèche-cheveux', 13),
(13, 4, 'Pengering Rambut', NULL),
(14, 1, 'Internet', 14),
(14, 2, 'Internet', 14),
(14, 3, 'Internet', 14),
(14, 4, 'Internet', 14),
(15, 1, 'Fer à repasser', 15),
(15, 2, 'Iron', NULL),
(15, 3, 'Fer à repasser', 15),
(15, 4, 'Setrika', NULL),
(16, 1, 'Micro-ondes', 16),
(16, 2, 'Microwave', 16),
(16, 3, 'Micro-ondes', 16),
(16, 4, 'Microwave', 16),
(17, 1, 'Mini-bar', 17),
(17, 2, 'Mini-bar', 17),
(17, 3, 'Mini-bar', 17),
(17, 4, 'Mini-bar', 17),
(18, 1, 'Non-fumeurs', 18),
(18, 2, 'Non-smoking', 18),
(18, 3, 'Non-fumeurs', 18),
(18, 4, 'Non-smoking', 18),
(19, 1, 'Parking payant', 19),
(19, 2, 'Paid parking', 19),
(19, 3, 'Parking payant', 19),
(19, 4, 'Paid parking', 19),
(20, 1, 'Animaux acceptés', 20),
(20, 2, 'Pets allowed', 20),
(20, 3, 'Animaux acceptés', 20),
(20, 4, 'Pets allowed', 20),
(21, 1, 'Animaux interdits', 21),
(21, 2, 'Pets not allowed', 21),
(21, 3, 'Animaux interdits', 21),
(21, 4, 'Pets not allowed', 21),
(22, 1, 'Radio', 22),
(22, 2, 'Radio', 22),
(22, 3, 'Radio', 22),
(22, 4, 'Radio', 22),
(23, 1, 'Coffre-fort', 23),
(23, 2, 'Safe', 23),
(23, 3, 'Coffre-fort', 23),
(23, 4, 'Safe', 23),
(24, 1, 'Chaines satellite', 24),
(24, 2, 'Satellite chanels', 24),
(24, 3, 'Chaines satellite', 24),
(24, 4, 'Satellite chanels', 24),
(25, 1, 'Salle d\'eau', 25),
(25, 2, 'Shower-room', 25),
(25, 3, 'Salle d\'eau', 25),
(25, 4, 'Shower-room', 25),
(26, 1, 'Coin salon', 26),
(26, 2, 'Small lounge', 26),
(26, 3, 'Coin salon', 26),
(26, 4, 'Small lounge', 26),
(27, 1, 'Telephone', 27),
(27, 2, 'Telephone', 27),
(27, 3, 'Telephone', 27),
(27, 4, 'Telephone', 27),
(28, 1, 'Téléviseur', 28),
(28, 2, 'Television', 28),
(28, 3, 'Téléviseur', 28),
(28, 4, 'Television', 28),
(29, 1, 'Terrasse', 29),
(29, 2, 'Terrasse', 29),
(29, 3, 'Terrasse', 29),
(29, 4, 'Terrasse', 29),
(30, 1, 'Machine à laver', 30),
(30, 2, 'Washing machine', 30),
(30, 3, 'Machine à laver', 30),
(30, 4, 'Washing machine', 30),
(31, 1, 'Accès handicapés', 31),
(31, 2, 'Wheelchair accessible', 31),
(31, 3, 'Accès handicapés', 31),
(31, 4, 'Wheelchair accessible', 31),
(32, 1, 'Wi-Fi', 31),
(32, 2, 'WiFi', 31),
(32, 3, 'Wi-Fi', 31),
(32, 4, 'WiFi', 31),
(33, 1, 'Chaine hifi', 32),
(33, 2, 'Hi-fi system', 32),
(33, 3, 'Chaine hifi', 32),
(33, 4, 'Hi-fi system', 32),
(34, 1, 'Lecteur DVD', 33),
(34, 2, 'DVD player', NULL),
(34, 3, 'Lecteur DVD', 33),
(34, 4, 'Pemutar DVD', NULL),
(35, 1, 'Ascenceur', 34),
(35, 2, 'Elevator', 34),
(35, 3, 'Ascenceur', 34),
(35, 4, 'Elevator', 34),
(36, 1, 'Coin salon', 35),
(36, 2, 'Lounge', 35),
(36, 3, 'Coin salon', 35),
(36, 4, 'Lounge', 35),
(37, 1, 'Restaurant', 36),
(37, 2, 'Restaurant', 36),
(37, 3, 'Restaurant', 36),
(37, 4, 'Restaurant', 36),
(38, 1, 'Service de chambre', 37),
(38, 2, 'Room service', 37),
(38, 3, 'Service de chambre', 37),
(38, 4, 'Room service', 37),
(39, 1, 'Vestiaire', 38),
(39, 2, 'Cloakroom', NULL),
(39, 3, 'Vestiaire', 38),
(39, 4, 'Penitipan Pakaian', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_facility_file`
--

CREATE TABLE `pm_facility_file` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `home` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 1,
  `rank` int(11) DEFAULT 0,
  `file` varchar(250) DEFAULT NULL,
  `label` varchar(250) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_facility_file`
--

INSERT INTO `pm_facility_file` (`id`, `lang`, `id_item`, `home`, `checked`, `rank`, `file`, `label`, `type`) VALUES
(1, 1, 31, 0, 1, 1, 'wheelchair.png', '', 'image'),
(1, 2, 31, 0, 1, 1, 'wheelchair.png', '', 'image'),
(1, 3, 31, 0, 1, 1, 'wheelchair.png', '', 'image'),
(1, 4, 31, 0, 1, 1, 'wheelchair.png', '', 'image'),
(2, 1, 20, 0, 1, 2, 'pet-allowed.png', '', 'image'),
(2, 2, 20, 0, 1, 2, 'pet-allowed.png', '', 'image'),
(2, 3, 20, 0, 1, 2, 'pet-allowed.png', '', 'image'),
(2, 4, 20, 0, 1, 2, 'pet-allowed.png', '', 'image'),
(3, 1, 21, 0, 1, 3, 'pet-not-allowed.png', '', 'image'),
(3, 2, 21, 0, 1, 3, 'pet-not-allowed.png', '', 'image'),
(3, 3, 21, 0, 1, 3, 'pet-not-allowed.png', '', 'image'),
(3, 4, 21, 0, 1, 3, 'pet-not-allowed.png', '', 'image'),
(4, 1, 3, 0, 1, 4, 'balcony.png', '', 'image'),
(4, 2, 3, 0, 1, 4, 'balcony.png', '', 'image'),
(4, 3, 3, 0, 1, 4, 'balcony.png', '', 'image'),
(4, 4, 3, 0, 1, 4, 'balcony.png', '', 'image'),
(5, 1, 4, 0, 1, 5, 'barbecue.png', '', 'image'),
(5, 2, 4, 0, 1, 5, 'barbecue.png', '', 'image'),
(5, 3, 4, 0, 1, 5, 'barbecue.png', '', 'image'),
(5, 4, 4, 0, 1, 5, 'barbecue.png', '', 'image'),
(6, 1, 8, 0, 1, 6, 'desk.png', '', 'image'),
(6, 2, 8, 0, 1, 6, 'desk.png', '', 'image'),
(6, 3, 8, 0, 1, 6, 'desk.png', '', 'image'),
(6, 4, 8, 0, 1, 6, 'desk.png', '', 'image'),
(7, 1, 6, 0, 1, 7, 'coffee.png', '', 'image'),
(7, 2, 6, 0, 1, 7, 'coffee.png', '', 'image'),
(7, 3, 6, 0, 1, 7, 'coffee.png', '', 'image'),
(7, 4, 6, 0, 1, 7, 'coffee.png', '', 'image'),
(8, 1, 24, 0, 1, 8, 'satellite.png', '', 'image'),
(8, 2, 24, 0, 1, 8, 'satellite.png', '', 'image'),
(8, 3, 24, 0, 1, 8, 'satellite.png', '', 'image'),
(8, 4, 24, 0, 1, 8, 'satellite.png', '', 'image'),
(9, 1, 1, 0, 1, 1, 'air-conditioning.png', '', 'image'),
(9, 2, 1, 0, 1, 1, 'air-conditioning.png', '', 'image'),
(9, 3, 1, 0, 1, 1, 'air-conditioning.png', '', 'image'),
(9, 4, 1, 0, 1, 1, 'air-conditioning.png', '', 'image'),
(10, 1, 23, 0, 1, 10, 'safe.png', '', 'image'),
(10, 2, 23, 0, 1, 10, 'safe.png', '', 'image'),
(10, 3, 23, 0, 1, 10, 'safe.png', '', 'image'),
(10, 4, 23, 0, 1, 10, 'safe.png', '', 'image'),
(11, 1, 26, 0, 1, 11, 'lounge.png', '', 'image'),
(11, 2, 26, 0, 1, 11, 'lounge.png', '', 'image'),
(11, 3, 26, 0, 1, 11, 'lounge.png', '', 'image'),
(11, 4, 26, 0, 1, 11, 'lounge.png', '', 'image'),
(12, 1, 15, 0, 1, 12, 'iron.png', '', 'image'),
(12, 2, 15, 0, 1, 12, 'iron.png', '', 'image'),
(12, 3, 15, 0, 1, 12, 'iron.png', '', 'image'),
(12, 4, 15, 0, 1, 12, 'iron.png', '', 'image'),
(13, 1, 14, 0, 1, 13, 'adsl.png', '', 'image'),
(13, 2, 14, 0, 1, 13, 'adsl.png', '', 'image'),
(13, 3, 14, 0, 1, 13, 'adsl.png', '', 'image'),
(13, 4, 14, 0, 1, 13, 'adsl.png', '', 'image'),
(14, 1, 9, 0, 1, 14, 'dishwasher.png', '', 'image'),
(14, 2, 9, 0, 1, 14, 'dishwasher.png', '', 'image'),
(14, 3, 9, 0, 1, 14, 'dishwasher.png', '', 'image'),
(14, 4, 9, 0, 1, 14, 'dishwasher.png', '', 'image'),
(15, 1, 2, 0, 1, 15, 'baby-cot.png', '', 'image'),
(15, 2, 2, 0, 1, 15, 'baby-cot.png', '', 'image'),
(15, 3, 2, 0, 1, 15, 'baby-cot.png', '', 'image'),
(15, 4, 2, 0, 1, 15, 'baby-cot.png', '', 'image'),
(16, 1, 30, 0, 1, 16, 'washing-machine.png', '', 'image'),
(16, 2, 30, 0, 1, 16, 'washing-machine.png', '', 'image'),
(16, 3, 30, 0, 1, 16, 'washing-machine.png', '', 'image'),
(16, 4, 30, 0, 1, 16, 'washing-machine.png', '', 'image'),
(17, 1, 16, 0, 1, 17, 'microwaves.png', '', 'image'),
(17, 2, 16, 0, 1, 17, 'microwaves.png', '', 'image'),
(17, 3, 16, 0, 1, 17, 'microwaves.png', '', 'image'),
(17, 4, 16, 0, 1, 17, 'microwaves.png', '', 'image'),
(18, 1, 17, 0, 1, 18, 'mini-bar.png', '', 'image'),
(18, 2, 17, 0, 1, 18, 'mini-bar.png', '', 'image'),
(18, 3, 17, 0, 1, 18, 'mini-bar.png', '', 'image'),
(18, 4, 17, 0, 1, 18, 'mini-bar.png', '', 'image'),
(19, 1, 18, 0, 1, 19, 'non-smoking.png', '', 'image'),
(19, 2, 18, 0, 1, 19, 'non-smoking.png', '', 'image'),
(19, 3, 18, 0, 1, 19, 'non-smoking.png', '', 'image'),
(19, 4, 18, 0, 1, 19, 'non-smoking.png', '', 'image'),
(20, 1, 11, 0, 1, 20, 'free-parking.png', '', 'image'),
(20, 2, 11, 0, 1, 20, 'free-parking.png', '', 'image'),
(20, 3, 11, 0, 1, 20, 'free-parking.png', '', 'image'),
(20, 4, 11, 0, 1, 20, 'free-parking.png', '', 'image'),
(21, 1, 19, 0, 1, 21, 'paid-parking.png', '', 'image'),
(21, 2, 19, 0, 1, 21, 'paid-parking.png', '', 'image'),
(21, 3, 19, 0, 1, 21, 'paid-parking.png', '', 'image'),
(21, 4, 19, 0, 1, 21, 'paid-parking.png', '', 'image'),
(22, 1, 7, 0, 1, 22, 'cooktop.png', '', 'image'),
(22, 2, 7, 0, 1, 22, 'cooktop.png', '', 'image'),
(22, 3, 7, 0, 1, 22, 'cooktop.png', '', 'image'),
(22, 4, 7, 0, 1, 22, 'cooktop.png', '', 'image'),
(23, 1, 22, 0, 1, 23, 'radio.png', '', 'image'),
(23, 2, 22, 0, 1, 23, 'radio.png', '', 'image'),
(23, 3, 22, 0, 1, 23, 'radio.png', '', 'image'),
(23, 4, 22, 0, 1, 23, 'radio.png', '', 'image'),
(24, 1, 12, 0, 1, 24, 'fridge.png', '', 'image'),
(24, 2, 12, 0, 1, 24, 'fridge.png', '', 'image'),
(24, 3, 12, 0, 1, 24, 'fridge.png', '', 'image'),
(24, 4, 12, 0, 1, 24, 'fridge.png', '', 'image'),
(25, 1, 25, 0, 1, 25, 'shower.png', '', 'image'),
(25, 2, 25, 0, 1, 25, 'shower.png', '', 'image'),
(25, 3, 25, 0, 1, 25, 'shower.png', '', 'image'),
(25, 4, 25, 0, 1, 25, 'shower.png', '', 'image'),
(26, 1, 5, 0, 1, 1, 'bath.png', '', 'image'),
(26, 2, 5, 0, 1, 1, 'bath.png', '', 'image'),
(26, 3, 5, 0, 1, 1, 'bath.png', '', 'image'),
(26, 4, 5, 0, 1, 1, 'bath.png', '', 'image'),
(27, 1, 13, 0, 1, 27, 'hairdryer.png', '', 'image'),
(27, 2, 13, 0, 1, 27, 'hairdryer.png', '', 'image'),
(27, 3, 13, 0, 1, 27, 'hairdryer.png', '', 'image'),
(27, 4, 13, 0, 1, 27, 'hairdryer.png', '', 'image'),
(28, 1, 27, 0, 1, 28, 'phone.png', '', 'image'),
(28, 2, 27, 0, 1, 28, 'phone.png', '', 'image'),
(28, 3, 27, 0, 1, 28, 'phone.png', '', 'image'),
(28, 4, 27, 0, 1, 28, 'phone.png', '', 'image'),
(29, 1, 28, 0, 1, 29, 'tv.png', '', 'image'),
(29, 2, 28, 0, 1, 29, 'tv.png', '', 'image'),
(29, 3, 28, 0, 1, 29, 'tv.png', '', 'image'),
(29, 4, 28, 0, 1, 29, 'tv.png', '', 'image'),
(30, 1, 29, 0, 1, 30, 'terrasse.png', '', 'image'),
(30, 2, 29, 0, 1, 30, 'terrasse.png', '', 'image'),
(30, 3, 29, 0, 1, 30, 'terrasse.png', '', 'image'),
(30, 4, 29, 0, 1, 30, 'terrasse.png', '', 'image'),
(31, 1, 10, 0, 1, 31, 'fan.png', '', 'image'),
(31, 2, 10, 0, 1, 31, 'fan.png', '', 'image'),
(31, 3, 10, 0, 1, 31, 'fan.png', '', 'image'),
(31, 4, 10, 0, 1, 31, 'fan.png', '', 'image'),
(32, 1, 32, 0, 1, 32, 'wifi.png', '', 'image'),
(32, 2, 32, 0, 1, 32, 'wifi.png', '', 'image'),
(32, 3, 32, 0, 1, 32, 'wifi.png', '', 'image'),
(32, 4, 32, 0, 1, 32, 'wifi.png', '', 'image'),
(33, 1, 33, 0, 1, 33, 'hifi.png', '', 'image'),
(33, 2, 33, 0, 1, 33, 'hifi.png', '', 'image'),
(33, 3, 33, 0, 1, 33, 'hifi.png', '', 'image'),
(33, 4, 33, 0, 1, 33, 'hifi.png', '', 'image'),
(34, 1, 34, 0, 1, 34, 'dvd.png', '', 'image'),
(34, 2, 34, 0, 1, 34, 'dvd.png', '', 'image'),
(34, 3, 34, 0, 1, 34, 'dvd.png', '', 'image'),
(34, 4, 34, 0, 1, 34, 'dvd.png', '', 'image'),
(35, 1, 33, 0, 1, 33, 'elevator.png', '', 'image'),
(35, 2, 33, 0, 1, 33, 'elevator.png', '', 'image'),
(35, 3, 33, 0, 1, 33, 'elevator.png', '', 'image'),
(35, 4, 33, 0, 1, 33, 'elevator.png', '', 'image'),
(36, 1, 33, 0, 1, 33, 'lounge.png', '', 'image'),
(36, 2, 33, 0, 1, 33, 'lounge.png', '', 'image'),
(36, 3, 33, 0, 1, 33, 'lounge.png', '', 'image'),
(36, 4, 33, 0, 1, 33, 'lounge.png', '', 'image'),
(37, 1, 33, 0, 1, 33, 'restaurant.png', '', 'image'),
(37, 2, 33, 0, 1, 33, 'restaurant.png', '', 'image'),
(37, 3, 33, 0, 1, 33, 'restaurant.png', '', 'image'),
(37, 4, 33, 0, 1, 33, 'restaurant.png', '', 'image'),
(38, 1, 33, 0, 1, 33, 'room-service.png', '', 'image'),
(38, 2, 33, 0, 1, 33, 'room-service.png', '', 'image'),
(38, 3, 33, 0, 1, 33, 'room-service.png', '', 'image'),
(38, 4, 33, 0, 1, 33, 'room-service.png', '', 'image'),
(39, 1, 33, 0, 1, 33, 'cloakroom.png', '', 'image'),
(39, 2, 33, 0, 1, 33, 'cloakroom.png', '', 'image'),
(39, 3, 33, 0, 1, 33, 'cloakroom.png', '', 'image'),
(39, 4, 33, 0, 1, 33, 'cloakroom.png', '', 'image');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_hotel`
--

CREATE TABLE `pm_hotel` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `users` text DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `subtitle` varchar(250) DEFAULT NULL,
  `alias` varchar(100) DEFAULT NULL,
  `class` int(11) DEFAULT 0,
  `address` varchar(250) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `web` varchar(250) DEFAULT NULL,
  `descr` longtext DEFAULT NULL,
  `facilities` varchar(250) DEFAULT NULL,
  `id_destination` int(11) DEFAULT NULL,
  `paypal_email` varchar(250) DEFAULT NULL,
  `home` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 0,
  `rank` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_hotel`
--

INSERT INTO `pm_hotel` (`id`, `lang`, `users`, `title`, `subtitle`, `alias`, `class`, `address`, `lat`, `lng`, `email`, `phone`, `web`, `descr`, `facilities`, `id_destination`, `paypal_email`, `home`, `checked`, `rank`) VALUES
(1, 1, '1', 'Royal Hotel', 'Hôtel luxueux avec vue sur la mer', 'royal-hotel', 4, 'Jalan Dago Giri', -6.8453585, 107.6290052, 'info@salsebandung.com', '+62 811 2131 900 / +62 821 3480 9350', 'https://www.salsebandung.com', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean aliquet felis massa, sed condimentum ligula feugiat et. Etiam facilisis euismod dignissim. Vivamus facilisis lorem ut purus pellentesque, nec sollicitudin lorem suscipit. Fusce sed enim ultricies, venenatis nunc ut, pharetra nunc. Quisque sollicitudin egestas varius. Nulla aliquet magna sapien, id malesuada felis lobortis id. Vivamus vulputate sed enim sit amet eleifend. Vivamus sit amet felis id urna vulputate maximus. Nullam fringilla sed turpis non volutpat. Cras ultrices diam velit, ac volutpat odio semper at. Sed pulvinar turpis imperdiet sapien hendrerit pulvinar.</p>\r\n', '1,11,20,37,32', NULL, '', 1, 1, 1),
(1, 2, '1', 'Villatel', 'Villatel Salse Bandung', 'villatel', 3, 'Jalan Dago Giri', -6.8453585, 107.6290052, 'info@salsebandung.com', '022 2504105  / +62 81 5601 6666', 'https://www.salsebandung.com', '<p>VillaTel Salse is a small resort meant as a model for a larger resort with similar characters. The Restaurant (Warung Salse) opened on March 2014 while the whole resort will be open in December 2014. The resort consist of 8 villas and 20 rooms each with 2 bedrooms. VillaTel is a functional design space – functioned as a resort.</p>\r\n\r\n<p> </p>\r\n\r\n<p>As a pilot model afor larger resorts, Salse is equipped with mini amphitheater placed above river streams, spa around the trees, and a restaurant named Warung Salse. At this concept restaurant, we provide innovative traditional food and drinks for every visitor to indulge, with helps from the splendid green ridge view. Far below, a valley with small river formed a zen-state peace with the sound of water dripping onto rocks.</p>\r\n', '11,21,37,23,32', NULL, '', 1, 1, 1),
(1, 3, '1', 'Royal Hotel', 'فندق فخم يطل على البحر', 'royal-hotel', 4, 'Jalan Dago Giri', -6.8453585, 107.6290052, 'info@salsebandung.com', '+62 811 2131 900 / +62 821 3480 9350', 'https://www.salsebandung.com', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean aliquet felis massa, sed condimentum ligula feugiat et. Etiam facilisis euismod dignissim. Vivamus facilisis lorem ut purus pellentesque, nec sollicitudin lorem suscipit. Fusce sed enim ultricies, venenatis nunc ut, pharetra nunc. Quisque sollicitudin egestas varius. Nulla aliquet magna sapien, id malesuada felis lobortis id. Vivamus vulputate sed enim sit amet eleifend. Vivamus sit amet felis id urna vulputate maximus. Nullam fringilla sed turpis non volutpat. Cras ultrices diam velit, ac volutpat odio semper at. Sed pulvinar turpis imperdiet sapien hendrerit pulvinar.</p>\r\n', '1,11,20,37,32', NULL, '', 1, 1, 1),
(1, 4, '1', 'Villatel', 'Villatel Salse Bandung', 'villatel', 3, 'Jalan Dago Giri', -6.8453585, 107.6290052, 'info@salsebandung.com', '022 2504105  / +62 81 5601 6666', 'https://www.salsebandung.com', '<p>VillaTel Salse adalah resor kecil yang dimaksudkan untuk menjadi model bagi resor yang lebih besar dengan karakter yang mirip. Restoran tersebut (Warung Salse) dibuka pada Maret 2014 sedangkan keseluruhan resor akan dibuka pada Desember 2014. Resor tersebut terdiri dari 8 villa dan 200 kamar masing-masing dengan 2 kamar tidur. VillaTel adalah ruang dengan desain fungsional yang berfungsi sebagai resor.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Sebagai perintis untuk resor yang lebih besar, Salse dilengkapi dengan ampiteater yang terletak diatas aliran sungai,spa yang dikelilingi pohon, dan restoran yang dinamakan Warung Salse. Dengan konsep restoran ini, kami menawarkan inovasi makanan dan minuman tradisional untuk memanjakan setiap pengunjung, ditambah dengan pemandangan dari hijaunya pegunungan. Jauh dibawah, ada lembah dengan sungai kecil membentuk kedamaian zen-state dengan suara air yang berjatuhan ke bebatuan.</p>\r\n', '11,21,37,23,32', NULL, '', 1, 1, 1),
(2, 1, '6', '', '', '', 3, ' Jl. Dago Giri No.101, Mekarwangi, Lembang, Kabupaten Bandung Barat, Jawa Barat 40391', -6.8455915, 107.62919, '', '', 'salsebandung.com', '', '5,11,18,37,27,32', NULL, '', 1, 1, 2),
(2, 2, '6', 'Villacamp', 'Villacamp Salse', 'jogle-camp', 3, ' Jl. Dago Giri No.101, Mekarwangi, Lembang, Kabupaten Bandung Barat, Jawa Barat 40391', -6.8455915, 107.62919, '', '', 'salsebandung.com', '<p>Exotic Cultural Resort Villacamp Salse, is place where you came back to old traditional life style</p>\r\n', '5,11,18,37,27,32', NULL, '', 1, 1, 2),
(2, 3, '6', '', '', '', 3, ' Jl. Dago Giri No.101, Mekarwangi, Lembang, Kabupaten Bandung Barat, Jawa Barat 40391', -6.8455915, 107.62919, '', '', 'salsebandung.com', '', '5,11,18,37,27,32', NULL, '', 1, 1, 2),
(2, 4, '6', 'Villacamp', 'Villacamp Salse', 'jogle-camp', 3, ' Jl. Dago Giri No.101, Mekarwangi, Lembang, Kabupaten Bandung Barat, Jawa Barat 40391', -6.8455915, 107.62919, '', '', 'salsebandung.com', '<p>Kultur Eksotik Resor Villatel Salse, adalah tempat dimana kamu kembali ke gaya hidup tradisional</p>\r\n', '5,11,18,37,27,32', NULL, '', 1, 1, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_hotel_file`
--

CREATE TABLE `pm_hotel_file` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `home` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 1,
  `rank` int(11) DEFAULT 0,
  `file` varchar(250) DEFAULT NULL,
  `label` varchar(250) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_hotel_file`
--

INSERT INTO `pm_hotel_file` (`id`, `lang`, `id_item`, `home`, `checked`, `rank`, `file`, `label`, `type`) VALUES
(3, 1, 1, NULL, 1, 2, 'villatel-salse-2.jpg', '', 'image'),
(3, 2, 1, NULL, 1, 2, 'villatel-salse-2.jpg', '', 'image'),
(3, 3, 1, NULL, 1, 2, 'villatel-salse-2.jpg', '', 'image'),
(3, 4, 1, NULL, 1, 2, 'villatel-salse-2.jpg', '', 'image'),
(4, 1, 1, NULL, 1, 4, 'villatel-salse.jpg', '', 'image'),
(4, 2, 1, NULL, 1, 4, 'villatel-salse.jpg', '', 'image'),
(4, 3, 1, NULL, 1, 4, 'villatel-salse.jpg', '', 'image'),
(4, 4, 1, NULL, 1, 4, 'villatel-salse.jpg', '', 'image'),
(5, 1, 1, NULL, 1, 5, 'warung-salse-jacuzzi.jpg', '', 'image'),
(5, 2, 1, NULL, 1, 5, 'warung-salse-jacuzzi.jpg', '', 'image'),
(5, 3, 1, NULL, 1, 5, 'warung-salse-jacuzzi.jpg', '', 'image'),
(5, 4, 1, NULL, 1, 5, 'warung-salse-jacuzzi.jpg', '', 'image'),
(7, 1, 1, NULL, 1, 3, '1159792-17040916560052250745.jpg', '', 'image'),
(7, 2, 1, NULL, 1, 3, '1159792-17040916560052250745.jpg', '', 'image'),
(7, 3, 1, NULL, 1, 3, '1159792-17040916560052250745.jpg', '', 'image'),
(7, 4, 1, NULL, 1, 3, '1159792-17040916560052250745.jpg', '', 'image'),
(8, 1, 2, NULL, 1, 10, 'whatsapp-image-2019-12-10-at-17-39-03-1.jpeg', '', 'image'),
(8, 2, 2, NULL, 1, 10, 'whatsapp-image-2019-12-10-at-17-39-03-1.jpeg', '', 'image'),
(8, 3, 2, NULL, 1, 10, 'whatsapp-image-2019-12-10-at-17-39-03-1.jpeg', '', 'image'),
(8, 4, 2, NULL, 1, 10, 'whatsapp-image-2019-12-10-at-17-39-03-1.jpeg', '', 'image'),
(9, 1, 2, NULL, 1, 7, 'whatsapp-image-2019-12-10-at-17-38-52.jpeg', '', 'image'),
(9, 2, 2, NULL, 1, 7, 'whatsapp-image-2019-12-10-at-17-38-52.jpeg', '', 'image'),
(9, 3, 2, NULL, 1, 7, 'whatsapp-image-2019-12-10-at-17-38-52.jpeg', '', 'image'),
(9, 4, 2, NULL, 1, 7, 'whatsapp-image-2019-12-10-at-17-38-52.jpeg', '', 'image'),
(10, 1, 2, NULL, 1, 2, 'whatsapp-image-2019-12-10-at-17-39-01.jpeg', '', 'image'),
(10, 2, 2, NULL, 1, 2, 'whatsapp-image-2019-12-10-at-17-39-01.jpeg', '', 'image'),
(10, 3, 2, NULL, 1, 2, 'whatsapp-image-2019-12-10-at-17-39-01.jpeg', '', 'image'),
(10, 4, 2, NULL, 1, 2, 'whatsapp-image-2019-12-10-at-17-39-01.jpeg', '', 'image'),
(11, 1, 2, NULL, 1, 5, 'whatsapp-image-2019-12-10-at-17-39-01-1.jpeg', '', 'image'),
(11, 2, 2, NULL, 1, 5, 'whatsapp-image-2019-12-10-at-17-39-01-1.jpeg', '', 'image'),
(11, 3, 2, NULL, 1, 5, 'whatsapp-image-2019-12-10-at-17-39-01-1.jpeg', '', 'image'),
(11, 4, 2, NULL, 1, 5, 'whatsapp-image-2019-12-10-at-17-39-01-1.jpeg', '', 'image'),
(12, 1, 2, NULL, 1, 1, 'whatsapp-image-2019-12-10-at-17-39-00.jpeg', '', 'image'),
(12, 2, 2, NULL, 1, 1, 'whatsapp-image-2019-12-10-at-17-39-00.jpeg', '', 'image'),
(12, 3, 2, NULL, 1, 1, 'whatsapp-image-2019-12-10-at-17-39-00.jpeg', '', 'image'),
(12, 4, 2, NULL, 1, 1, 'whatsapp-image-2019-12-10-at-17-39-00.jpeg', '', 'image'),
(13, 1, 2, NULL, 1, 8, 'whatsapp-image-2019-12-10-at-17-39-02.jpeg', '', 'image'),
(13, 2, 2, NULL, 1, 8, 'whatsapp-image-2019-12-10-at-17-39-02.jpeg', '', 'image'),
(13, 3, 2, NULL, 1, 8, 'whatsapp-image-2019-12-10-at-17-39-02.jpeg', '', 'image'),
(13, 4, 2, NULL, 1, 8, 'whatsapp-image-2019-12-10-at-17-39-02.jpeg', '', 'image'),
(14, 1, 2, NULL, 1, 9, 'whatsapp-image-2019-12-10-at-17-39-02-1.jpeg', '', 'image'),
(14, 2, 2, NULL, 1, 9, 'whatsapp-image-2019-12-10-at-17-39-02-1.jpeg', '', 'image'),
(14, 3, 2, NULL, 1, 9, 'whatsapp-image-2019-12-10-at-17-39-02-1.jpeg', '', 'image'),
(14, 4, 2, NULL, 1, 9, 'whatsapp-image-2019-12-10-at-17-39-02-1.jpeg', '', 'image'),
(15, 1, 2, NULL, 1, 4, 'whatsapp-image-2019-12-10-at-17-38-54-1.jpeg', '', 'image'),
(15, 2, 2, NULL, 1, 4, 'whatsapp-image-2019-12-10-at-17-38-54-1.jpeg', '', 'image'),
(15, 3, 2, NULL, 1, 4, 'whatsapp-image-2019-12-10-at-17-38-54-1.jpeg', '', 'image'),
(15, 4, 2, NULL, 1, 4, 'whatsapp-image-2019-12-10-at-17-38-54-1.jpeg', '', 'image'),
(16, 1, 2, NULL, 1, 3, 'whatsapp-image-2019-12-10-at-17-38-57.jpeg', '', 'image'),
(16, 2, 2, NULL, 1, 3, 'whatsapp-image-2019-12-10-at-17-38-57.jpeg', '', 'image'),
(16, 3, 2, NULL, 1, 3, 'whatsapp-image-2019-12-10-at-17-38-57.jpeg', '', 'image'),
(16, 4, 2, NULL, 1, 3, 'whatsapp-image-2019-12-10-at-17-38-57.jpeg', '', 'image'),
(17, 1, 2, NULL, 1, 6, 'whatsapp-image-2019-12-10-at-17-38-54.jpeg', '', 'image'),
(17, 2, 2, NULL, 1, 6, 'whatsapp-image-2019-12-10-at-17-38-54.jpeg', '', 'image'),
(17, 3, 2, NULL, 1, 6, 'whatsapp-image-2019-12-10-at-17-38-54.jpeg', '', 'image'),
(17, 4, 2, NULL, 1, 6, 'whatsapp-image-2019-12-10-at-17-38-54.jpeg', '', 'image'),
(18, 1, 1, NULL, 1, 1, 'front-office.jpeg', '', 'image'),
(18, 2, 1, NULL, 1, 1, 'front-office.jpeg', '', 'image'),
(18, 3, 1, NULL, 1, 1, 'front-office.jpeg', '', 'image'),
(18, 4, 1, NULL, 1, 1, 'front-office.jpeg', '', 'image');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_lang`
--

CREATE TABLE `pm_lang` (
  `id` int(11) NOT NULL,
  `title` varchar(20) DEFAULT NULL,
  `locale` varchar(20) DEFAULT NULL,
  `main` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 0,
  `rank` int(11) DEFAULT 0,
  `tag` varchar(20) DEFAULT NULL,
  `rtl` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_lang`
--

INSERT INTO `pm_lang` (`id`, `title`, `locale`, `main`, `checked`, `rank`, `tag`, `rtl`) VALUES
(1, 'Français', 'fr_FR', 0, 3, 3, 'fr', 0),
(2, 'English', 'en_GB', 1, 1, 1, 'en', 0),
(3, 'عربي', 'ar_MA', 0, 3, 4, 'ar', 1),
(4, 'Indonesia', 'ind', 0, 1, 2, 'ind', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_lang_file`
--

CREATE TABLE `pm_lang_file` (
  `id` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `home` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 1,
  `rank` int(11) DEFAULT 0,
  `file` varchar(250) DEFAULT NULL,
  `label` varchar(250) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_lang_file`
--

INSERT INTO `pm_lang_file` (`id`, `id_item`, `home`, `checked`, `rank`, `file`, `label`, `type`) VALUES
(1, 1, 0, 1, 2, 'fr.png', '', 'image'),
(2, 2, 0, 1, 1, 'gb.png', '', 'image'),
(3, 3, 0, 1, 3, 'ar.png', '', 'image'),
(6, 4, NULL, 1, 5, 'ind.png', NULL, 'image');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_location`
--

CREATE TABLE `pm_location` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `checked` int(11) DEFAULT 0,
  `pages` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_location`
--

INSERT INTO `pm_location` (`id`, `name`, `address`, `lat`, `lng`, `checked`, `pages`) VALUES
(1, 'Salse Resorts', 'Jalan Sukanegara No. 88 Pagerwangi, Lembang, Bandung Barat 40391', -6.852022, 107.6170171, 1, '2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_media`
--

CREATE TABLE `pm_media` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_media_file`
--

CREATE TABLE `pm_media_file` (
  `id` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `home` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 1,
  `rank` int(11) DEFAULT 0,
  `file` varchar(250) DEFAULT NULL,
  `label` varchar(250) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_menu`
--

CREATE TABLE `pm_menu` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `item_type` varchar(30) DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  `url` text DEFAULT NULL,
  `main` int(11) DEFAULT 1,
  `footer` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 0,
  `rank` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_menu`
--

INSERT INTO `pm_menu` (`id`, `lang`, `name`, `title`, `id_parent`, `item_type`, `id_item`, `url`, `main`, `footer`, `checked`, `rank`) VALUES
(1, 1, 'Accueil', 'Lorem ipsum dolor sit amet', NULL, 'page', 1, '', 1, 0, 1, 1),
(1, 2, 'Home', 'Salse Resorts', NULL, 'page', 1, '', 1, 0, 1, 1),
(1, 3, 'ترحيب', 'هو سقطت الساحلية ذات, أن.', NULL, 'page', 1, '', 1, 0, 1, 1),
(1, 4, 'Home', 'Salse Resorts', NULL, 'page', 1, '', 1, 0, 1, 1),
(2, 1, 'Contact', 'Contact', NULL, 'page', 2, NULL, 1, 1, 1, 8),
(2, 2, 'Contact', 'Contact', NULL, 'page', 2, '', 1, 1, 1, 8),
(2, 3, 'جهة الاتصال', 'جهة الاتصال', NULL, 'page', 2, NULL, 1, 1, 1, 8),
(2, 4, 'Kontak', 'Kontak', NULL, 'page', 2, '', 1, 1, 1, 8),
(3, 1, 'Mentions légales', 'Mentions légales', NULL, 'page', 3, '', 0, 1, 1, 11),
(3, 2, 'Term Of Use', 'Term Of Use', NULL, 'page', 3, '', 0, 1, 1, 11),
(3, 3, 'يذكر القانونية', 'يذكر القانونية', NULL, 'page', 3, '', 0, 1, 1, 11),
(3, 4, 'Ketentuan Pengguna', 'Ketentuan Pengguna', NULL, 'page', 3, '', 0, 1, 1, 11),
(4, 1, 'Plan du site', 'Plan du site', NULL, 'page', 4, NULL, 0, 1, 1, 12),
(4, 2, 'Sitemap', 'Sitemap', NULL, 'page', 4, '', 0, 1, 1, 12),
(4, 3, 'خريطة الموقع', 'خريطة الموقع', NULL, 'page', 4, NULL, 0, 1, 1, 12),
(4, 4, 'Situs Peta', 'Situs Peta', NULL, 'page', 4, '', 0, 1, 1, 12),
(5, 1, 'Qui sommes-nous ?', 'Qui sommes-nous ?', NULL, 'page', 5, '', 0, 1, 1, 10),
(5, 2, 'About us', 'About us', NULL, 'page', 5, '', 0, 1, 1, 10),
(5, 3, 'معلومات عنا', 'معلومات عنا', NULL, 'page', 5, '', 0, 1, 1, 10),
(5, 4, 'Tentang Kami', 'Tentang Kami', NULL, 'page', 5, '', 0, 1, 1, 10),
(6, 1, 'Galerie', 'Galerie', NULL, 'page', 7, NULL, 1, 0, 1, 7),
(6, 2, 'Gallery', 'Gallery', NULL, 'page', 7, '', 1, 0, 1, 7),
(6, 3, 'صور معرض', 'صور معرض', NULL, 'page', 7, NULL, 1, 0, 1, 7),
(6, 4, 'Galeri', 'Galeri', NULL, 'page', 7, '', 1, 0, 1, 7),
(7, 1, 'Hôtels', 'Hôtels', NULL, 'page', 9, NULL, 1, 0, 1, 2),
(7, 2, 'Hotels', 'Hotels', NULL, 'page', 9, '', 1, 0, 1, 2),
(7, 3, 'الفنادق', 'الفنادق', NULL, 'page', 9, NULL, 1, 0, 1, 2),
(7, 4, 'Hotel', 'Hotel', NULL, 'page', 9, '', 1, 0, 1, 2),
(8, 1, 'Réserver', 'Réserver', NULL, 'page', 10, NULL, 1, 0, 1, 3),
(8, 2, 'Booking', 'Booking', NULL, 'page', 10, '', 1, 0, 1, 3),
(8, 3, 'الحجز', 'الحجز', NULL, 'page', 10, NULL, 1, 0, 1, 3),
(8, 4, 'Pemesanan', 'Pemesanan', NULL, 'page', 10, '', 1, 0, 1, 3),
(9, 1, 'Activités', 'Activités', NULL, 'page', 16, NULL, 1, 0, 1, 4),
(9, 2, 'Activities', 'Activities', NULL, 'page', 16, '', 1, 0, 1, 4),
(9, 3, 'Activities', 'Activities', NULL, 'page', 16, NULL, 1, 0, 1, 4),
(9, 4, 'Kegiatan', 'Kegiatan', NULL, 'page', 16, '', 1, 0, 1, 4),
(10, 1, 'Destinations', '', NULL, 'page', 18, '', 0, 0, 1, 9),
(10, 2, 'Destinations', '', NULL, 'page', 18, '', 0, 0, 1, 9),
(10, 3, 'وجهات', '', NULL, 'page', 18, '', 0, 0, 1, 9),
(10, 4, 'Tujuan', 'Tujuan', NULL, 'page', 18, '', 0, 0, 1, 9),
(12, 1, 'Pembayaran', 'Pembayaran', NULL, 'page', 20, '', 1, 1, 3, 13),
(12, 2, 'Pembayaran', 'Pembayaran', NULL, 'page', 20, '', 1, 1, 3, 13),
(12, 3, 'Pembayaran', 'Pembayaran', NULL, 'page', 20, '', 1, 1, 3, 13),
(12, 4, 'Pembayaran', 'Pembayaran', NULL, 'page', 20, '', 1, 1, 3, 13),
(13, 1, '', '', NULL, 'page', 25, '', 1, 0, 1, 5),
(13, 2, 'COLABOREA ', 'COLABOREA ', NULL, 'url', NULL, 'https://colaborea.id/', 1, 0, 1, 5),
(13, 3, '', '', NULL, 'page', 25, '', 1, 0, 1, 5),
(13, 4, 'COLABOREA ', 'COLABOREA ', NULL, 'url', NULL, 'https://colaborea.id/', 1, 0, 1, 5),
(14, 1, '', '', NULL, 'page', 24, '', 1, 0, 1, 6),
(14, 2, 'Joglo Salse', 'Joglo Salse', NULL, 'page', 24, '', 1, 0, 1, 6),
(14, 3, '', '', NULL, 'page', 24, '', 1, 0, 1, 6),
(14, 4, 'Joglo Salse', 'Joglo Salse', NULL, 'page', 24, '', 1, 0, 1, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_message`
--

CREATE TABLE `pm_message` (
  `id` int(11) NOT NULL,
  `add_date` int(11) DEFAULT NULL,
  `edit_date` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` longtext DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `subject` varchar(250) DEFAULT NULL,
  `msg` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_message`
--

INSERT INTO `pm_message` (`id`, `add_date`, `edit_date`, `name`, `email`, `address`, `phone`, `subject`, `msg`) VALUES
(749, 1610357925, NULL, 'Erica', 'ericajacksonmi0@yahoo.com', 'Hi, \r\n\r\nWe\'re wondering if you\'d be interested in a \'dofollow\' backlink to salsebandung.com from our DA50 website?\r\n\r\nOur website is dedicated to facts/education, and so can host articles on pretty much any topic.\r\n\r\nYou can either write a new article yourself, or we can link from existing content. The price is just $45 and you can pay once the article/link has been published. This is a one-time fee, so there are no extra charges.\r\n\r\nIf you\'re interested, please reply to this email, including the word \'interested\' in the Subject Field.\r\n\r\nNot sure what DA is? Have a read here: https://moz.com/learn/seo/domain-authority\r\n\r\nKind Regards,\r\nErica', '079 1002 4999', 'DA50 Backlink to salsebandung.com', 'Hi, \r\n\r\nWe\'re wondering if you\'d be interested in a \'dofollow\' backlink to salsebandung.com from our DA50 website?\r\n\r\nOur website is dedicated to facts/education, and so can host articles on pretty much any topic.\r\n\r\nYou can either write a new article yourself, or we can link from existing content. The price is just $45 and you can pay once the article/link has been published. This is a one-time fee, so there are no extra charges.\r\n\r\nAlso: Once the article has been published, and your backlink has been added, it will be shared out to over 2.8 million social media followers (if it\'s educationally based). This means you aren\'t just getting the high valued backlink, you\'re also getting the potential of more traffic to your site.\r\n\r\nIf you\'re interested, please reply to this email, including the word \'interested\' in the Subject Field.\r\n\r\nKind Regards,\r\nErica'),
(750, 1611410418, NULL, 'mgiant', 'maximmad555@gmail.com', 'https://vk.com/public195417980', '87632318321', 'Купить XBOX игры  разнообразные игры:  Клюси игр XBOX ', 'xbox игры купить онлайн+ https://plati.market/itm/ea-sports-ufc-3-us-xbox-one-key/2892265 \r\n<a href=https://plati.market/itm/fifa-20-uk-xbox-one-kljuch-cifr-kod--podarok/2961184>  FIFA 20  XBOX ONE Ключ  / Цифр код + подарок </a> \r\n<a href=https://plati.market/itm/grand-theft-auto-v-gta-5-xbox-one-cifrovoj-kljuch/3008735> ? Grand Theft Auto V / GTA 5 XBOX ONE Цифровой Ключ ??</a> \r\n<a href=https://plati.market/itm/3051308> Mafia Definitive Edition XBOX CD KEY</a> \r\n<a href=https://plati.market/itm/2892901>Watch Dogs 2  - Xbox Live Key - +подарок </a> \r\n<a href=https://plati.market/itm/3037039>BATTLEFIELD V?  Key XBOX ONE+подарок</a> \r\n<a href=https://plati.market/itm/metro-last-light-redux-xbox-one-present/2915157>METRO: LAST LIGHT REDUX XBOX ONE+подарок</a> \r\n<a href=https://plati.market/itm/2961721>The Crew 2 ?? XBOX/PC+present </a> \r\n<a href=https://plati.market/itm/2970072>Diablo III: Eternal Collection Xbox One</a> \r\n<a href=https://plati.market/itm/3008085>Cities: Skylines - Xbox One Edition  </a> \r\n<a href=https://plati.market/itm/assassin-s-creed-odisseja-br-xbox-kljuch-podarok/2966161> Red dead Online XBOX Key+подарок  </a> \r\n<a href=https://plati.market/itm/3045341>Thief XBOX ONE Цифровой ключ</a> \r\n<a href=https://plati.market/itm/2976540>The Sims 4 XBOX One Digital Key</a> \r\n<a href=https://plati.market/itm/3036392>Call of Duty Black Ops Cold War-аккаунт Xbox=400р</a>'),
(751, 1611574133, NULL, 'bobbiepd1', 'jerryej60@masaaki1610.masato56.webproton.site', 'http://blondevideofree.xblognetwork.com', '88461629676', 'My new hot project', 'Sexy photo galleries, daily updated pics\r\nhttp://lbig.boobs.allproblog.com/?ann \r\n\r\n black lesbos porn sex asian porn sights mrs clause porn porn portfolio violent porn \r\n\r\n'),
(752, 1612299140, NULL, 'Jennifer', 'lela.buss@gmail.com', 'Hi,\r\n \r\nWe\'d like to introduce to you our video creation service which we feel may be beneficial for you and your site salsebandung.com.\r\n\r\nCheck out a few of our existing videos here:\r\n\r\n1. A video on Covid-19 which was a blog post/article turned into video format: https://www.youtube.com/watch?v=TaMaDwX7tBU\r\n2. A product review for Japanese teapots, including product images: https://www.youtube.com/watch?v=VqjToeQ6KOc\r\n3. Promotional video for Japanese Matcha tea: https://www.youtube.com/watch?v=y3nEeQoTtOE\r\n\r\nAll of our videos are made with stock footage in a similar format as the above examples and we have voice over artists with US/UK/Australian accents.\r\n\r\nOur videos can be used to explain your business, service, or product; we can convert your online articles/blog posts into video footage; they can be used to educate people; we can create adverts for social media advertising such as Facebook Ads, plus lots more.\r\n\r\nOur prices are as follows depending on video length:\r\n\r\n0-1 minutes = $159\r\n1-2 minutes = $269\r\n2-3 minutes = $379\r\n \r\n*All prices above are in USD and include a custom video, full script and a voice over.\r\n \r\nIf this is something you would like to discuss further, please contact me at: lindajenniferse8@yahoo.com\r\n\r\nKind Regards,\r\n\r\nJennifer', '07930823274', 'Video Creation for salsebandung.com', 'Hi,\r\n \r\nWe\'d like to introduce to you our video creation service which we feel may be beneficial for you and your site salsebandung.com.\r\n\r\nCheck out a few of our existing videos here:\r\n\r\n1. A video on Covid-19 which was a blog post/article turned into video format: https://www.youtube.com/watch?v=TaMaDwX7tBU\r\n2. A product review for Japanese teapots, including product images: https://www.youtube.com/watch?v=VqjToeQ6KOc\r\n3. Promotional video for Japanese Matcha tea: https://www.youtube.com/watch?v=y3nEeQoTtOE\r\n\r\nAll of our videos are made with stock footage in a similar format as the above examples and we have voice over artists with US/UK/Australian accents.\r\n\r\nOur videos can be used to explain your business, service, or product; we can convert your online articles/blog posts into video footage; they can be used to educate people; we can create adverts for social media advertising such as Facebook Ads, plus lots more.\r\n\r\nOur prices are as follows depending on video length:\r\n\r\n0-1 minutes = $159\r\n1-2 minutes = $269\r\n2-3 minutes = $379\r\n \r\n*All prices above are in USD and include a custom video, full script and a voice over.\r\n \r\nIf this is something you would like to discuss further, please contact me at: lindajenniferse8@yahoo.com\r\n\r\nKind Regards,\r\n\r\nJennifer'),
(753, 1614678593, NULL, 'QGWL5CQZUG75HB0WXF   www.bing.com', 'lioceven-mu41sf27@bk.ru', 'QGWL5CQZUG75HB0WXF   www.google.com     Where are you located ? I want to come to you one of these days\r\n', 'QGWL5CQZUG75HB0WXF   www.yahoo.com', 'QGWL5CQZUG75HB0WXF   www.ask.com', 'QGWL5CQZUG75HB0WXF   www.google.com     Where are you located ? I want to come to you one of these days\r\n'),
(754, 1615133049, NULL, 'Harry', 'martinshow@gmail.com', 'It looks like you\'ve misspelled the word \"spesial\" on your website.  I thought you would like to know :).  Silly mistakes can ruin your site\'s credibility.  I\'ve used a tool called SpellScan.com in the past to keep mistakes off of my website.\r\n\r\n-Harry', '(503) 380-6300', 'Error on your website', 'It looks like you\'ve misspelled the word \"spesial\" on your website.  I thought you would like to know :).  Silly mistakes can ruin your site\'s credibility.  I\'ve used a tool called SpellScan.com in the past to keep mistakes off of my website.\r\n\r\n-Harry'),
(755, 1616148812, NULL, 'Sssvegroug', 'fgsfgsgs@gmail.com', '', '88374663911', 'loli female', 'loli esprit de corps PT¦¦¦HC C¦¦¦P offline forum \r\n \r\n===>>> https://biturl.top/BnIVna <<<==='),
(756, 1617872411, NULL, 'QGWTMFFPD74AL45WXF   www.bing.com', 'cernisen-ogohh@mail.ru', 'QGWTMFFPD74AL45WXF   www.google.com     Where are you located ? I want to come to you one of these days\r\n', 'QGWTMFFPD74AL45WXF   www.yahoo.com', 'QGWTMFFPD74AL45WXF   www.ask.com', 'QGWTMFFPD74AL45WXF   www.google.com     Where are you located ? I want to come to you one of these days\r\n'),
(757, 1618255005, NULL, 'QGW8F1W91F6TABJWXF   www.bing.com', 'olya_romanenko_92@inbox.ru', 'QGW8F1W91F6TABJWXF   www.google.com\r\n   I have a small question for you', 'QGW8F1W91F6TABJWXF   www.yahoo.com', 'QGW8F1W91F6TABJWXF   www.ask.com', 'QGW8F1W91F6TABJWXF   www.google.com\r\n   I have a small question for you'),
(758, 1618427902, NULL, 'WandaDig', 'jo.an8.9.0.8.3@gmail.com', 'https://moverslosangeles.city ', '89181559678', 'Movers Los Angeles ', '<a href=https://moverslosangeles.city>Movers Los Angeles </a>'),
(759, 1618500228, NULL, 'qg11', 'pl16@riku76.webmail2.site', 'http://relayblog.com', '83889243723', 'Browse over 500 000 of the best porn galleries, daily updated collections', 'Scandal porn galleries, daily updated lists\r\nhttp://kanakox.com/?melany \r\n horny mature porn tubes free porn galerie pics homemade sleep porn best strapon porn videos free getting fuck porn \r\n\r\n'),
(760, 1619637554, NULL, 'Celesta', 'chase.amundsen@mail.ru', 'Hi,\r\n\r\nAre you currently operating Wordpress/Woocommerce or perhaps do you intend to make use of it someday ? We currently provide over 2500 premium plugins but also themes to download : http://shortjs.xyz/02oVf\r\n\r\nThank You,\r\n\r\nCelesta', '418 88 913', 'Question', 'Hello There,\r\n\r\nAre you operating Wordpress/Woocommerce or maybe you want to make use of it sooner or later ? We provide around 2500 premium plugins and themes to download : http://shortjs.xyz/02oVf\r\n\r\nRegards,\r\n\r\nCelesta'),
(761, 1620147593, NULL, 'mauricery11', 'andrekn60@eiji8210.kenshin90.webmailaccount.site', 'http://milf.porntars.instakink.com', '81858558166', 'Best Nude Playmates & Centerfolds, Beautiful galleries daily updates', 'Hardcore Galleries with hot Hardcore photos\r\nhttp://sahrapalinporn.kanakox.com/?emily \r\n\r\n maturbates with water hose porn fat teen porn video x renae cruz porn sample clip college male porn suid afrika porn \r\n\r\n'),
(762, 1620251745, NULL, 'Bernie', 'bernie.mcdaniels@yahoo.com', 'Hi,\r\n\r\nWe\'re wondering if you\'ve ever considered taking the content from salsebandung.com and converting it into videos to promote on social media platforms such as Youtube?\r\n\r\nIt\'s another \'rod in the pond\' in terms of traffic generation, as so many people use Youtube.\r\n\r\nYou can read a bit more about the software here: https://www.vidnami.com/c/James54-vn-freetrial\r\n\r\nKind Regards,\r\nBernie', '(02) 4047 9725', 'How To Get More Traffic to salsebandung.com in 2021', 'Hi,\r\n\r\nWe\'re wondering if you\'ve considered taking the written content from salsebandung.com and converting it into videos to promote on Youtube? It\'s another method of generating traffic.\r\n\r\nThere\'s a 14 day free trial available to you at the following link: https://www.vidnami.com/c/KiyoshiM-vn-freetrial\r\n\r\nRegards,\r\nBernie'),
(763, 1620653536, NULL, 'JeromePhala', 'xdgh54hdf@rambler.ua', '', '85928147537', 'Dating club 5001', 'List of finnish dating sites Dating site free london. <a href=http://o5.datacoreforu.com/en/11830-the-most-effective-dating-site-for-men-23888.html>http://o5.datacoreforu.com/en/11830-the-most-effective-dating-site-for-men-23888.html</a> Most popular dating chat apps Nice guys guide to dating. <a href=http://9j.datacoreforu.com/en/24058-asian-girl-dating-black-men-42625.html>http://9j.datacoreforu.com/en/24058-asian-girl-dating-black-men-42625.html</a> Cousins only dating site Sim dating games for xbox 360. <a href=http://o9.datacoreforu.com/en/11238-gay-sugar-daddy-dating-apps-review-39934.html>http://o9.datacoreforu.com/en/11238-gay-sugar-daddy-dating-apps-review-39934.html</a> Anime dating game app Best 100 free dating sites 2011. <a href=http://3s.datacoreforu.com/en/76989-whats-it-like-dating-an-older-woman-57190.html>http://3s.datacoreforu.com/en/76989-whats-it-like-dating-an-older-woman-57190.html</a> Free rich sugar daddy dating site Effy and freddie dating in real life. http://1l2.datacoreforu.com/en/30474-paying-for-dating-site-with-paypal-28971.html Sugar momma dating site south africa Free online hacked rpg dating games. <a href=http://9m.datacoreforu.com/en/45151-what-is-the-best-muslim-dating-site-52560.html>http://9m.datacoreforu.com/en/45151-what-is-the-best-muslim-dating-site-52560.html</a> \r\nSocial connect dating app Bodybuilding singles dating site uk. <a href=http://5g17.datacoreforu.com/en/24959-sims-3-online-dating-base-gam-77301.html>http://5g17.datacoreforu.com/en/24959-sims-3-online-dating-base-gam-77301.html</a> Free dating sites plus size New anime dating games. <a href=http://kn1n.datacoreforu.com/en/27323-the-best-dating-app-in-san-diego-18794.html>http://kn1n.datacoreforu.com/en/27323-the-best-dating-app-in-san-diego-18794.html</a>  Networking sites for dating Rules for dating my daughter from mom. <a href=http://5fe.datacoreforu.com/en/77994-dating-a-guy-whose-mom-is-crazy-17056.html>http://5fe.datacoreforu.com/en/77994-dating-a-guy-whose-mom-is-crazy-17056.html</a> Best free video dating sites Free indian dating sites without payment. http://5m0l.datacoreforu.com/en/41562-girls-on-vinny-and-pauly-dating-show-2337.html Dating guys lund'),
(764, 1620678919, NULL, 'Rogerdup', 'slabodzyan4493@list.ru', 'https://lindera.ru/', '84164927352', 'Test, just a test', '<a href=https://lindera.ru/>Здесь</a>'),
(765, 1620781914, NULL, 'sallyga4', 'deidrewb11@akio6910.takayuki33.sorataki.in.net', 'http://hotbike.miaxxx.com', '88699552742', 'Free Porn Galleries - Hot Sex Pictures', 'Big Ass Photos - Free Huge Butt Porn, Big Booty Pics\r\nhttp://pornprois.fetlifeblog.com/?ericka \r\n\r\n daddy fucks me pofn feud boc porn best porn ever home made movies my girlfriend porn video japenese full video porn \r\n\r\n'),
(766, 1621716065, NULL, 'Arthurslore', 'maximmad222@yandex.ru', 'https://vk.com/public195417980', '88553117311', 'Купить XBOX игры  разнообразные игры:  АССАСИН КРИД ', 'Игры +на xbox +one купить дешево ключи	-Thief-игра-не  воруй чтобы выживать а живи чтобы воровать  + https://plati.market/itm/just-cause-2-pc-xbox-key-podarok/2962133 \r\nигры +на xbox +one купить дешево+ https://plati.market/itm/3048052 \r\nигры +на xbox +one купить дешево+ https://plati.market/itm/the-crew-2-br-xbox-pc-podarok/2961721 \r\n<a href=https://plati.market/itm/3046110> ? Grand Theft Auto V / GTA 5 XBOX ONE Цифровой Ключ ??</a> \r\n<a href=https://plati.market/itm/3048213>Watch Dogs 2  - Xbox Live Key - +подарок </a> \r\n<a href=https://plati.market/itm/3037039>The Crew 2, F1 XBOX/PC+present </a> \r\n<a href=https://plati.market/itm/2970072>Diablo III: Eternal Collection Xbox One</a> \r\n \r\n<a href=https://plati.market/itm/2914909>FAR CRY  </a> \r\n<a href=https://plati.market/itm/3010680>Wolfenstein II: The New Colossus  </a> \r\n<a href=https://plati.market/itm/assassin-s-creed-odisseja-br-xbox-kljuch-podarok/2966161> Bioshock - The Collection US XBOX LIVE </a> \r\n<a href=https://plati.market/itm/3085162>Thief XBOX ONE Цифровой ключ</a> \r\n<a href=https://plati.market/itm/3016084>The Division 2 </a> \r\n<a href=https://plati.market/itm/3036392>DARK SOULS™ III Xbox </a> \r\n<a href=https://plati.market/itm/3085419>MORTAL KOMBAT 11 XBOX ONE X/S</a> \r\n<a href=https://plati.market/itm/3000915>STAR WARS Jedi: Fallen Order Key</a> \r\n<a href=https://plati.market/itm/3113988>GOOGLE PLAY US 15$</a> \r\n<a href=https://plati.market/itm/3116494> iTUNES GIFT CARD - $10 (USA) ?? +подарок </a> \r\n<a href=https://plati.market/itm/3114225>The Witcher 3: Wild Hunt Game of the Year XBOX Key</a> \r\n<a href=https://plati.market/itm/3025661>Call of Duty: Advanced Warfare Gold Edit XBOX</a> \r\n<a href=https://plati.market/itm/3116767>  Avast! internet security/ Premium Security   </a> \r\n<a href=https://plati.market/itm/3102867>Red Dead Redemption 2 XBOX ONE Key+present</a> \r\n<a href=https://plati.market/itm/2974252>Crysis Remastered XBOX ONE key =370 р </a> \r\n<a href=https://plati.market/itm/2892265>Vampyr XBOX ONE ключ+подарок</a>'),
(767, 1622068082, NULL, 'Herra', 'herra_saefullah@yahoo.com.au', '', '+31683034776', 'Booking/Info detail Villacamp', 'Berapa banyak kamar di Villacamp yang dapat disewa? apakah tersedia untuk 10 orang dewasa/anak yang terbagi menjadi 5 kamar? fasilitasnya dapat apa saja, harga Rp. 325.000 apakah sudah termasuk breakfast? '),
(768, 1622068141, NULL, 'Herra', 'herra_saefullah@yahoo.com.au', 'sosiologi 14, komp. perum UNPAD Cigadung Bandung 40191', '+6281573737138 (wa only)', 'Booking/Info detail Villacamp', 'Berapa banyak kamar di Villacamp yang dapat disewa? apakah tersedia untuk 10 orang dewasa/anak yang terbagi menjadi 5 kamar? fasilitasnya dapat apa saja, harga Rp. 325.000 apakah sudah termasuk breakfast? '),
(769, 1622068154, NULL, 'Herra', 'herra_saefullah@yahoo.com.au', 'sosiologi 14, komp. perum UNPAD Cigadung Bandung 40191', '+6281573737138 ', 'Booking/Info detail Villacamp', 'Berapa banyak kamar di Villacamp yang dapat disewa? apakah tersedia untuk 10 orang dewasa/anak yang terbagi menjadi 5 kamar? fasilitasnya dapat apa saja, harga Rp. 325.000 apakah sudah termasuk breakfast? '),
(770, 1623527094, NULL, 'Rogerdup', 'marina4q89fed@mail.ru', 'https://se023.ru/', '83863669193', 'СЕО продвижение', '<a href=https://se023.ru/>СЃРєРѕР»СЊРєРѕ СЃС‚РѕРёС‚ РїСЂРѕРґРІРёР¶РµРЅРёРµ СЃР°Р№С‚Р°</a>\r\n<a href=https://se023.ru/>СЃС‚РѕРёРјРѕСЃС‚СЊ СЃРѕР·РґР°РЅРёСЏ СЃР°Р№С‚Р°</a>\r\n<a href=https://alaev.co/>СЂР°Р·СЂР°Р±РѕС‚РєР° Рё СЃРѕР·РґР°РЅРёРµ СЃР°Р№С‚РѕРІ РІ РєСЂР°СЃРЅРѕРґР°СЂРµ</a>\r\n<a href=https://se023.ru/>РїРѕРёСЃРєРѕРІР°СЏ СЂР°СЃРєСЂСѓС‚РєР° СЃР°Р№С‚Р°</a>\r\n<a href=https://se023.ru/services/podderzhka_saytov/napolnenie_sayta/>РЅР°РїРѕР»РЅРµРЅРёРµ СЃР°Р№С‚Р°</a>\r\n'),
(771, 1623867464, NULL, 'HeatherFefly', 'hethertan@gmail.com', 'https://india-express.net/shop', '85768288833', ' Мы нашли все таки где купить этот препарат без огромных затрат ', 'Обратились в аптеку по рекомендации врача и не прогадали, препарат прекрасно действует! \r\n \r\n \r\n<a href=https://india-express.net/shop/118/desc/palbociclib>ибранса инструкция +по применению</a>'),
(772, 1624606410, NULL, 'morriesfrof', 'fwwfkwogkwwk@yandex.ua', 'https://www.beatport.com/label/mojoheadz-records/56099', '88347665724', 'Mojoheadz records is a Everything is OK!', 'MOJOHEADZ Records, is a house music brand that is set to make its mark on the world of dance music on its own terms.Check it review!! https://musicfromla.blogspot.com/2019/09/mojoheadz-records-is-defining-techno.html'),
(773, 1627966095, NULL, 'Jarred', 'jmiller22@hotmail.com', 'I\'m pretty sure you misspelled the word \"spesial\" on your site.  You might want to check out a site like SpellRadar.com or SiteCheck.com which have helped me with problems like this in the past.', '914-282-1841', 'Website Issues', 'I\'m pretty sure you misspelled the word \"spesial\" on your site.  You might want to check out a site like SpellRadar.com or SiteCheck.com which have helped me with problems like this in the past.'),
(774, 1628040111, NULL, 'Olivia', 'olivia@backlinkpro.club', 'Hi, \r\n\r\nWe\'re wondering if you\'d be interested in a \'dofollow\' backlink to salsebandung.com from our website that has a Moz Domain Authority of 50?\r\n\r\nOur website is dedicated to facts/education, and so can host articles on pretty much any topic.\r\n\r\nYou can either write a new article yourself, or we can link from existing content.\r\n\r\nThe price is just $30 to be paid via Paypal. This is a one-time fee, so there are no extra charges.\r\n\r\nAlso: Once the article has been published, and your backlink has been added, it will be shared out to almost 3 million social media followers (if it\'s educationally based). This means you aren\'t just getting the high valued backlink, you\'re also getting the potential of more traffic to your site.\r\n\r\nIf you\'re interested, please reply to this email, including the word \'interested\' in the Subject Field.\r\n\r\nKind Regards,\r\nOlivia', '052 876 68 61', 'DA50 Backlink to salsebandung.com', 'Hi, \r\n\r\nWe\'re wondering if you\'d be interested in a \'dofollow\' backlink to salsebandung.com from our website that has a Moz Domain Authority of 50?\r\n\r\nOur website is dedicated to facts/education, and so can host articles on pretty much any topic.\r\n\r\nYou can either write a new article yourself, or we can link from existing content.\r\n\r\nThe fee is just $40 to be paid via Paypal. This is a one-time fee, so there are no extra charges and the link is permanent.\r\n\r\nIf you\'re interested, please reply to this email, including the word \'interested\' in the Subject Field.\r\n\r\nKind Regards,\r\nOlivia'),
(775, 1628191706, NULL, 'Olivia', 'olivia@backlinkpro.club', 'Hi, \r\n\r\nWe\'re wondering if you\'d be interested in a \'dofollow\' backlink to salsebandung.com from our website that has a Moz Domain Authority of 50?\r\n\r\nOur website is dedicated to facts/education, and so can host articles on pretty much any topic.\r\n\r\nYou can either write a new article yourself, or we can link from existing content.\r\n\r\nThe price is just $30 to be paid via Paypal. This is a one-time fee, so there are no extra charges.\r\n\r\nAlso: Once the article has been published, and your backlink has been added, it will be shared out to almost 3 million social media followers (if it\'s educationally based). This means you aren\'t just getting the high valued backlink, you\'re also getting the potential of more traffic to your site.\r\n\r\nIf you\'re interested, please reply to this email, including the word \'interested\' in the Subject Field.\r\n\r\nKind Regards,\r\nOlivia', '0391 8263799', 'DA50 Backlink for your website', 'Hi, \r\n\r\nWe\'re wondering if you\'d be interested in a \'dofollow\' backlink to salsebandung.com from our Moz DA50 website?\r\n\r\nOur website is dedicated to facts/education, and so can host articles on pretty much any topic.\r\n\r\nYou can either write a new article yourself, or we can link from existing content.\r\n\r\nThe fee is just $40 to be paid via Paypal. This is a one-time fee, so there are no extra charges and the link is permanent.\r\n\r\nAlso: Once the article has been published, and your backlink has been added, it will be shared out to almost 3 million social media followers (if it\'s educationally based). This means you aren\'t just getting the high valued backlink, you\'re also getting the potential of more traffic to your site.\r\n\r\nIf you\'re interested, please reply to this email, including the word \'interested\' in the Subject Field.\r\n\r\nNot sure what DA is? Have a read here: https://moz.com/learn/seo/domain-authority\r\n\r\nKind Regards,\r\nOlivia'),
(776, 1628657162, NULL, 'Only on our dating site you will find a couple  https://gee.su/NXAdg', 'merri.kinsella@gmail.com', 'Only on our dating site you will find a couple  https://bit.ly/37wzh7t', 'The most beautiful ones are on our dating site  https://bit.ly/37wzh7t', 'On our dating site you will 100% find the one you are looking for', 'The most beautiful ones are on our dating site  https://gee.su/NXAdg'),
(777, 1628727447, NULL, 'Thomasmot', 'ninnaferris@gmail.com', 'http://steroman.biz/', '81997377182', 'define roid rage\r\n d bol legal\r\n', '<a href=\"http://steroman.biz/buy-duradexx-250-thaiger-pharma-10ml/\r\n \">steroids mood swings\r\n</a> \r\nclenbuterol for women\'s weight loss\r\nstrength and steroids\r\nmedichem labs winstrol\r\nequipoise meaning\r\ntrt steroid\r\nfluticasone propionate side effects\r\nbudesonide weight gain\r\n'),
(778, 1628727896, NULL, 'SteveLyday', 'lezopayrt@gmail.com', 'https://antikor.com.ua/', '81791627296', 'Мои любимые статьи, которые рекомендую почитать', 'Почему главный мошенник «Телетрейда» Сергей Сароян до сих пор не за решеткой? \r\n \r\nПочему главный мошенник «Телетрейда» Сергей Сароян до сих пор не за решеткой? \r\nЧетверть века обманывать народ – это надо уметь. В этом деле настоящий эксперт – мошенник Сергей Сароян, главарь грандиозного схематоза под названием «Телетрейд» в Украине. Почему же этот «крупнокалиберный» аферист до сих пор гуляет на свободе, а не сидит в тюрьме, как положено? И как о нем отзываются бывшие сотрудники и клиенты: радостного мало… \r\n \r\nНа данный момент главарем форексной аферы под неймингом «Телетрейд» на территории Украины является мошенник Сергей Сароян. Он же директор всех местных подразделений компании. Ранее «Телетрейд» возглавлял и «руководил» всеми аферами Владимир Чернобай, который скрываясь от правоохранительных органов, скончался в Европе. Его «дело» наследовали вдова Анна Чернобай и племяш Олег Суворов, а также остальные пособники. \r\n \r\nСергея Сарояна называют хитрым, одиозным и амбициозным. И это неспроста. Двуличный руководитель «с пеной у рта» раздает обещания, как клиентам, так и сотрудникам. Кстати, об отзывах, как первых, так и вторых, мы расскажем чуть позже в нашем материале, поэтому рекомендуем дочитать до конца. \r\n \r\nЧто известно о Сергее Сарояне: коротко \r\n \r\nВ 2001 году он закончил Одесскую национальную академию связи им.Попова. На старте своей карьере работал в телекоммуникациях. Женат. \r\n \r\nЧитайте также: НБУ предупредил о мошенническом сайте под видом BankID \r\n \r\nА на своем фейсбуке, как видите, не скрывает, что он «в теме» и работает в «Телетрейд»… Интересно, что на своей странице в Фейсбуке, а именно в разделе «Информация», он разместил такой «скромный» философский текст «обо всем и ни о чем»: \r\n \r\n \r\nНу, смешно, ей богу… Хотя, с другой стороны, грустно… Ведь люди читают этот бред, а многие ведутся на «профи»… Только вот потом жалуются… правда без толку, как показывает опыт многих пострадавших. \r\n \r\nНа протяжении с 2005-го по 2006-й года Сароян курировал региональные направления «Телетрейда» по стране. Под его «крылом» было свыше 30 офисов. Здесь и столица, и мама-Одесса, Харьков, город-Лев, Черновцы и т.д… Стоит отметить, что «повезло» с руководителем «Телетрейда» не только украинским городам. Так же под раздачу Сарояна попали европейские и азиатские офисы: Италия, Португалия, Польша, и т.д. Как понимаете, бабло «лилось рекой» в карманы остервенелым аферистам. \r\n \r\nКстати, «трудился» товарищ Сароян не за идею. Ему доставалось 3 процента от той самой «идеи»… Ну, вы поняли. Если озвучить простым языком, нам так не жить. Дорого. Богато. Красиво… Для понимания, Сароян получал на выходе свыше 30 тысяч баксов ежемесячно. И это не сейчас! А тогда… давным-давно, как говорится…. Без комментариев. Все подробности – здесь. \r\n \r\nКстати, вот интересное видео, где Сароян озвучивает, что он глава этой компании аферистов «Телетрейд» в Украине … \r\n \r\n \r\n \r\nА вот и еще одно занимательное видео о том, как «Телетрейд» общается с реальными клиентами… \r\n \r\n \r\n \r\nТелетрейд: что известно грандиозном схематозе \r\n \r\nПечально известная на многих континентах мира форексная компания был зарегистрирована в оффшорах на Карибах. Еще три года назад в 2018-м бренд «Телетрейд» был лишен лицензии, а также ему запретили работать на территории Российской Федерации и в Беларуси. Уже в 2019-м МВД России в отношении компании были возбуждены уголовные дела по части 4 статьи 159 УК РФ. Речь идет о  мошенничестве в особо крупных размерах. А вот, что касается руководства сей распрекрасной компании, то Владимир Чернобай с племянником Олегом Суворовым смотались за бугор, а также были объявлены в розыск. Кстати, в следующем 2020-м, уголовные дела на эту компанию завели и в Казахстане. Отметим, что там товарищи-руководители уже за решеткой, в отличии от наших… \r\n \r\nПодробности «Синхронной торговли» от Телетрейда \r\n \r\nВ сентябре 2020-го в пресс-центре «Интерфакса» прошла пресс-конференция по поводу того, как «давят» масс-медиа в связи со скандальными событиями вокруг трейдерской банды «Телетрейд». Бойня, в прямом смысле этого слова, уже давно продолжается между пострадавшими клиентами и мошенниками. \r\n \r\n \r\n \r\nНа «Телетрейд» уже заведено уйма криминальных дел и не в одной стране. Так, уголовки по мошенничеству в особо крупных размерах открыты в Российской Федерации (ч.4 ст.159 УК РФ). В Казахстане уголовные дела на компанию открыты по статье 190 УК РК. \r\n \r\nВо время пресс-конференции спикеры заявили о том, что на масс-медиа оказывают давление, запугивая. Делают это «защитники», остаивающие интересы «Телетрейда» с целью «закрыть рты» свободным СМИ. \r\n \r\nО мошеннических схематозах проекта «Синхронная торговля» рассказал подробно бизнес-эксперт, глава РК «Амиллидиус» Богдан Терзи. \r\n \r\nОн озвучил, что если раньше за аферы компании «Телетрейд» отвечали реальные люди-трейлеры с подачи руководства, то сейчас этот функционал выполняют роботизированные системы, которые контролируют программисты компании. Ясное дело, что все эти бото-роботы оформлены как «люди» с виртуальным баблом на счетах. Только вот проблема в том, что народ, который подключается к таким «товарищам», вполне реален и не подозревает о «зраде». По итогу люди теряют свои деньги, которые «уплывают» в карманы мошенников. Кстати, у этого «проекта» есть много названий, так как зараза распространилась по всему земному шару: «Sync trading», «Copy trading», «Teletrade invest», ну а в Украине - «Синхронная торговля». \r\n \r\nНевероятно, но факт: по распоряжению Сергея Сарояна бандиты похитили сотрудника «Телетрейд» и требовали 5 тысяч баксов, дабы «не похоронить в болоте» \r\n \r\n \r\nВроде бы мы все живем в реалиях 21 века, в свободном современном обществе, но оказывается, не все так просто. Резонансный случай произошел с одним из сотрудников «Телетрейда». И это только один пример, который придали огласке. А сколько их на самом деле? Вопрос риторический, а теперь подробнее о скандале, который просто «взорвал» информационное пространство. \r\n \r\nИтак, о «похищении с мешком на голове» рассказал сам пострадавший, служащий украинского подразделения Центра биржевых технологий «Телетрейда» Олег Фурдуй, уже бывший… \r\n \r\n \r\n \r\nУправляющего офисом в Одессе и кризис-менеджера киевского офиса «Телетрейда» Олега Фурдуя выкрали, надели мешок на голову, вывезли в дебри и требовали 5 тысяч баксов, угрожая «похоронить в болоте». Мотивировали тем, что он «давал подсказки» клиентам по выводу бабла. \r\n \r\nКак видим, за помощь клиентам по актуальным вопросам, связанным с выводом своих средств, глава «Телетрейда» Сергей Сароян при помощи своей банды похитил своего сотрудника и даже отобрал выкуп за него у жены Олега. \r\n \r\nИ, кстати, в Украине, «дочка» главной мошеннической структуры «Центра  Биржевых Технологий», а именно «Телетрейд» признана мошеннической, а сотрудниками СБУ и Генеральной прокуратуры Украины были проведены обыски, в ходе которых  изъята техника и ведется следствие. \r\n \r\nОтзывы сотрудников Сарояна и его «Телетрейд»: без комментариев \r\n \r\n \r\n \r\n \r\n \r\n \r\n \r\n \r\n \r\n \r\n \r\nОтзывы клиентов Сарояна и «Телетрейд»: здесь вообще аншлаг \r\n \r\nНе будем ничего комментировать, размещаем исключительно комментарии клиентов этой «шарашкиной конторы», позиционирующей себя как «международная компания»… \r\n \r\nВот видео-версии отзывов клиентов, которых «поимела» форекс-компания во главе с Сергеем Сарояном: \r\n \r\n- Наталье Лячиной, клиентке Центра Биржевых Технологий, отказала компания Телетрейд в выводе ее личных средств… \r\n \r\n \r\n \r\n- клиент Никита Лебединский попал в ситуацию, когда ему под разными предлогами отказывали в выведении средств… \r\n \r\n \r\n \r\n- еще одна жертва обстоятельств – Ольга Король… \r\n \r\n \r\n \r\n- компания «Телетрейд» также против того, чтобы вернуть деньги, принадлежавшие клиентке Светлане Клубковой… \r\n \r\n \r\n \r\n- еще одна жертва мошенников – Ольга Круглякова… \r\n \r\n \r\n \r\n- а вот Абдулатиф Хашим Альбаргави вообще вложил в «Телетрейд» свыше 500 000 миллиона баксов, а уже через полгода потерял порядка 50 000. При попытке вывести оставшиеся средства, а именно 465 000 долларов, ему доступ к счету заблокировали. Далее деньги со счета вообще пропали. Форексная компания «Телетрейд» украла у своего клиента полмиллиона баксов за ночь. Добиться справедливости вип-клиент не смог. \r\n \r\n \r\n \r\nТакже есть и другие отзывы клиентов «Телетрейда» на разных сайтах: \r\n \r\n \r\n \r\n \r\n \r\n \r\n \r\n \r\nЧто здесь сказать… Крайне непонятно, почему аферист и мошенник Сергей Сароян до сих пор на свободе, если против него собрано столько  компрометирующих материалов? Уже всему миру понятно, что эта «шарашкина контора», то есть «Телетрейд» - это мошенническая организация, которая сливает счета клиентов на собственные оффоры преступным путем. Всем понятно, а нам нет? Что не так с нашей правоохранительной системой? Или этот вопрос можно считать риторическим? \r\nБольше информации на портале Антикор: https://antikor.com.ua/articles/440986-pochemu_glavnyj_moshennik_teletrejda_sergej_sarojan_do_sih_por_ne_za_reshetkoj'),
(779, 1629114891, NULL, 'DennisSnorb', 'arsentiy.kalashnikov@bk.ru', 'https://steelcentury.ru/lestnichnye-ograzhdeniya/', '85253174322', '\r\nшкафы для кондиционеров стоимость', 'hi ,. \r\nнашла с подругой  сраничку нам нужно купить с цены на порошковую покраску нашими задумками по ттеме \r\nподскажите C советом и как мне <a href=https://steelcentury.ru/metalloizdeliya/ogradki/>РёР·РіРѕС‚РѕРІР»РµРЅРёРµ РјРµС‚Р°Р»Р»РѕРєРѕРЅСЃС‚СЂСѓРєС†РёР№</a>\r\n. \r\nнузно знать как контролировать и быть в предмете,.. \r\n<a href=https://steelcentury.ru/individualnye-proekty/>РїРѕСЂРѕС€РєРѕРІР°СЏ РїРѕРєСЂР°СЃРєР° РјРѕСЃРєРІР°</a>\r\nпрошу C данной теме буду Очень благодарствуем .. \r\nСпасибо за помощ ;=) ,.. нашли в instagremm @steelcentury             присылайтье мнение Спасибовзаимно буду рады'),
(780, 1629690638, NULL, 'krisjk1', 'marcicf16@haru5310.takayuki80.mokomichi.xyz', 'http://veryamateurporntopaz.topanasex.com', '84239634611', 'Hot teen pics', 'Browse over 500 000 of the best porn galleries, daily updated collections\r\nhttp://yot.pornbrush.danexxx.com/?yesenia \r\n lego porn games alphaq porn stepmom free anime porn web sites current free gay porn tube sites freaks of porn trailers \r\n\r\n\r\n'),
(781, 1629797199, NULL, 'Dunom', 'bakeero@hotmail.com', 'http://eclearusa.com/images/file/cgibin/ponstan.xml', '87219342963', 'Take Best Price Medications Online Without Recipe!', 'Purchase Cheapest Tablettes Now No Recipe! \r\n \r\nTake http://www.duda-tech.pl/dudatech/upload/file/cgibin/ciplox-tz.xml - ciplox tz\r\n \r\nChoose https://www.bussfuses.net/cms_admin/fckeditor/editor/filemanager/connectors/php/bin/cgibin/hair-regrowth.xml - hair regrowth\r\n \r\nPurchase http://eban.internetdsl.pl/userfiles/file/cgibin/hydiphen.xml - hydiphen\r\n \r\nBuy http://eclearusa.com/images/file/cgibin/clozaril.xml - clozaril\r\n \r\nSelect http://www.elektrownia.art.pl/userfiles/file/cgibin/kamini-oral-jelly.xml - kamini oral jelly\r\n \r\nTake http://www.etest.lt/userfiles/file/cgibin/truvada.xml - truvada\r\n \r\nPurchase https://ethio3f.com/ehpea/userfiles/file/cgibin/cyclosporine.xml - cyclosporine\r\n \r\nGet http://www.faurerom.com/userfiles/file/cgibin/florinef-floricot.xml - florinef floricot\r\n \r\nPurchase https://www.floridainvestment.cz/files/file/cgibin/viagra-capsules.xml - viagra capsules\r\n \r\nTake https://www.galika.com/userfiles/file/cgibin/anestacon.xml - anestacon\r\n \r\n \r\nAnd Get + 4 Free tabs for any order!'),
(782, 1630049876, NULL, 'Wiwi', 'dwiafria@rocketmail.com', 'Tangerang', '085710242440', 'Type kamar', 'Selamat siang mau tanya perbedaan type eksekutif dengan premium, sebelumnya tahun 2016 saya pernah menginap di type eksekutif, perbedaannya dengan premium apa ya?waktu itu saya mendapat kamar di atas. Apa masih ada warung salsenya juga ?terimakasih'),
(783, 1630500345, NULL, 'iphonevare.', 'maximmad222@yandex.ru', 'https://mysel.ru', '81434861951', ' Приобретение игр XBOX   разнообразные игры:  Game XBOX ', 'Купить игры +на xbox +one и получить накопительные скидки  + https://plati.market/itm/the-crew-2-us-xbox-live-podarok/2961721 \r\nключи игр xbox +one купить+ https://plati.market/itm/3016084 \r\nкакую игру xbox купить+ https://plati.market/itm/fallout-4-game-of-the-year-edition-xbox-one-kljuch/3014015 \r\nкупить игры +на xbox +one+  https://wmcentre.net/item/call-of-duty-ghosts-xbox-3085414  \r\n<b>игры XBOX XD KEY</b>  <b>MYSEL>RU</b> https://mysel.ru \r\n<a href=https://plati.market/itm/fifa-20-uk-xbox-one-kljuch-cifr-kod--podarok/2961184>  FIFA 20  XBOX ONE Ключ  / Цифр код + подарок </a> \r\n<a href=https://plati.market/itm/grand-theft-auto-v-gta-5-xbox-one-cifrovoj-kljuch/3008735> ? Grand Theft Auto V / GTA 5 XBOX ONE Цифровой Ключ ??</a> \r\n<a href=https://plati.market/itm/3051308> Mafia Definitive Edition XBOX CD KEY</a> \r\n<a href=https://plati.market/itm/2892901>Watch Dogs 2  - Xbox Live Key - +подарок </a> \r\n<a href=https://plati.market/itm/3051324>HITMAN 2</a> \r\n<a href=https://plati.market/itm/2889815>BATTLEFIELD V? +подарок</a> \r\n<a href=https://plati.market/itm/metro-last-light-redux-xbox-one-present/2915157>The Crew 2 ?? XBOX/PC+present </a> \r\n<a href=https://plati.market/itm/2970072>Diablo III: Eternal Collection Xbox One</a> \r\n<a href=https://my.digiseller.com/inside/description.asp?id_d=2920504>Гоночная игра с рулевым симулятором</a> \r\n<a href=https://plati.market/itm/2911572>?Assassin?s Creed Одиссея  XBOX ключ +подарок ??</a> \r\n<a href=https://plati.market/itm/3046307><b>GOOGLE PLAY GIFT CARD $15 (USA)</b> </a> \r\n<a href=https://plati.market/itm/2985407><b>Steam Turkey 50 TL Gift Card Code(FOR TURKEY ACCOUNTS)</b></a> \r\n<a href=https://plati.market/itm/2911572><b> Netflix Турция Подарочный код 100 TL</b></a> \r\n<a href=https://plati.market/itm/3076493> Resident Evil </a> \r\n<a href=https://plati.market/itm/3105472>The Division 2 -</a> \r\n<a href=https://plati.market/itm/3036392>   Call of Duty: Infinite Warfare - XBOX</a> \r\n<a href=https://plati.market/itm/3105586>Хочешь купить Машину, срочно нужны деньги, не хватает на покупку обращайся в HOME BANK </a> \r\n<a href=https://plati.market/itm/3085419>MORTAL KOMBAT 11 XBOX ONE X/S</a> \r\n<a href=https://plati.market/itm/3204091><b>King?s Bounty II - Lord?s Edition XBOX ONE</b></a> \r\n<a href=https://plati.market/itm/3116767><b>Avast Cleanup Premium  1пк /1 key 1годх</b></a> \r\n<a href=https://plati.market/itm/3012399>  <b>Amazon Gift Card US $10</b> </a> \r\n<a href=https://wmcentre.net/item/avg-tuneup-1-pk-1-god-3203401>  <b>AVG TuneUp 1 ПК 1 год -</b> </a> \r\n<a href=https://plati.market/itm/3116494> <b>iTUNES GIFT CARD - $15(USA) </b></a> \r\n<a href=https://wmcentre.net/item/cyberpunk-2077-xbox-one-xbox-series-x-s-3203346>Cyberpunk 2077 XBOX ONE SERIES</a> \r\n<a href=https://plati.market/itm/3016084>The Evil Within XBOX ONE ключ  </a> \r\n<a href=https://wmcentre.net/item/assassin-s-creed-valhalla-xbox-3203374>Wolfenstein: I, II xbox</a> \r\n<b>Покупай на ENOT-способ оплаты(0% при оплате банковской картой)6 способов оплаты</b>'),
(784, 1631526648, NULL, 'Alnom', 'alkeero@hotmail.com', 'http://www.mainraum-gruenderhaus.de/data/files/sbin/eremfat.xml', '89493652745', 'Choose Cheap Tablettes Online Without Prescription!', 'Order Cheap Pharmacy Now Without Recipe! \r\n \r\nGet http://www.goldgreiner.de/userfiles/file/sbin/cefaclorum.xml - cefaclorum\r\n \r\nSelect http://hifitness.hu/userfiles/file/sbin/penbritin.xml - penbritin\r\n \r\nSelect http://www.hospvetcentral.pt/site/upload/file/sbin/rifampin.xml - rifampin\r\n \r\nPick https://www.hotpod.net.au/userfiles/file/sbin/phenergan.xml - phenergan\r\n \r\nTake http://www.hurtglass.pl/upload/file/sbin/nasonex.xml - nasonex\r\n \r\nTake http://isi.irkutsk.ru/userfiles/file/sbin/ciplin.xml - ciplin\r\n \r\nGet http://karmatara.org.np/userfiles/file/sbin/xenical.xml - xenical\r\n \r\nChoose http://www.kurashi-kyoiku.com/cms/dat/upimg/file/sbin/lethyrox.xml - lethyrox\r\n \r\nPick http://www.laetitiabernard.fr/images/file/sbin/sporanox.xml - sporanox\r\n \r\nPurchase http://www.latgalesamatnieki.lv/files/file/sbin/barbers-itch.xml - barbers itch\r\n \r\n \r\nAnd Big Bonus: + 4 Free pills for any order!'),
(785, 1631641354, NULL, 'StevenHeesk', 'tammerabes5jk@mail.com', 'https://cdn.thingiverse.com/assets/37/6c/89/aa/9a/Young-girls-5-prettygirlrpmBtHQJYfBOuU-iMGSRCRU.pdf', '81965948253', 'Fole unfomb oscink inasia', 'https://cdn.thingiverse.com/assets/a3/c9/55/f4/e5/In-the-country-DSC06324-iMGSRCRU.html\r\nhttps://cdn.thingiverse.com/assets/df/33/e9/98/d1/the_jungle_book_play_script_free.pdf\r\nhttps://cdn.thingiverse.com/assets/a0/e4/f0/9d/72/bmwe90adaptiveheadlightfuse.pdf\r\nhttps://cdn.thingiverse.com/assets/2b/7d/83/4c/09/yamazerac.pdf\r\nhttps://cdn.thingiverse.com/assets/21/7a/4c/a7/66/whalvir.pdf\r\nhttps://kit.co/rosukenslea/moii-chan-google-drive\r\nhttps://trello.com/c/JB6y6GxQ/9-lovely020-30-imgsrcru\r\nhttps://cdn.thingiverse.com/assets/c7/dd/e4/31/74/Gardenscapes_Mansion_Makeover_CE_Full_PreCracked_Foxy_Game.pdf\r\nhttps://cdn.thingiverse.com/assets/3f/ca/4a/94/51/Sandra-23yo-Horny-German-Teen-Sandra-402J-iMGSRCRU.pdf\r\nhttps://kit.co/kowithdpertinf/birthday-spanked-at-age-11-imgsrc-ru\r\nhttps://kit.co/matabdete/zero-g-vr-ativador-download-ativador\r\nhttps://cdn.thingiverse.com/assets/99/89/53/e3/c4/Bochum-vs-Heidenheim-Streaming-gratuito-online.pdf\r\nhttps://mareekhqpoyer.wixsite.com/innenapon/post/the-sims-4-windows-ts4-exe\r\nhttps://cdn.thingiverse.com/assets/6c/2c/22/3d/74/georggovy.html\r\nhttps://cdn.thingiverse.com/assets/88/42/a3/4e/e9/Badr-Hari-vs-Benjamin-Adegbuyi-Live-Stream--FBStreams-Link-4.pdf\r\nhttps://cdn.thingiverse.com/assets/71/0e/1c/18/50/Boys-in-movie--Woensdagen-2011--Tyn-Hageman-Woensdagen-133-iMGSRCRU.html\r\nhttps://cdn.thingiverse.com/assets/72/2d/ac/b6/7a/kayovouchercodetelstra.html\r\nhttps://kit.co/hotecogi/file-upload-net-144444-zip\r\nhttps://kit.co/retedrasub/do-your-data-recovery-professional-v7-8-patched-macos\r\nhttps://trello.com/c/LtTmchf2/3-everton-fc-vs-chelsea-fc-live-stream-fbstreams-link-4\r\nhttps://cdn.thingiverse.com/assets/28/8c/43/ee/ca/wilpylla.pdf\r\nhttps://cdn.thingiverse.com/assets/e9/f4/29/76/44/Girl_friends_nylon_IMG20190712171547_iMGSRCRU.pdf\r\nhttps://kit.co/compsudmuter/pokemon-go-coordinates-live\r\nhttps://sergeynoskovuh.wixsite.com/unindote/post/fundamentals-of-turbomachinery-venkanna-pdf-free-download\r\nhttps://cdn.thingiverse.com/assets/ec/48/23/af/ae/Sleeping-boys-continues--23-iMGSRCRU.pdf\r\nhttps://cdn.thingiverse.com/assets/49/e5/9e/d4/0f/IT-Chapter-two--1337x-Torrents.pdf\r\nhttps://cdn.thingiverse.com/assets/85/ec/90/b4/df/indir-title-Yunan-Mitolojisi-2-Cilt-Bir-Arada.pdf\r\nhttps://trello.com/c/ixxAcWhh/92-gateway-le1936-monitor-driver\r\nhttps://kit.co/swarliciro/softmatic-barcode-factory-v4-incl-keygen\r\nhttps://trello.com/c/E3IGeseh/11-mcgraw20hill20connect20managerial20accounting20answers20chapter2012\r\nhttps://trello.com/c/NN7C59gE/8-pdf-merger-with-crack\r\nhttps://kit.co/nabivilsi/diskcatalogmaker-8-2-8-crack-mac-with-activation-key-latest-version\r\nhttps://trello.com/c/DD0t1xKk/9-is-the-nexus-mod-manager-login-server-down\r\nhttps://trello.com/c/951VrTld/90-should-i-become-a-gainer-quiz\r\nhttps://cdn.thingiverse.com/assets/1a/48/aa/c4/3b/Samples-of-my-favourites-9268-iMGSRCRU.pdf\r\nhttps://mooremelanie1985.wixsite.com/clasevweicros/post/petite-beautРі-2-120-imgsrc-ru\r\nhttps://cookmarieke.wixsite.com/discgekanlie/post/sue-pa090043-imgsrc-ru\r\nhttps://cdn.thingiverse.com/assets/78/2c/92/d9/bf/Manual-mainboard-msi-n1996.pdf\r\nhttps://kit.co/faitigompoero/i-64-imgsrc-ru\r\nhttps://kit.co/depmepeaga/a-christmas-story-free-movie\r\n \r\n9229450e8b \r\n<a href=https://bwnkeyasamajkalyansamity.webs.com/apps/guestbook/>Nona corm pege escatt</a>\r\n<a href=https://clubekarateloures.webs.com/apps/guestbook/>Beasty Pouppy tilk abum</a>\r\n<a href=https://kariarabians.webs.com/apps/guestbook/>Borm daum Cleaby dill</a>\r\n<a href=https://gamesexpert.webs.com/apps/guestbook/>dalt love Pres Scashy</a>\r\n<a href=https://radio-kollumerland.webs.com/apps/guestbook/>Pype Mulp Smogue Jano</a>\r\n<a href=https://bluebonnetmastiffs.com/apps/guestbook/>inPusa dund ornate tady</a>\r\n<a href=https://bigdmusic.webs.com/apps/guestbook/>Hoggem dops Sturdy cods</a>\r\n<a href=https://egyiptomiutazasok.hu/2018/11/30/pure-luxe-in-punta-mita/>lieddy velo Kani quor</a>\r\n<a href=https://tlrg-sa-mp.webs.com/apps/guestbook/>Skix actize guetly Advita</a>\r\n<a href=http://megadekoracje.pl/wabi-sabi-japonskie-nuty-we-wnetrzach/#comment-185054>Nady pero BimA ascent</a>\r\n 591_ef0 '),
(786, 1631949356, NULL, 'Randal987lciz', 'arielmathews9@gmail.com', 'https://telegra.ph/h1Braided-Bob-Hairstylesh1-06-04', '82294519771', 'Thanks very much', 'Thank you'),
(787, 1632361602, NULL, 'deborah widjaja', 'tipo_lala@yahoo.com', 'jl adhyaksa VI blok i no 3 karang tengah, Tanggerang', '081219412603', 'konfirmasi Booking room 24/09', 'Dear CS,\r\n\r\nMohon konfirmasinya, apakah bookingan saya atas nama deborah widjaja sudh berhasil untuk room executive double bed pada tgl 24 sept ini? krna saya tidak mendapatkan email bookingannya. saya melakukan booking online melalui Web. bagai mana cara saya mengecek bookingannya sudh berhasil atau belum? status pembayaran sudh berhasil menggunakan CC cimb. saya coba log in tetapi tidak berhasil. error selalu. mohon tanggapannya.\r\n\r\nThanks & regards,\r\n\r\nDeborah.w'),
(788, 1632361624, NULL, 'deborah widjaja', 'tipo_lala@yahoo.com', 'jl adhyaksa VI blok i no 3 karang tengah, Tanggerang', '081219412603', 'konfirmasi Booking room 24/09', 'Dear CS,\r\n\r\nMohon konfirmasinya, apakah bookingan saya atas nama deborah widjaja sudh berhasil untuk room executive double bed pada tgl 24 sept ini? krna saya tidak mendapatkan email bookingannya. saya melakukan booking online melalui Web. bagai mana cara saya mengecek bookingannya sudh berhasil atau belum? status pembayaran sudh berhasil menggunakan CC cimb. saya coba log in tetapi tidak berhasil. error selalu. mohon tanggapannya.\r\n\r\nThanks & regards,\r\n\r\nDeborah.w'),
(789, 1632676538, NULL, 'bradleyzu11', 'deannazk7@michio12.kiyoakari.xyz', 'http://lesbianslave.hotblognetwork.com', '83225771951', 'Hot teen pics', 'Dirty Porn Photos, daily updated galleries\r\nhttp://bestarmyporn.sexjanet.com/?macey \r\n\r\n porn comments and graphics girlie girl porn jenny finch porn jeff danes the porn star serbian porn videos \r\n\r\n'),
(790, 1633361695, NULL, 'iphonevare.', 'maximmad222@yandex.ru', 'https://mysel.ru', '85318643127', 'Купить XBOX игры  разнообразные игры: XBOX CD KEY', 'Игры +для xbox купить недорого и стань мером города управляя им Cities: Skylines - Xbox One  + https://plati.market/itm/diablo-iii-eternal-collection-ar-xbox-one/2970072 \r\nигры +на xbox +one купить дешево+ https://plati.market/itm/3162963  \r\nxbox +one игры купить +в россии+ https://plati.market/itm/fallout-4-game-of-the-year-edition-xbox-one-kljuch/3014015 \r\nключи игр xbox +one купить+ https://wmcentre.net/item/f1-2020-deluxe-schumacher-edition-xbox-one-key-3205691 \r\nxbox game купить+http://mysel.ru/goods_info.php?id=2961721 \r\n<b>игры XBOX XD KEY</b>  <b>MYSEL>RU</b> https://mysel.ru  Торговая площадка цифровых товаров \r\n<a href=https://plati.market/itm/fifa-20-uk-xbox-one-kljuch-cifr-kod--podarok/2961184>  FIFA 20  XBOX ONE Ключ  / Цифр код + подарок </a> \r\n<a href=https://plati.market/itm/3207281> ? Grand Theft Auto V / GTA 5 XBOX ONE Цифровой Ключ ??</a> \r\n<a href=https://plati.market/itm/3048213>Watch Dogs 2  - Xbox Live Key - +подарок </a> \r\n<a href=https://wmcentre.net/item/resident-evil-2-resident-evil-3-xbox-3203379>Resident Evil 3 Xbox One Key   </a> \r\n<a href=https://plati.market/itm/3074508>The Crew 2 ?? XBOX/PC+present </a> \r\n<a href=https://plati.market/itm/2970072>Diablo III: Eternal Collection Xbox One</a> \r\n<a href=https://plati.market/itm/3008085><b>?Adguard Premium 1PC на 1 year</b> </a> \r\n<a href=https://plati.market/itm/3113988><b>GOOGLE PLAY GIFT CARD $15 (USA)</b> </a> \r\n<a href=https://plati.market/itm/2985407><b>Steam Turkey 50 TL Gift Card Code(FOR TURKEY ACCOUNTS)</b></a> \r\n<a href=https://plati.market/itm/2911572><b> Netflix Турция Подарочный код 100 TL</b></a> \r\n<a href=https://plati.market/itm/3073272>Хочешь купить Машину, срочно нужны деньги, не хватает на покупку обращайся в HOME BANK </a> \r\n<a href=https://plati.market/itm/3085419>MORTAL KOMBAT 11 XBOX ONE X/S</a> \r\n<a href=https://plati.market/itm/3204091><b>King?s Bounty II - Lord?s Edition XBOX ONE</b></a> \r\n<a href=https://plati.market/itm/3116767><b>Avast Cleanup Premium  1пк /1 key 1годх</b></a> \r\n<a href=https://wmcentre.net/item/avg-internet-security-1-pc-1-year-3203399><b>AVG INTERNET SECURITY 1pc/1key </b></a> \r\n<a href=https://plati.market/itm/3203865>  <b>Amazon Gift Card US $10</b> </a> \r\n<a href=https://wmcentre.net/item/avg-tuneup-1-pk-1-god-3203401>  <b>AVG TuneUp 1 ПК 1 год -</b> </a> \r\n<a href=https://plati.market/itm/3116494> <b>iTUNES GIFT CARD - $15(USA) </b></a> \r\n<a href=https://plati.market/itm/2892265>The Evil Within XBOX ONE ключ  </a> \r\n<a href=https://wmcentre.net/item/assassin-s-creed-valhalla-xbox-3203374>Red Dead Redemption 2 XBOX ONE(Account)</a> \r\n<a href=https://wmcentre.net/item/hitman-3-call-of-duty-adv-warfare-xbox-3203356>Sniper Ghost Warrior Contracts 2 Deluxe Edition XBOX </a>/ <a href=https://www.plati.market/itm/3221892>Chernobylite Xbox One & Series XS КЛЮЧ</a>/ <a href=https://plati.market/itm/3221893>Dead Rising Triple Bundle Pack XBOX / КЛЮЧ</a> \r\n<b>Покупай на ENOT-способ оплаты(0% при оплате банковской картой)11 способов оплаты</b>');
INSERT INTO `pm_message` (`id`, `add_date`, `edit_date`, `name`, `email`, `address`, `phone`, `subject`, `msg`) VALUES
(791, 1633642604, NULL, 'Joseph', 'jbaliva2015@gmail.com', 'Hi there,\r\n\r\nWe offer a new form of crowdfunding that brings capital to eCommerce sites. It’s called Go Fish Crowdfunding. Crowdfunders can buy advertising for your site to bring in customers, in exchange for 10% commission. Aside from free customers, you also get to keep 20% of the crowdfunding in cash. It can add up to tens of thousands of dollars per month. Go to https://www.gofishcrowdfunding.com  It’s free!', '415-758-8808', 'Crowdfunding For WooCommerce', 'Hi there,\r\n\r\nWe offer a new form of crowdfunding that brings capital to eCommerce sites. It’s called Go Fish Crowdfunding. Crowdfunders can buy advertising for your site to bring in customers, in exchange for 10% commission. Aside from free customers, you also get to keep 20% of the crowdfunding in cash. It can add up to tens of thousands of dollars per month. Go to https://www.gofishcrowdfunding.com  It’s free!'),
(792, 1633829986, NULL, 'Edo', 'rolando_sw_01@yahoo.com', 'Jakarta', '08129693994', 'Perbedaan room', 'Perbedaan room deluxe dengan executive apa ya?'),
(793, 1633830022, NULL, 'Edo', 'rolando_sw_01@yahoo.com', 'Jakarta', '08129693994', 'Perbedaan room', 'Perbedaan room deluxe dengan executive apa ya?'),
(794, 1633830035, NULL, 'Edo', 'rolando_sw_01@yahoo.com', 'Jakarta', '08129693994', 'Perbedaan room', 'Perbedaan room deluxe dengan executive apa ya?'),
(795, 1633830222, NULL, 'Edo', 'rolando_sw_01@yahoo.com', 'Jakarta', '08129693994', 'Perbedaan room', 'Perbedaan room deluxe dengan executive apa ya?'),
(796, 1635736555, NULL, 'Erry Cahyono', 'errycahyono@gmail.com', 'PT. Asia Pacific Fibers. Ds. Kiarapayung, kecamatan Klari, Karawang\r\n', '081212078118', 'Meeting Paket', 'Mohon dikirimkan brocure paket meeting. '),
(797, 1635736561, NULL, 'Erry Cahyono', 'errycahyono@gmail.com', 'PT. Asia Pacific Fibers. Ds. Kiarapayung, kecamatan Klari, Karawang\r\n', '081212078118', 'Meeting Paket', 'Mohon dikirimkan brocure paket meeting. '),
(798, 1636502407, NULL, 'huohxxoo', 'gsuufabgq@riador.online', 'https://viagaracom.com', '88875889688', 'fqstyvbjmszs', 'viagra coupon walmart  https://viagaracom.com/ - cheap viagra generic  \r\nwhere to buy viagra online safely  \r\nenhancement male  <a href=https://viagaracom.com/#>cheapest generic viagra</a>  do generic viagra pills work '),
(799, 1636769788, NULL, 'melisapj16', 'esmeraldaym16@akio6910.takayuki33.sorataki.in.net', 'http://curveypornvids.hotblognetwork.com', '83823346369', 'New sexy website is available on the web', 'Girls of Desire: All babes in one place, crazy, art\r\nhttp://lesbianass.hotblognetwork.com/?abagail \r\n\r\n jobs for gay porn internet earnigs porn elmer porn porn girl wresling free porn clips deep throat \r\n\r\n'),
(800, 1637203230, NULL, 'Daltonsaf', 'yourmail@gmail.com', 'https://thevoguechoice.com/', '88477859613', 'Fashion shop', 'Best <a href=https://thevoguechoice.com/>vogue tracksuit price</a>. \r\nDiscover the last trendy clothes in your favorite fashion accessories store. International free shipping!'),
(801, 1637378359, NULL, 'Steve', 'steve@explainervideos4u.info', 'Hi,\r\n\r\nWe\'d like to introduce to you our explainer video service which we feel can benefit your site salsebandung.com.\r\n\r\nCheck out some of our existing videos here:\r\nhttps://www.youtube.com/watch?v=zvGF7uRfH04\r\nhttps://www.youtube.com/watch?v=cZPsp217Iik\r\nhttps://www.youtube.com/watch?v=JHfnqS2zpU8\r\n\r\nAll of our videos are in a similar animated format as the above examples and we have voice over artists with US/UK/Australian accents.\r\n\r\nThey can show a solution to a problem or simply promote one of your products or services. They are concise, can be uploaded to video such as Youtube, and can be embedded into your website or featured on landing pages.\r\n\r\nOur prices are as follows depending on video length:\r\n1 minute = $239\r\n1-2 minutes = $339\r\n2-3 minutes = $449\r\n\r\n*All prices above are in USD and include an engaging, and captivating video, full script and voice-over.\r\n\r\nIf this is something you would like to discuss further, don\'t hesitate to get in touch.\r\nIf you are not interested, simply delete this message and we won\'t contact you again.\r\n\r\nKind Regards,\r\nSteve', '078 4938 0361', 'Explainer Video for salsebandung.com', 'Hi,\r\n\r\nWe\'d like to introduce to you our explainer video service which we feel can benefit your site salsebandung.com.\r\n\r\nCheck out some of our existing videos here:\r\nhttps://www.youtube.com/watch?v=zvGF7uRfH04\r\nhttps://www.youtube.com/watch?v=cZPsp217Iik\r\nhttps://www.youtube.com/watch?v=JHfnqS2zpU8\r\n\r\nAll of our videos are in a similar animated format as the above examples and we have voice over artists with US/UK/Australian accents.\r\nWe can also produce voice overs in languages other than English.\r\n\r\nThey can show a solution to a problem or simply promote one of your products or services. They are concise, can be uploaded to video such as Youtube, and can be embedded into your website or featured on landing pages.\r\n\r\nOur prices are as follows depending on video length:\r\n1 minute = $189\r\n1-2 minutes = $339\r\n2-3 minutes = $449\r\n\r\n*All prices above are in USD and include an engaging, captivating video with full script and voice-over.\r\n\r\nIf this is something you would like to discuss further, don\'t hesitate to get in touch.\r\n\r\nKind Regards,\r\nSteve'),
(802, 1637446842, NULL, 'Steve', 'steve@explainervideos4u.info', 'Hi,\r\n\r\nWe\'d like to introduce to you our explainer video service which we feel can benefit your site salsebandung.com.\r\n\r\nCheck out some of our existing videos here:\r\nhttps://www.youtube.com/watch?v=oYoUQjxvhA0\r\nhttps://www.youtube.com/watch?v=MOnhn77TgDE\r\nhttps://www.youtube.com/watch?v=NKY4a3hvmUc\r\n\r\nAll of our videos are in a similar animated format as the above examples and we have voice over artists with US/UK/Australian accents.\r\n\r\nThey can show a solution to a problem or simply promote one of your products or services. They are concise, can be uploaded to video such as Youtube, and can be embedded into your website or featured on landing pages.\r\n\r\nOur prices are as follows depending on video length:\r\n1 minute = $239\r\n1-2 minutes = $339\r\n2-3 minutes = $449\r\n\r\n*All prices above are in USD and include an engaging, captivating video with full script and voice-over.\r\n\r\nIf this is something you would like to discuss further, don\'t hesitate to get in touch.\r\nIf you are not interested, simply delete this message and we won\'t contact you again.\r\n\r\nKind Regards,\r\nSteve', '078 4938 0361', 'Explainer Videos for salsebandung.com', 'Hi,\r\n\r\nWe\'d like to introduce to you our explainer video service which we feel can benefit your site salsebandung.com.\r\n\r\nCheck out some of our existing videos here:\r\nhttps://www.youtube.com/watch?v=oYoUQjxvhA0\r\nhttps://www.youtube.com/watch?v=MOnhn77TgDE\r\nhttps://www.youtube.com/watch?v=NKY4a3hvmUc\r\n\r\nThey can show a solution to a problem or simply promote one of your products or services.\r\n\r\nOur prices (in USD) are as follows depending on video length:\r\n1 minute = $189\r\n1-2 minutes = $339\r\n2-3 minutes = $449\r\n\r\nIf this is something you would like to discuss further, don\'t hesitate to get in touch.\r\n\r\nKind Regards,\r\nSteve'),
(803, 1637451036, NULL, 'Miltonsen', 'pdaria8875@yandex.ru', 'https://exmo.me/?ref=433652', '83528751314', 'I accidentally scrolled across GAFFL and I\' m so pleased I did. I met Amaia and immediately decided to road- trip together. We\' re spending 10 days together and it\' s great because we have so much in common. Couldn\' t have done it without GAFFL helpi', 'http://zazebok.ru/ford/tl50 \r\n \r\n \r\n8211;  Oh my girlfriend s is always begging me to go to Denmark tell me all about it.’'),
(804, 1637983260, NULL, 'eugeneqn69', 'lloydun69@fumio5610.hikaru19.toshikokaori.xyz', 'http://chinosformen.tshirtwithmodel.instasexyblog.com', '86335212837', 'Girls of Desire: All babes in one place, crazy, art', 'New sexy website is available on the web\r\nhttp://nightynightwear.allproblog.com/?nyasia \r\n\r\n free ametuer porn vids italian vintage porn tubes categorized crossdressing porn drunk nights porn free college porn fest \r\n\r\n'),
(805, 1638735888, NULL, 'ColemanHooli', 'biriukovigor399@yandex.ru', '', '85288215961', 'Есть возможность регистрации и через официальный сайт компании.', 'Что бы воспользоваться скидками до 99%, вам всего лишь нужно показать QR код на кассе кассиру или на кассе самообслуживания и попросить списать баллы. \r\nЕй можно расплатиться до 99% суммы чека. \r\nПотратить баллы, можно: \r\n- Магнит (Продукты, Косметика и Аптека) \r\n- Пятерочка \r\n- Перекресток \r\n- Аптека \r\n- Яндекс баллы \r\n- Такси \r\nЭксклюзивные Купоны / Промокоды \r\nhttps://t.me/SevenSale_bot?start=r_2561371 \r\nИтак, рассмотрим, как добавить и настроить такую скидку.'),
(806, 1639022308, NULL, 'txblwwsa', 'rkvyoofzg@vigabigo.online', 'https://cialisara.com', '81132919996', 'kyjqgqwfmool', 'cialis for daily use online  https://cialisara.com/ - cialis over the counter at walmart  \r\nonline prescription for cialis  \r\ncialis discount  <a href=https://cialisara.com/#>generic cialis online</a>  cialis canada pharmacy online '),
(807, 1639121623, NULL, 'Login', 'tomila.maryasina.90@mail.ru', '', '82453177948', ' Возврaт 202 815 р ', ' \r\n Вывод средств  \r\n \r\nПодробнее по ссылке: https://forms.yandex.ru/u/610fe0c0a7a9d9645645/success/04261?AAAAAsalsebandung.comBBBBB'),
(808, 1639572902, NULL, 'johannagg4', 'lewisuq60@tamotsu6910.takayuki51.sorataki.in.net', 'http://lesbian.slave.bloglag.com', '82947731847', 'Sexy photo galleries, daily updated pics', 'Daily updated super sexy photo galleries\r\nhttp://butch.lesbian.instakink.com/?ebony \r\n\r\n bbw 3 ten porn archive unlimited free amateur porn pic klara petra porn video 60s french porn dick ring porn \r\n\r\n'),
(809, 1639648631, NULL, 'ElenaciX', 'elenaciX@mail.com', 'http://renorme.tk/chk/1', '+1 2095034567', 'Can I find here serious man?...', 'Hello all, guys! I know, my message may be too specific,\r\nBut my sister found nice man here and they married, so how about me?! :)\r\nI am 26 years old, Maria, from Romania, know English and Russian languages also\r\nAnd... I have specific disease, named nymphomania. Who know what is this, can understand me (better to say it immediately)\r\nAh yes, I cook very tasty! and I love not only cook ;))\r\nIm real girl, not prostitute, and looking for serious and hot relationship...\r\nAnyway, you can find my profile here: http://renorme.tk/chk/1\r\n'),
(810, 1641493385, NULL, 'HaroldHal', 'coreen_boetel@web.de', '', '82248776369', 'dark souls 3 cracked steam download\r\n', '<a href=http://sys.idealcleanouts.com/unyvesuka/53718-download-windows-7-pro-32-bit-iso.html>download windows 7 pro 32 bit iso full crack</a>\r\n<a href=http://mexinter.net/temp30345/cysyxadici/18868-sketchup-pro-2018-mac-download-crack.html>sketchup pro 2018 mac download crack</a>\r\n<a href=http://megjob.com/sys72856/ogyzokibapu/37734-download-articulate-storyline-360-full-crack.html>download articulate storyline 360 full crack</a>\r\n<a href=http://tmp977831.berkhamstedaircadets.org.uk/ikejocizavybofo/34896-iclone-6-full-crack-download.html>iclone 6 full crack download</a>\r\n \r\n \r\n<a href=http://mmomemo.seesaa.net/article/172035918.html>download crack gta 4 for windows 7</a> 9cd8b7b '),
(811, 1642321503, NULL, 'PornoGog', 'petrischeffden@yandex.ru', 'https://porno-go.website', '89132185196', 'Новинки Секс Видео в HD', 'Чувственное секс ролики для взрослых онлайн на https://porno-go.website в высоком качестве \r\nВип секс ролики для всех онлайн на https://porno-go.ru в высоком качестве \r\nИз домашних архивов порно фильмы для взрослых онлайн на https://porno-go.top в высоком качестве \r\nНежное порно запись без регистрации смотреть на https://porno-go.pro в HD720 \r\nСо всего мира порно ролики бесплатно смотреть на https://porno-go.mobi в высоком качестве \r\n \r\n \r\n \r\n \r\n.'),
(812, 1642697301, NULL, 'BarryIsoks', 'oleggsw@gmail.com', 'https://stiralkarem.ru/', '82449821275', 'Если сломалась стиральная машина', '<a href=https://stiralkarem.ru/>ремонт стиральных машин Москва</a>'),
(813, 1645307530, NULL, 'Steve', 'steve@explainervideos4u.info', 'Hi,\r\n\r\nWe\'d like to introduce to you our explainer video service, which we feel can benefit your site salsebandung.com.\r\n\r\nCheck out some of our existing videos here:\r\nhttps://www.youtube.com/watch?v=2GvqwoG1Mrc\r\nhttps://www.youtube.com/watch?v=7TFiXX8PUGA\r\nhttps://www.youtube.com/watch?v=DibihevhSBU\r\n\r\nAll of our videos are in a similar animated format as the above examples, and we have voice over artists with US/UK/Australian/Canadian accents.\r\nWe can also produce voice overs in languages other than English.\r\n\r\nThey can show a solution to a problem or simply promote one of your products or services. They are concise, can be uploaded to video sites such as YouTube, and can be embedded into your website or featured on landing pages.\r\n\r\nOur prices are as follows depending on video length:\r\nUp to 1 minute = $259\r\n1-2 minutes = $379\r\n2-3 minutes = $489\r\n\r\n*All prices above are in USD and include an engaging, captivating video with full script and voice-over.\r\n\r\nIf this is something you would like to discuss further, don\'t hesitate to reply.\r\n\r\nKind Regards,\r\nSteve', '078 4938 0361', 'Explainer Video for salsebandung.com', 'Hi,\r\n\r\nWe\'d like to introduce to you our explainer video service, which we feel can benefit your site salsebandung.com.\r\n\r\nCheck out some of our existing videos here:\r\nhttps://www.youtube.com/watch?v=zvGF7uRfH04\r\nhttps://www.youtube.com/watch?v=cZPsp217Iik\r\nhttps://www.youtube.com/watch?v=JHfnqS2zpU8\r\n\r\nAll of our videos are in a similar animated format as the above examples, and we have voice over artists with US/UK/Australian/Canadian accents.\r\nWe can also produce voice overs in languages other than English.\r\n\r\nThey can show a solution to a problem or simply promote one of your products or services. They are concise, can be uploaded to video sites such as YouTube, and can be embedded into your website or featured on landing pages.\r\n\r\nOur prices are as follows depending on video length:\r\nUp to 1 minute = $239\r\n1-2 minutes = $339\r\n2-3 minutes = $439\r\n\r\n*All prices above are in USD and include an engaging, captivating video with full script and voice-over.\r\n\r\nIf this is something you would like to discuss further, don\'t hesitate to reply.\r\n\r\nKind Regards,\r\nSteve'),
(814, 1645986462, NULL, 'pedrodc1', 'karyneh16@naoki94.meta1.in.net', 'http://hotboobpornvids.fetlifeblog.com', '85936344695', 'Teen Girls Pussy Pics. Hot galleries', 'Hardcore Galleries with hot Hardcore photos\r\nhttp://dirtystageporn.topanasex.com/?olivia \r\n\r\n chubby feet porn is it ok to watch porn porn minigames very mature black porn simpsons porn cartoon gallery \r\n\r\n'),
(815, 1646081293, NULL, 'alejandrazd18', 'gordondp69@michio96.officemail.in.net', 'http://gayfreeporn.sexoralfree.bestsexyblog.com', '85561671379', 'Hardcore Galleries with hot Hardcore photos', 'Sexy pictures each day\r\nhttp://xxxwithporn.xblognetwork.com/?keeley \r\n\r\n 3d porn tpg asian teens free porn favorite porn films list jailhouse girls big gay cock free porn best mobile bi site porn \r\n\r\n'),
(816, 1646098784, NULL, 'Garrycug', 'fiverr-txc@masum.cc', '', '83361731849', 'Remove Your Website Toxic Backlinks & Rank Again [[Fiverr [', 'Backlinks are one of the most critical parts of SEO. You can\'t rank your Website without Backlinks, but TOXIC BACKLINK never Ranks your Website; instead, it down your Website Ranking and Penalizes your Website. So it is time to remove toxic Backlinks. I\'ll find out and remove/disavow toxic Backlinks. \r\n \r\nRemove your Toxic Backlinks for only $5 on Fiverr: \r\n-1 Website Toxic Backlinks Audit \r\n-Find out Toxic Links \r\n-Remove Toxic Backlinks \r\n-Provide Full Backlinks Audit \r\n-How to submit your links on Goole Disavow \r\n-Advice for links removing \r\n \r\nGet the service from here (Fiverr): https://itwise.link/backlinkaudit \r\n#1 Freelancing Site. 100% Secure Payment.'),
(817, 1646164947, NULL, 'RandySpeem', 'l.o.u.is.rg863.@gmail.com', '[url=https://rgpalletracking.com/contact-us/]<span style=color:#000> We buy used Simpson pallet racks or contractor\'s warehouse of Southern CA</span>[/url]', '86823554825', 'Cant beleve all the good info here', 'I\'m so glad to have found this blog, it is exactly what my grage buddies were looking in search of. The detailed information on the web page is with out a doubt enlightening and will contribute my wife and I a ton great help. It appears as if everyone on the blog extrapolates a lot of knowledge concerning the stuff I am interested in and the other hyper links and info really are shown. I\'m not typically on the web much however as we get a chance We\'re totally searching this kind of information and things closely having to do with it. I\'ll tell others about this forum. If anyone needed a little site work like: <a href=https://rgpalletracking.com/contact-us/><span style=color:#000>: We buy used Steel King pallet racks and used warehouse racks for sale near San Diego</span></a>'),
(818, 1646966587, NULL, 'concettarx11', 'maryanneli16@michio96.officemail.in.net', 'http://shemalehotels.fetlifeblog.com', '82163849128', 'Nude Sex Pics, Sexy Naked Women, Hot Girls Porn', 'My new hot project|enjoy new website\r\nhttp://latino.porn.bestsexyblog.com/?devin \r\n\r\n bahamiam porn kylie minogue nude porn free porn stream videos free porn full length duration young stepmother porn films \r\n\r\n'),
(819, 1647959328, NULL, 'Herbertsuize', 'eduard.m.19897270@mail.ru', 'https://buhexpert-in.ru ', '89545711627', 'Бухгалтерское Сопровождение Ооо Стоимость\r\n', 'Поможем исправить ошибки в ЕГРЮЛ, допущенные как по Вашей вине при подаче неправильно заполненных форм,  так и технические ошибки, допущенные налоговыми органами при регистрациии внесении данных в ЕГРЮЛ http://buhexpert-in.ru\r\n \r\nНалогоплательщики, ответственные участники КГН, исчисляющие ежемесячные авансовые платежи исходя из фактически полученной прибыли, в случае создания новых или ликвидации обособленных подразделений в течение 2013 г http://buhexpert-in.ru\r\n \r\nНаш сайт бесплатных объявлений представлен почти во всех городах России: Москве, Санкт-Петербурге, Нижнем-Новгороде, Перми, Волгограде, Самаре, Ростове-на-Дону, Казане, Уфе, Челябинске, Краснодаре и других регионах России http://buhexpert-in.ru\r\n \r\nЭкономию расходов, связанных с отчислениями в бюджетные и внебюджетные фонды, а также с обязательными налоговыми сборами https://buhexpert-in.ru\r\n   Недостаточная осведомленность в области налогового права нередко приводит к неправильному выбору формы налогообложения, что влечет за собой экономически необоснованные затраты для компании http://buhexpert-in.ru\r\n   Специалисты компании  предлагают бухгалтерское сопровождение бизнеса с выбором оптимальной схемы уплаты налогов, в зависимости от организационно-правовой формы предпринимательства https://buhexpert-in.ru\r\n \r\nВосстановление бухгалтерского учета представляет собой комплекс мероприятий, направленных на приведение восстановление финансовой отчетности и регистров учета предприятия за определенный период времени http://buhexpert-in.ru\r\n   Может предусматривать восстановление недостающей первичной документации http://buhexpert-in.ru\r\n \r\n'),
(820, 1647969665, NULL, 'Darrelmix', 's.a.m.tuc.ke.r.4.58.9.@gmail.com', '[url=https://drywallpatchguys.com/]<span style=color:#000>drywall point up in South County</span>[/url]', '88968313382', 'The Milky Way and how to find it in the night sky', 'Hey there to all. I am happy we heard the shares here. Ive been crawling the net for this info all year and I will be sure to tell my cohorts to hop on by. The other day I was flipping through the net trying to scope out an answer to my eternal questions. Now I am entrusted to take it all the way in whatever method I can. We are getting all spaced out on the smart ideas we are observing. Moreover, I just want to thank you tremendously for such uplifting information. This has opened me out of rough territory. Many spiritual things are shaping around my world. Its really a safe time to make new relationships. Id also add that I am looking into. If you have time, take a look my new spot:<a href=https://drywallpatchguys.com/><span style=color:#000>drywall ca near REDONDO BEACH CA</span></a>'),
(821, 1648408644, NULL, 'JessicaStamp', 'Jesusked1967@qeo.dogle.info', 'http://mukunaofficial.ru/potencagra-cena.html', '81841591121', 'Потенцагра таблетки отзывы', '<a href=http://mukunaofficial.ru/vitalife-a-zn-vitaminy-dlya-muzhchin-vitolajz-dlya-muzhchin.html>vitalife a zn</a>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_package`
--

CREATE TABLE `pm_package` (
  `id` int(11) NOT NULL,
  `users` text DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `days` text DEFAULT NULL,
  `min_nights` int(11) DEFAULT NULL,
  `max_nights` int(11) DEFAULT NULL,
  `day_start` int(11) DEFAULT NULL,
  `day_end` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_package`
--

INSERT INTO `pm_package` (`id`, `users`, `name`, `days`, `min_nights`, `max_nights`, `day_start`, `day_end`) VALUES
(2, '1', 'Night', '1,2,3,4,5,6,7', 0, 0, NULL, NULL),
(8, '6', 'PROMO VALENTINE 2020', '5,6', 1, 2, 5, 7),
(9, '6', 'Long Stay 7D6N', '1,2,3,4,5,6,7', 6, 6, NULL, NULL),
(10, '6', 'Long Stay 3D2N', '1,2,3,4,5,6,7', 2, 2, NULL, NULL),
(11, '6', 'Long Stay 31D30N', '1,2,3,4,5,6,7', 30, 30, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_page`
--

CREATE TABLE `pm_page` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `subtitle` varchar(250) DEFAULT NULL,
  `title_tag` varchar(250) DEFAULT NULL,
  `alias` varchar(100) DEFAULT NULL,
  `descr` longtext DEFAULT NULL,
  `robots` varchar(20) DEFAULT NULL,
  `keywords` varchar(250) DEFAULT NULL,
  `intro` longtext DEFAULT NULL,
  `text` longtext DEFAULT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `page_model` varchar(50) DEFAULT NULL,
  `article_model` varchar(50) DEFAULT NULL,
  `home` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 0,
  `rank` int(11) DEFAULT 0,
  `add_date` int(11) DEFAULT NULL,
  `edit_date` int(11) DEFAULT NULL,
  `comment` int(11) DEFAULT 0,
  `rating` int(11) DEFAULT 0,
  `system` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_page`
--

INSERT INTO `pm_page` (`id`, `lang`, `name`, `title`, `subtitle`, `title_tag`, `alias`, `descr`, `robots`, `keywords`, `intro`, `text`, `id_parent`, `page_model`, `article_model`, `home`, `checked`, `rank`, `add_date`, `edit_date`, `comment`, `rating`, `system`) VALUES
(1, 1, 'Accueil', 'Lorem ipsum dolor sit amet', 'Consectetur adipiscing elit', 'Accueil', '', '', 'index,follow', '', '', '<blockquote class=\"text-center\">\r\n<p>A man travels the world over in search of what he needs and returns home to find it.</p>\r\n</blockquote>\r\n\r\n<p class=\"text-muted\" style=\"text-align: center;\">- George A. Moore -</p>\r\n', NULL, 'home', '', 1, 1, 1, 1563677945, 1576810792, 0, 0, 0),
(1, 2, 'Home', 'Salse Bandung, Luxury Resort', '', 'Salse Resort, Bandung, Lembang', '', '', 'index,follow', '', '', '<blockquote class=\"text-center\">\r\n<p>A man travels the world over in search of what he needs and returns home to find it.</p>\r\n</blockquote>\r\n\r\n<p class=\"text-muted\" style=\"text-align: center;\">- George A. Moore -</p>\r\n', NULL, 'home', '', 1, 1, 1, 1563677945, 1580974950, 0, 0, 0),
(1, 3, 'ترحيب', 'هو سقطت الساحلية ذات, أن.', 'غير بمعارضة وهولندا، الإقتصادية قد, فقد الفرنسي المعاهدات قد من.', 'ترحيب', '', '', 'index,follow', '', '', '<blockquote class=\"text-center\">\r\n<p>A man travels the world over in search of what he needs and returns home to find it.</p>\r\n</blockquote>\r\n\r\n<p class=\"text-muted\" style=\"text-align: center;\">- George A. Moore -</p>\r\n', NULL, 'home', '', 1, 1, 1, 1563677945, 1576810792, 0, 0, 0),
(1, 4, 'Home', 'Salse Bandung, Resor Mewah', '', 'Salse Resort, Bandung, Lembang', '', '', 'index,follow', '', '', '<blockquote class=\"text-center\">\r\n<p>Seorang pria berkeliling dunia untuk mencari apa yang dia butuhkan dan kembali ke rumah untuk menemukannya.</p>\r\n</blockquote>\r\n\r\n<p class=\"text-muted\" style=\"text-align: center;\">- George A. Moore -</p>\r\n', NULL, 'home', '', 1, 1, 1, 1563677945, 1580974950, 0, 0, 0),
(2, 1, 'Contact', 'Contact', '', 'Contact', 'contact', '', 'index,follow', '', '', '', NULL, 'contact', '', 0, 1, 12, 1563677945, 1563677945, 0, 0, 0),
(2, 2, 'Contact', 'Contact', '', 'Contact', 'contact', '', 'index,follow', '', '', '', NULL, 'contact', '', 0, 1, 12, 1563677945, 1580286833, 0, 0, 0),
(2, 3, 'جهة الاتصال', 'جهة الاتصال', '', 'جهة الاتصال', 'contact', '', 'index,follow', '', '', '', NULL, 'contact', '', 0, 1, 12, 1563677945, 1563677945, 0, 0, 0),
(2, 4, 'Kontak', 'Kontak', '', 'Kontak', 'kontak', '', 'index,follow', '', '', '', NULL, 'contact', '', 0, 1, 12, 1563677945, 1580286833, 0, 0, 0),
(3, 1, 'Mentions légales', 'Mentions légales', '', 'Mentions légales', 'mentions-legales', '', 'index,follow', '', '', '', NULL, 'page', '', 0, 1, 13, 1563677945, 1566365894, 0, 0, 0),
(3, 2, 'Term Of Use', 'Term Of Use', '', 'Term Of Use', 'term-of-use', '', 'index,follow', '', '', '<p><strong>Kebijakan Privasi</strong></p>\r\n\r\n<p>Kebijakan Privasi ini berlaku untuk salsebandung.com dimilikidan dioperasikan oleh VillaTel Salse. Kebijakan Privasi inimendeskripsikan bagaimana kami mengumpulkan data, yang mungkin termasuk denga data pribadi, yang Anda berikankepada situs kami: [salsebandung.com]. Ini juga menjelaskanpilihan yang tersedia berkaitan dengan penggunaan data personal Anda oleh kami dan bagaimana Anda dapat mengaksesdan memperbarui data ini.</p>\r\n\r\n<p><b>Pengambilan Data</b></p>\r\n\r\n<p>Tipe informasi pribadi yang kami kumpulkan termasuk:</p>\r\n\r\n<p>• Nama Lengkap, alamat email, nomor telepon dan alamatrumah;</p>\r\n\r\n<p>• Rincian kartu kredit (tipe kartu, nomor kartu kredit, nama pada kartu, tanggal berakhir, dan kode keamanan);</p>\r\n\r\n<p>• Informasi menginap tamu, termasuk tanggal kedatangan dan keberangkatan, permintaan khusus yang ada, hasilpengamatan tentang preferensi layanan Anda (termasukpreferensi kamar, fasilitas atau layanan lain yang digunakan);</p>\r\n\r\n<p>• Informasi yang Anda berikan terkait dengan preferensipemasaran Anda atau dalam rangka partisipasi dalam survei, kontes atau penawaran promosi;</p>\r\n\r\n<p>Anda selalu dapat memilih data pribadi apa asaja (apabila ada) yang ingin Anda berikan pada kami. Namun apabila Anda memilih untuk tidak berbagi beberapa rincian tertentu, beberapatransaksi dengan kami mungkin akan terkena dampaknya.</p>\r\n\r\n<p>Data yang kami kumpulkan secara otomatis</p>\r\n\r\n<p>Saat menggunakan situs kami, kami juga mengumpulkan data secara otomatis, beberapa mungkinberupa data pribadi. Hal initermasuk data seperti pengaturan bahasa, alamat IP< lokasi, pengaturan perangkat, sistem operasi, informaaaasi log, waktupenggunaa, URL yang diminta, laporan status, agen pengguna(informasi tentang versi browser), sistem operasi, hasil (yang dilihat dan pemesan), sejarah penjelajahan (browsing history), BookingID pengguna, dan tipe data yang dilihat. Kami juga mengumpulkan data secara otomatis melalui cookies..</p>\r\n\r\n<p>Tujuan Pengolahan</p>\r\n\r\n<p>Kami menggunakan data pribadi Anda untuk tujuan berikut:</p>\r\n\r\n<p>• A. Reservasi: kami menggunakan data pribadi Anda untukmenyelesaikan dan mengelola reservasi online Anda.</p>\r\n\r\n<p>• B. Customer service: Kami menggunakan data pribadi Anda untuk memberikan customer service.</p>\r\n\r\n<p>• C. Ulasan tamu: Kami mungkin akan menggunakan data kontak untuk mengundang Anda via email agar menulisulasan setelah Anda selesai menginap. hal ini dapatmembantu traveler lainnya untuk memilih akomodasi yang sesuai dengan selera mereka. Apabila mengirimkan ulasantamu, ulasan Anda mungkin akan ditampilkan di situs kami.</p>\r\n\r\n<p>• D. Aktivitas marketing: Kami juga akan menggunakan data yang Anda berika untuk keperluan marketing, sejauhdiperbolehkan oleh hukum yang berlaku. Apabila kami menggunakan data pribadi Anda untuk tujuan pemasaranlangsung, seperti newsletter komersil dan komunikasimarketing untuk produk dan layanan baru atau penawaranlain yang kami anggap mungkin menarik untuk Anda, kami juga akan mengikutsertakan link berhenti berlangganan yang dapat Anda gunakan bila Anda tidak ingin kami mengirimkan pesan di masa mendatang.</p>\r\n\r\n<p>• E. Komunikasi lainnya: Akan ada saatnya kami menghubungiAnda melalui email, pos, dengan telepon atau dengan SMS, tergantung dengan data kontak yang Anda berikan kepadakami. Beberapa alasan untuk hal ini:</p>\r\n\r\n<p>o a. kami mungkin perlu merespon dan menganganipermintaan yang telah Anda buat.<br />\r\nb. Apabila Anda belum menfinalisasi reservasi online, kami mungkin akan mengirimkan email pengingat untukmelanjutkan reservasi Anda. Kami percaya bahwalayanan tambahan ini berguna untuk Anda, karena akanmemungkinkan Anda melanjutkan reservasi tanpa perluamencari akomodasi lagi atau mengisi detail reservasidari awal. <br />\r\nc. Saat Anda menggunakan layanan kami, kami mungkinakan mengirimkan kuesioner atau mengundang Anda untuk memberikan ulasan tentang pengalaman Anda di situs kami. Kami percaya bahwa layanan tambahan iniberguna untuk kita semua, karena kami dapatmemperbaiki situs kami berdasarkan masukan Anda.</p>\r\n\r\n<p>• F. Analisis, perbaikan dan riset: Kami menggunakan data pribadi untuk melakukan riset dan analisis. kami mungkinakan menggubnakan pihak ketiga untuk melakukan ini. Kami mungkin akan berbagi hasil riset terkait, termasuk kepadapihak ketiga, dalam bentuk agregat anonim. Kami menggunakan data pribadi Anda untuk tujuan analitis, untukmemperbaiki layanan kami, untuk memperkaya pengalamanpengguna, dan untuk memperbaiki fungsi dan kualitaslayanan travel online kami.</p>\r\n\r\n<p>• G. Keamanan, mendeteksi dan mencegah penipuan: Kami menggunakan informasi, yang mungkin termasuk data pribadi, untuk mencegah penipuan dan aktivitas ilegal dan melanggar hukum lainnya. Kami juga menggunakan informsiini untuk menginvestigasi dan mendeteksi penipuan. Kami juga menggunakan data pribadi untuk menaksir resiko dan tujuan keamanan, termasuk otentifikasi pengguna. Untuktujuan tersebut diatas, data pribadi mungkin akan dibagikankepada pihak ketiga, seperti aparat penegak hukumsebagaimana diatur oleh hukum yang berlaku dan penasihatdi luar institusi.</p>\r\n\r\n<p>• H. Hukum dan kepatuhan: Dalam kasus tertentu, kami perlumenggunakan informasi yang diberikan, yang mungkintermasuk data pribadi, untuk menangani dan menyelesaikanperselisihan hukum atau komplain, untuk penyelidikanperaturan dan kepatuhan, atau untuk menjalankan perjanjianatau untuk memenuhi permintaan hukum dari aparat penegakhukum selama diwajibkan oleh hukum yang berlaku.</p>\r\n\r\n<p>• Apabila kami menggunakan cara-cara otomatis untukmemproses data pribadi yang akan mengakibatkan persoalanhukum atau secara signifikan berpengaruh pada Anda, kami akan menerapkan langkah-langkah yang sesuai untukmelindungi hak dan kebebasan Anda, termasuk hak untukmendapatkan intervensi manusia.</p>\r\n\r\n<p>Basis Hukum</p>\r\n\r\n<p>• Terkait dengan tujuan A dan B, kami bergantung pada kinerjakontrak: Penggunaan data Anda mungkin diperlukan untukmenjalankan kontrak yang Anda miliki dengan kami. Misalnya, jika Anda menggunakan layanan kami untukmembuat reservasi online, kami akan menggunakan data Anda untuk menjalankan kewajiban kami untukmenyelesaikan dan memberikan reservasi itu di bawahkontrak yang kami miliki dengan Anda.</p>\r\n\r\n<p>• Terkait dengan tujuan C-H, kami bergantung pada kepentinganyang resmi: Kami menggunakan data Anda untukkepentingan yang sah, seperti memberi Anda konten yang sesuai untuk situs, email dan newsletter, untuk memperbaikidan mempromosikan produk, konten dan layanan di situs kami, dan untuk tujuan administratif, pendeteksi penipuandan hukum. Saat menggunakan data pribadi untuk tujuanresmi, kami akan selalu menyeimbangkan hak dan kepentingan Anda dalam melindungi informasi Anda terhadap hak dan kepentingan kami.</p>\r\n\r\n<p>• Terkait dengan tujuan H, kami juga bergantung pada, apabiladapat diterapkan, pada kewajiban kami untuk tunduk pada hukum yang berlaku.</p>\r\n\r\n<p>• Apabila diperlukan, di bawah hukum yang berlaku, kami akanmeminta persetujuan Anda sebelum memproses data pribadiuntuk tujuan pemasaran langsung.</p>\r\n\r\n<p>Apabila diperlukan, sesuai dengan hukum yang berlaku, kami akan meminta persetujuan Anda. Anda dapat mencabutpersetujuan ini kapan saja dengan menghubungi kami pada alamat manapun sebagaimana tertera pada akhir PernyataanPrivasi ini.</p>\r\n\r\n<p>Jika Anda ingin menolak pemprosesan yang ditetapkan di bawahC-F dan tidak ada mekanisme pengecualian tersedia untuk Anda secara langsung (misalnya dalam pengaturan akun Anda), sejauhyang berlaku, silakan hubungi <a href=\"mailto:reserve@ottenville.com\">villatel@salsebandung.com </a>.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Pengiriman Data Internasional</p>\r\n\r\n<p>Transmisi data pribadi seperti yang dijelaskan dalam PernyataanPrivasi ini dapat mencakup transfer data pribadi ke luar negeri ke negara-negara yang undang-undang perlindungan datanyatidak sekomprehensif negara-negara di Uni Eropa. Jikadiwajibkan oleh hukum Eropa, kami hanya akan mentransferdata pribadi ke penerima yang menawarkan tingkatperlindungan data yang memadai. Dalam situasi ini, kami membuat pengaturan kontrak untuk memastikan bahwa data pribadi Anda masih dilindungi sesuai dengan standar Eropa. Anda dapat meminta kami untuk melihat salinan klausul terkaitmenggunakan detail kontak di bawah ini.</p>\r\n\r\n<p>Keamanan</p>\r\n\r\n<p>BookingSuite berpaku pada prosedur yang wajar untukmencegah akses yang tidak sah, dan penyalahgunaan, informasitermasuk data pribadi. Kami menggunakan sistem dan prosedurbisnis yang tepat untuk melindungi dan menjaga informasitermasuk data pribadi. Kami juga menggunakan prosedurkeamanan dan pembatasan teknis dan fisik untuk mengakses dan menggunakan data pribadi di server kami. Hanya personel yang berwenang yang diizinkan untuk mengakses data pribadi dalampekerjaan mereka.</p>\r\n\r\n<p>Penyimpanan Data</p>\r\n\r\n<p>Kami akan menyimpan informasi Anda, yang mungkin termasukdata pribadi, selama kami anggap perlu untuk memberikanlayanan kepada Anda, mematuhi hukum yang berlaku, menyelesaikan perselisihan dengan pihak mana pun dan atau, yang diperlukan untuk memungkinkan kami <a name=\"_GoBack\"></a>melakukan bisniskami termasuk untuk mendeteksi dan mencegah penipuan ataukegiatan ilegal lainnya. Semua data pribadi yang kami simpanakan tunduk pada Pernyataan Privasi ini. Jika Anda memilikipertanyaan tentang periode retensi tertentu untuk jenis data pribadi tertentu yang kami proses tentang Anda, silakan hubungikami melalui rincian kontak yang disertakan di bawah ini.</p>\r\n\r\n<p>Pilihan dan Hak Anda</p>\r\n\r\n<p>Kami ingin Anda mengendalikan bagaimana data pribadi Anda digunakan oleh kami. Anda dapat melakukan ini dengan caraberikut:</p>\r\n\r\n<p>• Anda dapat meminta salinan data pribadi yang kami milikitentang Anda;</p>\r\n\r\n<p>• Anda dapat memberi tahu kami tentang perubahan apa pun terhadap data pribadi Anda, atau meminta kami untukmemperbaiki data pribadi apa pun yang kami miliki tentangAnda;</p>\r\n\r\n<p>• dalam situasi tertentu, Anda dapat meminta kami untukmenghapus atau memblokir atau membatasi pemrosesan data pribadi yang kami miliki tentang Anda, atau menolak caratertentu di mana kami menggunakan data pribadi Anda; dan</p>\r\n\r\n<p>• dalam situasi tertentu, Anda juga dapat meminta kami untukmengirim data pribadi yang telah Anda berikan kepada kami kepada pihak ketiga.</p>\r\n\r\n<p>Jika kami menggunakan data pribadi Anda atas dasarpersetujuan Anda, Anda berhak untuk menarik persetujuantersebut kapan saja dengan tunduk pada hukum yang berlaku. Selain itu, ketika kami memproses data pribadi Anda berdasarkan minat yang sah atau kepentingan publik, Anda memiliki hak untuk menolak setiap saat atas penggunaan data pribadi Anda itu tunduk pada hukum yang berlaku.</p>\r\n\r\n<p>Kami mengandalkan Anda untuk memastikan bahwa data pribadi Anda lengkap, akurat, dan terkini. Harap segerainformasikan kepada kami setiap perubahan atauketidakakuratan data pribadi Anda denganmenghubungi <a href=\"mailto:reserve@ottenville.com\">villatel@salsebandung.com </a>. Kami akanmenangani permintaan Anda sesuai dengan hukum yang berlaku.</p>\r\n\r\n<p>Pertanyaan atau Keluhan</p>\r\n\r\n<p>Jika Anda memiliki pertanyaan atau kekhawatiran tentangpemprosesan data pribadi Anda, atau jika Anda inginmenggunakan hak yang dimiliki berdasarkan pemberitahuan ini, silakan menghubungi kami melalui <a href=\"mailto:reserve@ottenville.com\">villatel@salsebandung.com </a>. Anda juga dapat menghubungi otoritas perlindungan data setempat dengan pertanyaan dan keluhan.</p>\r\n\r\n<p>Perubahan pada Pemberitahuan</p>\r\n\r\n<p>Sama seperti bisnis kami yang berubah secara konstan, Pernyataan Privasi ini juga dapat berubah dari waktu ke waktu. Jika Anda ingin melihat perubahan yang dibuat pada PernyataanPrivasi ini dari waktu ke waktu, kami mengundang Anda untukmengakses Pernyataan Privasi ini untuk melihat perubahannya. Jika kami membuat perubahan atau perubahan material yang akan berdampak pada Anda (misalnya ketika kami mulaimemproses data pribadi Anda untuk tujuan lain dari yang ditetapkan di atas), kami akan menghubungi Anda sebelummemulai pemprosesan tsb.</p>\r\n', NULL, 'page', '', 0, 1, 13, 1563677945, 1580286803, 0, 0, 0),
(3, 3, 'يذكر القانونية', 'يذكر القانونية', '', 'يذكر القانونية', 'legal-notices', '', 'index,follow', '', '', '', NULL, 'page', '', 0, 1, 13, 1563677945, 1566365894, 0, 0, 0),
(3, 4, 'Ketentuan Pengguna', 'Ketentuan Pengguna', '', 'Ketentuan Pengguna', 'ketentuan-pengguna', '', 'index,follow', '', '', '<p><strong>Kebijakan Privasi</strong></p>\r\n\r\n<p>Kebijakan Privasi ini berlaku untuk salsebandung.com dimilikidan dioperasikan oleh VillaTel Salse. Kebijakan Privasi inimendeskripsikan bagaimana kami mengumpulkan data, yang mungkin termasuk denga data pribadi, yang Anda berikankepada situs kami: [salsebandung.com]. Ini juga menjelaskanpilihan yang tersedia berkaitan dengan penggunaan data personal Anda oleh kami dan bagaimana Anda dapat mengaksesdan memperbarui data ini.</p>\r\n\r\n<p><b>Pengambilan Data</b></p>\r\n\r\n<p>Tipe informasi pribadi yang kami kumpulkan termasuk:</p>\r\n\r\n<p>• Nama Lengkap, alamat email, nomor telepon dan alamatrumah;</p>\r\n\r\n<p>• Rincian kartu kredit (tipe kartu, nomor kartu kredit, nama pada kartu, tanggal berakhir, dan kode keamanan);</p>\r\n\r\n<p>• Informasi menginap tamu, termasuk tanggal kedatangan dan keberangkatan, permintaan khusus yang ada, hasilpengamatan tentang preferensi layanan Anda (termasukpreferensi kamar, fasilitas atau layanan lain yang digunakan);</p>\r\n\r\n<p>• Informasi yang Anda berikan terkait dengan preferensipemasaran Anda atau dalam rangka partisipasi dalam survei, kontes atau penawaran promosi;</p>\r\n\r\n<p>Anda selalu dapat memilih data pribadi apa asaja (apabila ada) yang ingin Anda berikan pada kami. Namun apabila Anda memilih untuk tidak berbagi beberapa rincian tertentu, beberapatransaksi dengan kami mungkin akan terkena dampaknya.</p>\r\n\r\n<p>Data yang kami kumpulkan secara otomatis</p>\r\n\r\n<p>Saat menggunakan situs kami, kami juga mengumpulkan data secara otomatis, beberapa mungkinberupa data pribadi. Hal initermasuk data seperti pengaturan bahasa, alamat IP< lokasi, pengaturan perangkat, sistem operasi, informaaaasi log, waktupenggunaa, URL yang diminta, laporan status, agen pengguna(informasi tentang versi browser), sistem operasi, hasil (yang dilihat dan pemesan), sejarah penjelajahan (browsing history), BookingID pengguna, dan tipe data yang dilihat. Kami juga mengumpulkan data secara otomatis melalui cookies..</p>\r\n\r\n<p>Tujuan Pengolahan</p>\r\n\r\n<p>Kami menggunakan data pribadi Anda untuk tujuan berikut:</p>\r\n\r\n<p>• A. Reservasi: kami menggunakan data pribadi Anda untukmenyelesaikan dan mengelola reservasi online Anda.</p>\r\n\r\n<p>• B. Customer service: Kami menggunakan data pribadi Anda untuk memberikan customer service.</p>\r\n\r\n<p>• C. Ulasan tamu: Kami mungkin akan menggunakan data kontak untuk mengundang Anda via email agar menulisulasan setelah Anda selesai menginap. hal ini dapatmembantu traveler lainnya untuk memilih akomodasi yang sesuai dengan selera mereka. Apabila mengirimkan ulasantamu, ulasan Anda mungkin akan ditampilkan di situs kami.</p>\r\n\r\n<p>• D. Aktivitas marketing: Kami juga akan menggunakan data yang Anda berika untuk keperluan marketing, sejauhdiperbolehkan oleh hukum yang berlaku. Apabila kami menggunakan data pribadi Anda untuk tujuan pemasaranlangsung, seperti newsletter komersil dan komunikasimarketing untuk produk dan layanan baru atau penawaranlain yang kami anggap mungkin menarik untuk Anda, kami juga akan mengikutsertakan link berhenti berlangganan yang dapat Anda gunakan bila Anda tidak ingin kami mengirimkan pesan di masa mendatang.</p>\r\n\r\n<p>• E. Komunikasi lainnya: Akan ada saatnya kami menghubungiAnda melalui email, pos, dengan telepon atau dengan SMS, tergantung dengan data kontak yang Anda berikan kepadakami. Beberapa alasan untuk hal ini:</p>\r\n\r\n<p>o a. kami mungkin perlu merespon dan menganganipermintaan yang telah Anda buat.<br />\r\nb. Apabila Anda belum menfinalisasi reservasi online, kami mungkin akan mengirimkan email pengingat untukmelanjutkan reservasi Anda. Kami percaya bahwalayanan tambahan ini berguna untuk Anda, karena akanmemungkinkan Anda melanjutkan reservasi tanpa perluamencari akomodasi lagi atau mengisi detail reservasidari awal. <br />\r\nc. Saat Anda menggunakan layanan kami, kami mungkinakan mengirimkan kuesioner atau mengundang Anda untuk memberikan ulasan tentang pengalaman Anda di situs kami. Kami percaya bahwa layanan tambahan iniberguna untuk kita semua, karena kami dapatmemperbaiki situs kami berdasarkan masukan Anda.</p>\r\n\r\n<p>• F. Analisis, perbaikan dan riset: Kami menggunakan data pribadi untuk melakukan riset dan analisis. kami mungkinakan menggubnakan pihak ketiga untuk melakukan ini. Kami mungkin akan berbagi hasil riset terkait, termasuk kepadapihak ketiga, dalam bentuk agregat anonim. Kami menggunakan data pribadi Anda untuk tujuan analitis, untukmemperbaiki layanan kami, untuk memperkaya pengalamanpengguna, dan untuk memperbaiki fungsi dan kualitaslayanan travel online kami.</p>\r\n\r\n<p>• G. Keamanan, mendeteksi dan mencegah penipuan: Kami menggunakan informasi, yang mungkin termasuk data pribadi, untuk mencegah penipuan dan aktivitas ilegal dan melanggar hukum lainnya. Kami juga menggunakan informsiini untuk menginvestigasi dan mendeteksi penipuan. Kami juga menggunakan data pribadi untuk menaksir resiko dan tujuan keamanan, termasuk otentifikasi pengguna. Untuktujuan tersebut diatas, data pribadi mungkin akan dibagikankepada pihak ketiga, seperti aparat penegak hukumsebagaimana diatur oleh hukum yang berlaku dan penasihatdi luar institusi.</p>\r\n\r\n<p>• H. Hukum dan kepatuhan: Dalam kasus tertentu, kami perlumenggunakan informasi yang diberikan, yang mungkintermasuk data pribadi, untuk menangani dan menyelesaikanperselisihan hukum atau komplain, untuk penyelidikanperaturan dan kepatuhan, atau untuk menjalankan perjanjianatau untuk memenuhi permintaan hukum dari aparat penegakhukum selama diwajibkan oleh hukum yang berlaku.</p>\r\n\r\n<p>• Apabila kami menggunakan cara-cara otomatis untukmemproses data pribadi yang akan mengakibatkan persoalanhukum atau secara signifikan berpengaruh pada Anda, kami akan menerapkan langkah-langkah yang sesuai untukmelindungi hak dan kebebasan Anda, termasuk hak untukmendapatkan intervensi manusia.</p>\r\n\r\n<p>Basis Hukum</p>\r\n\r\n<p>• Terkait dengan tujuan A dan B, kami bergantung pada kinerjakontrak: Penggunaan data Anda mungkin diperlukan untukmenjalankan kontrak yang Anda miliki dengan kami. Misalnya, jika Anda menggunakan layanan kami untukmembuat reservasi online, kami akan menggunakan data Anda untuk menjalankan kewajiban kami untukmenyelesaikan dan memberikan reservasi itu di bawahkontrak yang kami miliki dengan Anda.</p>\r\n\r\n<p>• Terkait dengan tujuan C-H, kami bergantung pada kepentinganyang resmi: Kami menggunakan data Anda untukkepentingan yang sah, seperti memberi Anda konten yang sesuai untuk situs, email dan newsletter, untuk memperbaikidan mempromosikan produk, konten dan layanan di situs kami, dan untuk tujuan administratif, pendeteksi penipuandan hukum. Saat menggunakan data pribadi untuk tujuanresmi, kami akan selalu menyeimbangkan hak dan kepentingan Anda dalam melindungi informasi Anda terhadap hak dan kepentingan kami.</p>\r\n\r\n<p>• Terkait dengan tujuan H, kami juga bergantung pada, apabiladapat diterapkan, pada kewajiban kami untuk tunduk pada hukum yang berlaku.</p>\r\n\r\n<p>• Apabila diperlukan, di bawah hukum yang berlaku, kami akanmeminta persetujuan Anda sebelum memproses data pribadiuntuk tujuan pemasaran langsung.</p>\r\n\r\n<p>Apabila diperlukan, sesuai dengan hukum yang berlaku, kami akan meminta persetujuan Anda. Anda dapat mencabutpersetujuan ini kapan saja dengan menghubungi kami pada alamat manapun sebagaimana tertera pada akhir PernyataanPrivasi ini.</p>\r\n\r\n<p>Jika Anda ingin menolak pemprosesan yang ditetapkan di bawahC-F dan tidak ada mekanisme pengecualian tersedia untuk Anda secara langsung (misalnya dalam pengaturan akun Anda), sejauhyang berlaku, silakan hubungi <a href=\"mailto:reserve@ottenville.com\">villatel@salsebandung.com </a>.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Pengiriman Data Internasional</p>\r\n\r\n<p>Transmisi data pribadi seperti yang dijelaskan dalam PernyataanPrivasi ini dapat mencakup transfer data pribadi ke luar negeri ke negara-negara yang undang-undang perlindungan datanyatidak sekomprehensif negara-negara di Uni Eropa. Jikadiwajibkan oleh hukum Eropa, kami hanya akan mentransferdata pribadi ke penerima yang menawarkan tingkatperlindungan data yang memadai. Dalam situasi ini, kami membuat pengaturan kontrak untuk memastikan bahwa data pribadi Anda masih dilindungi sesuai dengan standar Eropa. Anda dapat meminta kami untuk melihat salinan klausul terkaitmenggunakan detail kontak di bawah ini.</p>\r\n\r\n<p>Keamanan</p>\r\n\r\n<p>BookingSuite berpaku pada prosedur yang wajar untukmencegah akses yang tidak sah, dan penyalahgunaan, informasitermasuk data pribadi. Kami menggunakan sistem dan prosedurbisnis yang tepat untuk melindungi dan menjaga informasitermasuk data pribadi. Kami juga menggunakan prosedurkeamanan dan pembatasan teknis dan fisik untuk mengakses dan menggunakan data pribadi di server kami. Hanya personel yang berwenang yang diizinkan untuk mengakses data pribadi dalampekerjaan mereka.</p>\r\n\r\n<p>Penyimpanan Data</p>\r\n\r\n<p>Kami akan menyimpan informasi Anda, yang mungkin termasukdata pribadi, selama kami anggap perlu untuk memberikanlayanan kepada Anda, mematuhi hukum yang berlaku, menyelesaikan perselisihan dengan pihak mana pun dan atau, yang diperlukan untuk memungkinkan kami <a name=\"_GoBack\"></a>melakukan bisniskami termasuk untuk mendeteksi dan mencegah penipuan ataukegiatan ilegal lainnya. Semua data pribadi yang kami simpanakan tunduk pada Pernyataan Privasi ini. Jika Anda memilikipertanyaan tentang periode retensi tertentu untuk jenis data pribadi tertentu yang kami proses tentang Anda, silakan hubungikami melalui rincian kontak yang disertakan di bawah ini.</p>\r\n\r\n<p>Pilihan dan Hak Anda</p>\r\n\r\n<p>Kami ingin Anda mengendalikan bagaimana data pribadi Anda digunakan oleh kami. Anda dapat melakukan ini dengan caraberikut:</p>\r\n\r\n<p>• Anda dapat meminta salinan data pribadi yang kami milikitentang Anda;</p>\r\n\r\n<p>• Anda dapat memberi tahu kami tentang perubahan apa pun terhadap data pribadi Anda, atau meminta kami untukmemperbaiki data pribadi apa pun yang kami miliki tentangAnda;</p>\r\n\r\n<p>• dalam situasi tertentu, Anda dapat meminta kami untukmenghapus atau memblokir atau membatasi pemrosesan data pribadi yang kami miliki tentang Anda, atau menolak caratertentu di mana kami menggunakan data pribadi Anda; dan</p>\r\n\r\n<p>• dalam situasi tertentu, Anda juga dapat meminta kami untukmengirim data pribadi yang telah Anda berikan kepada kami kepada pihak ketiga.</p>\r\n\r\n<p>Jika kami menggunakan data pribadi Anda atas dasarpersetujuan Anda, Anda berhak untuk menarik persetujuantersebut kapan saja dengan tunduk pada hukum yang berlaku. Selain itu, ketika kami memproses data pribadi Anda berdasarkan minat yang sah atau kepentingan publik, Anda memiliki hak untuk menolak setiap saat atas penggunaan data pribadi Anda itu tunduk pada hukum yang berlaku.</p>\r\n\r\n<p>Kami mengandalkan Anda untuk memastikan bahwa data pribadi Anda lengkap, akurat, dan terkini. Harap segerainformasikan kepada kami setiap perubahan atauketidakakuratan data pribadi Anda denganmenghubungi <a href=\"mailto:reserve@ottenville.com\">villatel@salsebandung.com </a>. Kami akanmenangani permintaan Anda sesuai dengan hukum yang berlaku.</p>\r\n\r\n<p>Pertanyaan atau Keluhan</p>\r\n\r\n<p>Jika Anda memiliki pertanyaan atau kekhawatiran tentangpemprosesan data pribadi Anda, atau jika Anda inginmenggunakan hak yang dimiliki berdasarkan pemberitahuan ini, silakan menghubungi kami melalui <a href=\"mailto:reserve@ottenville.com\">villatel@salsebandung.com </a>. Anda juga dapat menghubungi otoritas perlindungan data setempat dengan pertanyaan dan keluhan.</p>\r\n\r\n<p>Perubahan pada Pemberitahuan</p>\r\n\r\n<p>Sama seperti bisnis kami yang berubah secara konstan, Pernyataan Privasi ini juga dapat berubah dari waktu ke waktu. Jika Anda ingin melihat perubahan yang dibuat pada PernyataanPrivasi ini dari waktu ke waktu, kami mengundang Anda untukmengakses Pernyataan Privasi ini untuk melihat perubahannya. Jika kami membuat perubahan atau perubahan material yang akan berdampak pada Anda (misalnya ketika kami mulaimemproses data pribadi Anda untuk tujuan lain dari yang ditetapkan di atas), kami akan menghubungi Anda sebelummemulai pemprosesan tsb.</p>\r\n', NULL, 'page', '', 0, 1, 13, 1563677945, 1580286803, 0, 0, 0),
(4, 1, 'Plan du site', 'Plan du site', '', 'Plan du site', 'plan-site', '', 'index,follow', '', '', '', NULL, 'sitemap', '', 0, 1, 15, 1563677945, 1563677945, 0, 0, 0),
(4, 2, 'Sitemap', 'Sitemap', '', 'Sitemap', 'sitemap', '', 'index,follow', '', '', '', NULL, 'sitemap', '', 0, 1, 15, 1563677945, 1580286560, 0, 0, 0),
(4, 3, 'خريطة الموقع', 'خريطة الموقع', '', 'خريطة الموقع', 'sitemap', '', 'index,follow', '', '', '', NULL, 'sitemap', '', 0, 1, 15, 1563677945, 1563677945, 0, 0, 0),
(4, 4, 'Situsmap', 'Situsmap', '', 'Situsmap', 'situsmap', '', 'index,follow', '', '', '', NULL, 'sitemap', '', 0, 1, 15, 1563677945, 1580286560, 0, 0, 0),
(5, 1, 'Qui sommes-nous ?', 'Qui sommes-nous ?', '', 'Qui sommes-nous ?', 'qui-sommes-nous', '', 'index,follow', '', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla vel est at rhoncus. Cras porttitor ligula vel magna vehicula accumsan. Mauris eget elit et sem commodo interdum. Aenean dolor sem, tincidunt ac neque tempus, hendrerit blandit lacus. Vivamus placerat nulla in mi tristique, fringilla fermentum nisl vehicula. Nullam quis eros non magna tincidunt interdum ac eu eros. Morbi malesuada pulvinar ultrices. Etiam bibendum efficitur risus, sit amet venenatis urna ullamcorper non. Proin fermentum malesuada tortor, vitae mattis sem scelerisque in. Curabitur rutrum leo at mi efficitur suscipit. Vivamus tristique lorem eros, sit amet malesuada augue sodales sed.</p>\r\n', NULL, 'page', 'article', 0, 1, 2, 1563677945, 1580180621, 0, 0, 0),
(5, 2, 'About us', 'About us', '', 'About us', 'about-us', '', 'index,follow', '', '', '<p style=\"text-align: justify;\"><span style=\"font-size:16px;\"><span style=\"font-size:18px;\"></span><span style=\"font-size:16px;\"><span style=\"font-size:14px;\"></span><span style=\"font-size:16px;\"><span style=\"font-size:16px;\"><span style=\"font-size:14px;\"><span style=\"font-size:14px;\"><span style=\"font-size:16px;\"><span style=\"font-size:14px;\">Relax = salse! Roughly translated as ‘to unwind’. Salse is a moment when the stillness of time is embraced. An arduous design phase with Baskoro Tedjo and Hepta as the architects, Andre Gunarsa Kidarse and Ruang Hijau as landscape designers, has resulted in a thoughtful resort, a place to mend uneasiness, to leave weariness behind, to calm emotions and to spread love. The main keyword is energizing. <span data-cke-marker=\"1\"> </span></span></span></span></span></span></p>\r\n\r\n<p> </p>\r\n', NULL, 'page', 'article', 0, 1, 2, 1563677945, 1599719166, 0, 0, 0),
(5, 3, 'معلومات عنا', 'معلومات عنا', '', 'معلومات عنا', 'about-us', '', 'index,follow', '', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla vel est at rhoncus. Cras porttitor ligula vel magna vehicula accumsan. Mauris eget elit et sem commodo interdum. Aenean dolor sem, tincidunt ac neque tempus, hendrerit blandit lacus. Vivamus placerat nulla in mi tristique, fringilla fermentum nisl vehicula. Nullam quis eros non magna tincidunt interdum ac eu eros. Morbi malesuada pulvinar ultrices. Etiam bibendum efficitur risus, sit amet venenatis urna ullamcorper non. Proin fermentum malesuada tortor, vitae mattis sem scelerisque in. Curabitur rutrum leo at mi efficitur suscipit. Vivamus tristique lorem eros, sit amet malesuada augue sodales sed.</p>\r\n', NULL, 'page', 'article', 0, 1, 2, 1563677945, 1580180621, 0, 0, 0),
(5, 4, 'Tentang Kami', 'Tentang Kami', '', 'Tentang Kami', 'tentang-kami', '', 'index,follow', '', '', '<p style=\"text-align: justify;\"><span style=\"font-size:18px;\"><span style=\"font-size:16px;\"><span style=\"font-size:18px;\"><span style=\"font-size:16px;\"><span style=\"font-size:14px;\">Rileks = Salse! Salse adalah istirahat, sebuah suasana ketika jam berhenti sejenak. Dirancang cukup lama, melibatkan Baskoro Tedjo dan Hepta sebagai Arsitek, Andre Gunarsa Kidarse dan Ruang Hijau sebagai desainer lanskap. Resort ini dirancang sebagai tempat untuk mengobati keresahan, menghilangkan keletihan, meredakan amarah dan menumbuhkan cinta. ENERGIZING, kata kuncinya.</span></span></span></p>\r\n\r\n<p> </p>\r\n', NULL, 'page', 'article', 0, 1, 2, 1563677945, 1599719166, 0, 0, 0),
(6, 1, 'Recherche', 'Recherche', '', 'Recherche', 'search', '', 'noindex,nofollow', '', '', '', NULL, 'search', '', 0, 1, 16, 1563677945, 1563677945, 0, 0, 1),
(6, 2, 'Search', 'Search', '', 'Search', 'search', '', 'noindex,nofollow', '', '', '', NULL, 'search', '', 0, 1, 16, 1563677945, 1580286500, 0, 0, 1),
(6, 3, 'بحث', 'بحث', '', 'بحث', 'search', '', 'noindex,nofollow', '', '', '', NULL, 'search', '', 0, 1, 16, 1563677945, 1563677945, 0, 0, 1),
(6, 4, 'Pencarian', 'Pencarian', '', 'Pencarian', 'pencarian', '', 'noindex,nofollow', '', '', '', NULL, 'search', '', 0, 1, 16, 1563677945, 1580286500, 0, 0, 1),
(7, 1, 'Galerie', 'Galerie', '', 'Galerie', 'galerie', '', 'index,follow', '', '', '', NULL, 'page', 'gallery', 0, 1, 5, 1563677945, 1563677945, 0, 0, 0),
(7, 2, 'Gallery', 'Gallery', '', 'Gallery', 'gallery', '', 'index,follow', '', '', '', NULL, 'page', 'gallery', 0, 1, 5, 1563677945, 1580286376, 0, 0, 0),
(7, 3, 'صور معرض', 'صور معرض', '', 'صور معرض', 'gallery', '', 'index,follow', '', '', '', NULL, 'page', 'gallery', 0, 1, 5, 1563677945, 1563677945, 0, 0, 0),
(7, 4, 'Galeri', 'Galeri', '', 'Galeri', 'galeri', '', 'index,follow', '', '', '', NULL, 'page', 'gallery', 0, 1, 5, 1563677945, 1580286376, 0, 0, 0),
(8, 1, '404', 'Erreur 404 : Page introuvable !', '', '404 Page introuvable', '404', '', 'noindex,nofollow', '', '', '<p>L\'URL demandée n\'a pas été trouvée sur ce serveur.<br />\r\nLa page que vous voulez afficher n\'existe pas, ou est temporairement indisponible.</p>\r\n\r\n<p>Merci d\'essayer les actions suivantes :</p>\r\n\r\n<ul>\r\n    <li>Assurez-vous que l\'URL dans la barre d\'adresse de votre navigateur est correctement orthographiée et formatée.</li>\r\n    <li>Si vous avez atteint cette page en cliquant sur un lien ou si vous pensez que cela concerne une erreur du serveur, contactez l\'administrateur pour l\'alerter.</li>\r\n</ul>\r\n', NULL, '404', '', 0, 1, 17, 1563677945, 1563677945, 0, 0, 1),
(8, 2, '404', '404 Error: Page not found!', '', '404 Not Found', '404', '', 'noindex,nofollow', '', '', '<p>The wanted URL was not found on this server.<br />\r\nThe page you wish to display does not exist, or is temporarily unavailable.</p>\r\n\r\n<p>Thank you for trying the following actions :</p>\r\n\r\n<ul>\r\n    <li>Be sure the URL in the address bar of your browser is correctly spelt and formated.</li>\r\n    <li>If you reached this page by clicking a link or if you think that it is about an error of the server, contact the administrator to alert him.</li>\r\n</ul>\r\n', NULL, '404', '', 0, 1, 17, 1563677945, 1563677945, 0, 0, 1),
(8, 3, '404', '404 Error: Page not found!', '', '404 Not Found', '404', '', 'noindex,nofollow', '', '', '<p>L\'URL demandée n\'a pas été trouvée sur ce serveur.<br />\r\nLa page que vous voulez afficher n\'existe pas, ou est temporairement indisponible.</p>\r\n\r\n<p>Merci d\'essayer les actions suivantes :</p>\r\n\r\n<ul>\r\n    <li>Assurez-vous que l\'URL dans la barre d\'adresse de votre navigateur est correctement orthographiée et formatée.</li>\r\n    <li>Si vous avez atteint cette page en cliquant sur un lien ou si vous pensez que cela concerne une erreur du serveur, contactez l\'administrateur pour l\'alerter.</li>\r\n</ul>\r\n', NULL, '404', '', 0, 1, 17, 1563677945, 1563677945, 0, 0, 1),
(8, 4, '404', '404 Error: Page not found!', '', '404 Not Found', '404', '', 'noindex,nofollow', '', '', '<p>The wanted URL was not found on this server.<br />\r\nThe page you wish to display does not exist, or is temporarily unavailable.</p>\r\n\r\n<p>Thank you for trying the following actions :</p>\r\n\r\n<ul>\r\n    <li>Be sure the URL in the address bar of your browser is correctly spelt and formated.</li>\r\n    <li>If you reached this page by clicking a link or if you think that it is about an error of the server, contact the administrator to alert him.</li>\r\n</ul>\r\n', NULL, '404', '', 0, 1, 17, 1563677945, 1563677945, 0, 0, 1),
(9, 1, 'Hôtels', 'Hôtels', '', 'Hôtels', 'hotels', '', 'index,follow', '', '', '', NULL, 'hotels', 'hotel', 0, 1, 3, 1563677945, 1563677945, 0, 0, 1),
(9, 2, 'Hotels', 'Hotels', '', 'Hotels', 'hotels', '', 'index,follow', '', '', '', NULL, 'hotels', 'hotel', 0, 1, 3, 1563677945, 1580286294, 0, 0, 1),
(9, 3, 'الفنادق', 'الفنادق', '', 'الفنادق', 'hotels', '', 'index,follow', '', '', '', NULL, 'hotels', 'hotel', 0, 1, 3, 1563677945, 1563677945, 0, 0, 1),
(9, 4, 'Hotel', 'Hotel', '', 'Hotel', 'hotel', '', 'index,follow', '', '', '', NULL, 'hotels', 'hotel', 0, 1, 3, 1563677945, 1580286294, 0, 0, 1),
(10, 1, 'Réserver', 'Réserver', '', 'Réserver', 'reserver', '', 'index,nofollow', '', '', '', NULL, 'booking', 'booking', 0, 1, 6, 1563677945, 1563677945, 0, 0, 1),
(10, 2, 'Booking', 'Booking', '', 'Booking', 'booking', '', 'index,nofollow', '', '', '', NULL, 'booking', 'booking', 0, 1, 6, 1563677945, 1580975467, 0, 0, 1),
(10, 3, 'الحجز', 'الحجز', '', 'الحجز', 'booking', '', 'index,nofollow', '', '', '', NULL, 'booking', 'booking', 0, 1, 6, 1563677945, 1563677945, 0, 0, 1),
(10, 4, 'Pemesanan', 'Pemesanan', '', 'Pemesanan', 'pemesanan', '', 'index,nofollow', '', '', '', NULL, 'booking', 'booking', 0, 1, 6, 1563677945, 1580975467, 0, 0, 1),
(11, 1, 'Coordonnées', 'Coordonnées', '', 'Coordonnées', 'coordonnees', '', 'noindex,nofollow', '', '', '', 10, 'details', '', 0, 1, 9, 1563677945, 1563677945, 0, 0, 1),
(11, 2, 'Details', 'Booking details', '', 'Booking details', 'booking-details', '', 'noindex,nofollow', '', '', '', 10, 'details', '', 0, 1, 9, 1563677945, 1580287053, 0, 0, 1),
(11, 3, 'تفاصيل الحجز', 'تفاصيل الحجز', '', 'تفاصيل الحجز', 'booking-details', '', 'noindex,nofollow', '', '', '', 10, 'details', '', 0, 1, 9, 1563677945, 1563677945, 0, 0, 1),
(11, 4, 'Detail', 'Detail Pemesanan', '', 'Detail Pemesanan', 'detail-pemesanan', '', 'noindex,nofollow', '', '', '', 10, 'details', '', 0, 1, 9, 1563677945, 1580287053, 0, 0, 1),
(12, 1, 'Paiement', 'Paiement', '', 'Paiement', 'paiement', '', 'noindex,nofollow', '', '', '', 13, 'payment', '', 0, 1, 11, 1563677945, 1563677945, 0, 0, 1),
(12, 2, 'Payment', 'Payment', '', 'Payment', 'payment', '', 'noindex,nofollow', '', '', '', 13, 'payment', '', 0, 1, 11, 1563677945, 1580286891, 0, 0, 1),
(12, 3, 'دفع', 'دفع', '', 'دفع', 'payment', '', 'noindex,nofollow', '', '', '', 13, 'payment', '', 0, 1, 11, 1563677945, 1563677945, 0, 0, 1),
(12, 4, 'Pembayaran', 'Pembayaran', '', 'Pembayaran', 'pembayaran', '', 'noindex,nofollow', '', '', '', 13, 'payment', '', 0, 1, 11, 1563677945, 1580286891, 0, 0, 1),
(13, 1, 'Résumé de la réservation', 'Résumé de la réservation', '', 'Résumé de la réservation', 'resume-reservation', '', 'noindex,nofollow', '', '', '', 11, 'summary', '', 0, 1, 10, 1563677945, 1563677945, 0, 0, 1),
(13, 2, 'Summary', 'Booking summary', '', 'Booking summary', 'booking-summary', '', 'noindex,nofollow', '', '', '', 11, 'summary', '', 0, 1, 10, 1563677945, 1580286982, 0, 0, 1),
(13, 3, 'ملخص الحجز', 'ملخص الحجز', '', 'ملخص الحجز', 'booking-summary', '', 'noindex,nofollow', '', '', '', 11, 'summary', '', 0, 1, 10, 1563677945, 1563677945, 0, 0, 1),
(13, 4, 'Ringkasan', 'Ringkasan Pemesanan', '', 'Ringkasan Pemesanan', 'ringkasan-pemesanan', '', 'noindex,nofollow', '', '', '', 11, 'summary', '', 0, 1, 10, 1563677945, 1580286982, 0, 0, 1),
(14, 1, 'Espace client', 'Espace client', '', 'Espace client', 'espace-client', '', 'noindex,nofollow', '', '', '', NULL, 'account', '', 0, 1, 18, 1563677945, 1563677945, 0, 0, 1),
(14, 2, 'Account', 'Account', '', 'Account', 'account', '', 'noindex,nofollow', '', '', '', NULL, 'account', '', 0, 1, 18, 1563677945, 1580370895, 0, 0, 1),
(14, 3, 'Account', 'Account', '', 'Account', 'account', '', 'noindex,nofollow', '', '', '', NULL, 'account', '', 0, 1, 18, 1563677945, 1563677945, 0, 0, 1),
(14, 4, 'Akun', 'Akun', '', 'Akun', 'akun', '', 'noindex,nofollow', '', '', '', NULL, 'account', '', 0, 1, 18, 1563677945, 1580370895, 0, 0, 1),
(15, 1, 'Activités', 'Activités', '', 'Activités', 'reservation-activitees', '', 'noindex,nofollow', '', '', '', 10, 'booking-activities', '', 0, 1, 7, 1563677945, 1563677945, 0, 0, 1),
(15, 2, 'Activities', 'Activities', '', 'Activities', 'booking-activities', '', 'noindex,nofollow', '', '', '', 10, 'booking-activities', '', 0, 1, 7, 1563677945, 1580287179, 0, 0, 1),
(15, 3, 'Activities', 'Activities', '', 'Activities', 'booking-activities', '', 'noindex,nofollow', '', '', '', 10, 'booking-activities', '', 0, 1, 7, 1563677945, 1563677945, 0, 0, 1),
(15, 4, 'Kegiatan', 'Kegiatan', '', 'Kegiatan', 'kegiatan-pemesanan', '', 'noindex,nofollow', '', '', '', 10, 'booking-activities', '', 0, 1, 7, 1563677945, 1580287179, 0, 0, 1),
(16, 1, 'Activités', 'Activités', '', 'Activités', 'activitees', '', 'index,follow', '', '', '', NULL, 'activities', 'activity', 0, 1, 4, 1563677945, 1563677945, 0, 0, 1),
(16, 2, 'Activities', 'Activities', '', 'Activities', 'activities', '', 'index,follow', '', '', '', NULL, 'activities', 'activity', 0, 1, 4, 1563677945, 1580286337, 0, 0, 1),
(16, 3, 'Activities', 'Activities', '', 'Activities', 'activities', '', 'index,follow', '', '', '', NULL, 'activities', 'activity', 0, 1, 4, 1563677945, 1563677945, 0, 0, 1),
(16, 4, 'Kegiatan', 'Kegiatan', '', 'Kegiatan', 'kegiatan', '', 'index,follow', '', '', '', NULL, 'activities', 'activity', 0, 1, 4, 1563677945, 1580286337, 0, 0, 1),
(17, 1, 'Blog', 'Blog', '', 'Blog', 'blog', '', 'index,follow', '', '', '', NULL, 'blog', 'article-blog', 0, 1, 14, 1563677945, 1563677945, 0, 0, 0),
(17, 2, 'Blog', 'Blog', '', 'Blog', 'blog', '', 'index,follow', '', '', '', NULL, 'blog', 'article-blog', 0, 1, 14, 1563677945, 1563677945, 0, 0, 0),
(17, 3, 'مدونة', 'مدونة', '', 'مدونة', 'blog', '', 'index,follow', '', '', '', NULL, 'blog', 'article-blog', 0, 1, 14, 1563677945, 1563677945, 0, 0, 0),
(17, 4, 'Blog', 'Blog', '', 'Blog', 'blog', '', 'index,follow', '', '', '', NULL, 'blog', 'article-blog', 0, 1, 14, 1563677945, 1563677945, 0, 0, 0),
(18, 1, 'Destinations', 'Destinations', '', 'Destinations', 'destinations', '', 'index,follow', '', '', '', NULL, 'destinations', '', 0, 1, 8, 1563677945, 1563677945, 0, 0, 1),
(18, 2, 'Destinations', 'Destinations', '', 'Destinations', 'destinations', '', 'index,follow', '', '', '', NULL, 'destinations', '', 0, 1, 8, 1563677945, 1580287123, 0, 0, 1),
(18, 3, 'وجهات', 'وجهات', '', 'وجهات', 'destinations', '', 'index,follow', '', '', '', NULL, 'destinations', '', 0, 1, 8, 1563677945, 1563677945, 0, 0, 1),
(18, 4, 'Destinations', 'Destinations', '', 'Destinations', 'destinations', '', 'index,follow', '', '', '', NULL, 'destinations', '', 0, 1, 8, 1563677945, 1580287123, 0, 0, 1),
(19, 1, 'Payment', 'Halaman Pembayaran', 'Halaman Pembayaran', 'Halaman Pembayaran', 'payment-method', '', 'index,follow', NULL, 'Halaman Pembayaran', '<p>Halaman Pembayaran</p>\r\n', NULL, 'payment', '', 0, 1, 19, 1564298928, 1564298928, 0, NULL, NULL),
(19, 2, 'Payment', 'Halaman Pembayaran', 'Halaman Pembayaran', 'Halaman Pembayaran', 'payment-method', '', 'index,follow', NULL, 'Halaman Pembayaran', '<p>Halaman Pembayaran</p>\r\n', NULL, 'payment', '', 0, 1, 19, 1564298928, 1580370780, 0, NULL, NULL),
(19, 3, 'Payment', 'Halaman Pembayaran', 'Halaman Pembayaran', 'Halaman Pembayaran', 'payment-method', '', 'index,follow', NULL, 'Halaman Pembayaran', '<p>Halaman Pembayaran</p>\r\n', NULL, 'payment', '', 0, 1, 19, 1564298928, 1564298928, 0, NULL, NULL),
(19, 4, 'Pembayaran', 'Halaman Pembayaran', 'Halaman Pembayaran', 'Halaman Pembayaran', 'payment-method', '', 'index,follow', NULL, 'Halaman Pembayaran', '<p>Halaman Pembayaran</p>\r\n', NULL, 'payment', '', 0, 1, 19, 1564298928, 1580370780, 0, NULL, NULL),
(20, 1, '', '', '', '', '', '', 'noindex,nofollow', NULL, '', '', NULL, 'pembayaran', '', 0, 1, 20, 1564891129, 1564891129, 0, NULL, NULL),
(20, 2, 'Pembayaran', 'Pembayaran', 'Halaman Pembayaran', 'Pembayaran', 'pembayaran', 'Halaman Pembayaran', 'noindex,nofollow', NULL, 'Halaman Pembayaran', '', NULL, 'pembayaran', '', 0, 1, 20, 1564891129, 1564891129, 0, NULL, NULL),
(20, 3, '', '', '', '', '', '', 'noindex,nofollow', NULL, '', '', NULL, 'pembayaran', '', 0, 1, 20, 1564891129, 1564891129, 0, NULL, NULL),
(20, 4, 'Pembayaran', 'Pembayaran', 'Halaman Pembayaran', 'Pembayaran', 'pembayaran', 'Halaman Pembayaran', 'noindex,nofollow', NULL, 'Halaman Pembayaran', '', NULL, 'pembayaran', '', 0, 1, 20, 1564891129, 1564891129, 0, NULL, NULL),
(21, 1, '', '', '', '', '', '', '', NULL, '', '', NULL, 'finish', '', 0, 1, 20, 1569122190, 1569122190, 0, NULL, NULL),
(21, 2, 'Finish', 'Finish', 'Finish', 'Finish', 'finish', '', '', NULL, 'Finish', '', NULL, 'finish', '', 0, 1, 20, 1569122190, 1569122190, 0, NULL, NULL),
(21, 3, '', '', '', '', '', '', '', NULL, '', '', NULL, 'finish', '', 0, 1, 20, 1569122190, 1569122190, 0, NULL, NULL),
(21, 4, 'Finish', 'Finish', 'Finish', 'Finish', 'finish', '', '', NULL, 'Finish', '', NULL, 'finish', '', 0, 1, 20, 1569122190, 1569122190, 0, NULL, NULL),
(22, 1, '', '', '', '', '', '', 'noindex,nofollow', NULL, '', '', NULL, 'thanks', '', 0, 1, 20, 1569122277, 1569122274, 0, NULL, NULL),
(22, 2, 'Thanks', 'Thanks', 'Thanks', 'Thanks', 'thanks', '', 'noindex,nofollow', NULL, 'Thanks', '', NULL, 'thanks', '', 0, 1, 20, 1569122277, 1580370663, 0, NULL, NULL),
(22, 3, '', '', '', '', '', '', 'noindex,nofollow', NULL, '', '', NULL, 'thanks', '', 0, 1, 20, 1569122277, 1569122274, 0, NULL, NULL),
(22, 4, 'Terimakasih', 'Terimakasih', 'Terimakasih', 'Terimakasih', 'terimakasih', '', 'noindex,nofollow', NULL, 'Terimakasih', '', NULL, 'thanks', '', 0, 1, 20, 1569122277, 1580370663, 0, NULL, NULL),
(24, 1, '', '', '', '', '', '', 'noindex,nofollow', NULL, '', '', NULL, 'page', 'gallery', 0, 1, 21, 1570441578, 1580194615, 1, NULL, NULL),
(24, 2, 'Joglo Salse', 'Joglo Salse', 'Resto Joglo Salse', 'Joglo Salse', 'joglo-salse', 'Halaman Pembayaran', 'noindex,nofollow', NULL, 'Joglo Salse', '<p style=\"text-align: center;\"><span style=\"font-size:18px;\"><span style=\"font-size:14px;\"><span style=\"font-size:16px;\">Resto <span style=\"font-size:18px;\"></span>Joglo Salse is not only a place to enjoy great breakfast but also to satisfy the tastebuds with hearty traditional Indonesian</span></span></span> <span style=\"font-size:18px;\"><span style=\"font-size:16px;\">delicacies. Its wide-open space is ideal for gatherings, reunion, meetings, artistic and sports activities.</span></span></span></span></p>\r\n\r\n<p style=\"text-align: center;\"> </p>\r\n\r\n<hr />\r\n<p> </p>\r\n', NULL, 'page', 'gallery', 0, 1, 21, 1570441578, 1580974411, 1, NULL, NULL),
(24, 3, '', '', '', '', '', '', 'noindex,nofollow', NULL, '', '', NULL, 'page', 'gallery', 0, 1, 21, 1570441578, 1580194615, 1, NULL, NULL),
(24, 4, 'Joglo Salse', 'Joglo Salse', 'Resto Joglo Salse', 'Joglo Salse', 'joglo-salse', 'Halaman Pembayaran', 'noindex,nofollow', NULL, 'Joglo Salse', '<p style=\"text-align: center;\"><span style=\"font-size:16px;\"><span style=\"font-size:18px;\"><span style=\"font-size:20px;\"><span style=\"font-size:22px;\"><span style=\"font-size:14px;\"><span style=\"font-size:16px;\"><span style=\"font-size:18px;\"><span style=\"font-size:16px;\">Resto Joglo Salse bukan sekedar tempat untuk menikmati sarapan pagi, namun juga sebagai restoran yang terbuka untuk publik dengan hidangan tradisional Indonesia yang lezat yang dapat memuaskan selera anda. Memiliki ruang terbuka yang luas, sangat ideal untuk acara berkumpul, reuni, pertemuan, kegiatan seni dan olahraga.</span></span></span></span></span></span></span></span></span></p>\r\n\r\n<p style=\"text-align: center;\"> </p>\r\n\r\n<hr />\r\n<p> </p>\r\n', NULL, 'page', 'gallery', 0, 1, 21, 1570441578, 1580974411, 1, NULL, NULL),
(25, 1, '', '', '', '', '', '', 'index,follow', NULL, '', '', NULL, 'page', '', 0, 1, 22, 1570441886, 1580191276, 1, NULL, NULL),
(25, 2, 'COLABOREA', 'COLABOREA', 'Working and Living', 'COLABOREA', 'colaborea-working-and-living', 'Restaurant Nusantara', 'index,follow', NULL, '', '<p style=\"text-align: center;\"><span style=\"font-size:18px;\"><span style=\"font-size:16px;\"> </span> From tech-savy work environment in the coworking space to nature-friendly village with farm and barn, everything connect each other in the compact Co-Working and Co-Living space. <span data-cke-marker=\"1\"> </span></span></p>\r\n\r\n<p style=\"text-align: center;\"> </p>\r\n\r\n<hr />\r\n<p> </p>\r\n', NULL, 'page', 'gallery', 0, 1, 22, 1570441886, 1617182629, 1, NULL, NULL),
(25, 3, '', '', '', '', '', '', 'index,follow', NULL, '', '', NULL, 'page', '', 0, 1, 22, 1570441886, 1580191276, 1, NULL, NULL),
(25, 4, 'Warung Salse', 'Restoran Nusantara', 'Restoran Bandung', 'Warung Salse', 'warung-salse-lembang', 'Restaurant Nusantara', 'index,follow', NULL, 'Warung Salse, adalah restauran bernuasa nusantara', '<p style=\"text-align: center;\"><span style=\"font-size:18px;\"><span style=\"font-size:16px;\">Warung Salse menyediakan pilihan masakan tradisional yang lezat untuk dinikmati setiap pengunjung. Tempat makan yang dikelilingi oleh pemandangan pegunungan hijau yang indah dan sungai kecil yang membentuk kedamaian “zen” dengan suara air yang menabrak bebatuan.</span> <span data-cke-marker=\"1\"> </span></span></p>\r\n\r\n<p style=\"text-align: center;\"> </p>\r\n\r\n<hr />\r\n<p> </p>\r\n', NULL, 'page', 'gallery', 0, 1, 22, 1570441886, 1617182629, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_page_file`
--

CREATE TABLE `pm_page_file` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `home` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 1,
  `rank` int(11) DEFAULT 0,
  `file` varchar(250) DEFAULT NULL,
  `label` varchar(250) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_page_file`
--

INSERT INTO `pm_page_file` (`id`, `lang`, `id_item`, `home`, `checked`, `rank`, `file`, `label`, `type`) VALUES
(1, 1, 25, NULL, 1, 1, 'warungsalse1.jpg', '', 'image'),
(1, 2, 25, NULL, 1, 1, 'warungsalse1.jpg', '', 'image'),
(1, 3, 25, NULL, 1, 1, 'warungsalse1.jpg', '', 'image'),
(1, 4, 25, NULL, 1, 1, 'warungsalse1.jpg', '', 'image'),
(2, 1, 25, 1, 1, 2, 'warungsalse4.jpg', '', 'image'),
(2, 2, 25, 1, 1, 2, 'warungsalse4.jpg', '', 'image'),
(2, 3, 25, 1, 1, 2, 'warungsalse4.jpg', '', 'image'),
(2, 4, 25, 1, 1, 2, 'warungsalse4.jpg', '', 'image'),
(3, 1, 25, NULL, 1, 3, 'warungsalse2.jpg', '', 'image'),
(3, 2, 25, NULL, 1, 3, 'warungsalse2.jpg', '', 'image'),
(3, 3, 25, NULL, 1, 3, 'warungsalse2.jpg', '', 'image'),
(3, 4, 25, NULL, 1, 3, 'warungsalse2.jpg', '', 'image'),
(4, 1, 25, NULL, 1, 4, 'warungsalse3.jpg', '', 'image'),
(4, 2, 25, NULL, 1, 4, 'warungsalse3.jpg', '', 'image'),
(4, 3, 25, NULL, 1, 4, 'warungsalse3.jpg', '', 'image'),
(4, 4, 25, NULL, 1, 4, 'warungsalse3.jpg', '', 'image');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_rate`
--

CREATE TABLE `pm_rate` (
  `id` int(11) NOT NULL,
  `id_room` int(11) NOT NULL,
  `id_hotel` int(11) DEFAULT NULL,
  `id_package` int(11) NOT NULL,
  `users` text DEFAULT NULL,
  `start_date` int(11) DEFAULT NULL,
  `end_date` int(11) DEFAULT NULL,
  `price` double DEFAULT 0,
  `child_price` double DEFAULT 0,
  `discount` double DEFAULT 0,
  `discount_type` varchar(10) DEFAULT 'rate',
  `people` int(11) DEFAULT NULL,
  `price_sup` double DEFAULT NULL,
  `fixed_sup` double DEFAULT NULL,
  `id_tax` int(11) DEFAULT NULL,
  `taxes` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_rate`
--

INSERT INTO `pm_rate` (`id`, `id_room`, `id_hotel`, `id_package`, `users`, `start_date`, `end_date`, `price`, `child_price`, `discount`, `discount_type`, `people`, `price_sup`, `fixed_sup`, `id_tax`, `taxes`) VALUES
(547, 7, 2, 2, '6', 1641340800, 1648684800, 325000, 0, 125000, '', 2, 0, 0, 1, ''),
(551, 1, 1, 2, '6', 1641340800, 1648684800, 550000, 0, 0, '', 2, 0, 0, 1, ''),
(552, 2, 1, 2, '6', 1641340800, 1648684800, 550000, 0, 200000, '', 2, 0, 0, 1, ''),
(553, 3, 1, 2, '6', 1641340800, 1648684800, 650000, 0, 200000, '', 2, 0, 0, 1, ''),
(554, 4, 1, 2, '6', 1641340800, 1648684800, 650000, 0, 200000, '', 2, 0, 0, 1, ''),
(555, 5, 1, 2, '6', 1641340800, 1648684800, 850000, 0, 250000, '', 2, 0, 0, 1, ''),
(556, 6, 1, 2, '6', 1641340800, 1648684800, 850000, 0, 250000, '', 2, 0, 0, 1, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_rate_child`
--

CREATE TABLE `pm_rate_child` (
  `id` int(11) NOT NULL,
  `id_rate` int(11) NOT NULL,
  `min_age` int(11) DEFAULT NULL,
  `max_age` int(11) DEFAULT NULL,
  `price` double DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_room`
--

CREATE TABLE `pm_room` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `id_hotel` int(11) NOT NULL,
  `users` text DEFAULT NULL,
  `max_children` int(11) DEFAULT 1,
  `max_adults` int(11) DEFAULT 1,
  `max_people` int(11) DEFAULT NULL,
  `min_people` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `subtitle` varchar(250) DEFAULT NULL,
  `alias` varchar(100) DEFAULT NULL,
  `descr` longtext DEFAULT NULL,
  `facilities` text DEFAULT NULL,
  `stock` int(11) DEFAULT 1,
  `price` double DEFAULT 0,
  `home` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 0,
  `rank` int(11) DEFAULT 0,
  `start_lock` int(11) DEFAULT NULL,
  `end_lock` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_room`
--

INSERT INTO `pm_room` (`id`, `lang`, `id_hotel`, `users`, `max_children`, `max_adults`, `max_people`, `min_people`, `title`, `subtitle`, `alias`, `descr`, `facilities`, `stock`, `price`, `home`, `checked`, `rank`, `start_lock`, `end_lock`) VALUES
(1, 1, 1, '1', 8, 4, 8, 1, 'Chambre Double Deluxe', 'Petit-déjeuner inclus', 'chambre-double-deluxe', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut eleifend diam. Etiam molestie quam at nunc tempus, ac porttitor ante rutrum. Donec ipsum orci, molestie sit amet nibh a, accumsan varius nisl. Suspendisse blandit efficitur interdum. Nulla auctor tortor eu volutpat imperdiet. Nam at tempus sapien, sit amet porttitor neque. Nam lacinia ex libero, vel egestas ante vehicula nec.</p>\r\n\r\n<p>Sed sed dignissim est. Donec egestas nisl eu congue rhoncus. Nulla finibus malesuada mauris, et pellentesque diam scelerisque non. Duis auctor dapibus augue sed malesuada. Nam placerat at libero quis aliquam. Phasellus quam orci, dapibus sit amet finibus a, convallis volutpat arcu. Nullam condimentum quam id urna scelerisque varius. Duis a metus metus.</p>\r\n', '3,6,8,18,38,27,32', 4, 800000, 1, 1, 1, NULL, NULL),
(1, 2, 1, '1', 1, 2, 2, 1, 'Deluxe Double Bedroom', 'Deluxe With Balcony (Breakfast)', 'deluxe-double-bedroom', '<p>VillaTel Salse is a small conglomerate of Villas located in the North of Bandung surrounded by green area. It is combination of Nature, Design, and Art. It consists of 20, 34 sqr m each, private rooms. Salse is only 200m from Lawangwangi.</p>\r\n\r\n<div class=\"standard-arrow bullet-top\">\r\n<ul>\r\n	<li>Complement Free: Mineral Water, Tea , Sugar, Coffee, Creamer</li>\r\n	<li><em>Tile Floor</em></li>\r\n	<li>Shower</li>\r\n	<li>Water Heater</li>\r\n	<li>Safety Box</li>\r\n	<li>Free WiFi</li>\r\n	<li>LCD TV & TV Cable</li>\r\n	<li>Artwork</li>\r\n</ul>\r\n</div>\r\n', '3,6,8,18,38,25,27,32', 1, 800000, 1, 1, 1, NULL, NULL),
(1, 3, 1, '1', 8, 4, 8, 1, 'Deluxe Double Bedroom', 'Breakfast included', 'deluxe-double-bedroom', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut eleifend diam. Etiam molestie quam at nunc tempus, ac porttitor ante rutrum. Donec ipsum orci, molestie sit amet nibh a, accumsan varius nisl. Suspendisse blandit efficitur interdum. Nulla auctor tortor eu volutpat imperdiet. Nam at tempus sapien, sit amet porttitor neque. Nam lacinia ex libero, vel egestas ante vehicula nec.</p>\r\n\r\n<p>Sed sed dignissim est. Donec egestas nisl eu congue rhoncus. Nulla finibus malesuada mauris, et pellentesque diam scelerisque non. Duis auctor dapibus augue sed malesuada. Nam placerat at libero quis aliquam. Phasellus quam orci, dapibus sit amet finibus a, convallis volutpat arcu. Nullam condimentum quam id urna scelerisque varius. Duis a metus metus.</p>\r\n', '3,6,8,18,38,27,32', 4, 800000, 1, 1, 1, NULL, NULL),
(1, 4, 1, '1', 1, 2, 2, 1, 'Deluxe Double Bedroom', 'Deluxe With Balcon', 'deluxe-double-bedroom', '<p class=\"tw-data-text tw-text-large tw-ta\" data-placeholder=\"Translation\" dir=\"ltr\" id=\"tw-target-text\" style=\"text-align: left;\">VillaTel Salse adalah konglomerat kecil Villas yang terletak di Bandung Utara yang dikelilingi oleh area hijau. Ini adalah kombinasi dari Alam, Desain, dan Seni. Terdiri dari 20, 34 meter persegi, masing-masing, kamar pribadi. Salse hanya berjarak 200 m dari Lawangwangi.</span></p>\r\n\r\n<div class=\"standard-arrow bullet-top\">\r\n<ul>\r\n	<li>Gratis Pelengkap: Air Mineral, Teh, Gula, Kopi, Creamer</li>\r\n	<li><em>Lantai Keramik</em></li>\r\n	<li>Shower</li>\r\n	<li>Pemanasan Air</li>\r\n	<li>Kotak Keselamatan</li>\r\n	<li>WiFi Gratis</li>\r\n	<li>TV LCD & TV Kabel</li>\r\n	<li>Karya seni</li>\r\n</ul>\r\n</div>\r\n', '3,6,8,18,38,25,27,32', 1, 800000, 1, 1, 1, NULL, NULL),
(2, 1, 1, '1', 4, 2, 4, 1, '', '', '', '', '3,5,8,18,38,27,28,32', 4, 800000, 1, 1, 2, NULL, NULL),
(2, 2, 1, '1', 1, 2, 2, 1, 'Deluxe Twin Bedroom', 'Deluxe Twin Bedroom (Breakfast)', 'deluxe-twin-bedroom', '<p>VillaTel Salse is a small conglomerate of Villas located in the North of Bandung surrounded by green area. It is combination of Nature, Design, and Art. It consists of 20, 34 sqr m each, private rooms. Salse is only 200m from Lawangwangi.</p>\r\n\r\n<div class=\"standard-arrow bullet-top\">\r\n<ul>\r\n	<li>Complement Free: Mineral Water, Tea Bags, Sugar, Coffee, Creamer</li>\r\n	<li><em>Tile Floor</em></li>\r\n	<li>Shower</li>\r\n	<li>Water Heater</li>\r\n	<li>Safety Box</li>\r\n	<li>Free WiFi</li>\r\n	<li>LCD TV & TV Cable</li>\r\n	<li>Artwork</li>\r\n</ul>\r\n</div>\r\n', '3,5,8,18,38,27,28,32', 1, 800000, 1, 1, 2, NULL, NULL),
(2, 3, 1, '1', 4, 2, 4, 1, '', '', '', '', '3,5,8,18,38,27,28,32', 4, 800000, 1, 1, 2, NULL, NULL),
(2, 4, 1, '1', 1, 2, 2, 1, 'Deluxe Twin Bedroom', 'Twin Bedroom', 'deluxe-twin-bedroom', '<p>VillaTel Salse adalah konglomerat kecil Villas yang terletak di Bandung Utara yang dikelilingi oleh area hijau. Ini adalah kombinasi dari Alam, Desain, dan Seni. Terdiri dari 20, 34 meter persegi, masing-masing, kamar pribadi. Salse hanya berjarak 200 m dari Lawangwangi.</p>\r\n\r\n<div class=\"standard-arrow bullet-top\">\r\n<ul>\r\n	<li>Pelengkap Gratis: Air Mineral, Teh, Gula, Kopi, Creamer</li>\r\n	<li><em>Lantai Keramik</em></li>\r\n	<li>Shower</li>\r\n	<li>Pemanas Air</li>\r\n	<li>Kotak Keselamatan</li>\r\n	<li>WiFi Gratis</li>\r\n	<li>TV LCD & Kabel TV</li>\r\n	<li>Karya Seni</li>\r\n</ul>\r\n</div>\r\n', '3,5,8,18,38,27,28,32', 1, 800000, 1, 1, 2, NULL, NULL),
(3, 1, 1, '1', 8, 4, 8, 1, '', '', '', '', '3,6,8,13,18,38,27,28,32', 4, 900000, 0, 1, 3, NULL, NULL),
(3, 2, 1, '1', 1, 2, 2, 1, 'Executive Double Bedroom', 'Executive Double Bedroom with Balcone (Breakfast)', 'executive-double-bedroom-balcone', '<p>VillaTel Salse is a small conglomerate of Villas located in the North of Bandung surrounded by green area. It is combination of Nature, Design, and Art. It consists of 20, 34 sqr m each, private rooms. Salse is only 200m from Lawangwangi.</p>\r\n\r\n<div class=\"standard-arrow bullet-top\">\r\n<ul>\r\n	<li>Complement Free: Mineral Water, Tea Bags, Sugar, Coffee, Creamer</li>\r\n	<li><em>Parquet Floor</em></li>\r\n	<li>Shower</li>\r\n	<li>Water Heater</li>\r\n	<li>Safety Box</li>\r\n	<li>Free WiFi</li>\r\n	<li>LCD TV & TV Cable</li>\r\n	<li>Artwork</li>\r\n</ul>\r\n</div>\r\n', '3,6,8,13,18,38,27,28,32', 1, 900000, 0, 1, 3, NULL, NULL),
(3, 3, 1, '1', 8, 4, 8, 1, '', '', '', '', '3,6,8,13,18,38,27,28,32', 4, 900000, 0, 1, 3, NULL, NULL),
(3, 4, 1, '1', 1, 2, 2, 1, 'Executive Double Bedroom', 'Executive Double Bedroom with Balcone', 'executive-double-bedroom-balcone', '<p>VillaTel Salse adalah konglomerat kecil Villas yang terletak di Bandung Utara yang dikelilingi oleh area hijau. Ini adalah kombinasi dari Alam, Desain, dan Seni. Terdiri dari 20, 34 meter persegi, masing-masing, kamar pribadi. Salse hanya berjarak 200 m dari Lawangwangi.</span> </p>\r\n\r\n<div class=\"standard-arrow bullet-top\">\r\n<ul>\r\n	<li>Pelengkap Gratis: Air Mineral, Teh, Gula, Kopi, Creamer</li>\r\n	<li><em>Lantai Parket</em></li>\r\n	<li>Shower</li>\r\n	<li>Pemanas Air</li>\r\n	<li>Kotak Keselamatan</li>\r\n	<li>WiFi Gratis</li>\r\n	<li>TV LCD & Kabel TV</li>\r\n	<li>Karya Seni</li>\r\n</ul>\r\n</div>\r\n', '3,6,8,13,18,38,27,28,32', 1, 900000, 0, 1, 3, NULL, NULL),
(4, 1, 1, '1', 4, 2, 4, 1, '', '', '', '', '3,5,6,8,13,18,38,27,28,32', 4, 900000, 0, 1, 4, NULL, NULL),
(4, 2, 1, '1', 1, 2, 2, 1, 'Executive Twin Bedroom', 'Executive Rooms with Twin bed (Breakfast)', 'executive-twin-bedroom', '<p>VillaTel Salse is a small conglomerate of Villas located in the North of Bandung surrounded by green area. It is combination of Nature, Design, and Art. It consists of 20, 34 sqr m each, private rooms. Salse is only 200m from Lawangwangi.</p>\r\n\r\n<div class=\"standard-arrow bullet-top\">\r\n<ul>\r\n	<li>Complement Free: Mineral Water, Tea Bags, Sugar, Coffee, Creamer</li>\r\n	<li><em>Parquet Floor</em></li>\r\n	<li>Shower</li>\r\n	<li>Water Heater</li>\r\n	<li>Safety Box</li>\r\n	<li>Free WiFi</li>\r\n	<li>LCD TV & TV Cable</li>\r\n	<li>Artwork</li>\r\n</ul>\r\n</div>\r\n', '3,5,6,8,13,18,38,27,28,32', 1, 900000, 0, 1, 4, NULL, NULL),
(4, 3, 1, '1', 4, 2, 4, 1, '', '', '', '', '3,5,6,8,13,18,38,27,28,32', 4, 900000, 0, 1, 4, NULL, NULL),
(4, 4, 1, '1', 1, 2, 2, 1, 'Executive Twin Bedroom', 'Executive Rooms with Twin bed', 'executive-twin-bedroom', '<p></span></p>\r\n\r\n<p>VillaTel Salse adalah konglomerat kecil Villas yang terletak di Bandung Utara yang dikelilingi oleh area hijau. Ini adalah kombinasi dari Alam, Desain, dan Seni. Terdiri dari 20, 34 meter persegi, masing-masing, kamar pribadi. Salse hanya berjarak 200 m dari Lawangwangi.</span> </p>\r\n\r\n<div class=\"standard-arrow bullet-top\">\r\n<ul>\r\n	<li>Pelengkap Gratis: Air Mineral, Teh, Gula, Kopi, Creamer</li>\r\n	<li><em>Lantai Parket</em></li>\r\n	<li>Shower</li>\r\n	<li>Pemanas Air</li>\r\n	<li>Kotak Keselamatan</li>\r\n	<li>WiFi Gratis</li>\r\n	<li>TV LCD & Kabel TV</li>\r\n	<li>Karya Seni</li>\r\n</ul>\r\n</div>\r\n', '3,5,6,8,13,18,38,27,28,32', 1, 900000, 0, 1, 4, NULL, NULL),
(5, 1, 1, '1', 8, 4, 8, 1, '', '', '', '', '3,5,6,8,13,18,27,28,32', 4, 1150000, 0, 1, 5, NULL, NULL),
(5, 2, 1, '1', 1, 2, 2, 1, 'Premium Double Bedroom', 'Premium Double Bedroom With Balcone (Breakfast)', 'premium-double-bedroom-balcone', '<p>VillaTel Salse is a small conglomerate of Villas located in the North of Bandung surrounded by green area. It is combination of Nature, Design, and Art. It consists of 20, 34 sqr m each, private rooms. Salse is only 200m from Lawangwangi.</p>\r\n\r\n<div class=\"standard-arrow bullet-top\">\r\n<ul>\r\n	<li>Complement Free: Mineral Water, Tea Bags, Sugar, Coffee, Creamer</li>\r\n	<li><em>Parquet Floor</em></li>\r\n	<li>Shower</li>\r\n	<li>Bath Tub</li>\r\n	<li>Water Heater</li>\r\n	<li>Mini Refrigerator</li>\r\n	<li>Safety Box</li>\r\n	<li>Free WiFi</li>\r\n	<li>LCD TV & TV Cable</li>\r\n	<li>Artwork</li>\r\n</ul>\r\n</div>\r\n', '3,5,6,8,13,18,27,28,32', 1, 1150000, 0, 1, 5, NULL, NULL),
(5, 3, 1, '1', 8, 4, 8, 1, '', '', '', '', '3,5,6,8,13,18,27,28,32', 4, 1150000, 0, 1, 5, NULL, NULL),
(5, 4, 1, '1', 1, 2, 2, 1, 'Premium Double Bedroom', 'Premium Double Bedroom With Balcone', 'premium-double-bedroom-balcone', '<p>VillaTel Salse adalah konglomerat kecil Villas yang terletak di Bandung Utara yang dikelilingi oleh area hijau. Ini adalah kombinasi dari Alam, Desain, dan Seni. Terdiri dari 20, 34 meter persegi, masing-masing, kamar pribadi. Salse hanya berjarak 200 m dari Lawangwangi.</p>\r\n\r\n<div class=\"standard-arrow bullet-top\">\r\n<ul>\r\n	<li>Pelengkap Gratis: Air Mineral, Teh, Gula, Kopi, Creamer</li>\r\n	<li><em>Lantai Parket</em></li>\r\n	<li>Shower</li>\r\n	<li>Bak Mandi</li>\r\n	<li>Pemanas Air</li>\r\n	<li>Kulkas Mini</li>\r\n	<li>Kotak Keselamatan</li>\r\n	<li>WiFi Gratis</li>\r\n	<li>TV LCD & Kabel TV</li>\r\n	<li>Karya Seni</li>\r\n</ul>\r\n</div>\r\n', '3,5,6,8,13,18,27,28,32', 1, 1150000, 0, 1, 5, NULL, NULL),
(6, 1, 1, '1', 4, 2, 4, 1, '', '', '', '', '3,5,6,8,13,17,18,27,28,32', 4, 1150000, 0, 1, 6, NULL, NULL),
(6, 2, 1, '1', 2, 2, 2, 1, 'Premium Twin Bedroom', 'Premium Room with Twin bed (Breakfast)', 'premium-twin-bedroom', '<p>VillaTel Salse is a small conglomerate of Villas located in the North of Bandung surrounded by green area. It is combination of Nature, Design, and Art. It consists of 20, 42 sqr m each, private rooms. Salse is only 200m from Lawangwangi.</p>\r\n\r\n<div class=\"standard-arrow bullet-top\">\r\n<ul>\r\n	<li>Complement Free: Mineral Water, Tea Bags, Sugar, Coffee, Creamer</li>\r\n	<li><em>Parquet Floor</em></li>\r\n	<li>Shower</li>\r\n	<li>Water Heater</li>\r\n	<li>Mini Refrigerator</li>\r\n	<li>Safety Box</li>\r\n	<li>Free WiFi</li>\r\n	<li>LCD TV & TV Cable</li>\r\n	<li>Artwork</li>\r\n</ul>\r\n</div>\r\n', '3,5,6,8,13,17,18,27,28,32', 1, 1150000, 0, 1, 6, NULL, NULL),
(6, 3, 1, '1', 4, 2, 4, 1, '', '', '', '', '3,5,6,8,13,17,18,27,28,32', 4, 1150000, 0, 1, 6, NULL, NULL),
(6, 4, 1, '1', 2, 2, 2, 1, 'Premium Twin Bedroom', 'Premium Room with Twin bed', 'premium-twin-bedroom', '<p>VillaTel Salse adalah konglomerat kecil Villas yang terletak di Bandung Utara yang dikelilingi oleh area hijau. Ini adalah kombinasi dari Alam, Desain, dan Seni. Ini terdiri dari 20, 42 sqr m masing-masing, kamar pribadi. Salse hanya berjarak 200 m dari Lawangwangi.</p>\r\n\r\n<div class=\"standard-arrow bullet-top\">\r\n<ul>\r\n	<li>Pelengkap Gratis: Air Mineral, Teh, Gula, Kopi, Creamer</li>\r\n	<li><em>Lantai Parket</em></li>\r\n	<li>Shower</li>\r\n	<li>Pemanas Air</li>\r\n	<li>Kulkas Mini</li>\r\n	<li>Kotak Keselamatan</li>\r\n	<li>WiFi Gratis</li>\r\n	<li>TV LCD & TV Kabel</li>\r\n	<li>Karya Seni</li>\r\n</ul>\r\n</div>\r\n', '3,5,6,8,13,17,18,27,28,32', 1, 1150000, 0, 1, 6, NULL, NULL),
(7, 1, 2, '6', 1, 2, 3, 1, '', '', '', '', '5,11,14,18,37,38,27', 8, 500000, 1, 1, 7, NULL, NULL),
(7, 2, 2, '6', 0, 2, 2, 1, 'Villacamp Salse', 'Villacamp (Room Only)', 'villacamp', '<p>As an extension of VillaTel Salse, the VillaCamp Salse consists of eight rooms in thoughtfully designed stilt houses</span> which are inspired by Central Java’s local culture. Developed in a green rural space, the </span>hilly</span> landscape and a flowing river complete the relaxing atmosphere of the VillaCamp</span> It consists of 8, 16 sqr m each, private rooms. Salse is only 200m from Lawangwangi.</p>\r\n\r\n<ul>\r\n	<li><em>Wooden Floor</em></li>\r\n	<li>Shower</li>\r\n	<li>Water Heater</li>\r\n	<li>Free WiFi</li>\r\n	<li>Bathroom Outside the room</li>\r\n</ul>\r\n', '5,11,14,18,37,38,27', 1, 450000, 1, 1, 7, NULL, NULL),
(7, 3, 2, '6', 1, 2, 3, 1, '', '', '', '', '5,11,14,18,37,38,27', 8, 500000, 1, 1, 7, NULL, NULL),
(7, 4, 2, '6', 0, 2, 2, 1, 'Villacamp Salse', 'Villacamp', 'villacamp', '<p>Sebagai perpanjangan dari VillaTel Salse, VillaCamp Salse terdiri dari delapan kamar di rumah panggung yang dirancang dengan cermat yang terinspirasi oleh budaya lokal Jawa Tengah. Dikembangkan di ruang pedesaan yang hijau, lanskap berbukit, dan sungai yang mengalir melengkapi suasana santai di VillaCamp. Terdiri dari 8, 16 meter persegi, masing-masing, kamar pribadi. Salse hanya berjarak 200 m dari Lawangwangi.</p>\r\n\r\n<ul>\r\n	<li><em>Lantai Kayu</em></li>\r\n	<li>Shower</li>\r\n	<li>Pemanis Air</li>\r\n	<li>WiFi Gratis</li>\r\n	<li>Kamar Mandi diluar Ruangan</li>\r\n</ul>\r\n', '5,11,14,18,37,38,27', 1, 450000, 1, 1, 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_room_closing`
--

CREATE TABLE `pm_room_closing` (
  `id` int(11) NOT NULL,
  `id_room` int(11) NOT NULL,
  `from_date` int(11) DEFAULT NULL,
  `to_date` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_room_closing`
--

INSERT INTO `pm_room_closing` (`id`, `id_room`, `from_date`, `to_date`, `stock`) VALUES
(128, 3, 1646092800, 1672444800, 1),
(129, 4, 1646092800, 1672531200, 1),
(132, 1, 1646092800, 1672444800, 1),
(134, 5, 1646092800, 1672444800, 1),
(146, 7, 1646092800, 1672531200, 1),
(152, 2, 1646092800, 1672444800, 1),
(154, 2, 1642204800, 1646006400, 1),
(162, 6, 1646092800, 1672444800, 1),
(172, 3, 1642204800, 1646006400, 1),
(179, 7, 1641686400, 1646006400, 1),
(180, 1, 1642204800, 1646006400, 1),
(202, 4, 1642809600, 1642896000, 1),
(213, 6, 1642809600, 1642896000, 1),
(214, 5, 1642809600, 1642896000, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_room_file`
--

CREATE TABLE `pm_room_file` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `home` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 1,
  `rank` int(11) DEFAULT 0,
  `file` varchar(250) DEFAULT NULL,
  `label` varchar(250) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_room_file`
--

INSERT INTO `pm_room_file` (`id`, `lang`, `id_item`, `home`, `checked`, `rank`, `file`, `label`, `type`) VALUES
(2, 1, 1, NULL, 1, 1, 'deluxe-with-balcone-2.jpg', '', 'image'),
(2, 2, 1, NULL, 1, 1, 'deluxe-with-balcone-2.jpg', 'Bed Room', 'image'),
(2, 3, 1, NULL, 1, 1, 'deluxe-with-balcone-2.jpg', '', 'image'),
(2, 4, 1, NULL, 1, 1, 'deluxe-with-balcone-2.jpg', 'Bed Room', 'image'),
(3, 1, 1, NULL, 1, 3, 'deluxe-with-balcone-3.jpg', '', 'image'),
(3, 2, 1, NULL, 1, 3, 'deluxe-with-balcone-3.jpg', 'Bath Room', 'image'),
(3, 3, 1, NULL, 1, 3, 'deluxe-with-balcone-3.jpg', '', 'image'),
(3, 4, 1, NULL, 1, 3, 'deluxe-with-balcone-3.jpg', 'Bath Room', 'image'),
(4, 1, 1, NULL, 1, 2, 'deluxe-with-balcone.jpg', '', 'image'),
(4, 2, 1, NULL, 1, 2, 'deluxe-with-balcone.jpg', 'Bed Room', 'image'),
(4, 3, 1, NULL, 1, 2, 'deluxe-with-balcone.jpg', '', 'image'),
(4, 4, 1, NULL, 1, 2, 'deluxe-with-balcone.jpg', 'Bed Room', 'image'),
(5, 1, 3, NULL, 1, 1, 'executive-balcone1.jpg', '', 'image'),
(5, 2, 3, NULL, 1, 1, 'executive-balcone1.jpg', 'Bed Room', 'image'),
(5, 3, 3, NULL, 1, 1, 'executive-balcone1.jpg', '', 'image'),
(5, 4, 3, NULL, 1, 1, 'executive-balcone1.jpg', 'Bed Room', 'image'),
(6, 1, 3, NULL, 1, 2, 'executive-balcone2.jpg', '', 'image'),
(6, 2, 3, NULL, 1, 2, 'executive-balcone2.jpg', 'Bed Room', 'image'),
(6, 3, 3, NULL, 1, 2, 'executive-balcone2.jpg', '', 'image'),
(6, 4, 3, NULL, 1, 2, 'executive-balcone2.jpg', 'Bed Room', 'image'),
(7, 1, 3, NULL, 1, 3, 'executive-balcone3.jpg', '', 'image'),
(7, 2, 3, NULL, 1, 3, 'executive-balcone3.jpg', 'Bed Room', 'image'),
(7, 3, 3, NULL, 1, 3, 'executive-balcone3.jpg', '', 'image'),
(7, 4, 3, NULL, 1, 3, 'executive-balcone3.jpg', 'Bed Room', 'image'),
(8, 1, 3, NULL, 1, 4, 'executive-balcone4.jpg', '', 'image'),
(8, 2, 3, NULL, 1, 4, 'executive-balcone4.jpg', '', 'image'),
(8, 3, 3, NULL, 1, 4, 'executive-balcone4.jpg', '', 'image'),
(8, 4, 3, NULL, 1, 4, 'executive-balcone4.jpg', '', 'image'),
(9, 1, 5, NULL, 1, 3, 'premium-balcone3.jpg', '', 'image'),
(9, 2, 5, NULL, 1, 3, 'premium-balcone3.jpg', 'Bath Room', 'image'),
(9, 3, 5, NULL, 1, 3, 'premium-balcone3.jpg', '', 'image'),
(9, 4, 5, NULL, 1, 3, 'premium-balcone3.jpg', 'Bath Room', 'image'),
(10, 1, 5, NULL, 1, 1, 'premium-balcone1.jpg', '', 'image'),
(10, 2, 5, NULL, 1, 1, 'premium-balcone1.jpg', 'Bed Room', 'image'),
(10, 3, 5, NULL, 1, 1, 'premium-balcone1.jpg', '', 'image'),
(10, 4, 5, NULL, 1, 1, 'premium-balcone1.jpg', 'Bed Room', 'image'),
(11, 1, 5, NULL, 1, 2, 'premium-balcone2.jpg', '', 'image'),
(11, 2, 5, NULL, 1, 2, 'premium-balcone2.jpg', 'Bath Tub', 'image'),
(11, 3, 5, NULL, 1, 2, 'premium-balcone2.jpg', '', 'image'),
(11, 4, 5, NULL, 1, 2, 'premium-balcone2.jpg', 'Bath Tub', 'image'),
(12, 1, 6, NULL, 1, 1, 'premium-twin-bedroom.jpg', '', 'image'),
(12, 2, 6, NULL, 1, 1, 'premium-twin-bedroom.jpg', 'Bed Room', 'image'),
(12, 3, 6, NULL, 1, 1, 'premium-twin-bedroom.jpg', '', 'image'),
(12, 4, 6, NULL, 1, 1, 'premium-twin-bedroom.jpg', 'Bed Room', 'image'),
(15, 1, 4, NULL, 1, 2, 'executive-balcone4.jpg', '', 'image'),
(15, 2, 4, NULL, 1, 2, 'executive-balcone4.jpg', 'Bath Room', 'image'),
(15, 3, 4, NULL, 1, 2, 'executive-balcone4.jpg', '', 'image'),
(15, 4, 4, NULL, 1, 2, 'executive-balcone4.jpg', 'Bath Room', 'image'),
(16, 1, 4, NULL, 1, 1, 'executive-twin.jpg', '', 'image'),
(16, 2, 4, NULL, 1, 1, 'executive-twin.jpg', 'Bed Room', 'image'),
(16, 3, 4, NULL, 1, 1, 'executive-twin.jpg', '', 'image'),
(16, 4, 4, NULL, 1, 1, 'executive-twin.jpg', 'Bed Room', 'image'),
(17, 1, 7, NULL, 1, 3, 'whatsapp-image-2019-12-10-at-17-39-05.jpeg', '', 'image'),
(17, 2, 7, NULL, 1, 3, 'whatsapp-image-2019-12-10-at-17-39-05.jpeg', '', 'image'),
(17, 3, 7, NULL, 1, 3, 'whatsapp-image-2019-12-10-at-17-39-05.jpeg', '', 'image'),
(17, 4, 7, NULL, 1, 3, 'whatsapp-image-2019-12-10-at-17-39-05.jpeg', '', 'image'),
(18, 1, 7, NULL, 1, 6, 'whatsapp-image-2019-12-10-at-17-38-57.jpeg', '', 'image'),
(18, 2, 7, NULL, 1, 6, 'whatsapp-image-2019-12-10-at-17-38-57.jpeg', '', 'image'),
(18, 3, 7, NULL, 1, 6, 'whatsapp-image-2019-12-10-at-17-38-57.jpeg', '', 'image'),
(18, 4, 7, NULL, 1, 6, 'whatsapp-image-2019-12-10-at-17-38-57.jpeg', '', 'image'),
(19, 1, 7, NULL, 1, 4, 'whatsapp-image-2019-12-10-at-17-39-06-1.jpeg', '', 'image'),
(19, 2, 7, NULL, 1, 4, 'whatsapp-image-2019-12-10-at-17-39-06-1.jpeg', '', 'image'),
(19, 3, 7, NULL, 1, 4, 'whatsapp-image-2019-12-10-at-17-39-06-1.jpeg', '', 'image'),
(19, 4, 7, NULL, 1, 4, 'whatsapp-image-2019-12-10-at-17-39-06-1.jpeg', '', 'image'),
(20, 1, 7, NULL, 1, 1, 'whatsapp-image-2019-12-10-at-17-39-00.jpeg', '', 'image'),
(20, 2, 7, NULL, 1, 1, 'whatsapp-image-2019-12-10-at-17-39-00.jpeg', '', 'image'),
(20, 3, 7, NULL, 1, 1, 'whatsapp-image-2019-12-10-at-17-39-00.jpeg', '', 'image'),
(20, 4, 7, NULL, 1, 1, 'whatsapp-image-2019-12-10-at-17-39-00.jpeg', '', 'image'),
(21, 1, 7, NULL, 1, 5, 'whatsapp-image-2019-12-10-at-17-39-06.jpeg', '', 'image'),
(21, 2, 7, NULL, 1, 5, 'whatsapp-image-2019-12-10-at-17-39-06.jpeg', '', 'image'),
(21, 3, 7, NULL, 1, 5, 'whatsapp-image-2019-12-10-at-17-39-06.jpeg', '', 'image'),
(21, 4, 7, NULL, 1, 5, 'whatsapp-image-2019-12-10-at-17-39-06.jpeg', '', 'image'),
(22, 1, 7, NULL, 1, 2, 'whatsapp-image-2019-12-10-at-17-39-05-1.jpeg', '', 'image'),
(22, 2, 7, NULL, 1, 2, 'whatsapp-image-2019-12-10-at-17-39-05-1.jpeg', '', 'image'),
(22, 3, 7, NULL, 1, 2, 'whatsapp-image-2019-12-10-at-17-39-05-1.jpeg', '', 'image'),
(22, 4, 7, NULL, 1, 2, 'whatsapp-image-2019-12-10-at-17-39-05-1.jpeg', '', 'image'),
(23, 1, 7, NULL, 1, 7, 'whatsapp-image-2019-12-10-at-17-39-07.jpeg', '', 'image'),
(23, 2, 7, NULL, 1, 7, 'whatsapp-image-2019-12-10-at-17-39-07.jpeg', '', 'image'),
(23, 3, 7, NULL, 1, 7, 'whatsapp-image-2019-12-10-at-17-39-07.jpeg', '', 'image'),
(23, 4, 7, NULL, 1, 7, 'whatsapp-image-2019-12-10-at-17-39-07.jpeg', '', 'image'),
(24, 1, 2, NULL, 1, 1, 'delux-twin-bed.jpeg', '', 'image'),
(24, 2, 2, NULL, 1, 1, 'delux-twin-bed.jpeg', 'Bed Room', 'image'),
(24, 3, 2, NULL, 1, 1, 'delux-twin-bed.jpeg', '', 'image'),
(24, 4, 2, NULL, 1, 1, 'delux-twin-bed.jpeg', 'Bed Room', 'image'),
(25, 1, 2, NULL, 1, 2, '1159792-16011413200039129179.jpg', '', 'image'),
(25, 2, 2, NULL, 1, 2, '1159792-16011413200039129179.jpg', 'Bed Room', 'image'),
(25, 3, 2, NULL, 1, 2, '1159792-16011413200039129179.jpg', '', 'image'),
(25, 4, 2, NULL, 1, 2, '1159792-16011413200039129179.jpg', 'Bed Room', 'image'),
(26, 1, 2, NULL, 1, 3, 'deluxe-bath-room.jpeg', '', 'image'),
(26, 2, 2, NULL, 1, 3, 'deluxe-bath-room.jpeg', 'Bath Room', 'image'),
(26, 3, 2, NULL, 1, 3, 'deluxe-bath-room.jpeg', '', 'image'),
(26, 4, 2, NULL, 1, 3, 'deluxe-bath-room.jpeg', 'Bath Room', 'image'),
(27, 1, 3, NULL, 1, 5, 'deluxe-bath-room.jpeg', '', 'image'),
(27, 2, 3, NULL, 1, 5, 'deluxe-bath-room.jpeg', 'Bath Room', 'image'),
(27, 3, 3, NULL, 1, 5, 'deluxe-bath-room.jpeg', '', 'image'),
(27, 4, 3, NULL, 1, 5, 'deluxe-bath-room.jpeg', 'Bath Room', 'image'),
(28, 1, 6, NULL, 1, 2, 'bath-room.jpeg', '', 'image'),
(28, 2, 6, NULL, 1, 2, 'bath-room.jpeg', 'Bath Room', 'image'),
(28, 3, 6, NULL, 1, 2, 'bath-room.jpeg', '', 'image'),
(28, 4, 6, NULL, 1, 2, 'bath-room.jpeg', 'Bath Room', 'image');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_room_lock`
--

CREATE TABLE `pm_room_lock` (
  `id` int(11) NOT NULL,
  `id_room` int(11) DEFAULT NULL,
  `from_date` int(11) DEFAULT NULL,
  `to_date` int(11) DEFAULT NULL,
  `add_date` int(11) DEFAULT NULL,
  `sessid` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_room_lock`
--

INSERT INTO `pm_room_lock` (`id`, `id_room`, `from_date`, `to_date`, `add_date`, `sessid`) VALUES
(69, 2, 1584144000, 1584230400, 1583896848, '5e685910b0f6b'),
(70, 4, 1584144000, 1584230400, 1583913191, '5e6898e7a64aa'),
(73, 3, 1584144000, 1584230400, 1583913782, '5e689b36ce34a'),
(74, 3, 1584662400, 1584835200, 1583985408, '5e69b30032161'),
(78, 1, 1592006400, 1592179200, 1592028672, '5ee46e0028c24'),
(79, 1, 1592006400, 1592092800, 1592048338, '5ee4bad22fd34'),
(82, 1, 1592524800, 1592611200, 1592488776, '5eeb734838710'),
(83, 1, 1592438400, 1592524800, 1592501581, '5eeba54dc5941'),
(84, 1, 1593129600, 1593216000, 1592968303, '5ef2c46f5b03e'),
(85, 2, 1593129600, 1593216000, 1592968303, '5ef2c46f5b03e'),
(86, 1, 1593820800, 1593907200, 1593394003, '5ef943535d4b8'),
(90, 2, 1593820800, 1593907200, 1593848730, '5f00339a2150b'),
(91, 2, 1593820800, 1593907200, 1593848767, '5f0033bf58946'),
(93, 3, 1593820800, 1593907200, 1593849191, '5f00356775206'),
(94, 4, 1593820800, 1593907200, 1593849504, '5f0036a005381'),
(96, 3, 1595030400, 1595116800, 1594960653, '5f112b0d1301c'),
(97, 6, 1595030400, 1595116800, 1594960653, '5f112b0d1301c'),
(99, 1, 1595635200, 1595721600, 1595615049, '5f1b2749dc5a7'),
(100, 5, 1595980800, 1596067200, 1595993018, '5f20ebbad6f4a'),
(104, 1, 1597881600, 1597968000, 1597560174, '5f38d56e1b8c8'),
(106, 1, 1597881600, 1597968000, 1597594141, '5f395a1d9a5f8'),
(107, 1, 1597881600, 1597968000, 1597594192, '5f395a50effcc'),
(112, 2, 1599868800, 1599955200, 1597802770, '5f3c891200405'),
(114, 3, 1598054400, 1598140800, 1597849630, '5f3d401e7df96'),
(115, 3, 1597881600, 1598054400, 1597860846, '5f3d6beecfc3f'),
(119, 1, 1599177600, 1599264000, 1598814297, '5f4bf85915096'),
(120, 1, 1599782400, 1599868800, 1599640610, '5f5894220f05f'),
(125, 2, 1601683200, 1601769600, 1601699782, '5f77ffc6b815a'),
(127, 5, 1603843200, 1603929600, 1603273346, '5f900282395e8'),
(128, 6, 1603843200, 1603929600, 1603273346, '5f900282395e8'),
(129, 1, 1603843200, 1603929600, 1603345301, '5f911b95391ac'),
(130, 2, 1603843200, 1603929600, 1603345302, '5f911b95391ac'),
(133, 1, 1604102400, 1604188800, 1603719268, '5f96d06497398'),
(134, 5, 1603929600, 1604016000, 1603815280, '5f98477043c5b'),
(135, 6, 1603929600, 1604016000, 1603815280, '5f98477043c5b'),
(136, 4, 1603843200, 1604016000, 1603871429, '5f9922c5c6c7b'),
(138, 7, 1604102400, 1604188800, 1604078172, '5f9c4a5c6f0c7'),
(145, 6, 1604707200, 1604793600, 1604476732, '5fa25f3c70903'),
(147, 5, 1606435200, 1606608000, 1604899236, '5fa8d1a440454'),
(151, 5, 1606435200, 1606608000, 1604900809, '5fa8d7c94f6d7'),
(152, 1, 1605312000, 1605398400, 1605108264, '5fac02282022d'),
(153, 2, 1605312000, 1605398400, 1605324292, '5faf4e049bf94'),
(154, 7, 1605312000, 1605398400, 1605336012, '5faf7bcc68c06'),
(155, 7, 1605312000, 1605398400, 1605336481, '5faf7da12ae11'),
(160, 1, 1605916800, 1606003200, 1605849754, '5fb7529a7b6d5'),
(163, 1, 1607731200, 1607817600, 1606782557, '5fc58e5d3595d'),
(164, 2, 1607731200, 1607817600, 1606782557, '5fc58e5d3595d'),
(165, 3, 1607731200, 1607817600, 1606782557, '5fc58e5d3595d'),
(169, 1, 1606780800, 1606867200, 1606835630, '5fc65dae07a9a'),
(170, 2, 1606780800, 1606867200, 1606835630, '5fc65dae07a9a'),
(171, 3, 1606780800, 1606867200, 1606835630, '5fc65dae07a9a'),
(172, 7, 1607644800, 1607817600, 1607233516, '5fcc6fec51c56'),
(175, 2, 1608336000, 1608422400, 1608251952, '5fdbfa30118af'),
(176, 1, 1608336000, 1608422400, 1608261089, '5fdc1de1b0023'),
(177, 1, 1608422400, 1608508800, 1608339070, '5fdd4e7e9e3bd'),
(178, 7, 1608940800, 1609027200, 1608705888, '5fe2e7609cc5e'),
(179, 7, 1608940800, 1609027200, 1608823744, '5fe4b3c0302ea'),
(180, 7, 1608940800, 1609027200, 1608953983, '5fe6b07f1d3ae'),
(183, 4, 1609372800, 1609459200, 1609255018, '5feb486aeb66b'),
(187, 1, 1610409600, 1610496000, 1610463508, '5ffdb9143c140'),
(190, 7, 1613779200, 1613865600, 1613570994, '602d23b221ab3'),
(191, 7, 1613779200, 1613865600, 1613571037, '602d23dd12630'),
(196, 1, 1614384000, 1614470400, 1614343478, '6038ed36b2752'),
(197, 2, 1615680000, 1615766400, 1615549610, '604b54aa6143d'),
(198, 7, 1615766400, 1615852800, 1615784271, '604ee94f4c842'),
(200, 1, 1616371200, 1616457600, 1616151817, '605485092535f'),
(205, 2, 1617235200, 1617408000, 1616726627, '605d4a639569e'),
(206, 2, 1617235200, 1617408000, 1616742675, '605d89131b08b'),
(209, 5, 1617148800, 1617235200, 1616899025, '605febd195071'),
(221, 7, 1617580800, 1617667200, 1617603224, '606aaa983a2ef'),
(222, 7, 1617580800, 1617667200, 1617606332, '606ab6bc62492'),
(232, 2, 1617926400, 1618099200, 1617766614, '606d28d601bf1'),
(238, 1, 1618617600, 1618704000, 1617890954, '606f0e8a3f506'),
(240, 3, 1622246400, 1622332800, 1620539173, '6097772583e93'),
(246, 3, 1621641600, 1621728000, 1620987720, '609e4f48259b2'),
(247, 3, 1621641600, 1621728000, 1621086743, '609fd2174efd9'),
(248, 1, 1621900800, 1621987200, 1621835551, '60ab3f1f29107'),
(254, 1, 1628294400, 1628380800, 1628062628, '610a43a48215e'),
(260, 7, 1628899200, 1628985600, 1628895708, '6116f9dc8705d'),
(263, 2, 1630627200, 1630713600, 1629532509, '6120b15dc1483'),
(268, 3, 1635552000, 1635638400, 1630052695, '6128a157710ca'),
(269, 2, 1630713600, 1630800000, 1630297413, '612c5d459e6c0'),
(270, 2, 1632009600, 1632096000, 1630807446, '61342596350c4'),
(273, 1, 1631318400, 1631404800, 1631187560, '6139f268d09d8'),
(285, 2, 1633737600, 1633824000, 1633668576, '615fcde0d1fa0'),
(287, 7, 1638576000, 1638662400, 1636268780, '61877aec84830'),
(288, 7, 1639180800, 1639267200, 1636429610, '6189ef2adf45e'),
(289, 2, 1637193600, 1637280000, 1636454231, '618a4f5772528'),
(296, 1, 1637193600, 1637280000, 1636637414, '618d1ae69945b'),
(297, 1, 1637193600, 1637280000, 1636637415, '618d1ae7dcecc'),
(301, 1, 1637971200, 1638057600, 1636652722, '618d56b25623b'),
(302, 1, 1637971200, 1638057600, 1636652724, '618d56b4e7236'),
(303, 5, 1636848000, 1637020800, 1636727207, '618e79a7900f2'),
(304, 7, 1636761600, 1636848000, 1636768814, '618f1c2e452e1'),
(306, 1, 1637971200, 1638057600, 1636981632, '61925b80c2706'),
(309, 3, 1638489600, 1638662400, 1637475788, '6199e5ccaa611'),
(318, 1, 1640908800, 1640995200, 1640088377, '61c1c339a3dad'),
(323, 7, 1641513600, 1641600000, 1641263166, '61d3b03e5d1b1'),
(324, 7, 1641513600, 1641600000, 1641263245, '61d3b08dbb88c'),
(325, 3, 1641600000, 1641686400, 1641281448, '61d3f7a873001'),
(331, 1, 1641859200, 1641945600, 1641875094, '61dd0696135a6'),
(332, 2, 1642118400, 1642204800, 1642130271, '61e0eb5f7772e'),
(333, 1, 1646438400, 1646524800, 1643034235, '61eeb67bdd55d'),
(340, 1, 1646265600, 1646352000, 1645766027, '6218658b615d3'),
(341, 1, 1646179200, 1646265600, 1645923844, '621ace04ee0ab'),
(342, 2, 1646179200, 1646265600, 1645923845, '621ace04ee0ab'),
(346, 1, 1646265600, 1646352000, 1646292365, '62206d8de6816');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_service`
--

CREATE TABLE `pm_service` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `users` text DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `descr` text DEFAULT NULL,
  `long_descr` text DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `rooms` varchar(250) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `id_tax` int(11) DEFAULT NULL,
  `taxes` text DEFAULT NULL,
  `mandatory` int(11) DEFAULT 0,
  `start_date` int(11) DEFAULT NULL,
  `end_date` int(11) DEFAULT NULL,
  `checked` int(11) DEFAULT 0,
  `rank` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_slide`
--

CREATE TABLE `pm_slide` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `legend` text DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `id_page` int(11) DEFAULT NULL,
  `checked` int(11) DEFAULT 0,
  `rank` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_slide`
--

INSERT INTO `pm_slide` (`id`, `lang`, `legend`, `url`, `id_page`, `checked`, `rank`) VALUES
(1, 1, '<h1>Book your holydays with Panda Multi Resorts</h1>\r\n\r\n<h2>Fast, Easy and Powerfull</h2>\r\n', '', 1, 1, 4),
(1, 2, '<h2> </h2>\r\n\r\n<h2>Best price guarantee</h2>\r\n\r\n<h2> </h2>\r\n', '', 1, 1, 4),
(1, 3, '<h1>Book your holydays with Panda Multi Resorts</h1>\r\n\r\n<h2>Fast, Easy and Powerfull</h2>\r\n', '', 1, 1, 4),
(1, 4, '<h2> </h2>\r\n\r\n<h2>Jaminan Harga Terbaik</h2>\r\n\r\n<h2> </h2>\r\n', '', 1, 1, 4),
(2, 1, '<h1>A dream stay at the best price</h1>\r\n\r\n<h2>Best price guarantee</h2>\r\n', '', 1, 1, 5),
(2, 2, '<h2> </h2>\r\n\r\n<h2>Nature, Hidden Paradise</h2>\r\n\r\n<h2> </h2>\r\n', '', 1, 1, 5),
(2, 3, '<h1>A dream stay at the best price</h1>\r\n\r\n<h2>Best price guarantee</h2>\r\n', '', 1, 1, 5),
(2, 4, '<h2> </h2>\r\n\r\n<h2>Alam, Surga Tersembunyi</h2>\r\n\r\n<h2> </h2>\r\n', '', 1, 1, 5),
(3, 1, '<h1>Find Hotels, Activities and Tours</h1>\r\n\r\n<h2>Your whole vacation starts here</h2>\r\n', '', 1, 1, 3),
(3, 2, '<h2> </h2>\r\n\r\n<h2>Salse, Rileks, Relax, Relaks</h2>\r\n\r\n<h2> </h2>\r\n', '', 1, 1, 3),
(3, 3, '<h1>Find Hotels, Activities and Tours</h1>\r\n\r\n<h2>Your whole vacation starts here</h2>\r\n', '', 1, 1, 3),
(3, 4, '<h2> </h2>\r\n\r\n<h2>Salse, Santai, Santai, Santai</h2>\r\n\r\n<h2> </h2>\r\n', '', 1, 1, 3),
(5, 1, '', '', 1, 2, 6),
(5, 2, '<h2> </h2>\r\n\r\n<h2>Culture, Resort</h2>\r\n', '', 1, 2, 6),
(5, 3, '', '', 1, 2, 6),
(5, 4, '<h2> </h2>\r\n\r\n<h2>Budaya, Resor</h2>\r\n', '', 1, 2, 6),
(7, 2, '<h2> </h2>\r\n\r\n<h2>Jacuzzi</h2>\r\n', '', 1, 1, 2),
(7, 4, '<h2> </h2>\r\n\r\n<h2>Jacuzzi</h2>\r\n\r\n<h2> </h2>\r\n', '', 1, 1, 2),
(15, 2, '<h2> </h2>\r\n\r\n<h2> </h2>\r\n', '', 1, 2, 1),
(15, 4, '', '', 1, 2, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_slide_file`
--

CREATE TABLE `pm_slide_file` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `home` int(11) DEFAULT 0,
  `checked` int(11) DEFAULT 1,
  `rank` int(11) DEFAULT 0,
  `file` varchar(250) DEFAULT NULL,
  `label` varchar(250) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_slide_file`
--

INSERT INTO `pm_slide_file` (`id`, `lang`, `id_item`, `home`, `checked`, `rank`, `file`, `label`, `type`) VALUES
(12, 1, 1, NULL, 1, 7, 'villatel-salse-2.jpg', NULL, 'image'),
(12, 2, 1, NULL, 1, 7, 'villatel-salse-2.jpg', '', 'image'),
(12, 3, 1, NULL, 1, 7, 'villatel-salse-2.jpg', NULL, 'image'),
(12, 4, 1, NULL, 1, 7, 'villatel-salse-2.jpg', '', 'image'),
(14, 1, 2, NULL, 1, 8, 'whatsapp-image-2019-12-10-at-17-39-00.jpeg', NULL, 'image'),
(14, 2, 2, NULL, 1, 8, 'whatsapp-image-2019-12-10-at-17-39-00.jpeg', '', 'image'),
(14, 3, 2, NULL, 1, 8, 'whatsapp-image-2019-12-10-at-17-39-00.jpeg', NULL, 'image'),
(14, 4, 2, NULL, 1, 8, 'whatsapp-image-2019-12-10-at-17-39-00.jpeg', '', 'image'),
(15, 1, 3, NULL, 1, 9, 'img-20190222-wa0002.jpg', NULL, 'image'),
(15, 2, 3, NULL, 1, 9, 'img-20190222-wa0002.jpg', '', 'image'),
(15, 3, 3, NULL, 1, 9, 'img-20190222-wa0002.jpg', NULL, 'image'),
(15, 4, 3, NULL, 1, 9, 'img-20190222-wa0002.jpg', '', 'image'),
(16, 1, 5, NULL, 1, 10, 'whatsapp-image-2019-12-10-at-17-39-02.jpeg', NULL, 'image'),
(16, 2, 5, NULL, 1, 10, 'whatsapp-image-2019-12-10-at-17-39-02.jpeg', '', 'image'),
(16, 3, 5, NULL, 1, 10, 'whatsapp-image-2019-12-10-at-17-39-02.jpeg', NULL, 'image'),
(16, 4, 5, NULL, 1, 10, 'whatsapp-image-2019-12-10-at-17-39-02.jpeg', '', 'image'),
(18, 2, 7, NULL, 1, 12, 'warung-salse-jacuzzi.jpg', NULL, 'image'),
(18, 4, 7, NULL, 1, 12, 'warung-salse-jacuzzi.jpg', NULL, 'image'),
(26, 2, 15, NULL, 1, 18, 'promo-salse-en.jpg', NULL, 'image'),
(26, 4, 15, NULL, 1, 18, 'promo-salse-en.jpg', NULL, 'image');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_social`
--

CREATE TABLE `pm_social` (
  `id` int(11) NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `url` text DEFAULT NULL,
  `checked` int(11) DEFAULT 1,
  `rank` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_tag`
--

CREATE TABLE `pm_tag` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `value` varchar(250) DEFAULT NULL,
  `pages` varchar(250) DEFAULT NULL,
  `checked` int(11) DEFAULT 0,
  `rank` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_tax`
--

CREATE TABLE `pm_tax` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `value` double DEFAULT 0,
  `checked` int(11) DEFAULT 0,
  `rank` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_tax`
--

INSERT INTO `pm_tax` (`id`, `lang`, `name`, `value`, `checked`, `rank`) VALUES
(1, 1, 'TVA', 15, 1, 1),
(1, 2, 'TAX & SERVICE', 15, 1, 1),
(1, 3, 'VAT', 15, 1, 1),
(1, 4, 'TAX & SERVICE', 15, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_text`
--

CREATE TABLE `pm_text` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `value` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_text`
--

INSERT INTO `pm_text` (`id`, `lang`, `name`, `value`) VALUES
(1, 1, 'CREATION', 'Création'),
(1, 2, 'CREATION', 'Creation'),
(1, 3, 'CREATION', 'إنشاء'),
(1, 4, 'CREATION', 'Kreasi'),
(2, 1, 'MESSAGE', 'Message'),
(2, 2, 'MESSAGE', 'Message'),
(2, 3, 'MESSAGE', 'رسالة'),
(2, 4, 'MESSAGE', 'Pesan'),
(3, 1, 'EMAIL', 'E-mail'),
(3, 2, 'EMAIL', 'E-mail'),
(3, 3, 'EMAIL', 'بَرِيدٌ إلِكْترونيّ'),
(3, 4, 'EMAIL', 'E-mail'),
(4, 1, 'PHONE', 'Tél.'),
(4, 2, 'PHONE', 'Phone'),
(4, 3, 'PHONE', 'رقم هاتف'),
(4, 4, 'PHONE', 'Telepon'),
(5, 1, 'FAX', 'Fax'),
(5, 2, 'FAX', 'Fax'),
(5, 3, 'FAX', 'فاكس'),
(5, 4, 'FAX', 'Fax'),
(6, 1, 'COMPANY', 'Société'),
(6, 2, 'COMPANY', 'Company'),
(6, 3, 'COMPANY', 'مشروع'),
(6, 4, 'COMPANY', 'Perusahaan'),
(7, 1, 'COPY_CODE', 'Recopiez le code'),
(7, 2, 'COPY_CODE', 'Copy the code'),
(7, 3, 'COPY_CODE', 'رمز الأمان'),
(7, 4, 'COPY_CODE', 'Salin Kodenya'),
(8, 1, 'SUBJECT', 'Sujet'),
(8, 2, 'SUBJECT', 'Subject'),
(8, 3, 'SUBJECT', 'موضوع'),
(8, 4, 'SUBJECT', 'Subjek'),
(9, 1, 'REQUIRED_FIELD', 'Champ requis'),
(9, 2, 'REQUIRED_FIELD', 'Required field'),
(9, 3, 'REQUIRED_FIELD', 'الحقل المطلوب'),
(9, 4, 'REQUIRED_FIELD', 'Kolom yang harus diisi'),
(10, 1, 'INVALID_CAPTCHA_CODE', 'Le code de sécurité saisi est incorrect'),
(10, 2, 'INVALID_CAPTCHA_CODE', 'Invalid security code'),
(10, 3, 'INVALID_CAPTCHA_CODE', 'رمز الحماية أدخلته غير صحيح'),
(10, 4, 'INVALID_CAPTCHA_CODE', 'Kode Keamanan Salah'),
(11, 1, 'INVALID_EMAIL', 'Adresse e-mail invalide'),
(11, 2, 'INVALID_EMAIL', 'Invalid email address'),
(11, 3, 'INVALID_EMAIL', 'بريد إلكتروني خاطئ'),
(11, 4, 'INVALID_EMAIL', 'Alamat Email Salah'),
(12, 1, 'FIRSTNAME', 'Prénom'),
(12, 2, 'FIRSTNAME', 'Firstname'),
(12, 3, 'FIRSTNAME', 'الاسم الأول'),
(12, 4, 'FIRSTNAME', 'Nama Depan'),
(13, 1, 'LASTNAME', 'Nom'),
(13, 2, 'LASTNAME', 'Lastname'),
(13, 3, 'LASTNAME', 'اسم العائلة'),
(13, 4, 'LASTNAME', 'Nama Belakang'),
(14, 1, 'ADDRESS', 'Adresse'),
(14, 2, 'ADDRESS', 'Address'),
(14, 3, 'ADDRESS', 'عنوان الشارع'),
(14, 4, 'ADDRESS', 'Alamat'),
(15, 1, 'POSTCODE', 'Code postal'),
(15, 2, 'POSTCODE', 'Post code'),
(15, 3, 'POSTCODE', 'الرمز البريدي'),
(15, 4, 'POSTCODE', 'Kode Pos'),
(16, 1, 'CITY', 'Ville'),
(16, 2, 'CITY', 'City'),
(16, 3, 'CITY', 'مدينة'),
(16, 4, 'CITY', 'Kota'),
(17, 1, 'MOBILE', 'Portable'),
(17, 2, 'MOBILE', 'Mobile'),
(17, 3, 'MOBILE', 'هاتف'),
(17, 4, 'MOBILE', 'Ponsel'),
(18, 1, 'ADD', 'Ajouter'),
(18, 2, 'ADD', 'Add'),
(18, 3, 'ADD', 'إضافة على'),
(18, 4, 'ADD', 'Add'),
(19, 1, 'EDIT', 'Modifier'),
(19, 2, 'EDIT', 'Edit'),
(19, 3, 'EDIT', 'تغيير'),
(19, 4, 'EDIT', 'Sunting'),
(20, 1, 'INVALID_INPUT', 'Saisie invalide'),
(20, 2, 'INVALID_INPUT', 'Invalid input'),
(20, 3, 'INVALID_INPUT', 'إدخال غير صالح'),
(20, 4, 'INVALID_INPUT', 'Input tidak valid'),
(21, 1, 'MAIL_DELIVERY_FAILURE', 'Echec lors de l\'envoi du message.'),
(21, 2, 'MAIL_DELIVERY_FAILURE', 'A failure occurred during the delivery of this message.'),
(21, 3, 'MAIL_DELIVERY_FAILURE', 'حدث فشل أثناء تسليم هذه الرسالة.'),
(21, 4, 'MAIL_DELIVERY_FAILURE', 'Kegagalan terjadi selama pengiriman pesan ini.'),
(22, 1, 'MAIL_DELIVERY_SUCCESS', 'Merci de votre intérêt, votre message a bien été envoyé.\nNous vous contacterons dans les plus brefs délais.'),
(22, 2, 'MAIL_DELIVERY_SUCCESS', 'Thank you for your interest, your message has been sent.\r\nWe will contact you as soon as possible.'),
(22, 3, 'MAIL_DELIVERY_SUCCESS', 'خزان لاهتمامك ، تم إرسال رسالتك . سوف نتصل بك في أقرب وقت ممكن .'),
(22, 4, 'MAIL_DELIVERY_SUCCESS', 'Terima kasih atas minat Anda, pesan Anda telah terkirim.\r\nKami akan menghubungi Anda sesegera mungkin.'),
(23, 1, 'SEND', 'Envoyer'),
(23, 2, 'SEND', 'Send'),
(23, 3, 'SEND', 'ارسل انت'),
(23, 4, 'SEND', 'Kirim'),
(24, 1, 'FORM_ERRORS', 'Le formulaire comporte des erreurs.'),
(24, 2, 'FORM_ERRORS', 'The following form contains some errors.'),
(24, 3, 'FORM_ERRORS', 'النموذج التالي يحتوي على بعض الأخطاء.'),
(24, 4, 'FORM_ERRORS', 'Formulir berikut berisi beberapa kesalahan.'),
(25, 1, 'FROM_DATE', 'Du'),
(25, 2, 'FROM_DATE', 'From'),
(25, 3, 'FROM_DATE', 'من'),
(25, 4, 'FROM_DATE', 'Dari'),
(26, 1, 'TO_DATE', 'au'),
(26, 2, 'TO_DATE', 'till'),
(26, 3, 'TO_DATE', 'حتى'),
(26, 4, 'TO_DATE', 'hingga'),
(27, 1, 'FROM', 'De'),
(27, 2, 'FROM', 'From'),
(27, 3, 'FROM', 'من'),
(27, 4, 'FROM', 'Dari'),
(28, 1, 'TO', 'à'),
(28, 2, 'TO', 'to'),
(28, 3, 'TO', 'إلى'),
(28, 4, 'TO', 'untuk'),
(29, 1, 'BOOK', 'Réserver'),
(29, 2, 'BOOK', 'Book'),
(29, 3, 'BOOK', 'للحجز'),
(29, 4, 'BOOK', 'Pesan'),
(30, 1, 'READMORE', 'Lire la suite'),
(30, 2, 'READMORE', 'Read more'),
(30, 3, 'READMORE', 'اقرأ المزيد'),
(30, 4, 'READMORE', 'Baca lebih lanjut'),
(31, 1, 'BACK', 'Retour'),
(31, 2, 'BACK', 'Back'),
(31, 3, 'BACK', 'عودة'),
(31, 4, 'BACK', 'Kembali'),
(32, 1, 'DISCOVER', 'Découvrir'),
(32, 2, 'DISCOVER', 'Discover'),
(32, 3, 'DISCOVER', 'اكتشف'),
(32, 4, 'DISCOVER', 'Temukan'),
(33, 1, 'ALL', 'Tous'),
(33, 2, 'ALL', 'All'),
(33, 3, 'ALL', 'كل'),
(33, 4, 'ALL', 'Semua'),
(34, 1, 'ALL_RIGHTS_RESERVED', 'Tous droits réservés'),
(34, 2, 'ALL_RIGHTS_RESERVED', 'All rights reserved'),
(34, 3, 'ALL_RIGHTS_RESERVED', 'جميع الحقوق محفوظه'),
(34, 4, 'ALL_RIGHTS_RESERVED', 'Semua hak dilindungi undang-undang'),
(35, 1, 'FORGOTTEN_PASSWORD', 'Mot de passe oublié ?'),
(35, 2, 'FORGOTTEN_PASSWORD', 'Forgotten password?'),
(35, 3, 'FORGOTTEN_PASSWORD', 'هل نسيت كلمة المرور؟'),
(35, 4, 'FORGOTTEN_PASSWORD', 'Lupa kata sandi?'),
(36, 1, 'LOG_IN', 'Connexion'),
(36, 2, 'LOG_IN', 'Log in'),
(36, 3, 'LOG_IN', 'تسجيل الدخول'),
(36, 4, 'LOG_IN', 'Masuk'),
(37, 1, 'SIGN_UP', 'Inscription'),
(37, 2, 'SIGN_UP', 'Sign up'),
(37, 3, 'SIGN_UP', 'تسجيل'),
(37, 4, 'SIGN_UP', 'Daftar'),
(38, 1, 'LOG_OUT', 'Déconnexion'),
(38, 2, 'LOG_OUT', 'Log out'),
(38, 3, 'LOG_OUT', 'تسجيل الخروج'),
(38, 4, 'LOG_OUT', 'Keluar'),
(39, 1, 'SEARCH', 'Rechercher'),
(39, 2, 'SEARCH', 'Search'),
(39, 3, 'SEARCH', 'ابحث عن'),
(39, 4, 'SEARCH', 'Cari'),
(40, 1, 'RESET_PASS_SUCCESS', 'Votre nouveau mot de passe vous a été envoyé sur votre adresse e-mail.'),
(40, 2, 'RESET_PASS_SUCCESS', 'Your new password was sent to you on your e-mail.'),
(40, 3, 'RESET_PASS_SUCCESS', 'تم إرسال كلمة المرور الجديدة إلى عنوان البريد الإلكتروني الخاص بك'),
(40, 4, 'RESET_PASS_SUCCESS', 'Kata sandi baru Anda akan dikirimkan melalui Email Anda.'),
(41, 1, 'PASS_TOO_SHORT', 'Le mot de passe doit contenir 6 caractères au minimum'),
(41, 2, 'PASS_TOO_SHORT', 'The password must contain 6 characters at least'),
(41, 3, 'PASS_TOO_SHORT', 'يجب أن يحتوي على كلمة المرور ستة أحرف على الأقل'),
(41, 4, 'PASS_TOO_SHORT', 'Kata sandi minimal harus mengandung 6 karakter'),
(42, 1, 'PASS_DONT_MATCH', 'Les mots de passe doivent correspondre'),
(42, 2, 'PASS_DONT_MATCH', 'The passwords don\'t match'),
(42, 3, 'PASS_DONT_MATCH', 'يجب أن تتطابق كلمات المرور'),
(42, 4, 'PASS_DONT_MATCH', 'Kata sandi tidak cocok'),
(43, 1, 'ACCOUNT_EXISTS', 'Un compte existe déjà avec cette adresse e-mail'),
(43, 2, 'ACCOUNT_EXISTS', 'An account already exists with this e-mail'),
(43, 3, 'ACCOUNT_EXISTS', 'حساب موجود بالفعل مع هذا عنوان البريد الإلكتروني'),
(43, 4, 'ACCOUNT_EXISTS', 'Akun sudah ada dengan Email ini'),
(44, 1, 'ACCOUNT_CREATED', 'Votre compte a bien été créé. Vous allez recevoir un email de confirmation. Cliquez sur le lien de cet e-mail pour confirmer votre compte avant de continuer.'),
(44, 2, 'ACCOUNT_CREATED', 'Your account has been created. You will receive a confirmation email. Click on the link in this email to confirm your account before continuing.'),
(44, 3, 'ACCOUNT_CREATED', 'Your account has been created. You will receive a confirmation email. Click on the link in this email to confirm your account before continuing.'),
(44, 4, 'ACCOUNT_CREATED', 'Akun anda telah dibuat. Kamu akan menerima email konfirmasi. Klik tautan dalam email ini untuk mengonfirmasi akun Anda sebelum melanjutkan.'),
(45, 1, 'INCORRECT_LOGIN', 'Les informations de connexion sont incorrectes.'),
(45, 2, 'INCORRECT_LOGIN', 'Incorrect login information.'),
(45, 3, 'INCORRECT_LOGIN', 'معلومات تسجيل الدخول غير صحيحة.'),
(45, 4, 'INCORRECT_LOGIN', 'Informasi login salah'),
(46, 1, 'I_SIGN_UP', 'Je m\'inscris'),
(46, 2, 'I_SIGN_UP', 'I sign up'),
(46, 3, 'I_SIGN_UP', 'يمكنني الاشتراك'),
(46, 4, 'I_SIGN_UP', 'Saya mendaftar'),
(47, 1, 'ALREADY_HAVE_ACCOUNT', 'J\'ai déjà un compte'),
(47, 2, 'ALREADY_HAVE_ACCOUNT', 'I already have an account'),
(47, 3, 'ALREADY_HAVE_ACCOUNT', 'لدي بالفعل حساب'),
(47, 4, 'ALREADY_HAVE_ACCOUNT', 'Saya sudah punya akun'),
(48, 1, 'MY_ACCOUNT', 'Mon compte'),
(48, 2, 'MY_ACCOUNT', 'My account'),
(48, 3, 'MY_ACCOUNT', 'حسابي'),
(48, 4, 'MY_ACCOUNT', 'Akun saya'),
(49, 1, 'COMMENTS', 'Commentaires'),
(49, 2, 'COMMENTS', 'Comments'),
(49, 3, 'COMMENTS', 'تعليقات'),
(49, 4, 'COMMENTS', 'Komentar'),
(50, 1, 'LET_US_KNOW', 'Faîtes-nous savoir ce que vous pensez'),
(50, 2, 'LET_US_KNOW', 'Let us know what you think'),
(50, 3, 'LET_US_KNOW', 'ماذا عن رايك؟'),
(50, 4, 'LET_US_KNOW', 'Beri tahu kami pendapat Anda'),
(51, 1, 'COMMENT_SUCCESS', 'Merci de votre intérêt, votre commentaire va être soumis à validation.'),
(51, 2, 'COMMENT_SUCCESS', 'Thank you for your interest, your comment will be checked.'),
(51, 3, 'COMMENT_SUCCESS', 'شكرا ل اهتمامك، و سيتم التحقق من صحة للتعليق.'),
(51, 4, 'COMMENT_SUCCESS', 'Terima kasih atas minat Anda, komentar Anda akan diperiksa.'),
(52, 1, 'NO_SEARCH_RESULT', 'Aucun résultat. Vérifiez l\'orthographe des termes de recherche (> 3 caractères) ou essayez d\'autres mots.'),
(52, 2, 'NO_SEARCH_RESULT', 'No result. Check the spelling terms of search (> 3 characters) or try other words.'),
(52, 3, 'NO_SEARCH_RESULT', 'لا نتيجة. التدقيق الإملائي للكلمات (أكثر من ثلاثة أحرف ) أو محاولة بعبارة أخرى .'),
(52, 4, 'NO_SEARCH_RESULT', 'Tidak ada hasil. Periksa ketentuan ejaan pencarian (> 3 karakter) atau coba kata lain.'),
(53, 1, 'SEARCH_EXCEEDED', 'Nombre de recherches dépassé.'),
(53, 2, 'SEARCH_EXCEEDED', 'Number of researches exceeded.'),
(53, 3, 'SEARCH_EXCEEDED', 'عدد من الأبحاث السابقة .'),
(53, 4, 'SEARCH_EXCEEDED', 'Jumlah penelitian terlampaui'),
(54, 1, 'SECONDS', 'secondes'),
(54, 2, 'SECONDS', 'seconds'),
(54, 3, 'SECONDS', 'ثواني'),
(54, 4, 'SECONDS', 'detik'),
(55, 1, 'FOR_A_TOTAL_OF', 'sur un total de'),
(55, 2, 'FOR_A_TOTAL_OF', 'for a total of'),
(55, 3, 'FOR_A_TOTAL_OF', 'من الكل'),
(55, 4, 'FOR_A_TOTAL_OF', 'total'),
(56, 1, 'COMMENT', 'Commentaire'),
(56, 2, 'COMMENT', 'Comment'),
(56, 3, 'COMMENT', 'تعقيب'),
(56, 4, 'COMMENT', 'Komentar'),
(57, 1, 'VIEW', 'Visionner'),
(57, 2, 'VIEW', 'View'),
(57, 3, 'VIEW', 'ل عرض'),
(57, 4, 'VIEW', 'Melihat'),
(58, 1, 'RECENT_ARTICLES', 'Articles récents'),
(58, 2, 'RECENT_ARTICLES', 'Recent articles'),
(58, 3, 'RECENT_ARTICLES', 'المقالات الأخيرة'),
(58, 4, 'RECENT_ARTICLES', 'Artikel terbaru'),
(59, 1, 'RSS_FEED', 'Flux RSS'),
(59, 2, 'RSS_FEED', 'RSS feed'),
(59, 3, 'RSS_FEED', 'تغذية RSS'),
(59, 4, 'RSS_FEED', 'RSS feed'),
(60, 1, 'COUNTRY', 'Pays'),
(60, 2, 'COUNTRY', 'Country'),
(60, 3, 'COUNTRY', 'Country'),
(60, 4, 'COUNTRY', 'Negara'),
(61, 1, 'ROOM', 'Chambre'),
(61, 2, 'ROOM', 'Room'),
(61, 3, 'ROOM', 'Room'),
(61, 4, 'ROOM', 'Kamar'),
(62, 1, 'INCL_VAT', 'TTC'),
(62, 2, 'INCL_VAT', 'incl. VAT'),
(62, 3, 'INCL_VAT', 'incl. VAT'),
(62, 4, 'INCL_VAT', 'incl. VAT'),
(63, 1, 'NIGHTS', 'nuits'),
(63, 2, 'NIGHTS', 'nights'),
(63, 3, 'NIGHTS', 'nights'),
(63, 4, 'NIGHTS', 'malam'),
(64, 1, 'ADULTS', 'Adultes'),
(64, 2, 'ADULTS', 'Adults'),
(64, 3, 'ADULTS', 'Adults'),
(64, 4, 'ADULTS', 'Dewasa'),
(65, 1, 'CHILDREN', 'Enfants'),
(65, 2, 'CHILDREN', 'Children'),
(65, 3, 'CHILDREN', 'Children'),
(65, 4, 'CHILDREN', 'Anak-anak'),
(66, 1, 'PERSONS', 'personnes'),
(66, 2, 'PERSONS', 'persons'),
(66, 3, 'PERSONS', 'persons'),
(66, 4, 'PERSONS', 'orang'),
(67, 1, 'CONTACT_DETAILS', 'Coordonnées'),
(67, 2, 'CONTACT_DETAILS', 'Contact details'),
(67, 3, 'CONTACT_DETAILS', 'Contact details'),
(67, 4, 'CONTACT_DETAILS', 'Rincian kontak'),
(68, 1, 'NO_AVAILABILITY', 'Aucune disponibilité'),
(68, 2, 'NO_AVAILABILITY', 'No availability'),
(68, 3, 'NO_AVAILABILITY', 'No availability'),
(68, 4, 'NO_AVAILABILITY', 'Tidak tersedia'),
(69, 1, 'AVAILABILITIES', 'Disponibilités'),
(69, 2, 'AVAILABILITIES', 'Availabilities'),
(69, 3, 'AVAILABILITIES', 'Availabilities'),
(69, 4, 'AVAILABILITIES', 'Ketersediaan'),
(70, 1, 'CHECK', 'Vérifier'),
(70, 2, 'CHECK', 'Check'),
(70, 3, 'CHECK', 'Check'),
(70, 4, 'CHECK', 'Periksa'),
(71, 1, 'BOOKING_DETAILS', 'Détails de la réservation'),
(71, 2, 'BOOKING_DETAILS', 'Booking details'),
(71, 3, 'BOOKING_DETAILS', 'Booking details'),
(71, 4, 'BOOKING_DETAILS', 'Detail pemesanan'),
(72, 1, 'SPECIAL_REQUESTS', 'Demandes spéciales'),
(72, 2, 'SPECIAL_REQUESTS', 'Special requests'),
(72, 3, 'SPECIAL_REQUESTS', 'Special requests'),
(72, 4, 'SPECIAL_REQUESTS', 'Permintaan spesial'),
(73, 1, 'PREVIOUS_STEP', 'Étape précédente'),
(73, 2, 'PREVIOUS_STEP', 'Previous step'),
(73, 3, 'PREVIOUS_STEP', 'Previous step'),
(73, 4, 'PREVIOUS_STEP', 'Langkah sebelumnya'),
(74, 1, 'CONFIRM_BOOKING', 'Confirmer la réservation'),
(74, 2, 'CONFIRM_BOOKING', 'Confirm the booking'),
(74, 3, 'CONFIRM_BOOKING', 'Confirm the booking'),
(74, 4, 'CONFIRM_BOOKING', 'Konfirmasi pemesanan'),
(75, 1, 'ALSO_DISCOVER', 'Découvrez aussi'),
(75, 2, 'ALSO_DISCOVER', 'Also discover'),
(75, 3, 'ALSO_DISCOVER', 'Also discover'),
(75, 4, 'ALSO_DISCOVER', 'Juga ditemukan'),
(76, 1, 'CHECK_PEOPLE', 'Merci de vérifier le nombre de personnes pour l\'hébergement choisi.'),
(76, 2, 'CHECK_PEOPLE', 'Please verify the number of people for the chosen accommodation'),
(76, 3, 'CHECK_PEOPLE', 'Please verify the number of people for the chosen accommodation'),
(76, 4, 'CHECK_PEOPLE', 'Harap verifikasi jumlah orang untuk akomodasi yang dipilih'),
(77, 1, 'BOOKING', 'Réservation'),
(77, 2, 'BOOKING', 'Booking'),
(77, 3, 'BOOKING', 'Booking'),
(77, 4, 'BOOKING', 'Pemesanan'),
(78, 1, 'NIGHT', 'nuit'),
(78, 2, 'NIGHT', 'night'),
(78, 3, 'NIGHT', 'night'),
(78, 4, 'NIGHT', 'malam'),
(79, 1, 'WEEK', 'semaine'),
(79, 2, 'WEEK', 'week'),
(79, 3, 'WEEK', 'week'),
(79, 4, 'WEEK', 'minggu'),
(80, 1, 'EXTRA_SERVICES', 'Services supplémentaires'),
(80, 2, 'EXTRA_SERVICES', 'Extra services'),
(80, 3, 'EXTRA_SERVICES', 'Extra services'),
(80, 4, 'EXTRA_SERVICES', 'Layanan tambahan'),
(81, 1, 'BOOKING_TERMS', ''),
(81, 2, 'BOOKING_TERMS', ''),
(81, 3, 'BOOKING_TERMS', ''),
(81, 4, 'BOOKING_TERMS', ''),
(82, 1, 'NEXT_STEP', 'Étape suivante'),
(82, 2, 'NEXT_STEP', 'Next step'),
(82, 3, 'NEXT_STEP', 'Next step'),
(82, 4, 'NEXT_STEP', 'Langkah berikutnya'),
(83, 1, 'TOURIST_TAX', 'Taxe de séjour'),
(83, 2, 'TOURIST_TAX', 'Tourist tax'),
(83, 3, 'TOURIST_TAX', 'Tourist tax'),
(83, 4, 'TOURIST_TAX', 'Pajak turis'),
(84, 1, 'CHECK_IN', 'Arrivée'),
(84, 2, 'CHECK_IN', 'Check in'),
(84, 3, 'CHECK_IN', 'Check in'),
(84, 4, 'CHECK_IN', 'Masuk'),
(85, 1, 'CHECK_OUT', 'Départ'),
(85, 2, 'CHECK_OUT', 'Check out'),
(85, 3, 'CHECK_OUT', 'Check out'),
(85, 4, 'CHECK_OUT', 'Keluar'),
(86, 1, 'TOTAL', 'Total'),
(86, 2, 'TOTAL', 'Total'),
(86, 3, 'TOTAL', 'Total'),
(86, 4, 'TOTAL', 'Total'),
(87, 1, 'CAPACITY', 'Capacité'),
(87, 2, 'CAPACITY', 'Capacity'),
(87, 3, 'CAPACITY', 'Capacity'),
(87, 4, 'CAPACITY', 'Kapasitas'),
(88, 1, 'FACILITIES', 'Équipements'),
(88, 2, 'FACILITIES', 'Facilities'),
(88, 3, 'FACILITIES', 'Facilities'),
(88, 4, 'FACILITIES', 'Fasilitas'),
(89, 1, 'PRICE', 'Prix'),
(89, 2, 'PRICE', 'Price'),
(89, 3, 'PRICE', 'Price'),
(89, 4, 'PRICE', 'Harga'),
(90, 1, 'MORE_DETAILS', 'Plus d\'infos'),
(90, 2, 'MORE_DETAILS', 'More details'),
(90, 3, 'MORE_DETAILS', 'More details'),
(90, 4, 'MORE_DETAILS', 'Keterangan lebih lanjut'),
(91, 1, 'FROM_PRICE', 'À partir de'),
(91, 2, 'FROM_PRICE', 'From'),
(91, 3, 'FROM_PRICE', 'From'),
(91, 4, 'FROM_PRICE', 'Dari'),
(92, 1, 'AMOUNT', 'Montant'),
(92, 2, 'AMOUNT', 'Amount'),
(92, 3, 'AMOUNT', 'Amount'),
(92, 4, 'AMOUNT', 'Jumlah'),
(93, 1, 'PAY', 'Payer'),
(93, 2, 'PAY', 'Check out'),
(93, 3, 'PAY', 'Check out'),
(93, 4, 'PAY', 'Keluar'),
(94, 1, 'PAYMENT_PAYPAL_NOTICE', 'Cliquez sur \"Payer\" ci-dessous, vous allez être redirigé vers le site sécurisé de PayPal'),
(94, 2, 'PAYMENT_PAYPAL_NOTICE', 'Click on \"Check Out\" below, you will be redirected towards the secure site of PayPal'),
(94, 3, 'PAYMENT_PAYPAL_NOTICE', 'Click on \"Check Out\" below, you will be redirected towards the secure site of PayPal'),
(94, 4, 'PAYMENT_PAYPAL_NOTICE', 'Klik \"Keluar\" di bawah, Anda akan diarahkan ke situs PayPal yang aman'),
(95, 1, 'PAYMENT_CANCEL_NOTICE', 'Le paiement a été annulé.<br>Merci de votre visite et à bientôt.'),
(95, 2, 'PAYMENT_CANCEL_NOTICE', 'The payment has been cancelled.<br>Thank you for your visit and see you soon.'),
(95, 3, 'PAYMENT_CANCEL_NOTICE', 'The payment has been cancelled.<br>Thank you for your visit and see you soon.'),
(95, 4, 'PAYMENT_CANCEL_NOTICE', 'Pembayaran telah dibatalkan. <br> Terima kasih atas kunjungan Anda dan sampai jumpa lagi.'),
(96, 1, 'PAYMENT_SUCCESS_NOTICE', 'Le paiement a été réalisé avec succès.<br>Merci de votre visite et à bientôt !'),
(96, 2, 'PAYMENT_SUCCESS_NOTICE', 'Your payment has been successfully processed.<br>Thank you for your visit and see you soon!'),
(96, 3, 'PAYMENT_SUCCESS_NOTICE', 'Your payment has been successfully processed.<br>Thank you for your visit and see you soon!'),
(96, 4, 'PAYMENT_SUCCESS_NOTICE', 'Pembayaran Anda telah berhasil diproses. <br> Terima kasih atas kunjungan Anda dan sampai jumpa lagi!'),
(97, 1, 'BILLING_ADDRESS', 'Adresse de facturation'),
(97, 2, 'BILLING_ADDRESS', 'Billing address'),
(97, 3, 'BILLING_ADDRESS', 'Billing address'),
(97, 4, 'BILLING_ADDRESS', 'Alamat penagihan'),
(98, 1, 'DOWN_PAYMENT', 'Acompte'),
(98, 2, 'DOWN_PAYMENT', 'Down payment'),
(98, 3, 'DOWN_PAYMENT', 'Down payment'),
(98, 4, 'DOWN_PAYMENT', 'Uang muka'),
(99, 1, 'PAYMENT_CHECK_NOTICE', 'Merci d\'envoyer un chèque à \"Panda Multi Resorts, Neeloafaru Magu, Maldives\" d\'un montant de {amount}.<br>Votre réservation sera validée à réception du paiement.<br>Merci de votre visite et à bientôt !'),
(99, 2, 'PAYMENT_CHECK_NOTICE', 'Thank you for sending a check of {amount} to \"Panda Multi Resorts, Neeloafaru Magu, Maldives\".<br>Your reservation will be confirmed upon receipt of the payment.<br>Thank you for your visit and see you soon!'),
(99, 3, 'PAYMENT_CHECK_NOTICE', 'Thank you for sending a check of {amount} to \"Panda Multi Resorts, Neeloafaru Magu, Maldives\".<br>Your reservation will be confirmed upon receipt of the payment.<br>Thank you for your visit and see you soon!'),
(99, 4, 'PAYMENT_CHECK_NOTICE', 'Terima kasih telah mengirim cek {jumlah} ke \"Resor Multi Panda, Neeloafaru Magu, Maladewa\". <br> Reservasi Anda akan dikonfirmasikan setelah menerima pembayaran. <br> Terima kasih atas kunjungan Anda dan sampai jumpa lagi!'),
(100, 1, 'PAYMENT_ARRIVAL_NOTICE', 'Veuillez régler le solde de votre réservation d\'un montant de {amount} à votre arrivée.<br>Merci de votre visite et à bientôt !'),
(100, 2, 'PAYMENT_ARRIVAL_NOTICE', 'Thank you for paying the balance of {amount} for your booking on your arrival.<br>Thank you for your visit and see you soon!'),
(100, 3, 'PAYMENT_ARRIVAL_NOTICE', 'Thank you for paying the balance of {amount} for your booking on your arrival.<br>Thank you for your visit and see you soon!'),
(100, 4, 'PAYMENT_ARRIVAL_NOTICE', 'Terima kasih telah membayar saldo {jumlah} untuk pemesanan Anda pada saat kedatangan Anda. <br> Terima kasih atas kunjungan Anda dan sampai jumpa lagi!'),
(101, 1, 'MAX_PEOPLE', 'Pers. max'),
(101, 2, 'MAX_PEOPLE', 'Max people'),
(101, 3, 'MAX_PEOPLE', 'Max people'),
(101, 4, 'MAX_PEOPLE', 'Maks orang'),
(102, 1, 'VAT_AMOUNT', 'Dont TVA'),
(102, 2, 'VAT_AMOUNT', 'VAT amount'),
(102, 3, 'VAT_AMOUNT', 'VAT amount'),
(102, 4, 'VAT_AMOUNT', 'Juamlah tangki'),
(103, 1, 'MIN_NIGHTS', 'Nuits min'),
(103, 2, 'MIN_NIGHTS', 'Min nights'),
(103, 3, 'MIN_NIGHTS', 'Min nights'),
(103, 4, 'MIN_NIGHTS', 'Min malam'),
(104, 1, 'ROOMS', 'Chambres'),
(104, 2, 'ROOMS', 'Rooms'),
(104, 3, 'ROOMS', 'Rooms'),
(104, 4, 'ROOMS', 'Kamar'),
(105, 1, 'RATINGS', 'Note(s)'),
(105, 2, 'RATINGS', 'Rating(s)'),
(105, 3, 'RATINGS', 'Rating(s)'),
(105, 4, 'RATINGS', 'Peringkat'),
(106, 1, 'MIN_PEOPLE', 'Pers. min'),
(106, 2, 'MIN_PEOPLE', 'Min people'),
(106, 3, 'MIN_PEOPLE', 'Min people'),
(106, 4, 'MIN_PEOPLE', 'Min orang'),
(107, 1, 'HOTEL', 'Hôtel'),
(107, 2, 'HOTEL', 'Hotel'),
(107, 3, 'HOTEL', 'Hotel'),
(107, 4, 'HOTEL', 'Hotel'),
(108, 1, 'MAKE_A_REQUEST', 'Faire une demande'),
(108, 2, 'MAKE_A_REQUEST', 'Make a request'),
(108, 3, 'MAKE_A_REQUEST', 'Make a request'),
(108, 4, 'MAKE_A_REQUEST', 'Buat sebuah permintaan'),
(109, 1, 'FULLNAME', 'Nom complet'),
(109, 2, 'FULLNAME', 'Full Name'),
(109, 3, 'FULLNAME', 'Full Name'),
(109, 4, 'FULLNAME', 'Nama Lengkap'),
(110, 1, 'PASSWORD', 'Mot de passe'),
(110, 2, 'PASSWORD', 'Password'),
(110, 3, 'PASSWORD', 'Password'),
(110, 4, 'PASSWORD', 'Kata sandi'),
(111, 1, 'LOG_IN_WITH_FACEBOOK', 'Enregistrez-vous avec Facebook'),
(111, 2, 'LOG_IN_WITH_FACEBOOK', 'Log in with Facebook'),
(111, 3, 'LOG_IN_WITH_FACEBOOK', 'Log in with Facebook'),
(111, 4, 'LOG_IN_WITH_FACEBOOK', 'Masuk dengan Facebook'),
(112, 1, 'OR', 'Ou'),
(112, 2, 'OR', 'Or'),
(112, 3, 'OR', 'Or'),
(112, 4, 'OR', 'Atau'),
(113, 1, 'NEW_PASSWORD', 'Nouveau mot de passe'),
(113, 2, 'NEW_PASSWORD', 'New password'),
(113, 3, 'NEW_PASSWORD', 'New password'),
(113, 4, 'NEW_PASSWORD', 'Kata sandi baru'),
(114, 1, 'NEW_PASSWORD_NOTICE', 'Merci d\'entrer l\'adresse e-mail correspondant à votre compte. Un nouveau mot de passe vous sera envoyé par e-mail.'),
(114, 2, 'NEW_PASSWORD_NOTICE', 'Please enter your e-mail address corresponding to your account. A new password will be sent to you by e-mail.'),
(114, 3, 'NEW_PASSWORD_NOTICE', 'Please enter your e-mail address corresponding to your account. A new password will be sent to you by e-mail.'),
(114, 4, 'NEW_PASSWORD_NOTICE', 'Silakan masukkan alamat email Anda yang sesuai dengan akun Anda. Kata sandi baru akan dikirimkan kepada Anda melalui e-mail.'),
(115, 1, 'USERNAME', 'Utilisateur'),
(115, 2, 'USERNAME', 'Username'),
(115, 3, 'USERNAME', 'Username'),
(115, 4, 'USERNAME', 'Nama pengguna'),
(116, 1, 'PASSWORD_CONFIRM', 'Confirmer mot de passe'),
(116, 2, 'PASSWORD_CONFIRM', 'Confirm password'),
(116, 3, 'PASSWORD_CONFIRM', 'Confirm password'),
(116, 4, 'PASSWORD_CONFIRM', 'Konfirmasi kata sandi'),
(117, 1, 'USERNAME_EXISTS', 'Un compte existe déjà avec ce nom d\'utilisateur'),
(117, 2, 'USERNAME_EXISTS', 'An account already exists with this username'),
(117, 3, 'USERNAME_EXISTS', 'An account already exists with this username'),
(117, 4, 'USERNAME_EXISTS', 'Akun sudah ada dengan nama pengguna ini'),
(118, 1, 'ACCOUNT_EDIT_SUCCESS', 'Les informations de votre compte ont bien été modifiées.'),
(118, 2, 'ACCOUNT_EDIT_SUCCESS', 'Your account information have been changed.'),
(118, 3, 'ACCOUNT_EDIT_SUCCESS', 'Your account information have been changed.'),
(118, 4, 'ACCOUNT_EDIT_SUCCESS', 'Informasi akun Anda telah diubah.'),
(119, 1, 'ACCOUNT_EDIT_FAILURE', 'Echec de la modification des informations de compte.'),
(119, 2, 'ACCOUNT_EDIT_FAILURE', 'An error occured during the modification of the account information.'),
(119, 3, 'ACCOUNT_EDIT_FAILURE', 'An error occured during the modification of the account information.'),
(119, 4, 'ACCOUNT_EDIT_FAILURE', 'Terjadi kesalahan selama modifikasi informasi akun.'),
(120, 1, 'ACCOUNT_CREATE_FAILURE', 'Echec de la création du compte.'),
(120, 2, 'ACCOUNT_CREATE_FAILURE', 'Failed to create account.'),
(120, 3, 'ACCOUNT_CREATE_FAILURE', 'Failed to create account.'),
(120, 4, 'ACCOUNT_CREATE_FAILURE', 'Gagal membuat akun'),
(121, 1, 'PAYMENT_CHECK', 'Par chèque'),
(121, 2, 'PAYMENT_CHECK', 'By check'),
(121, 3, 'PAYMENT_CHECK', 'By check'),
(121, 4, 'PAYMENT_CHECK', 'Dengan cek'),
(122, 1, 'PAYMENT_ARRIVAL', 'A l\'arrivée'),
(122, 2, 'PAYMENT_ARRIVAL', 'On arrival'),
(122, 3, 'PAYMENT_ARRIVAL', 'On arrival'),
(122, 4, 'PAYMENT_ARRIVAL', 'Pada saat kedatangan'),
(123, 1, 'CHOOSE_PAYMENT', 'Choisissez un moyen de paiement'),
(123, 2, 'CHOOSE_PAYMENT', 'Choose a method of payment'),
(123, 3, 'CHOOSE_PAYMENT', 'Choose a method of payment'),
(123, 4, 'CHOOSE_PAYMENT', 'Pilih metode pembayaran'),
(124, 1, 'PAYMENT_CREDIT_CARDS', 'Cartes de credit'),
(124, 2, 'PAYMENT_CREDIT_CARDS', 'Credit cards'),
(124, 3, 'PAYMENT_CREDIT_CARDS', 'Credit cards'),
(124, 4, 'PAYMENT_CREDIT_CARDS', 'Kartu kredit'),
(125, 1, 'MAX_ADULTS', 'Adultes max'),
(125, 2, 'MAX_ADULTS', 'Max adults'),
(125, 3, 'MAX_ADULTS', 'Max adults'),
(125, 4, 'MAX_ADULTS', 'Maks dewasa'),
(126, 1, 'MAX_CHILDREN', 'Enfants max'),
(126, 2, 'MAX_CHILDREN', 'Max children'),
(126, 3, 'MAX_CHILDREN', 'Max children'),
(126, 4, 'MAX_CHILDREN', 'Maks anak-anak'),
(127, 1, 'PAYMENT_CARDS_NOTICE', 'Cliquez sur \"Payer\" ci-dessous, vous allez être redirigé vers le site sécurisé de 2Checkout.com'),
(127, 2, 'PAYMENT_CARDS_NOTICE', 'Click on \"Check Out\" below, you will be redirected towards the secure site of 2Checkout.com'),
(127, 3, 'PAYMENT_CARDS_NOTICE', 'Click on \"Check Out\" below, you will be redirected towards the secure site of 2Checkout.com'),
(127, 4, 'PAYMENT_CARDS_NOTICE', 'Klik \"Check Out\" di bawah, Anda akan diarahkan ke situs aman 2Checkout.com'),
(128, 1, 'COOKIES_NOTICE', 'Les cookies nous aident à fournir une meilleure expérience utilisateur. En utilisant notre site, vous acceptez l\'utilisation de cookies.'),
(128, 2, 'COOKIES_NOTICE', 'Cookies help us provide better user experience. By using our website, you agree to the use of cookies.'),
(128, 3, 'COOKIES_NOTICE', 'Cookies help us provide better user experience. By using our website, you agree to the use of cookies.'),
(128, 4, 'COOKIES_NOTICE', 'Cookies membantu kami memberikan pengalaman pengguna yang lebih baik. Dengan menggunakan situs web kami, Anda menyetujui penggunaan cookies.'),
(129, 1, 'DURATION', 'Durée'),
(129, 2, 'DURATION', 'Duration'),
(129, 3, 'DURATION', 'Duration'),
(129, 4, 'DURATION', 'Durasi'),
(130, 1, 'PERSON', 'Personne'),
(130, 2, 'PERSON', 'Person'),
(130, 3, 'PERSON', 'Person'),
(130, 4, 'PERSON', 'Orang'),
(131, 1, 'CHOOSE_A_DATE', 'Choisissez une date'),
(131, 2, 'CHOOSE_A_DATE', 'Choose a date'),
(131, 3, 'CHOOSE_A_DATE', 'Choose a date'),
(131, 4, 'CHOOSE_A_DATE', 'Pilih tanggal'),
(132, 1, 'TIMESLOT', 'Horaire'),
(132, 2, 'TIMESLOT', 'Time slot'),
(132, 3, 'TIMESLOT', 'Time slot'),
(132, 4, 'TIMESLOT', 'Slot waktu'),
(133, 1, 'ACTIVITIES', 'Activités'),
(133, 2, 'ACTIVITIES', 'Activities'),
(133, 3, 'ACTIVITIES', 'Activities'),
(133, 4, 'ACTIVITIES', 'Kegiatan'),
(134, 1, 'DESTINATION', 'Destination'),
(134, 2, 'DESTINATION', 'Destination'),
(134, 3, 'DESTINATION', 'Destination'),
(134, 4, 'DESTINATION', 'Tujuan'),
(135, 1, 'NO_HOTEL_FOUND', 'Aucun hotel trouvé'),
(135, 2, 'NO_HOTEL_FOUND', 'No hotel found'),
(135, 3, 'NO_HOTEL_FOUND', 'No hotel found'),
(135, 4, 'NO_HOTEL_FOUND', 'Tidak ada hotel yang ditemukan'),
(136, 1, 'FOR', 'pour'),
(136, 2, 'FOR', 'for'),
(136, 3, 'FOR', 'for'),
(136, 4, 'FOR', 'untuk'),
(137, 1, 'FIND_ACTIVITIES_AND_TOURS', 'Découvrez nos activités et excursions'),
(137, 2, 'FIND_ACTIVITIES_AND_TOURS', 'Find out our activities and tours'),
(137, 3, 'FIND_ACTIVITIES_AND_TOURS', 'Find out our activities and tours'),
(137, 4, 'FIND_ACTIVITIES_AND_TOURS', 'Cari tahu kegiatan dan wisata kami'),
(138, 1, 'MINUTES', 'minute(s)'),
(138, 2, 'MINUTES', 'minute(s)'),
(138, 3, 'MINUTES', 'minute(s)'),
(138, 4, 'MINUTES', 'menit'),
(139, 1, 'HOURS', 'heure(s)'),
(139, 2, 'HOURS', 'hour(s)'),
(139, 3, 'HOURS', 'hour(s)'),
(139, 4, 'HOURS', 'jam'),
(140, 1, 'DAYS', 'jour(s)'),
(140, 2, 'DAYS', 'day(s)'),
(140, 3, 'DAYS', 'day(s)'),
(140, 4, 'DAYS', 'hari'),
(141, 1, 'WEEKS', 'semaine(s)'),
(141, 2, 'WEEKS', 'week(s)'),
(141, 3, 'WEEKS', 'week(s)'),
(141, 4, 'WEEKS', 'minggu'),
(142, 1, 'RESULTS', 'Résultats'),
(142, 2, 'RESULTS', 'Results'),
(142, 3, 'RESULTS', 'Results'),
(142, 4, 'RESULTS', 'Hasil'),
(143, 1, 'BOOKING_HISTORY', 'Historique des réservations'),
(143, 2, 'BOOKING_HISTORY', 'Booking history'),
(143, 3, 'BOOKING_HISTORY', 'Booking history'),
(143, 4, 'BOOKING_HISTORY', 'Riwayat Pemesanan'),
(144, 1, 'BOOKING_SUMMARY', 'Résumé de la réservation'),
(144, 2, 'BOOKING_SUMMARY', 'Booking summary'),
(144, 3, 'BOOKING_SUMMARY', 'Booking summary'),
(144, 4, 'BOOKING_SUMMARY', 'Ringkasan Pemesanan'),
(145, 1, 'BOOKING_DATE', 'Date de la réservations'),
(145, 2, 'BOOKING_DATE', 'Booking date'),
(145, 3, 'BOOKING_DATE', 'Booking date'),
(145, 4, 'BOOKING_DATE', 'Tanggal Pemesanan'),
(146, 1, 'NO_BOOKING_YET', 'Pas encore de réservation...'),
(146, 2, 'NO_BOOKING_YET', 'No booking yet...'),
(146, 3, 'NO_BOOKING_YET', 'No booking yet...'),
(146, 4, 'NO_BOOKING_YET', 'Belum ada pemesanan...'),
(147, 1, 'PAYMENT', 'Paiement'),
(147, 2, 'PAYMENT', 'Payment'),
(147, 3, 'PAYMENT', 'Payment'),
(147, 4, 'PAYMENT', 'Pembayaran'),
(148, 1, 'PAYMENT_DATE', 'Date du paiement'),
(148, 2, 'PAYMENT_DATE', 'Payment date'),
(148, 3, 'PAYMENT_DATE', 'Payment date'),
(148, 4, 'PAYMENT_DATE', 'Tanggal Pembayaran'),
(149, 1, 'PAYMENT_METHOD', 'Méthode de paiement'),
(149, 2, 'PAYMENT_METHOD', 'Payment method'),
(149, 3, 'PAYMENT_METHOD', 'Payment method'),
(149, 4, 'PAYMENT_METHOD', 'Metode pembayaran'),
(150, 1, 'NUM_TRANSACTION', 'N°transaction'),
(150, 2, 'NUM_TRANSACTION', 'Num. transaction'),
(150, 3, 'NUM_TRANSACTION', 'Num. transaction'),
(150, 4, 'NUM_TRANSACTION', 'Transaksi.num'),
(151, 1, 'STATUS', 'Statut'),
(151, 2, 'STATUS', 'Status'),
(151, 3, 'STATUS', 'Status'),
(151, 4, 'STATUS', 'Status'),
(152, 1, 'AWAITING', 'En attente'),
(152, 2, 'AWAITING', 'Awaiting'),
(152, 3, 'AWAITING', 'Awaiting'),
(152, 4, 'AWAITING', 'Menunggu'),
(153, 1, 'CANCELLED', 'Annulé'),
(153, 2, 'CANCELLED', 'Cancelled'),
(153, 3, 'CANCELLED', 'Cancelled'),
(153, 4, 'CANCELLED', 'Dibatalkan'),
(154, 1, 'REJECTED_PAYMENT', 'Paiement rejeté'),
(154, 2, 'REJECTED_PAYMENT', 'Rejected payment'),
(154, 3, 'REJECTED_PAYMENT', 'Rejected payment'),
(154, 4, 'REJECTED_PAYMENT', 'Pembayaran ditolak'),
(155, 1, 'PAYED', 'Payé'),
(155, 2, 'PAYED', 'Payed'),
(155, 3, 'PAYED', 'Payed'),
(155, 4, 'PAYED', 'Dibayar'),
(156, 1, 'INCL_TAX', 'TTC'),
(156, 2, 'INCL_TAX', 'incl. tax'),
(156, 3, 'INCL_TAX', 'incl. tax'),
(156, 4, 'INCL_TAX', 'incl. tax'),
(157, 1, 'TAGS', 'Tags'),
(157, 2, 'TAGS', 'Tags'),
(157, 3, 'TAGS', 'Tags'),
(157, 4, 'TAGS', 'Tag'),
(158, 1, 'ARCHIVES', 'Archives'),
(158, 2, 'ARCHIVES', 'Archives'),
(158, 3, 'ARCHIVES', 'Archives'),
(158, 4, 'ARCHIVES', 'Arsip'),
(159, 1, 'STARS', 'Étoiles'),
(159, 2, 'STARS', 'Stars'),
(159, 3, 'STARS', 'Stars'),
(159, 4, 'STARS', 'Bintang'),
(160, 1, 'HOTEL_CLASS', 'Catégorie d\'Hôtel'),
(160, 2, 'HOTEL_CLASS', 'Hotel Class'),
(160, 3, 'HOTEL_CLASS', 'Hotel Class'),
(160, 4, 'HOTEL_CLASS', 'Kelas hotel'),
(161, 1, 'YOUR_BUDGET', 'Votre Budget'),
(161, 2, 'YOUR_BUDGET', 'Your Budget'),
(161, 3, 'YOUR_BUDGET', 'Your Budget'),
(161, 4, 'YOUR_BUDGET', 'Anggaran Mu'),
(162, 1, 'LOAD_MORE', 'Voir plus'),
(162, 2, 'LOAD_MORE', 'Load more'),
(162, 3, 'LOAD_MORE', 'Load more'),
(162, 4, 'LOAD_MORE', 'Muat lebih banyak'),
(163, 1, 'DO_YOU_HAVE_A_COUPON', 'Avez-vous un code promo ?'),
(163, 2, 'DO_YOU_HAVE_A_COUPON', 'Do you have a coupon?'),
(163, 3, 'DO_YOU_HAVE_A_COUPON', 'Do you have a coupon?'),
(163, 4, 'DO_YOU_HAVE_A_COUPON', 'Apakah Anda memiliki kode kupon?'),
(164, 1, 'DISCOUNT', 'Réduction'),
(164, 2, 'DISCOUNT', 'Discount'),
(164, 3, 'DISCOUNT', 'Discount'),
(164, 4, 'DISCOUNT', 'Diskon'),
(165, 1, 'COUPON_CODE_SUCCESS', 'Félicitations ! Le code promo a été ajouté avec succès.'),
(165, 2, 'COUPON_CODE_SUCCESS', 'Congratulations! The coupon code has been successfully added.'),
(165, 3, 'COUPON_CODE_SUCCESS', 'Congratulations! The coupon code has been successfully added.'),
(165, 4, 'COUPON_CODE_SUCCESS', 'Selamat! Kode kupon telah berhasil ditambahkan.'),
(166, 1, 'ROOMS', 'chambres'),
(166, 2, 'ROOMS', 'rooms'),
(166, 3, 'ROOMS', 'rooms'),
(166, 4, 'ROOMS', 'kamar'),
(167, 1, 'ADULT', 'adulte'),
(167, 2, 'ADULT', 'adult'),
(167, 3, 'ADULT', 'adult'),
(167, 4, 'ADULT', 'dewasa'),
(168, 1, 'CHILD', 'enfant'),
(168, 2, 'CHILD', 'child'),
(168, 3, 'CHILD', 'child'),
(168, 4, 'CHILD', 'anak-anak'),
(169, 1, 'ACTIVITY', 'Activité'),
(169, 2, 'ACTIVITY', 'Activity'),
(169, 3, 'ACTIVITY', 'Activity'),
(169, 4, 'ACTIVITY', 'Aktivitas'),
(170, 1, 'DATE', 'Date'),
(170, 2, 'DATE', 'Date'),
(170, 3, 'DATE', 'Date'),
(170, 4, 'DATE', 'Tanggal'),
(171, 1, 'QUANTITY', 'Quantité'),
(171, 2, 'QUANTITY', 'Quantity'),
(171, 3, 'QUANTITY', 'Quantity'),
(171, 4, 'QUANTITY', 'Kuantitas'),
(172, 1, 'SERVICE', 'Service'),
(172, 2, 'SERVICE', 'Service'),
(172, 3, 'SERVICE', 'Service'),
(172, 4, 'SERVICE', 'Layanan'),
(173, 1, 'BOOKING_NOTICE', '<h2>Réservez sur notre site</h2><p class=\"lead mb0\">Dépêchez-vous ! Sélectionnez vos chambres, complétez votre réservation et profitez de nos packages et offres spéciales ! <br><b>Meilleur prix garanti !</b></p>'),
(173, 2, 'BOOKING_NOTICE', '<h2>Book on our website</h2><p class=\"lead mb0\">Hurry up! Select the your rooms, complete your booking and take advantage of our special offers and packages!<br><b>Best price guarantee!</b></p>'),
(173, 3, 'BOOKING_NOTICE', '<h2>Book on our website</h2><p class=\"lead mb0\">Hurry up! Select the your rooms, complete your booking and take advantage of our special offers and packages!<br><b>Best price guarantee!</b></p>'),
(173, 4, 'BOOKING_NOTICE', '<h2>Pesan di situs web kami</h2><p class=\"lead mb0\">Cepatlah! Pilih kamar Anda, lengkapi pemesanan Anda, dan manfaatkan penawaran dan paket spesial kami!<br><b>Jaminan harga terbaik!</b></p>'),
(174, 1, 'NUM_ROOMS', 'Nb chambres'),
(174, 2, 'NUM_ROOMS', 'Num rooms'),
(174, 3, 'NUM_ROOMS', 'Num rooms'),
(174, 4, 'NUM_ROOMS', 'Num rooms'),
(175, 1, 'TOP_DESTINATIONS', 'Top Destinations'),
(175, 2, 'TOP_DESTINATIONS', 'Top Destinations'),
(175, 3, 'TOP_DESTINATIONS', 'Top Destinations'),
(175, 4, 'TOP_DESTINATIONS', 'Destinasi Teratas'),
(176, 1, 'BEST_RATES_SUBTITLE', 'Meilleurs tarifs à partir de seulement {min_price}'),
(176, 2, 'BEST_RATES_SUBTITLE', 'Best rates starting at just {min_price}'),
(176, 3, 'BEST_RATES_SUBTITLE', 'Best rates starting at just {min_price}'),
(176, 4, 'BEST_RATES_SUBTITLE', 'Harga terbaik mulai dari {min_price}'),
(177, 1, 'CONTINUE_AS_GUEST', 'Continuer sans m\'enregistrer'),
(177, 2, 'CONTINUE_AS_GUEST', 'Continue as guest'),
(177, 3, 'CONTINUE_AS_GUEST', 'Continue as guest'),
(177, 4, 'CONTINUE_AS_GUEST', 'Lanjutkan sebagai tamu'),
(178, 1, 'PRIVACY_POLICY_AGREEMENT', '<small>J\'accepte que les informations recueillies par ce formulaire soient stockées dans un fichier informatisé afin de traiter ma demande.<br>Conformément au \"Réglement Général sur la Protection des Données\", vous pouvez exercer votre droit d\'accès aux données vous concernant et les faire rectifier via le formulaire de contact.</small>'),
(178, 2, 'PRIVACY_POLICY_AGREEMENT', '<small>I agree that the information collected by this form will be stored in a database in order to process my request.<br>In accordance with the \"General Data Protection Regulation\", you can exercise your right to access to your data and make them rectified via the contact form.</small>'),
(178, 3, 'PRIVACY_POLICY_AGREEMENT', '<small>I agree that the information collected by this form will be stored in a database in order to process my request.<br>In accordance with the \"General Data Protection Regulation\", you can exercise your right to access to your data and make them rectified via the contact form.</small>'),
(178, 4, 'PRIVACY_POLICY_AGREEMENT', '<small>Saya setuju bahwa informasi yang dikumpulkan oleh formulir ini akan disimpan dalam basis data untuk memproses permintaan saya.<br>Sesuai dengan \"Peraturan Perlindungan Data Umum\", Anda dapat menggunakan hak Anda untuk mengakses data Anda dan membuatnya diperbaiki melalui formulir kontak.</small>'),
(179, 1, 'COMPLETE_YOUR_BOOKING', 'Terminez votre réservation !'),
(179, 2, 'COMPLETE_YOUR_BOOKING', 'Complete your booking!'),
(179, 3, 'COMPLETE_YOUR_BOOKING', 'Complete your booking!'),
(179, 4, 'COMPLETE_YOUR_BOOKING', 'Lengkapi pemesanan Anda!'),
(180, 1, 'CHILDREN_AGE', 'Age des enfants'),
(180, 2, 'CHILDREN_AGE', 'Age of children'),
(180, 3, 'CHILDREN_AGE', 'Age of children'),
(180, 4, 'CHILDREN_AGE', 'Usia Anak-anak'),
(181, 1, 'I_AM_HOTEL_OWNER', 'Je suis propriétaire'),
(181, 2, 'I_AM_HOTEL_OWNER', 'I am a hotel owner'),
(181, 3, 'I_AM_HOTEL_OWNER', 'I am a hotel owner'),
(181, 4, 'I_AM_HOTEL_OWNER', 'Saya adalah pemilik hotel'),
(182, 1, 'I_AM_TRAVELER', 'Je suis vacancier'),
(182, 2, 'I_AM_TRAVELER', 'I am a traveler'),
(182, 3, 'I_AM_TRAVELER', 'I am a traveler'),
(182, 4, 'I_AM_TRAVELER', 'Saya seorang wisatawan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_user`
--

CREATE TABLE `pm_user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `login` varchar(50) DEFAULT NULL,
  `pass` varchar(100) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `add_date` int(11) DEFAULT NULL,
  `edit_date` int(11) DEFAULT NULL,
  `checked` int(11) DEFAULT 0,
  `fb_id` varchar(50) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `postcode` varchar(10) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `token` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_user`
--

INSERT INTO `pm_user` (`id`, `firstname`, `lastname`, `email`, `login`, `pass`, `type`, `add_date`, `edit_date`, `checked`, `fb_id`, `address`, `postcode`, `city`, `company`, `country`, `mobile`, `phone`, `token`) VALUES
(1, 'Administrator', ' ', 'payment.lawangwangi@gmail.com', 'devsalse', 'db507706f2397373ff441ce5622b0c01', 'administrator', 1563677945, 1583297306, 1, '', 'Bandung', ' ', ' ', ' ', ' ', ' ', '0987654321', ''),
(6, 'admin2', NULL, 'admin2@salsebandung.com', 'admin2', '0192023a7bbd73250516f069df18b500', 'administrator', 1576652000, 1580974589, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'admin3', NULL, 'admin3@salsebandung.com', 'admin3', '81d9ef4b3aea3e3e72b6f19ff7524e97', 'administrator', 1580973219, 1617181263, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '', '', 'tunkirachman@gmail.com', 'tsukiar', '130e218011d5fe1308f78258fd91f983', 'registered', 1583900906, NULL, NULL, NULL, '', '', '', '', '', '', '', '20893a1cb0c2f41264535bb9a0de012e'),
(11, 'Lisana R', '', 'lisana.rachmawati@gmail.com', 'lisana.rachmawati93', 'b6f32f5832f1200e07b7cb24c44d3440', 'registered', 1584071879, NULL, NULL, NULL, 'Baleendah, Kab. Bandung', '', '', '', '0', '', '085730533593', '1048ace442833cab4bfedc070913c386'),
(12, '', '', 'lisana.rachmawati93@pln.co.id', 'lisana.rachmawati93@pln.co.id', 'b6f32f5832f1200e07b7cb24c44d3440', 'registered', 1584072257, NULL, NULL, NULL, '', '', '', '', '', '', '', 'e91f89f9028fda2bac7b64b80d6a8eca'),
(13, 'Manager', NULL, 'managersalse@gmail.com', 'manager', '0795151defba7a4b5dfa89170de46277', 'manager', 1593694744, 1593694750, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, '', '', 'andika.syahli@yahoo.co.id', 'andikasjahli', '1a91f528d680b103b083bd51eb89fe50', 'registered', 1604078255, NULL, NULL, NULL, '', '', '', '', '', '', '', '89a2cd1796b35f9b8348f239d52b7dc0'),
(15, '', '', 'andika828.as@gmail.com', 'andikasyahlie', '1a91f528d680b103b083bd51eb89fe50', 'registered', 1604080658, NULL, NULL, NULL, '', '', '', '', '', '', '', '589d08fed3c375701f971d5a2d9ede9f'),
(16, 'Yunita Ng', '', 'yunita.ng36@gmail.com', 'yunita.ng36', '9d7b31ff407608ebfd7f375f15ed44d3', 'registered', 1604323194, NULL, NULL, NULL, 'Jl. Permata biru I H3/15', '', '', '', '0', '', '081585550306', '8bae000c04e33cc818c09b3215d74a0c'),
(17, 'marini', '', 'lini_darwin@yahoo.com', 'marinidarwin', '8f92d7206128c54899c8cce24d47cf3f', 'registered', 1604900651, NULL, NULL, NULL, 'Jl.Boulevard River Garden Clust LA Seine Blok F16/21 Cakung - Jakarta Timur', '', '', '', '0', '', '085719119659', '596c7ebddcf830099c068bbc262f8fd5'),
(18, 'marini', '', 'marini.darwin@gmail.com', 'marini', '8f92d7206128c54899c8cce24d47cf3f', 'registered', 1604900709, NULL, NULL, NULL, 'Jl.Boulevard River Garden Clust LA Seine Blok F16/21 Cakung - Jakarta Timur', '', '', '', '0', '', '085719119659', 'eb383ea8a092f6532057ba0682742a0b'),
(19, 'Rachel F Azhari', '', 'Rachelfaiqa15@gmail.com', 'Rachel19', '11a72d043d3cea632b68e095671b84a9', 'registered', 1605336164, NULL, NULL, NULL, 'Jl purba kencana II no 12', '', '', '', '0', '', '081380536711', 'd8c04f8e22c10c0fd3ae60b15bd0d680'),
(20, 'Rachel F Azhari', '', 'Rachelazhari19@gmail.com', 'Rachelfaiqaa ', '11a72d043d3cea632b68e095671b84a9', 'registered', 1605336360, NULL, NULL, NULL, 'Jl purba kencana II no 12', '', '', '', '0', '', '081380536711', '2d6023941d870ba928a7bc58d66dc896'),
(21, '', '', 'rachelfaiqa15@gmail.com', 'Rachelfaiqa', '11a72d043d3cea632b68e095671b84a9', 'registered', 1605336618, NULL, NULL, NULL, '', '', '', '', '', '', '', '4aea233b93f55f68f4ebee7e9e5d23cc'),
(22, 'Ribka', '', 'ribka_berliana@yahoo.com', 'RIBKA', 'ed199b75b8e63a46fed0b277be206154', 'registered', 1606782811, NULL, NULL, NULL, 'Jl Lotus Garden 5 No 17, Grand Galaxy City ', '', '', '', 'Indonesia', '', '085723603439', '84f3b9e1f44233969e16b5291f75aa41'),
(23, '', '', 'ribkaberliana8@gmail.com', 'eliana', 'ed199b75b8e63a46fed0b277be206154', 'registered', 1606834816, NULL, NULL, NULL, '', '', '', '', '', '', '', '899e314935fa5c4eebdc6fb923e2ee8c'),
(24, '', '', 'risniry@gmail.com', 'risni ', 'e807f1fcf82d132f9bb018ca6738a19f', 'registered', 1609746545, NULL, NULL, NULL, '', '', '', '', '', '', '', '226c2de39ae9c1a9c459f524228e4b50'),
(25, '', '', 'rissun63@gmail.com', 'risni sundari', 'e807f1fcf82d132f9bb018ca6738a19f', 'registered', 1609746589, NULL, NULL, NULL, '', '', '', '', '', '', '', '35c095107b9e670b6aa819809bc13a28'),
(26, 'Bella Sandra Pratiwi', '', 'bellasandrapratiwi@gmail.com', 'Bella Sandra Pratiwi', 'c5d2385f674a24197fef0b79e3923a79', 'registered', 1616223171, NULL, NULL, NULL, 'Komplek bandung inten indah', '', '', '', '0', '', '081321910149', 'a382c9c1d1000e9a33f3a8e192601adb'),
(27, '', '', 'ooossyy@gmail.com', 'Osy Permatasari Nurakhmantari', '38ca4c2992d5532340c7164daf8d7897', 'registered', 1616727404, NULL, NULL, NULL, '', '', '', '', '', '', '', '2489bb2bbded7e3029b73763f5cbe859'),
(28, '', '', 'osypermatasari@gmail.com', 'Osy Permatasari', '38ca4c2992d5532340c7164daf8d7897', 'registered', 1616727448, NULL, NULL, NULL, '', '', '', '', '', '', '', '7cc73dd7a27518e56063add9189145eb'),
(29, '', '', 'kid_lyfe@yahoo.com', 'donny', 'c95961717ce9b078410fd7f9ef8186e3', 'registered', 1616939666, NULL, NULL, NULL, '', '', '', '', '', '', '', '4664570206e7e947136bf21b32ff7826'),
(30, '', '', 'akunbelanjaonline@yahoo.co.id', 'efyl7', 'c95961717ce9b078410fd7f9ef8186e3', 'registered', 1616939933, NULL, NULL, NULL, '', '', '', '', '', '', '', 'a28db6985a9a3cba6fdabb708295ab3d'),
(31, 'Dani Yudansyah', '', 'dani.lawangwangi@gmail.com', 'dani yudansyah', 'b42cc5889b30bb234ea633eec1a06333', 'registered', 1617183767, NULL, NULL, NULL, 'Kpm Pasir Ipis Lembang', '', '', '', '0', '', '088218373819', '0e622bfe222e257414d000ceb2d2a4eb'),
(32, 'Non tiara', '', 'Nontiara789@gmail.com', 'Nontiara789', 'e98abf334941372b5aa5a023d7352436', 'registered', 1617597870, NULL, NULL, NULL, 'Karawang, Jawa barat', '', '', '', '0', '', '085738135149', '416ef28dc8f4534852dde9bf9ec14f7c'),
(33, 'Hilma maulani', '', 'hilmamaw.mh@gmail.com', 'hilma.maulani', 'ab4dfb4c64e5a08dc4a422c29486b854', 'registered', 1621086920, NULL, NULL, NULL, 'Jl. Cendrawasih 5 blok D1 no. 8 rt006 rw014, komplek TNI AD 201, sukamaju baru, tapos, depok, jawa barat 16455', '', '', '', '0', '', '+62 81211984397', '9ba6b134770f29629f4e103d565c4f63'),
(34, 'Ruhiat', '', 'ruhiathermawana@gmail.com', 'ruhiathermawana@gmail.com', 'a89fb691ba26f4395b5dc9e882ea7653', 'registered', 1628896000, NULL, NULL, NULL, 'Jln .Terusan Bojongsoang Kp. Cijagra Rt 01 Rw 09', '', '', '', 'Indonesia', '', '082217635856', '101b09bc82bbc2a530de41a39b60c820'),
(35, '', '', 'ruhiathermawana1@gmail.com', 'ruhiat12', 'a89fb691ba26f4395b5dc9e882ea7653', 'registered', 1628898299, NULL, NULL, NULL, '', '', '', '', '', '', '', '613d78325727b38fb8191377635444f0'),
(36, '', '', 'tipo_lala@yahoo.com', 'tipo_lala', '1bedae126a1125f22683fda858898e2c', 'registered', 1632326727, NULL, NULL, NULL, '', '', '', '', '', '', '', '3fe335bee4b45905e8b83ef35532b4d0'),
(37, '', '', 'nuryuri.nissa@gmail.com', 'nissa', '9465535d22d6c779a64250bb93e3fe18', 'registered', 1636511276, NULL, NULL, NULL, '', '', '', '', '', '', '', '1946003a73939612e94603bdda0fddd2'),
(38, 'Yunita Saridewi', '', 'yunitasaridewi8@gmail.com', 'yunita', '01fee4f0262c67bfd05a302bc1d533aa', 'registered', 1637027506, NULL, NULL, NULL, 'Komp. Kiara Sari Asri, Jl. Kiara sari permai V no. 24, Margacinta, Buah batu', '', '', '', '0', '', '082127692398', 'a66cd711f2047098d672a3f789a1ee3c'),
(39, '', '', 'fenny_vs27@hotmail.com', 'Fenny', 'cbaaf56ea0656c8033b912e7793183f3', 'registered', 1640144807, NULL, NULL, NULL, '', '', '', '', '', '', '', '69f4b810ab72b740b7c9558cb830e2bc'),
(40, 'Ali fadli', '', 'grafitistinus@gmail.com', 'Alifadli', '6622ca590f56c0d2ba78cab5e864a0ac', 'registered', 1640760721, NULL, NULL, NULL, 'Vila nusa indah', '', '', '', '0', '', '081288054689', '7b73bc27a71327b6682a5eafe8a56fd0'),
(41, 'siti hoerunisa', '', 'anisanisa140@gmail.com', 'sitihoerunisa', 'affd5bf4ac6de152a86503d2d274724c', 'registered', 1641604438, NULL, NULL, NULL, 'jl ah nasution no 117', '', '', '', '0', '', '087826440800', '85f1411b73ea230060ba1705c79f6e4f');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pm_widget`
--

CREATE TABLE `pm_widget` (
  `id` int(11) NOT NULL,
  `lang` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `showtitle` int(11) DEFAULT NULL,
  `pos` varchar(20) DEFAULT NULL,
  `allpages` int(11) DEFAULT NULL,
  `pages` varchar(250) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `class` varchar(200) DEFAULT NULL,
  `checked` int(11) DEFAULT 0,
  `rank` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pm_widget`
--

INSERT INTO `pm_widget` (`id`, `lang`, `title`, `showtitle`, `pos`, `allpages`, `pages`, `type`, `content`, `class`, `checked`, `rank`) VALUES
(1, 1, 'Qui sommes-nous ?', 1, 'footer_col_1', 1, '', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget auctor ipsum. Mauris pharetra neque a mauris commodo, at aliquam leo malesuada. Maecenas eget elit eu ligula rhoncus dapibus at non erat. In sed velit eget eros gravida consectetur varius imperdiet lectus.</p>\r\n', '', 1, 1),
(1, 2, 'About us', 1, 'footer_col_1', 1, '', '', '<div class=\"widget-content\">\r\n<p><b>Villatel Salse</b></p>\r\n\r\n<p style=\"text-align: justify;\">Relax = salse! Roughly translated as ‘to unwind’. Salse is a moment when the stillness of time is embraced. An arduous design phase with Baskoro Tedjo and Hepta as the architects, Andre Gunarsa Kidarse and Ruang Hijau as landscape designers, has resulted in a thoughtful resort, a place to mend uneasiness, to leave weariness behind, to calm emotions and to spread love. The main keyword is energizing.</span></p>\r\n</div>\r\n', '', 1, 1),
(1, 3, 'عنا', 1, 'footer_col_1', 1, '', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget auctor ipsum. Mauris pharetra neque a mauris commodo, at aliquam leo malesuada. Maecenas eget elit eu ligula rhoncus dapibus at non erat. In sed velit eget eros gravida consectetur varius imperdiet lectus.</p>\r\n', '', 1, 1),
(1, 4, 'Tentang Kami', 1, 'footer_col_1', 1, '', '', '<div class=\"widget-content\">\r\n<p><b>Villatel Salse</b></p>\r\n\r\n<p style=\"text-align: justify;\">Rileks = Salse! Salse adalah istirahat, sebuah suasana ketika jam berhenti sejenak. Dirancang cukup lama, melibatkan Baskoro Tedjo dan Hepta sebagai Arsitek, Andre Gunarsa Kidarse dan Ruang Hijau sebagai desainer lanskap. Resort ini dirancang sebagai tempat untuk mengobati keresahan, menghilangkan keletihan, meredakan amarah dan menumbuhkan cinta. ENERGIZING, kata kuncinya.</span></span></p>\r\n</div>\r\n', '', 1, 1),
(3, 1, 'Derniers articles', 1, 'footer_col_2', 1, '', 'latest_articles', '', '', 1, 2),
(3, 2, 'Latest articles', 1, 'footer_col_2', 1, '', 'latest_articles', '', '', 1, 2),
(3, 3, 'المقالات الأخيرة', 1, 'footer_col_2', 1, '', 'latest_articles', '', '', 1, 2),
(3, 4, 'Artikel Terbaru', 1, 'footer_col_2', 1, '', 'latest_articles', '', '', 1, 2),
(4, 1, 'Contactez-nous', 0, 'footer_col_3', 1, '', 'contact_informations', '', '', 1, 3),
(4, 2, 'Contact Information', 0, 'footer_col_3', 1, '', 'contact_informations', '', '', 1, 3),
(4, 3, 'اتصل بنا', 0, 'footer_col_3', 1, '', 'contact_informations', '', '', 1, 3),
(4, 4, 'Informasi Kontak', 0, 'footer_col_3', 1, '', 'contact_informations', '', '', 1, 3),
(5, 1, 'Footer form', 0, 'footer_col_3', 1, '', 'footer_form', '', 'footer-form mt10', 0, 4),
(5, 2, 'Footer form', 0, 'footer_col_3', 1, '', 'footer_form', '', 'footer-form mt10', 0, 4),
(5, 3, 'Footer form', 0, 'footer_col_3', 1, '', 'footer_form', '', 'footer-form mt10', 0, 4),
(5, 4, 'Footer form', 0, 'footer_col_3', 1, '', 'footer_form', '', 'footer-form mt10', 0, 4),
(6, 1, 'Blog side', 0, 'right', 0, '17', 'blog_side', '', '', 1, 5),
(6, 2, 'Blog side', 0, 'right', 0, '17', 'blog_side', '', '', 1, 5),
(6, 3, 'Blog side', 0, 'right', 0, '17', 'blog_side', '', '', 1, 5),
(6, 4, 'Sisi Blog', 0, 'right', 0, '17', 'blog_side', '', '', 1, 5);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `doku`
--
ALTER TABLE `doku`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pm_activity`
--
ALTER TABLE `pm_activity`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `activity_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_activity_file`
--
ALTER TABLE `pm_activity_file`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `activity_file_fkey` (`id_item`,`lang`),
  ADD KEY `activity_file_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_activity_session`
--
ALTER TABLE `pm_activity_session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_session_fkey` (`id_activity`);

--
-- Indeks untuk tabel `pm_activity_session_hour`
--
ALTER TABLE `pm_activity_session_hour`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_session_hour_fkey` (`id_activity_session`);

--
-- Indeks untuk tabel `pm_article`
--
ALTER TABLE `pm_article`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `article_lang_fkey` (`lang`),
  ADD KEY `article_page_fkey` (`id_page`,`lang`);

--
-- Indeks untuk tabel `pm_article_file`
--
ALTER TABLE `pm_article_file`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `article_file_fkey` (`id_item`,`lang`),
  ADD KEY `article_file_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_booking`
--
ALTER TABLE `pm_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pm_booking_activity`
--
ALTER TABLE `pm_booking_activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_activity_fkey` (`id_booking`);

--
-- Indeks untuk tabel `pm_booking_payment`
--
ALTER TABLE `pm_booking_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_payment_fkey` (`id_booking`);

--
-- Indeks untuk tabel `pm_booking_room`
--
ALTER TABLE `pm_booking_room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_room_fkey` (`id_booking`);

--
-- Indeks untuk tabel `pm_booking_service`
--
ALTER TABLE `pm_booking_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_service_fkey` (`id_booking`);

--
-- Indeks untuk tabel `pm_booking_tax`
--
ALTER TABLE `pm_booking_tax`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_tax_fkey` (`id_booking`);

--
-- Indeks untuk tabel `pm_comment`
--
ALTER TABLE `pm_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pm_country`
--
ALTER TABLE `pm_country`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pm_coupon`
--
ALTER TABLE `pm_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pm_currency`
--
ALTER TABLE `pm_currency`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pm_destination`
--
ALTER TABLE `pm_destination`
  ADD PRIMARY KEY (`id`,`lang`);

--
-- Indeks untuk tabel `pm_destination_file`
--
ALTER TABLE `pm_destination_file`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `destination_file_fkey` (`id_item`,`lang`),
  ADD KEY `destination_file_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_email_content`
--
ALTER TABLE `pm_email_content`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `email_content_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_facility`
--
ALTER TABLE `pm_facility`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `facility_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_facility_file`
--
ALTER TABLE `pm_facility_file`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `facility_file_fkey` (`id_item`,`lang`),
  ADD KEY `facility_file_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_hotel`
--
ALTER TABLE `pm_hotel`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `hotel_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_hotel_file`
--
ALTER TABLE `pm_hotel_file`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `hotel_file_fkey` (`id_item`,`lang`),
  ADD KEY `hotel_file_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_lang`
--
ALTER TABLE `pm_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pm_lang_file`
--
ALTER TABLE `pm_lang_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lang_file_fkey` (`id_item`);

--
-- Indeks untuk tabel `pm_location`
--
ALTER TABLE `pm_location`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pm_media`
--
ALTER TABLE `pm_media`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pm_media_file`
--
ALTER TABLE `pm_media_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_file_fkey` (`id_item`);

--
-- Indeks untuk tabel `pm_menu`
--
ALTER TABLE `pm_menu`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `menu_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_message`
--
ALTER TABLE `pm_message`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pm_package`
--
ALTER TABLE `pm_package`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pm_page`
--
ALTER TABLE `pm_page`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `page_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_page_file`
--
ALTER TABLE `pm_page_file`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `page_file_fkey` (`id_item`,`lang`),
  ADD KEY `page_file_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_rate`
--
ALTER TABLE `pm_rate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rate_room_fkey` (`id_room`);

--
-- Indeks untuk tabel `pm_rate_child`
--
ALTER TABLE `pm_rate_child`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rate_child_fkey` (`id_rate`);

--
-- Indeks untuk tabel `pm_room`
--
ALTER TABLE `pm_room`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `room_lang_fkey` (`lang`),
  ADD KEY `room_hotel_fkey` (`id_hotel`,`lang`);

--
-- Indeks untuk tabel `pm_room_closing`
--
ALTER TABLE `pm_room_closing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_closing_fkey` (`id_room`);

--
-- Indeks untuk tabel `pm_room_file`
--
ALTER TABLE `pm_room_file`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `room_file_fkey` (`id_item`,`lang`),
  ADD KEY `room_file_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_room_lock`
--
ALTER TABLE `pm_room_lock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_lock_fkey` (`id_room`);

--
-- Indeks untuk tabel `pm_service`
--
ALTER TABLE `pm_service`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `service_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_slide`
--
ALTER TABLE `pm_slide`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `slide_lang_fkey` (`lang`),
  ADD KEY `slide_page_fkey` (`id_page`,`lang`);

--
-- Indeks untuk tabel `pm_slide_file`
--
ALTER TABLE `pm_slide_file`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `slide_file_fkey` (`id_item`,`lang`),
  ADD KEY `slide_file_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_social`
--
ALTER TABLE `pm_social`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pm_tag`
--
ALTER TABLE `pm_tag`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `tag_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_tax`
--
ALTER TABLE `pm_tax`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `tax_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_text`
--
ALTER TABLE `pm_text`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `text_lang_fkey` (`lang`);

--
-- Indeks untuk tabel `pm_user`
--
ALTER TABLE `pm_user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pm_widget`
--
ALTER TABLE `pm_widget`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `widget_lang_fkey` (`lang`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `doku`
--
ALTER TABLE `doku`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT untuk tabel `pm_activity`
--
ALTER TABLE `pm_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `pm_activity_file`
--
ALTER TABLE `pm_activity_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT untuk tabel `pm_activity_session`
--
ALTER TABLE `pm_activity_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `pm_activity_session_hour`
--
ALTER TABLE `pm_activity_session_hour`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `pm_article`
--
ALTER TABLE `pm_article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `pm_article_file`
--
ALTER TABLE `pm_article_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT untuk tabel `pm_booking`
--
ALTER TABLE `pm_booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT untuk tabel `pm_booking_activity`
--
ALTER TABLE `pm_booking_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pm_booking_payment`
--
ALTER TABLE `pm_booking_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT untuk tabel `pm_booking_room`
--
ALTER TABLE `pm_booking_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT untuk tabel `pm_booking_service`
--
ALTER TABLE `pm_booking_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pm_booking_tax`
--
ALTER TABLE `pm_booking_tax`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `pm_comment`
--
ALTER TABLE `pm_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `pm_country`
--
ALTER TABLE `pm_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;

--
-- AUTO_INCREMENT untuk tabel `pm_coupon`
--
ALTER TABLE `pm_coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pm_currency`
--
ALTER TABLE `pm_currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `pm_destination`
--
ALTER TABLE `pm_destination`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pm_destination_file`
--
ALTER TABLE `pm_destination_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pm_email_content`
--
ALTER TABLE `pm_email_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `pm_facility`
--
ALTER TABLE `pm_facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT untuk tabel `pm_facility_file`
--
ALTER TABLE `pm_facility_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT untuk tabel `pm_hotel`
--
ALTER TABLE `pm_hotel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `pm_hotel_file`
--
ALTER TABLE `pm_hotel_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `pm_lang`
--
ALTER TABLE `pm_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `pm_lang_file`
--
ALTER TABLE `pm_lang_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `pm_location`
--
ALTER TABLE `pm_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pm_media`
--
ALTER TABLE `pm_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pm_media_file`
--
ALTER TABLE `pm_media_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pm_menu`
--
ALTER TABLE `pm_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `pm_message`
--
ALTER TABLE `pm_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=822;

--
-- AUTO_INCREMENT untuk tabel `pm_package`
--
ALTER TABLE `pm_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `pm_page`
--
ALTER TABLE `pm_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT untuk tabel `pm_page_file`
--
ALTER TABLE `pm_page_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `pm_rate`
--
ALTER TABLE `pm_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=561;

--
-- AUTO_INCREMENT untuk tabel `pm_rate_child`
--
ALTER TABLE `pm_rate_child`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `pm_room`
--
ALTER TABLE `pm_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `pm_room_closing`
--
ALTER TABLE `pm_room_closing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=219;

--
-- AUTO_INCREMENT untuk tabel `pm_room_file`
--
ALTER TABLE `pm_room_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `pm_room_lock`
--
ALTER TABLE `pm_room_lock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=348;

--
-- AUTO_INCREMENT untuk tabel `pm_service`
--
ALTER TABLE `pm_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `pm_slide`
--
ALTER TABLE `pm_slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `pm_slide_file`
--
ALTER TABLE `pm_slide_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `pm_social`
--
ALTER TABLE `pm_social`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pm_tag`
--
ALTER TABLE `pm_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pm_tax`
--
ALTER TABLE `pm_tax`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pm_text`
--
ALTER TABLE `pm_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;

--
-- AUTO_INCREMENT untuk tabel `pm_user`
--
ALTER TABLE `pm_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT untuk tabel `pm_widget`
--
ALTER TABLE `pm_widget`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `pm_activity`
--
ALTER TABLE `pm_activity`
  ADD CONSTRAINT `activity_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_activity_file`
--
ALTER TABLE `pm_activity_file`
  ADD CONSTRAINT `activity_file_fkey` FOREIGN KEY (`id_item`,`lang`) REFERENCES `pm_activity` (`id`, `lang`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `activity_file_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_activity_session`
--
ALTER TABLE `pm_activity_session`
  ADD CONSTRAINT `activity_session_fkey` FOREIGN KEY (`id_activity`) REFERENCES `pm_activity` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_activity_session_hour`
--
ALTER TABLE `pm_activity_session_hour`
  ADD CONSTRAINT `activity_session_hour_fkey` FOREIGN KEY (`id_activity_session`) REFERENCES `pm_activity_session` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_article`
--
ALTER TABLE `pm_article`
  ADD CONSTRAINT `article_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `article_page_fkey` FOREIGN KEY (`id_page`,`lang`) REFERENCES `pm_page` (`id`, `lang`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_article_file`
--
ALTER TABLE `pm_article_file`
  ADD CONSTRAINT `article_file_fkey` FOREIGN KEY (`id_item`,`lang`) REFERENCES `pm_article` (`id`, `lang`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `article_file_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_booking_activity`
--
ALTER TABLE `pm_booking_activity`
  ADD CONSTRAINT `booking_activity_fkey` FOREIGN KEY (`id_booking`) REFERENCES `pm_booking` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_booking_payment`
--
ALTER TABLE `pm_booking_payment`
  ADD CONSTRAINT `booking_payment_fkey` FOREIGN KEY (`id_booking`) REFERENCES `pm_booking` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_booking_room`
--
ALTER TABLE `pm_booking_room`
  ADD CONSTRAINT `booking_room_fkey` FOREIGN KEY (`id_booking`) REFERENCES `pm_booking` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_booking_service`
--
ALTER TABLE `pm_booking_service`
  ADD CONSTRAINT `booking_service_fkey` FOREIGN KEY (`id_booking`) REFERENCES `pm_booking` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_booking_tax`
--
ALTER TABLE `pm_booking_tax`
  ADD CONSTRAINT `booking_tax_fkey` FOREIGN KEY (`id_booking`) REFERENCES `pm_booking` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_destination_file`
--
ALTER TABLE `pm_destination_file`
  ADD CONSTRAINT `destination_file_fkey` FOREIGN KEY (`id_item`,`lang`) REFERENCES `pm_destination` (`id`, `lang`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `destination_file_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_email_content`
--
ALTER TABLE `pm_email_content`
  ADD CONSTRAINT `email_content_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_facility`
--
ALTER TABLE `pm_facility`
  ADD CONSTRAINT `facility_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_facility_file`
--
ALTER TABLE `pm_facility_file`
  ADD CONSTRAINT `facility_file_fkey` FOREIGN KEY (`id_item`,`lang`) REFERENCES `pm_facility` (`id`, `lang`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `facility_file_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_hotel`
--
ALTER TABLE `pm_hotel`
  ADD CONSTRAINT `hotel_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_hotel_file`
--
ALTER TABLE `pm_hotel_file`
  ADD CONSTRAINT `hotel_file_fkey` FOREIGN KEY (`id_item`,`lang`) REFERENCES `pm_hotel` (`id`, `lang`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `hotel_file_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_lang_file`
--
ALTER TABLE `pm_lang_file`
  ADD CONSTRAINT `lang_file_fkey` FOREIGN KEY (`id_item`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_media_file`
--
ALTER TABLE `pm_media_file`
  ADD CONSTRAINT `media_file_fkey` FOREIGN KEY (`id_item`) REFERENCES `pm_media` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_menu`
--
ALTER TABLE `pm_menu`
  ADD CONSTRAINT `menu_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_page`
--
ALTER TABLE `pm_page`
  ADD CONSTRAINT `page_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_page_file`
--
ALTER TABLE `pm_page_file`
  ADD CONSTRAINT `page_file_fkey` FOREIGN KEY (`id_item`,`lang`) REFERENCES `pm_page` (`id`, `lang`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `page_file_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_rate`
--
ALTER TABLE `pm_rate`
  ADD CONSTRAINT `rate_room_fkey` FOREIGN KEY (`id_room`) REFERENCES `pm_room` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_rate_child`
--
ALTER TABLE `pm_rate_child`
  ADD CONSTRAINT `rate_child_fkey` FOREIGN KEY (`id_rate`) REFERENCES `pm_rate` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_room`
--
ALTER TABLE `pm_room`
  ADD CONSTRAINT `room_hotel_fkey` FOREIGN KEY (`id_hotel`,`lang`) REFERENCES `pm_hotel` (`id`, `lang`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `room_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_room_closing`
--
ALTER TABLE `pm_room_closing`
  ADD CONSTRAINT `room_closing_fkey` FOREIGN KEY (`id_room`) REFERENCES `pm_room` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_room_file`
--
ALTER TABLE `pm_room_file`
  ADD CONSTRAINT `room_file_fkey` FOREIGN KEY (`id_item`,`lang`) REFERENCES `pm_room` (`id`, `lang`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `room_file_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_room_lock`
--
ALTER TABLE `pm_room_lock`
  ADD CONSTRAINT `room_lock_fkey` FOREIGN KEY (`id_room`) REFERENCES `pm_room` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_service`
--
ALTER TABLE `pm_service`
  ADD CONSTRAINT `service_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_slide`
--
ALTER TABLE `pm_slide`
  ADD CONSTRAINT `slide_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `slide_page_fkey` FOREIGN KEY (`id_page`,`lang`) REFERENCES `pm_page` (`id`, `lang`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_slide_file`
--
ALTER TABLE `pm_slide_file`
  ADD CONSTRAINT `slide_file_fkey` FOREIGN KEY (`id_item`,`lang`) REFERENCES `pm_slide` (`id`, `lang`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `slide_file_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_tag`
--
ALTER TABLE `pm_tag`
  ADD CONSTRAINT `tag_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_tax`
--
ALTER TABLE `pm_tax`
  ADD CONSTRAINT `tax_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_text`
--
ALTER TABLE `pm_text`
  ADD CONSTRAINT `text_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pm_widget`
--
ALTER TABLE `pm_widget`
  ADD CONSTRAINT `widget_lang_fkey` FOREIGN KEY (`lang`) REFERENCES `pm_lang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
