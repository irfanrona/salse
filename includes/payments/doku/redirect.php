<?php
require_once('../../../common/lib.php');
require_once('../../../common/define.php');

$order_number = @$_POST['TRANSIDMERCHANT'];
$purchase_amt = @$_POST['AMOUNT'];
$status_code = @$_POST['STATUSCODE'];
$words = @$_POST['WORDS'];
$paymentchannel = @$_POST['PAYMENTCHANNEL'];
$session_id = @$_POST['SESSIONID'];
$paymentcode = @$_POST['PAYMENTCODE'];

//FIREFOX FIX
$redirect_url = '../../../';
$redirect_url = str_replace('&amp;', '&', $redirect_url);

?>
<body> 
    <form name="param_pass" id="param_pass" method="post" action="<?php echo $redirect_url; ?>">
        <input name="order_number" type="hidden" id="order_number" value="<?php echo $order_number; ?>">
        <input name="purchase_amt" type="hidden" id="purchase_amt" value="<?php echo $purchase_amt; ?>">
        <input name="status_code" type="hidden" id="status_code" value="<?php echo $status_code; ?>">
        <input name="words" type="hidden" id="words" value="<?php echo $words; ?>">
        <input name="paymentchannel" type="hidden" id="paymentchannel" value="<?php echo $paymentchannel; ?>">
        <input name="session_id" type="hidden" id="session_id" value="<?php echo $session_id; ?>">
        <input name="paymentcode" type="hidden" id="paymentcode" value="<?php echo $paymentcode; ?>">
    </form>

    <script language="JavaScript" type="text/javascript">
        setTimeout(function(){ document.getElementById('param_pass').submit(); }, 5000);
    </script>

    <noscript>
    If you are not redirected please <a href="<?php echo $redirect_url; ?>">click here</a> to confirm your order.
    </noscript>

    <section>
        <?php 
        $article_id = 0;
        $page['title'] = "redirect";
        $page['subtitle'] = "redirect";
        $page['name'] = "redirect";
        $breadcrumbs = array();
        include(getFromTemplate('common/page_header.php', false)); ?>
        <div id="content" class="pt30 pb30">
            <div class="container">

                <div class="alert alert-success" style="display:none;"></div>
                <div class="alert alert-danger" style="display:none;"></div>
                
                <div class="row mb30">
                    You will be redirected soon to confirm your order.
                    Thank You.
                </div>
            </div>
        </div>
    </section>

</body>
<?php

$msg_error = '';
$msg_success = '';

/* ==============================================
 * CSS AND JAVASCRIPT USED IN THIS MODEL
 * ==============================================
 */

$javascripts[] = 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js';
$stylesheets[] = array('file' => 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css', 'media' => 'all');

$javascripts[] = 'https://staging.doku.com/doku-js/assets/js/doku.js';
$stylesheets[] = array('file' => 'https://staging.doku.com/doku-js/assets/css/doku.css', 'media' => 'all');

// $javascripts[] = 'https://pay.doku.com/doku-js/assets/js/doku.js';
// $stylesheets[] = array('file' => 'https://pay.doku.com/doku-js/assets/css/doku.css', 'media' => 'all');

$page['descr'] = "";
$page['text'] = "";
$title_tag = "Redirect";

require(getFromTemplate('common/header.php', false));

?>